<?php
/**
 * Created by PhpStorm.
 * User: Наталия
 * Date: 07.12.2017
 * Time: 17:43
 */

namespace app\modules\admin\controllers;

use app\libs\Parsing;
use app\models\PlayerCareers;
use app\models\PlayerHighs;
use app\models\PlayerPhotos;
use app\models\Players;
use app\models\PlayerToSeason;
use app\models\Seasons;
use app\models\Teams;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use phpQuery;

class PlayerController extends Controller
{
    public function actionIndex()
    {
        session_start();
        unset($_SESSION['players']);
        return $this->render('index');
    }
    
    public function actionParsAll()
    {
        session_start();
        $seasons = Seasons::find()->asArray()->all();
        if(isset($_SESSION['players']['queryToParsing']['now']) && $_SESSION['players']['queryToParsing']['now'] == $_SESSION['players']['queryToParsing']['count'])
        {
            unset($_SESSION['players']['queryToParsing']['now']);
            return $this->render('pars-finish');
        }
        else
        {
            if(!isset($_SESSION['players']['queryToParsing']['now']) || $_SESSION['players']['queryToParsing']['now'] <= 0)
            {
                $_SESSION['players']['queryToParsing']['count'] = count($seasons); // количество запросов для парсинга
                $_SESSION['players']['queryToParsing']['now'] = 1; // выставляем стартовое значение
            }
            else
            {
                $_SESSION['players']['queryToParsing']['now'] += 1;
            }
            $resBaskethotel['count'] = $_SESSION['players']['queryToParsing']['count'];
            $resBaskethotel['now'] = $_SESSION['players']['queryToParsing']['now'];
            // получаем текущую лигку
            $parsing = new Parsing();
            $url[] = "http://widgets.baskethotel.com/widget-service/show?";
            $url[] = "&api=aaed6059969d182ff2a08e7d61458ce2d1a372c1";
            $url[] = "&lang=uk";
            $url[] = "&nnav=1";
            $url[] = "&nav_object=0";
            $url[] = "&flash=0";
            $url[] = "&request[0][container]=5-105-list-container";
            $url[] = "&request[0][widget]=105&request[0][part]=list";
            $url[] = "&request[0][state]=MzI5MjEyOTI2N2E6MTE6e3M6MTc6InRlYW1fbGlua192aXNpYmxlIjtzOjE6IjEiO3M6MTc6ImdhbWVfbGlua192aXNpYmxlIjtzOjE6IjEiO3M6MTk6InBsYXllcl9saW5rX3Zpc2libGUiO3M6MToiMSI7czoxNDoidGVhbV9saW5rX3R5cGUiO3M6MToiMyI7czoxNDoiZ2FtZV9saW5rX3R5cGUiO3M6MToiMyI7czoxNjoicGxheWVyX2xpbmtfdHlwZSI7czoxOiIzIjtzOjE3OiJ0ZWFtX2xpbmtfaGFuZGxlciI7czoxMjoibmF2aWdhdGVUZWFtIjtzOjE3OiJnYW1lX2xpbmtfaGFuZGxlciI7czoxMjoibmF2aWdhdGVHYW1lIjtzOjE5OiJwbGF5ZXJfbGlua19oYW5kbGVyIjtzOjE0OiJuYXZpZ2F0ZVBsYXllciI7czo5OiJsZWFndWVfaWQiO3M6MzoiMjI0IjtzOjE4OiJzaW5nbGVfbGV0dGVyX21vZGUiO3M6MToiMCI7fQ==";
            $url[] = "&request[0][param][league_id]=".$seasons[$_SESSION['players']['queryToParsing']['now']-1]['league_id'];
            $url[] = "&request[0][param][filter][season]=".$seasons[$_SESSION['players']['queryToParsing']['now']-1]['id'];
            $url[] = "&request[0][param][filter][team]=";
            $url[] = "&request[0][param][filter][q]=";
            $url[] = "&request[0][param][filter][fl]=";
            $allPlayersHtml = $parsing->request(implode($url));
            $pq = phpQuery::newDocument($allPlayersHtml);
            $allPlayers = [];
            $allPlayersArr = $pq->find('.mbt-holder-content1 a');
            if(!empty($allPlayersArr))
            {
                foreach($allPlayersArr as $key => $player)
                {
                    if(!empty(pq($player)->attr('player_id')))
                    {
                        $allPlayers[$key]['player_id'] = pq($player)->attr('player_id');
                        $allPlayers[$key]['season_id'] = pq($player)->attr('season_id');
                        $playerDetals = [];
                        $nameAndComand = explode('(', pq($player)->text());
                        $playerDetals['name'] = explode(' ', $nameAndComand[0]);
                        $allPlayers[$key]['first_name'] = $playerDetals['name'][0];
                        $allPlayers[$key]['last_name'] = $playerDetals['name'][1];
                        $findPlayer = Players::find()->where(['id' => $allPlayers[$key]['player_id']])->all()[0];
                        if(empty($findPlayer))
                        {
                            $palyerModel = new Players();
                            $palyerModel->id = $allPlayers[$key]['player_id'];
                            $palyerModel->first_name = $allPlayers[$key]['first_name'];
                            $palyerModel->last_name = $allPlayers[$key]['last_name'];
                            $palyerModel->save();
                        }
                        else
                        {
                            $findPlayer->first_name = $allPlayers[$key]['first_name'];
                            $findPlayer->last_name = $allPlayers[$key]['last_name'];
                            $findPlayer->save();
                        }
                        $findPlayerToSeason = PlayerToSeason::find()->where(['and', ['player_id' => $allPlayers[$key]['player_id']], ['season_id' => $allPlayers[$key]['season_id']]])->all();
                        if(empty($findPlayerToSeason))
                        {
                            $palyerToSeason = new PlayerToSeason();
                            $palyerToSeason->player_id = $allPlayers[$key]['player_id'];
                            $palyerToSeason->season_id = $allPlayers[$key]['season_id'];
                            $palyerToSeason->save();
                        }
                    }
                    else
                    {
                        continue; // пропускаем если пусто значение
                    }
                }
                $resBaskethotel['data'] = $allPlayersHtml;
            }
            else
            {
                $resBaskethotel['data'] = 'По даному сезону игроков нет';
            }
            return $this->render('pars-all', ['resBaskethotel' => $resBaskethotel, 'urlQuery' => $parsing->finishUrl]);
        }
    }

    public function actionParsOne()
    {
        $findTeamModels = Teams::find()->all();
        if(empty($findTeamModels))
        {
            return $this->asJson('При парсинге подробной информации об каждом игроке нужно спарсить все команды');
        }
        else
        {
            session_start();
            $allPlayers = Players::find()->all();
            $_SESSION['players']['count'] = count($allPlayers);
            if(count($allPlayers) > 0)
            {
                if(!isset($_SESSION['players']['now']) || $_SESSION['players']['now'] <= 0)
                {
                    $_SESSION['players']['now'] = 1;
                }
                else
                {
                    if($_SESSION['players']['now'] > $_SESSION['players']['count'] + 1)
                    {
                        unset($_SESSION['players']['now']);
                        unset($_SESSION['players']['count']);
                        return $this->render('pars-one-finish');
                    }
                    else
                    {

                        $playerId = $allPlayers[$_SESSION['players']['now'] - 1]->id;
                        //        $playerId = '16924';
                        $parsing = new Parsing();
                        $playerDetals = [];
                        $playerDetals['dataPlayers'] = $_SESSION['players'];
                        $playerDetals['errors'];
                        $playerDetals['info'];
                        $seasons = Seasons::find()->all();
                        if(!empty($seasons))
                        {
                            foreach($seasons as $season)
                            {
                                // страница игрока
                                $colum = $parsing->playerRequestPart[0];
                                $seasonId = $season->id;
                                //                $seasonId = 99965;
                                $url = [];
                                $url[] = 'http://widgets.baskethotel.com/widget-service/show?';
                                $url[] = '&api=aaed6059969d182ff2a08e7d61458ce2d1a372c1';
                                $url[] = '&lang=uk';
                                $url[] = '&nnav=1';
                                $url[] = '&nav_object=0';
                                $url[] = '&request[0][container]=5-100-tab-container';
                                $url[] = '&request[0][widget]=100';
                                $url[] = '&request[0][part]='.$colum;
                                $url[] = '&request[0][state]=IDU3NjEwODM0MGE6MTY6e3M6MTc6InRlYW1fbGlua192aXNpYmxlIjtzOjE6IjEiO3M6MTc6ImdhbWVfbGlua192aXNpYmxlIjtzOjE6IjEiO3M6MTk6InBsYXllcl9saW5rX3Zpc2libGUiO3M6MToiMSI7czoxNDoidGVhbV9saW5rX3R5cGUiO3M6MToiMyI7czoxNDoiZ2FtZV9saW5rX3R5cGUiO3M6MToiMyI7czoxNjoicGxheWVyX2xpbmtfdHlwZSI7czoxOiIzIjtzOjE3OiJ0ZWFtX2xpbmtfaGFuZGxlciI7czoxMjoibmF2aWdhdGVUZWFtIjtzOjE3OiJnYW1lX2xpbmtfaGFuZGxlciI7czoxMjoibmF2aWdhdGVHYW1lIjtzOjE5OiJwbGF5ZXJfbGlua19oYW5kbGVyIjtzOjE0OiJuYXZpZ2F0ZVBsYXllciI7czo5OiJsZWFndWVfaWQiO3M6MzoiMjI0IjtzOjk6InNlYXNvbl9pZCI7czo1OiI5OTk2NSI7czo5OiJwbGF5ZXJfaWQiO3M6NzoiNTM5MTM3MSI7czoxNToiZ3JvdXBfYnlfbGVhZ3VlIjtzOjE6IjAiO3M6MTY6ImhpZGVfbmF0aW9uYWxpdHkiO3M6MToiMSI7czoyMDoic2hvd19zZWFzb25fc2VsZWN0b3IiO3M6MToiMSI7czoyNjoicGxheWVyX3BpY3R1cmVfZGltbWVuc2lvbnMiO3M6NzoiMTAweDE1MCI7fQ==';
                                $url[] = '&request[0][param][player_id]='.$playerId;
                                $url[] = '&request[0][param][season_id]='.$seasonId;
                                $urlString = implode($url);
                                $playerHome = $parsing->request($urlString);
                                $pqHome = phpQuery::newDocument($playerHome);
                                if(!empty($pqHome->find('.mbt-player-stats div:eq(3) div:eq(0) strong:eq(0)')->text()) &&
                                    !empty(explode(' ', $pqHome->find('.mbt-player-stats div:eq(3) div:eq(0) strong:eq(2)')->text())[0]) &&
                                    !empty(explode(' ', $pqHome->find('.mbt-player-stats div:eq(3) div:eq(0) strong:eq(2)')->text())[0]) &&
                                    !empty($pqHome->find('.mbt-holder-content .mbt-player-stats .mbt-stat-holder-inl .mbt-value')->text()) &&
                                    !empty($pqHome->find('.mbt-layout-top .mbt-holder-content .mbt-player-stats .mbt-pstats-table tr:eq(2) td:eq(0)')->text()) &&
                                    !empty($pqHome->find('.mbt-layout-top .mbt-holder-content .mbt-player-stats .mbt-pstats-table tr:eq(2) td:eq(1)')->text()))
                                {
                                    $playerDetals['player'][$seasonId][$parsing->playerRequestPart[0]]['position'] = $pqHome->find('.mbt-player-stats div:eq(3) div:eq(0) strong:eq(0)')->text();
                                    $playerDetals['player'][$seasonId][$parsing->playerRequestPart[0]]['height'] = explode(' ', $pqHome->find('.mbt-player-stats div:eq(3) div:eq(0) strong:eq(1)')->text())[0];
                                    $playerDetals['player'][$seasonId][$parsing->playerRequestPart[0]]['weight'] = explode(' ', $pqHome->find('.mbt-player-stats div:eq(3) div:eq(0) strong:eq(2)')->text())[0];
                                    $playerDetals['player'][$seasonId][$parsing->playerRequestPart[0]]['count_game'] = $pqHome->find('.mbt-holder-content .mbt-player-stats .mbt-stat-holder-inl .mbt-value')->text();
                                    $playerDetals['player'][$seasonId][$parsing->playerRequestPart[0]]['date_of_birth'] = date('Y-m-d', strtotime($pqHome->find('.mbt-holder-content .mbt-player-stats .mbt-player-info-holder div:eq(1) strong:eq(1)')->text()));
                                    $playerDetals['player'][$seasonId][$parsing->playerRequestPart[0]]['points'] = $pqHome->find('.mbt-layout-top .mbt-holder-content .mbt-player-stats .mbt-pstats-table tr:eq(2) td:eq(0)')->text();
                                    $playerDetals['player'][$seasonId][$parsing->playerRequestPart[0]]['pink_up'] = $pqHome->find('.mbt-layout-top .mbt-holder-content .mbt-player-stats .mbt-pstats-table tr:eq(2) td:eq(1)')->text();
                                    $playerDetals['player'][$seasonId][$parsing->playerRequestPart[0]]['forwarding'] = $pqHome->find('.mbt-layout-top .mbt-holder-content .mbt-player-stats .mbt-pstats-table tr:eq(2) td:eq(2)')->text();
                                    $playerDetals['player'][$seasonId][$parsing->playerRequestPart[0]]['efficiency'] = $pqHome->find('.mbt-layout-top .mbt-holder-content .mbt-player-stats .mbt-pstats-table tr:eq(2) td:eq(3)')->text();
                                    $playerDetals['player'][$seasonId][$parsing->playerRequestPart[0]]['plus_minus'] = $pqHome->find('.mbt-layout-top .mbt-holder-content .mbt-player-stats .mbt-pstats-table tr:eq(2) td:eq(4)')->text();
                                    $playerDetals['player'][$seasonId][$parsing->playerRequestPart[0]]['photo'] = $pqHome->find('.mbt-layout-top .mbt-holder-content .mbt-player-photo img')->attr('src');
                                    $playerDetals['player'][$seasonId][$parsing->playerRequestPart[0]]['club_id'] = $pqHome->find('.mbt-layout-top .mbt-imageholder-smalllogo a')->attr('team_id');
                                    $findPlayer = Players::find()->where(['id' => $playerId])->all()[0];
                                    if(!empty($findPlayer))
                                    {
                                        $findPlayer->position = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[0]]['position'];
                                        $findPlayer->height = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[0]]['height'];
                                        $findPlayer->weight = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[0]]['weight'];
                                        $findPlayer->date_of_birth = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[0]]['date_of_birth'];
                                        $findPlayer->save();
                                    }
                                    else
                                    {
                                        $playerModel = new Players();
                                        $playerModel->id = $playerId;
                                        $playerModel->height = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[0]]['height'];
                                        $playerModel->weight = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[0]]['weight'];
                                        $playerModel->date_of_birth = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[0]]['date_of_birth'];
                                        $playerModel->position = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[0]]['position'];
                                        $playerModel->save();
                                    }
                                    $generatePhotoName = $playerId.'/'.time().'-'.$seasonId.'-'.$playerId.'.jpg';
                                    $dirPath = Url::to('@app/web/img/photo-players');
                                    $newFilePhotoPath = $dirPath.'/'.$generatePhotoName;
                                    if(!file_exists($dirPath.'/'.$playerId))
                                    {
                                        mkdir($dirPath.'/'.$playerId);
                                    }
                                    $findPlayerPhoto = PlayerPhotos::find()->where(['season_id' => $seasonId, 'player_id' => $playerId])->one();
                                    if(empty($findPlayerPhoto))
                                    {
                                        copy($playerDetals['player'][$seasonId][$parsing->playerRequestPart[0]]['photo'], $newFilePhotoPath); // сохранение фото игрока
                                        $playerPhoto = new PlayerPhotos();
                                        $playerPhoto->player_id = $playerId;
                                        $playerPhoto->season_id = $seasonId;
                                        $playerPhoto->name = $generatePhotoName;
                                        $playerPhoto->save();
                                    }
                                    $playerToSeason = PlayerToSeason::find()->where([
                                        'player_id' => $playerId,
                                        'season_id' => $seasonId,
                                    ])->all()[0];
                                    if(!empty($playerToSeason))
                                    {
                                        $playerToSeason->count_game = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[0]]['count_game'];
                                        $playerToSeason->points = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[0]]['points'];
                                        $playerToSeason->pink_up = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[0]]['pink_up'];
                                        $playerToSeason->forwarding = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[0]]['forwarding'];
                                        $playerToSeason->efficiency = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[0]]['efficiency'];
                                        $playerToSeason->plus_minus = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[0]]['plus_minus'];
                                        $playerToSeason->club_id = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[0]]['club_id'];
                                        $playerToSeason->save();
                                        $playerDetals['info'][] = "Изменена информация об игроке в определенный сезон";
                                    }
                                    else
                                    {
                                        $newPlayerToSeason = new PlayerToSeason();
                                        $newPlayerToSeason->player_id = $playerId;
                                        $newPlayerToSeason->season_id = $seasonId;
                                        $newPlayerToSeason->count_game = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[0]]['count_game'];
                                        $newPlayerToSeason->points = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[0]]['points'];
                                        $newPlayerToSeason->pink_up = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[0]]['pink_up'];
                                        $newPlayerToSeason->forwarding = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[0]]['forwarding'];
                                        $newPlayerToSeason->efficiency = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[0]]['efficiency'];
                                        $newPlayerToSeason->plus_minus = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[0]]['plus_minus'];
                                        $newPlayerToSeason->club_id = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[0]]['club_id'];
                                        $newPlayerToSeason->save();
                                        $playerDetals['info'][] = "Создана новая информация об игроке в определенный сезон";
                                    }
                                    // страница рекорды
                                    $colum = $parsing->playerRequestPart[2];
                                    $url = [];
                                    $url[] = 'http://widgets.baskethotel.com/widget-service/show?';
                                    $url[] = '&api=aaed6059969d182ff2a08e7d61458ce2d1a372c1';
                                    $url[] = '&lang=uk';
                                    $url[] = '&nnav=1';
                                    $url[] = '&nav_object=0';
                                    $url[] = '&request[0][container]=5-100-tab-container';
                                    $url[] = '&request[0][widget]=100';
                                    $url[] = '&request[0][part]='.$colum;
                                    $url[] = '&request[0][state]=IDU3NjEwODM0MGE6MTY6e3M6MTc6InRlYW1fbGlua192aXNpYmxlIjtzOjE6IjEiO3M6MTc6ImdhbWVfbGlua192aXNpYmxlIjtzOjE6IjEiO3M6MTk6InBsYXllcl9saW5rX3Zpc2libGUiO3M6MToiMSI7czoxNDoidGVhbV9saW5rX3R5cGUiO3M6MToiMyI7czoxNDoiZ2FtZV9saW5rX3R5cGUiO3M6MToiMyI7czoxNjoicGxheWVyX2xpbmtfdHlwZSI7czoxOiIzIjtzOjE3OiJ0ZWFtX2xpbmtfaGFuZGxlciI7czoxMjoibmF2aWdhdGVUZWFtIjtzOjE3OiJnYW1lX2xpbmtfaGFuZGxlciI7czoxMjoibmF2aWdhdGVHYW1lIjtzOjE5OiJwbGF5ZXJfbGlua19oYW5kbGVyIjtzOjE0OiJuYXZpZ2F0ZVBsYXllciI7czo5OiJsZWFndWVfaWQiO3M6MzoiMjI0IjtzOjk6InNlYXNvbl9pZCI7czo1OiI5OTk2NSI7czo5OiJwbGF5ZXJfaWQiO3M6NzoiNTM5MTM3MSI7czoxNToiZ3JvdXBfYnlfbGVhZ3VlIjtzOjE6IjAiO3M6MTY6ImhpZGVfbmF0aW9uYWxpdHkiO3M6MToiMSI7czoyMDoic2hvd19zZWFzb25fc2VsZWN0b3IiO3M6MToiMSI7czoyNjoicGxheWVyX3BpY3R1cmVfZGltbWVuc2lvbnMiO3M6NzoiMTAweDE1MCI7fQ==';
                                    $url[] = '&request[0][param][player_id]='.$playerId;
                                    $url[] = '&request[0][param][season_id]='.$seasonId;
                                    $urlString = implode($url);
                                    $playerHighs = $parsing->request($urlString);
                                    $pqHighs = phpQuery::newDocument($playerHighs);
                                    $categoryNameArr []['categoryName'] = $pqHighs->find('.mbt-table tr:eq(2) .first')->text(); // хвилин зіграно
                                    $categoryNameArr []['categoryName'] = $pqHighs->find('.mbt-table tr:eq(3) .first')->text(); // двоочкові забиті
                                    $categoryNameArr []['categoryName'] = $pqHighs->find('.mbt-table tr:eq(4) .first')->text(); // двоочкові спроби
                                    $categoryNameArr []['categoryName'] = $pqHighs->find('.mbt-table tr:eq(5) .first')->text(); // % двоочкових
                                    $categoryNameArr []['categoryName'] = $pqHighs->find('.mbt-table tr:eq(6) .first')->text(); // забиті триочкові
                                    $categoryNameArr []['categoryName'] = $pqHighs->find('.mbt-table tr:eq(7) .first')->text(); // триочкові спроби
                                    $categoryNameArr []['categoryName'] = $pqHighs->find('.mbt-table tr:eq(8) .first')->text(); // % триочкових
                                    $categoryNameArr []['categoryName'] = $pqHighs->find('.mbt-table tr:eq(9) .first')->text(); // забиті штрафні
                                    $categoryNameArr []['categoryName'] = $pqHighs->find('.mbt-table tr:eq(10) .first')->text(); // штрафні спроби
                                    $categoryNameArr []['categoryName'] = $pqHighs->find('.mbt-table tr:eq(11) .first')->text(); // % штрафних
                                    $categoryNameArr []['categoryName'] = $pqHighs->find('.mbt-table tr:eq(12) .first')->text(); // підбирання чужий
                                    $categoryNameArr []['categoryName'] = $pqHighs->find('.mbt-table tr:eq(13) .first')->text(); // підбирання чужий
                                    $categoryNameArr []['categoryName'] = $pqHighs->find('.mbt-table tr:eq(14) .first')->text(); // Підбирання
                                    $categoryNameArr []['categoryName'] = $pqHighs->find('.mbt-table tr:eq(15) .first')->text(); // Передачі
                                    $categoryNameArr []['categoryName'] = $pqHighs->find('.mbt-table tr:eq(16) .first')->text(); // фолів зроблено
                                    $categoryNameArr []['categoryName'] = $pqHighs->find('.mbt-table tr:eq(17) .first')->text(); // втрати
                                    $categoryNameArr []['categoryName'] = $pqHighs->find('.mbt-table tr:eq(18) .first')->text(); // перехопл
                                    $categoryNameArr []['categoryName'] = $pqHighs->find('.mbt-table tr:eq(19) .first')->text(); // блоки
                                    $categoryNameArr []['categoryName'] = $pqHighs->find('.mbt-table tr:eq(20) .first')->text(); // ефективность
                                    $categoryNameArr []['categoryName'] = $pqHighs->find('.mbt-table tr:eq(21) .first')->text(); // Очки
                                    $playerDetals['player'][$seasonId][$parsing->playerRequestPart[2]]['categoryNameArr'] = $categoryNameArr;
                                    foreach($categoryNameArr as $keyCategoryNameArr => $valueCategoryNameArr)
                                    {
                                        $dateHighs = date('Y-m-d H:i:s', strtotime(str_replace('/', '-', trim($pqHighs->find('.mbt-table tr:eq('.($keyCategoryNameArr + 2).') td:eq(2) a')->text()))));
                                        $categoryNumberHighs = $pqHighs->find('.mbt-table tr:eq('.($keyCategoryNameArr + 2).') td:eq(1)')->text();
                                        $positionHighs = $pqHighs->find('.mbt-table tr:eq('.($keyCategoryNameArr + 2).') td:eq(3)')->text();
                                        $playerDetals['player'][$seasonId][$parsing->playerRequestPart[2]]['categoryNameArr'][$keyCategoryNameArr]['dateHighs'] = $dateHighs;
                                        $playerDetals['player'][$seasonId][$parsing->playerRequestPart[2]]['categoryNameArr'][$keyCategoryNameArr]['categoryNumberHighs'] = $categoryNumberHighs;
                                        $playerDetals['player'][$seasonId][$parsing->playerRequestPart[2]]['categoryNameArr'][$keyCategoryNameArr]['positionHighs'] = $positionHighs;
                                        if(!empty($categoryNumberHighs) &
                                            !empty($positionHighs))
                                        {
                                            $findPlayerHeghs = PlayerHighs::find()->where(['player_id' => $playerId, 'season_id' => $seasonId, 'category_name' => $valueCategoryNameArr['categoryName']])->all()[0];
                                            if(!empty($findPlayerHeghs))
                                            {
                                                $findPlayerHeghs->category_number = $categoryNumberHighs;
                                                $findPlayerHeghs->date = $dateHighs;
                                                $findPlayerHeghs->position = $positionHighs;
                                                $findPlayerHeghs->save();
                                                $playerDetals['info'][] = "Перезаписаны даные об рекорде игрока";
                                            }
                                            else
                                            {
                                                $playerHighsModel = new PlayerHighs();
                                                $playerHighsModel->player_id = $playerId;
                                                $playerHighsModel->season_id = $seasonId;
                                                $playerHighsModel->category_name = $valueCategoryNameArr['categoryName'];
                                                $playerHighsModel->category_number = $categoryNumberHighs;
                                                $playerHighsModel->date = $dateHighs;
                                                $playerHighsModel->position = $positionHighs;
                                                $playerHighsModel->save();
                                                $playerDetals['info'][] = "Созданы новые даные об рекорде игрока";
                                            }
                                        }
                                    }
                                    // страница карьера
                                    $colum = $parsing->playerRequestPart[4];
                                    $url = [];
                                    $url[] = 'http://widgets.baskethotel.com/widget-service/show?';
                                    $url[] = '&api=aaed6059969d182ff2a08e7d61458ce2d1a372c1';
                                    $url[] = '&lang=uk';
                                    $url[] = '&nnav=1';
                                    $url[] = '&nav_object=0';
                                    $url[] = '&request[0][container]=5-100-tab-container';
                                    $url[] = '&request[0][widget]=100';
                                    $url[] = '&request[0][part]='.$colum;
                                    $url[] = '&request[0][state]=IDU3NjEwODM0MGE6MTY6e3M6MTc6InRlYW1fbGlua192aXNpYmxlIjtzOjE6IjEiO3M6MTc6ImdhbWVfbGlua192aXNpYmxlIjtzOjE6IjEiO3M6MTk6InBsYXllcl9saW5rX3Zpc2libGUiO3M6MToiMSI7czoxNDoidGVhbV9saW5rX3R5cGUiO3M6MToiMyI7czoxNDoiZ2FtZV9saW5rX3R5cGUiO3M6MToiMyI7czoxNjoicGxheWVyX2xpbmtfdHlwZSI7czoxOiIzIjtzOjE3OiJ0ZWFtX2xpbmtfaGFuZGxlciI7czoxMjoibmF2aWdhdGVUZWFtIjtzOjE3OiJnYW1lX2xpbmtfaGFuZGxlciI7czoxMjoibmF2aWdhdGVHYW1lIjtzOjE5OiJwbGF5ZXJfbGlua19oYW5kbGVyIjtzOjE0OiJuYXZpZ2F0ZVBsYXllciI7czo5OiJsZWFndWVfaWQiO3M6MzoiMjI0IjtzOjk6InNlYXNvbl9pZCI7czo1OiI5OTk2NSI7czo5OiJwbGF5ZXJfaWQiO3M6NzoiNTM5MTM3MSI7czoxNToiZ3JvdXBfYnlfbGVhZ3VlIjtzOjE6IjAiO3M6MTY6ImhpZGVfbmF0aW9uYWxpdHkiO3M6MToiMSI7czoyMDoic2hvd19zZWFzb25fc2VsZWN0b3IiO3M6MToiMSI7czoyNjoicGxheWVyX3BpY3R1cmVfZGltbWVuc2lvbnMiO3M6NzoiMTAweDE1MCI7fQ==';
                                    $url[] = '&request[0][param][player_id]='.$playerId;
                                    $url[] = '&request[0][param][season_id]='.$seasonId;
                                    $url[] = '&request[0][param][avgtot]=1';
                                    $url[] = '&request[0][param][stage]=0';
                                    $urlString = implode($url);
                                    $playerCareer = $parsing->request($urlString);
                                    $pqCarеer = phpQuery::newDocument($playerCareer);
                                    if(!empty($pqCarеer->find('.mbt-table tr')))
                                    {
                                        foreach($pqCarеer->find('.mbt-table tr') as $keyCareer => $trCareer)
                                        {
                                            $careerTrs = phpQuery::newDocument(pq($trCareer)->html());
                                            if(!empty($careerTrs->find('td:eq(0) a')->attr('player_id')))
                                            {
                                                $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['player_id'] = $careerTrs->find('td:eq(0) a')->attr('player_id'); // игрок
                                                $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['season_id'] = $careerTrs->find('td:eq(0) a')->attr('season_id');// сезон
                                                $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['club_id'] = $careerTrs->find('td:eq(1) a')->attr('team_id');// комманда
                                                $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['team_name'] = $careerTrs->find('td:eq(1) a')->text();// название команды
                                                $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['count_games'] = $careerTrs->find('td:eq(2)')->text();// количество игор
                                                $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['game_time'] = $careerTrs->find('td:eq(3)')->text();// время в игре
                                                $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['two_point_kiddies'] = $careerTrs->find('td:eq(4) span:eq(1)')->text();// двохочковых бросков
                                                $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['two_point_kiddies_ok'] = $careerTrs->find('td:eq(4) span:eq(0)')->text();// двохочковых закинутых
                                                $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['two_point_kiddies_percent'] = $careerTrs->find('td:eq(5)')->text(); // проценты двохочковых закинутых
                                                $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['three_point_kiddies'] = $careerTrs->find('td:eq(6) span:eq(1)')->text();// трехочковых бросков
                                                $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['three_point_kiddies_ok'] = $careerTrs->find('td:eq(6) span:eq(0)')->text();// трехочковых закинутых
                                                $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['three_point_kiddies_percent'] = $careerTrs->find('td:eq(7)')->text(); // проценты трехочковых закинутых
                                                $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['one_point_kiddies'] = $careerTrs->find('td:eq(8) span:eq(1)')->text();// штрафных бросков
                                                $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['one_point_kiddies_ok'] = $careerTrs->find('td:eq(8) span:eq(0)')->text();// штрафных закинутых
                                                $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['one_point_kiddies_percent'] = $careerTrs->find('td:eq(9)')->text(); // проценты штрафных закинутых
                                                $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['picking_up_in_an_attack'] = $careerTrs->find('td:eq(10)')->text();// Підбирання у нападі
                                                $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['picking_up_in_defense'] = $careerTrs->find('td:eq(11)')->text();// Підбирання у захисті
                                                $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['overall_picking_up'] = $careerTrs->find('td:eq(12)')->text();// Загалом підбирань
                                                $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['transmissions'] = $careerTrs->find('td:eq(13)')->text();// Передачі
                                                $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['staff_foul_1'] = $careerTrs->find('td:eq(14)')->text(); // Персоналні фоли 1
                                                $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['staff_fouls_2'] = $careerTrs->find('td:eq(15)')->text();// Персоналні фоли 2
                                                $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['losses'] = $careerTrs->find('td:eq(16)')->text();// Втрати
                                                $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['interception'] = $careerTrs->find('td:eq(17)')->text();// Перехоплення
                                                $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['block_shots_1'] = $careerTrs->find('td:eq(18)')->text();// Блок-шоти 1
                                                $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['block_shots_2'] = $careerTrs->find('td:eq(19)')->text();// Блок-шоти 2
                                                $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['efficiency'] = $careerTrs->find('td:eq(20)')->text();// Ефективність
                                                $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['gained_points'] = $careerTrs->find('td:eq(21)')->text();// Здобуті очки
                                                $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['url'] = $parsing->finishUrl; // адресс
                                                $findCareer = PlayerCareers::find()->where([
                                                    'player_id' => $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['player_id'],
                                                    'season_id' => $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['season_id'],
                                                    'club_id' => $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['club_id'],
                                                ])->one();
                                                if(empty($findCareer))
                                                {
                                                    $findSeason = Seasons::find()->where(['id' => $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['season_id']])->all()[0];
                                                    if(!empty($findSeason))
                                                    {
                                                        $findTeams = Teams::find()->where(['team_id' => $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['club_id'], 'league_id' => $findSeason['league_id']])->all()[0];
                                                        if(empty($findTeams))
                                                        {
                                                            $teamNew = new Teams();
                                                            $teamNew->team_id = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['club_id'];
                                                            $teamNew->league_id = $findSeason['league_id'];
                                                            $teamNew->name = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['team_name'];
                                                            $teamNew->save();
                                                        }
                                                    }
                                                    $careerModel = new PlayerCareers();
                                                    $careerModel->player_id = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['player_id'];
                                                    $careerModel->season_id = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['season_id'];
                                                    $careerModel->club_id = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['club_id'];
                                                    $careerModel->count_games = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['count_games'];
                                                    $careerModel->game_time = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['game_time'];
                                                    $careerModel->two_point_kiddies = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['two_point_kiddies'];
                                                    $careerModel->two_point_kiddies_ok = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['two_point_kiddies_ok'];
                                                    $careerModel->two_point_kiddies_percent = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['two_point_kiddies_percent'];
                                                    $careerModel->three_point_kiddies = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['three_point_kiddies'];
                                                    $careerModel->three_point_kiddies_ok = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['three_point_kiddies_ok'];
                                                    $careerModel->three_point_kiddies_percent = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['three_point_kiddies_percent'];
                                                    $careerModel->one_point_kiddies = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['one_point_kiddies'];
                                                    $careerModel->one_point_kiddies_ok = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['one_point_kiddies_ok'];
                                                    $careerModel->one_point_kiddies_percent = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['one_point_kiddies_percent'];
                                                    $careerModel->picking_up_in_an_attack = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['picking_up_in_an_attack'];
                                                    $careerModel->picking_up_in_defense = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['picking_up_in_defense'];
                                                    $careerModel->overall_picking_up = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['overall_picking_up'];
                                                    $careerModel->transmissions = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['transmissions'];
                                                    $careerModel->staff_foul_1 = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['staff_foul_1'];
                                                    $careerModel->staff_fouls_2 = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['staff_fouls_2'];
                                                    $careerModel->losses = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['losses'];
                                                    $careerModel->interception = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['interception'];
                                                    $careerModel->block_shots_1 = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['block_shots_1'];
                                                    $careerModel->block_shots_2 = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['block_shots_2'];
                                                    $careerModel->efficiency = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['efficiency'];
                                                    $careerModel->gained_points = $playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]][$keyCareer]['gained_points'];
                                                    if($careerModel->save())
                                                    {
                                                        $playerDetals['info']['career'] = 'Даные карьеры игрока успешно сохранены';
                                                    }
                                                    else
                                                    {
                                                        $playerDetals['errors']['career'] = 'При записи данных карьеры игрока не прошли валидацию';
                                                    }
//                                                return $this->asJson($playerDetals['player'][$seasonId][$parsing->playerRequestPart[4]]);
                                                }
                                            }
                                            else
                                            {
                                                $playerDetals['errors']['career'] = 'не найдена карьерная таблица';
                                            }
                                        }
                                    }
                                    else
                                    {
                                        $playerDetals['errors']['career'] = 'Не найдена таблица рарьеры';
                                    }
                                }
                                else
                                {
                                    $playerDetals['errors']['player_to_season'] = "Игрок не участвовал в некоторых сезонах";
                                }
                            }
                        }
                        else
                        {
                            $playerDetals['errors'][] = 'Не найдены сезоны, для парсинга игроков нужно отпарсить сезоны';
                        }
                        $_SESSION['players']['now'] += 1;
                    }
                }
            }
            else
            {
                $playerDetals['errors'][] = 'Не найдено ни одного игрока для парсинга. Спарсите для начала игроков';
            }
            return $this->render('pars-one', ['playerDetals' => $playerDetals]);
        }
    }

}