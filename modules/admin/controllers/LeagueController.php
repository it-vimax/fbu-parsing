<?php
/**
 * Created by PhpStorm.
 * User: Наталия
 * Date: 07.12.2017
 * Time: 17:22
 */

namespace app\modules\admin\controllers;

use app\libs\Parsing;
use app\models\Leagues;
use phpQuery;
use yii\web\Controller;

class LeagueController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionParsAll()
    {
        session_start();
        $findLeages = Leagues::find()->all();
        if(!empty($findLeages))
        {
            if(isset($_SESSION['automat']['firstData']['value']) && $_SESSION['automat']['firstData']['value'] == 1)
            {
                $_SESSION['automat']['firstData']['succes'] = 1;
                return $this->redirect('/admin/automat/first-data');
            }
            else
            {
                return $this->asJson(['error' => 'Лиги уже существуют']);
            }
        }
        else
        {
            $url = "http://fbu.ua/statistics/league-32079/calendar";
            $parsing = new Parsing();
            $allLeagueHtml = $parsing->request($url, true);
            $pq = phpQuery::newDocument($allLeagueHtml);
            $allLeague = [];
            $allLeagueArr = $pq->find('.drop_down_list_leages li a');
            foreach($allLeagueArr as $key => $league)
            {
                $allLeague[$key]['id'] = str_replace('/calendar', '', str_replace('/statistics/league-', '', pq($league)->attr('href')));
                $allLeague[$key]['name'] = pq($league)->text();
            }
            if(!empty($allLeague))
            {
                foreach($allLeague as $item)
                {
                    $league = new Leagues();
                    $league->id = $item['id'];
                    $league->name = $item['name'];
                    $league->save();
                }
                if(isset($_SESSION['automat']['firstData']['value']) && $_SESSION['automat']['firstData']['value'] == 1)
                {
                    $_SESSION['automat']['firstData']['succes'] = 1;
                    return $this->redirect('/admin/automat/first-data');
                }
                else
                {
                    return $this->render('pars-all', compact('allLeague'));
                }
            }
            else
            {
                return $this->asJson(['error' => 'Не найдено лиг']);
            }
        }
    }
}