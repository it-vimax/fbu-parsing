<?php
/**
 * Created by PhpStorm.
 * User: Maks
 * Date: 08.12.2017
 * Time: 0:18
 */

namespace app\modules\admin\controllers;

use app\models\Leagues;
use app\models\Players;
use app\models\Teams;
use phpQuery;
use app\libs\Parsing;
use yii\web\Controller;

class TeamsController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionAllTeams()
    {
        $allLeague = Leagues::find()->all();
        $teamData = [];
        $teamData['info'] = [];
        $teamData['errors'] = [];
        $parsing = new Parsing();
        if(!empty($allLeague))
        {
            foreach($allLeague as $league)
            {
                $league_id = $league->id;
                $url = [];
                $url[] = "http://widgets.baskethotel.com/widget-service/show?";
                $url[] = "&api=aaed6059969d182ff2a08e7d61458ce2d1a372c1";
                $url[] = "&lang=uk";
                $url[] = "&nnav=1";
                $url[] = "&nav_object=0";
                $url[] = "&request[0][container]=teams";
                $url[] = "&request[0][widget]=201";
                $url[] = "&request[0][param][team_link_visible]=1";
                $url[] = "&request[0][param][game_link_visible]=1";
                $url[] = "&request[0][param][player_link_visible]=1";
                $url[] = "&request[0][param][show_birth_place]=1";
                $url[] = "&request[0][param][team_link_type]=3";
                $url[] = "&request[0][param][game_link_type]=3";
                $url[] = "&request[0][param][player_link_type]=3";
                $url[] = "&request[0][param][team_link_handler]=navigateTeam";
                $url[] = "&request[0][param][game_link_handler]=navigateGame";
                $url[] = "&request[0][param][player_link_handler]=navigatePlayer";
                $url[] = "&request[0][param][league_id]=".$league_id;
                $url[] = "&request[0][param][layout]=v";
                $url[] = "&request[0][param][photo_size]=small";
                $urlString = implode($url);
                $reamsHtml = $parsing->request($urlString);
                $pqTeams = phpQuery::newDocument($reamsHtml);
                if (!empty($pqTeams->find('.mbt-table tr')))
                {
                    foreach ($pqTeams->find('.mbt-table tr') as $keyTeams => $trTeams) // кажда ячейка команды
                    {
                        if(!empty(pq($trTeams)->find('td:eq(2) a')->attr('team_id')))
                        {
                            $teamData['teams'][$league_id][$keyTeams]['id'] = pq($trTeams)->find('td:eq(2) a')->attr('team_id');
                            $teamData['teams'][$league_id][$keyTeams]['name'] = pq($trTeams)->find('td:eq(2) a')->html();
                            $arrTeamsCity = explode('<br>', pq($trTeams)->find('td:eq(2)')->html());
                            $teamData['teams'][$league_id][$keyTeams]['city'] = trim($arrTeamsCity[1]);
                            $findTeam = Teams::find()->where(['team_id' => $teamData['teams'][$league_id][$keyTeams]['id'], 'name' => $teamData['teams'][$league_id][$keyTeams]['name']])->all()[0];
                            if(count($findTeam) <= 0)
                            {
                                $teamModel = new Teams();
                                $teamModel->team_id = $teamData['teams'][$league_id][$keyTeams]['id'];
                                $teamModel->league_id = $league_id;
                                $teamModel->name = $teamData['teams'][$league_id][$keyTeams]['name'];
                                $teamModel->city =$teamData['teams'][$league_id][$keyTeams]['city'];
                                $teamModel->save();
                            }
                        }
                    }
                }
                else
                {
                    $teamData['errors'] = "Не загрузилась таблица команд";
                }
            }
            if(isset($_SESSION['automat']['firstData']['value']) && $_SESSION['automat']['firstData']['value'] == 3)
            {
                $_SESSION['automat']['firstData']['succes'] = 3;
                return $this->redirect('/admin/automat/first-data');
            }
            else
            {
                $teamData['info'] = "Все команды спарсены";
            }
        }
        else
        {
            $teamData['errors'] = 'Не существуют лиги. Перед парсингом лиг ножно спарсить все лиги, в которх участвуют команы';
        }
        if(isset($_SESSION['automat']['firstData']['value']) && $_SESSION['automat']['firstData']['value'] == 3)
        {
            $_SESSION['automat']['firstData']['succes'] = 3;
            return $this->redirect('/admin/automat/first-data');
        }
        else
        {
            return $this->render('all-teams', ['teamData' => $teamData]);
        }
    }

}