<?php
/**
 * Created by PhpStorm.
 * User: Maks
 * Date: 23.12.2017
 * Time: 17:30
 */

namespace app\modules\admin\controllers;


use yii\web\Controller;

class AutomatController extends Controller
{
    public function actionIndex()
    {
        session_start();
        unset($_SESSION['automat']);
        return $this->render('index');
    }

    public function actionAll()
    {
        return $this->render('all');
    }

    public function actionFirstData()
    {
        session_start();
        if(isset($_SESSION['automat']['firstData']['value']) &&
            !empty($_SESSION['automat']['firstData']['value']))
        {
            $_SESSION['automat']['firstData']['value'] += 1;
        }
        else
        {
            $_SESSION['automat']['firstData']['value'] = 1;
        }
        $parsingType = $_SESSION['automat']['firstData']['value'];
        switch ($parsingType)
        {
            case 1: // парсинг лиг
                $_SESSION['automat']['firstData']['value'] = 1;
                return $this->redirect('/admin/league/pars-all');
            case 2: // парсинг сезонов
                $_SESSION['automat']['firstData']['success'][] = 'Парсинг лиг успешно завершен';
                $_SESSION['automat']['firstData']['value'] = 2;
                return $this->redirect('/admin/season/all');
            case 3: // парсинг команд
                $_SESSION['automat']['firstData']['success'][] = 'Парсинг сезонов успешно завершен';
                    $_SESSION['automat']['firstData']['value'] = 3;
                    return $this->redirect('/admin/teams/all-teams');
            case 4: // парсинг комисаров
                $_SESSION['automat']['firstData']['success'][] = 'Парсинг команд успешно завершен';
                $_SESSION['automat']['firstData']['value'] = 4;
                return $this->redirect('/admin/personnel/commissar-all');
            case 5: // парсинг арбитров
                $_SESSION['automat']['firstData']['success'][] = 'Парсинг комисаров успешно завершен';
                $_SESSION['automat']['firstData']['value'] = 5;
                return $this->redirect('/admin/personnel/judiciary-all');
            case 6: // парсинг тренеров
                $_SESSION['automat']['firstData']['success'][] = 'Парсинг арбитров успешно завершен';
                $_SESSION['automat']['firstData']['value'] = 6;
                return $this->redirect('/admin/personnel/trainer-all');
        }
        if($_SESSION['automat']['firstData']['succes'] == 6)
        {
            $_SESSION['automat']['firstData']['success'][] = 'Парсинг тренеров успешно завершен.';
        }
        return $this->render('first-data', ['result' => $_SESSION['automat']['firstData']['success']]);
    }
}