<?php
/**
 * Created by PhpStorm.
 * User: Наталия
 * Date: 07.12.2017
 * Time: 17:41
 */

namespace app\modules\admin\controllers;

use app\libs\Parsing;
use app\models\Leagues;
use app\models\Seasons;
use yii\web\Controller;
use phpQuery;

class SeasonController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionAll()
    {
        session_start();
        $seasons = Seasons::find()->all();
        if(empty($seasons))
        {
            $leagues = Leagues::find()->all();
            if(!empty($leagues))
            {
                $allSeasonWithLeagues = [];
                foreach($leagues as $league)
                {
                    $url[] = "http://widgets.baskethotel.com/widget-service/show?";
                    $url[] = "&api=aaed6059969d182ff2a08e7d61458ce2d1a372c1";
                    $url[] = "&lang=uk";
                    $url[] = "&nnav=1";
                    $url[] = "&nav_object=0";
                    $url[] = "&flash=0";
                    $url[] = "&request[0][container]=players";
                    $url[] = "&request[0][widget]=105";
                    $url[] = "&request[0][param][team_link_visible]=1";
                    $url[] = "&request[0][param][game_link_visible]=1";
                    $url[] = "&request[0][param][player_link_visible]=1";
                    $url[] = "&request[0][param][team_link_type]=3";
                    $url[] = "&request[0][param][game_link_type]=3";
                    $url[] = "&request[0][param][player_link_type]=3";
                    $url[] = "&request[0][param][team_link_handler]=navigateTeam";
                    $url[] = "&request[0][param][game_link_handler]=navigateGame";
                    $url[] = "&request[0][param][player_link_handler]=navigatePlayer";
                    $url[] = "&request[0][param][league_id]=".$league->id;
                    $url[] = "&request[0][param][single_letter_mode]=0";
                    $parsing = new Parsing();
                    $parsing->state = "";
                    $allSeasonHtml = $parsing->request(implode($url));

                    $pq = phpQuery::newDocument($allSeasonHtml);
                    $allSeason = [];
                    $allSeasonArr = $pq->find('#5-105-filter-season option');
                    foreach ($allSeasonArr as $key => $season)
                    {
                        $allSeason[$key]['id'] = pq($season)->attr('value');
                        $allSeason[$key]['name'] = pq($season)->text();
                        $seasonModel = new Seasons();
                        $seasonModel->id = $allSeason[$key]['id'];
                        $seasonModel->league_id = $league->id;
                        $seasonModel->name = $allSeason[$key]['name'];
                        $seasonModel->save();
                    }
                    $allSeasonWithLeagues [$league->name] = $allSeason;
                }
                if(isset($_SESSION['automat']['firstData']['value']) && $_SESSION['automat']['firstData']['value'] == 2)
                {
                    $_SESSION['automat']['firstData']['succes'] = 2;
                    return $this->redirect('/admin/automat/first-data');
                }
                else
                {
                    return $this->render('all', compact('allSeasonWithLeagues'));
                }
            }
            else
            {
                return $this->asJson(['error' => 'Вы не создали лиги. Для создания сезонов, создайте сначала лиги']);
            }
        }
        else
        {
            if(isset($_SESSION['automat']['firstData']['value']) && $_SESSION['automat']['firstData']['value'] == 2)
            {
                $_SESSION['automat']['firstData']['succes'] = 2;
                return $this->redirect('/admin/automat/first-data');
            }
            else
            {
                return $this->asJson(['error' => 'Сезоны уже созданы']);
            }
        }
    }
}