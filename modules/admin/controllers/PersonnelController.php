<?php
/**
 * Created by PhpStorm.
 * User: Maksim
 * Date: 22.12.2017
 * Time: 17:04
 */

namespace app\modules\admin\controllers;

use app\models\PersonnelJudiciarys;
use app\models\PersonnelTrainers;
use app\models\PersonnelCommissars;
use app\models\Seasons;
use app\models\TrainersToSeasons;
use phpQuery;
use app\libs\Parsing;
use yii\web\Controller;

class PersonnelController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionTrainerAll()
    {
        session_start();
        $parsing = new Parsing();
        $trainerData = [];
        $trainerData['success'] = [];
        $trainerData['error'] = [];
        $allSeason = Seasons::find()->all();
        foreach($allSeason as $season)
        {
            $url = [];
            $url[] = "http://widgets.baskethotel.com/widget-service/show?";
            $url[] = "&api=aaed6059969d182ff2a08e7d61458ce2d1a372c1";
            $url[] = "&lang=uk";
            $url[] = "&nnav=1";
            $url[] = "&nav_object=0";
            $url[] = "&request[0][container]=5-105-list-container&request[0][widget]=105";
            $url[] = "&request[0][part]=list";
            $url[] = "&request[0][state]=MzI5MjEyOTI2N2E6MTE6e3M6MTc6InRlYW1fbGlua192aXNpYmxlIjtzOjE6IjEiO3M6MTc6ImdhbWVfbGlua192aXNpYmxlIjtzOjE6IjEiO3M6MTk6InBsYXllcl9saW5rX3Zpc2libGUiO3M6MToiMSI7czoxNDoidGVhbV9saW5rX3R5cGUiO3M6MToiMyI7czoxNDoiZ2FtZV9saW5rX3R5cGUiO3M6MToiMyI7czoxNjoicGxheWVyX2xpbmtfdHlwZSI7czoxOiIzIjtzOjE3OiJ0ZWFtX2xpbmtfaGFuZGxlciI7czoxMjoibmF2aWdhdGVUZWFtIjtzOjE3OiJnYW1lX2xpbmtfaGFuZGxlciI7czoxMjoibmF2aWdhdGVHYW1lIjtzOjE5OiJwbGF5ZXJfbGlua19oYW5kbGVyIjtzOjE0OiJuYXZpZ2F0ZVBsYXllciI7czo5OiJsZWFndWVfaWQiO3M6MzoiMjI0IjtzOjE4OiJzaW5nbGVfbGV0dGVyX21vZGUiO3M6MToiMCI7fQ==";
            $url[] = "&request[0][param][filter][season]=".$season->id;
            $url[] = "&request[0][param][filter][team]=";
            $url[] = "&request[0][param][filter][q]=";
            $url[] = "&request[0][param][filter][fl]=";
            $url[] = "&request[0][param][type]=3";
            $allTrainerHtml = $parsing->request(implode($url));
            $pqTrainer = phpQuery::newDocument($allTrainerHtml);
            $trainerArr = [];
            foreach($pqTrainer->find('.mbt-layout-top') as $keyTrainers=>$valueTrainers)
            {
                $trainerArrPars = explode('<br>', trim(pq($valueTrainers)->find('.mbt-holder-content1')->html()));
                foreach($trainerArrPars as $keyTrainerArrPars=>$valueTrainerArrPars)
                {
                    if($keyTrainerArrPars == count($trainerArrPars) - 1)
                    {
                        continue;
                    }
                    else
                    {
                        $trainerArr[$keyTrainers][$keyTrainerArrPars] = explode('(', $valueTrainerArrPars);
                        $trainerArr[$keyTrainers][$keyTrainerArrPars][0] = explode(' ', strip_tags($trainerArr[$keyTrainers][$keyTrainerArrPars][0]));
                        $trainerArr[$keyTrainers][$keyTrainerArrPars][1] = trim(str_replace(',', '' , str_replace(')', '', $trainerArr[$keyTrainers][$keyTrainerArrPars][1])));
                        $firstName = trim(strip_tags($trainerArr[$keyTrainers][$keyTrainerArrPars][0][0]));
                        $lastName = trim(strip_tags($trainerArr[$keyTrainers][$keyTrainerArrPars][0][1]));
                        $teamName = $trainerArr[$keyTrainers][$keyTrainerArrPars][1];
                        $findTrainer = PersonnelTrainers::find()->where(['first_name' => $firstName, 'last_name' => $lastName])->all()[0];
                        if(!empty($findTrainer))
                        {
                            // если тренер есть
                            $findTrainerSeason = TrainersToSeasons::find()->where(['trainer_id' => $findTrainer->id, 'season_id' => $season->id])->one();
                            if(!empty($findTrainerSeason))
                            {
                                // если такой теренер уже работает в сезоне
                                continue;
                            }
                            else
                            {
                                // если тренер есть, но он не работает в таком сезоне
                                $addTrainerToSeason = new TrainersToSeasons();
                                $addTrainerToSeason->trainer_id = $findTrainer->id;
                                $addTrainerToSeason->season_id = $season->id;
                                $addTrainerToSeason->team_name = $teamName;
                                $addTrainerToSeason->save();
                                if($addTrainerToSeason->save())
                                {
                                    $trainerData['success'] = 'Существующему тренеру добавили новый сезон';
                                }
                                else
                                {
                                    $trainerData['error'] = 'Тренен уже сществует, но при добавлении сезона непрошли валидацию';
                                }
                            }
                        }
                        else
                        {
                            // если такого тренера не существует
                            $addTrainer = new PersonnelTrainers();
                            $addTrainer->first_name = $firstName;
                            $addTrainer->last_name = $lastName;
                            $addTrainer->save();
                            $addTrainerToSeason = new TrainersToSeasons();
                            $addTrainerToSeason->trainer_id = $addTrainer->id;
                            $addTrainerToSeason->season_id = $season->id;
                            $addTrainerToSeason->team_name = $teamName;
                            $addTrainerToSeason->save();
                            if($addTrainer->save() && $addTrainerToSeason->save())
                            {
                                $trainerData['success'] = 'Создан новый тренер и ему присвоен новый сезон';
                            }
                            else
                            {
                                $trainerData['error'] = 'Пре сохранении нового тренера или сохнанения сезона не прошли валидацию';
                            }
                        }
                    }
                }
            }
            $trainerData[$season->id] = $trainerArr;
        }
        if(isset($_SESSION['automat']['firstData']['value']) && $_SESSION['automat']['firstData']['value'] == 6)
        {
            $_SESSION['automat']['firstData']['succes'] = 6;
            return $this->redirect('/admin/automat/first-data');
        }
        else
        {
            return $this->render('trainer-all', ['res' => $trainerData]);
        }
    }

    public function actionCommissarAll()
    {
        session_start();
        $parsing = new Parsing();
        $commissarData = [];
        $url = [];
        $url[] = "http://widgets.baskethotel.com/widget-service/show?";
        $url[] = "&api=aaed6059969d182ff2a08e7d61458ce2d1a372c1";
        $url[] = "&lang=uk";
        $url[] = "&nnav=1";
        $url[] = "&nav_object=0";
        $url[] = "&request[0][container]=5-105-tab-container";
        $url[] = "&request[0][widget]=105";
        $url[] = "&request[0][part]=comissioners";
        $url[] = "&request[0][state]=Mzg0Njk0NDM0MWE6MTE6e3M6MTc6InRlYW1fbGlua192aXNpYmxlIjtzOjE6IjEiO3M6MTc6ImdhbWVfbGlua192aXNpYmxlIjtzOjE6IjEiO3M6MTk6InBsYXllcl9saW5rX3Zpc2libGUiO3M6MToiMSI7czoxNDoidGVhbV9saW5rX3R5cGUiO3M6MToiMyI7czoxNDoiZ2FtZV9saW5rX3R5cGUiO3M6MToiMyI7czoxNjoicGxheWVyX2xpbmtfdHlwZSI7czoxOiIzIjtzOjE3OiJ0ZWFtX2xpbmtfaGFuZGxlciI7czoxMjoibmF2aWdhdGVUZWFtIjtzOjE3OiJnYW1lX2xpbmtfaGFuZGxlciI7czoxMjoibmF2aWdhdGVHYW1lIjtzOjE5OiJwbGF5ZXJfbGlua19oYW5kbGVyIjtzOjE0OiJuYXZpZ2F0ZVBsYXllciI7czo5OiJsZWFndWVfaWQiO3M6NDoiODE4NSI7czoxODoic2luZ2xlX2xldHRlcl9tb2RlIjtzOjE6IjAiO30=";
        $allCommissarHtml = $parsing->request(implode($url));
        $pqCommissar = phpQuery::newDocument($allCommissarHtml);
        foreach($pqCommissar->find('#5-105-list-container .mbt-layout-top') as $key=>$div)
        {
            $namesArr = explode('<br>', trim(pq($div)->find('.mbt-holder-content1 .mbt-layout-half')->html()));
            foreach($namesArr as $keyName=>$value)
            {
                if(!empty($value))
                {
                    $commissarData['data'][$key][$keyName] = explode(' ', trim(explode('(', $value)[0]));
                    $findPersonnerCommissar = PersonnelCommissars::find()->where([
                        'first_name' => $commissarData['data'][$key][$keyName][0],
                        'last_name' => $commissarData['data'][$key][$keyName][1]
                    ])->one();
                    if(empty($findPersonnerCommissar))
                    {
                        $personnerCommissar = new PersonnelCommissars();
                        $personnerCommissar->first_name = $commissarData['data'][$key][$keyName][0];
                        $personnerCommissar->last_name = $commissarData['data'][$key][$keyName][1];
                        $personnerCommissar->save();
                    }
                }
                else
                {
                    continue;
                }
            }
        }
        if(isset($_SESSION['automat']['firstData']['value']) && $_SESSION['automat']['firstData']['value'] == 4)
        {
            $_SESSION['automat']['firstData']['succes'] = 4;
            return $this->redirect('/admin/automat/first-data');
        }
        else
        {
            return $this->render('commissar-all', ['res' => $commissarData]);
        }
    }

    public function actionJudiciaryAll()
    {
        session_start();
        $parsing = new Parsing();
        $judiciaryData = [];
        $judiciaryData['success'] = [];
        $judiciaryData['error'] = [];
        $url = [];
        $url[] = "http://widgets.baskethotel.com/widget-service/show?";
        $url[] = "&api=aaed6059969d182ff2a08e7d61458ce2d1a372c1";
        $url[] = "&lang=uk";
        $url[] = "&nnav=1";
        $url[] = "&nav_object=0";
        $url[] = "&request[0][container]=5-105-tab-container";
        $url[] = "&request[0][widget]=105";
        $url[] = "&request[0][part]=referees";
        $url[] = "&request[0][state]=MzI5MjEyOTI2N2E6MTE6e3M6MTc6InRlYW1fbGlua192aXNpYmxlIjtzOjE6IjEiO3M6MTc6ImdhbWVfbGlua192aXNpYmxlIjtzOjE6IjEiO3M6MTk6InBsYXllcl9saW5rX3Zpc2libGUiO3M6MToiMSI7czoxNDoidGVhbV9saW5rX3R5cGUiO3M6MToiMyI7czoxNDoiZ2FtZV9saW5rX3R5cGUiO3M6MToiMyI7czoxNjoicGxheWVyX2xpbmtfdHlwZSI7czoxOiIzIjtzOjE3OiJ0ZWFtX2xpbmtfaGFuZGxlciI7czoxMjoibmF2aWdhdGVUZWFtIjtzOjE3OiJnYW1lX2xpbmtfaGFuZGxlciI7czoxMjoibmF2aWdhdGVHYW1lIjtzOjE5OiJwbGF5ZXJfbGlua19oYW5kbGVyIjtzOjE0OiJuYXZpZ2F0ZVBsYXllciI7czo5OiJsZWFndWVfaWQiO3M6MzoiMjI0IjtzOjE4OiJzaW5nbGVfbGV0dGVyX21vZGUiO3M6MToiMCI7fQ==";
        $allJudiciaryHtml = $parsing->request(implode($url));
        $pqJudiciary = phpQuery::newDocument($allJudiciaryHtml);
        $judiciaryResultPars = [];
        foreach($pqJudiciary->find('#5-105-list-container .mbt-layout-top') as $key=>$div)
        {
            $judiciaryResultPars[$key] = explode('<br>', trim(pq($div)->find('.mbt-holder-content1 .mbt-layout-half')->html()));
            foreach($judiciaryResultPars[$key] as $keyJudiciaryResultPars=>$valueJudiciaryResultPars)
            {
                if(!empty($valueJudiciaryResultPars))
                {
                    $judiciaryData[$key][$keyJudiciaryResultPars] = explode(' ', trim(explode('(', $valueJudiciaryResultPars)[0]));
                    $findPersonnerJudiciary = PersonnelJudiciarys::find()->where([
                        'first_name' => $judiciaryData[$key][$keyJudiciaryResultPars][0],
                        'last_name' => $judiciaryData[$key][$keyJudiciaryResultPars][1],
                    ])->one();
                    if(empty($findPersonnerJudiciary))
                    {
                        $personnerJudiciary = new PersonnelJudiciarys();
                        $personnerJudiciary->first_name = $judiciaryData[$key][$keyJudiciaryResultPars][0];
                        $personnerJudiciary->last_name = $judiciaryData[$key][$keyJudiciaryResultPars][1];
                        $personnerJudiciary->save();
                    }
                }
                else
                {
                    continue;
                }
            }
        }
        if(isset($_SESSION['automat']['firstData']['value']) && $_SESSION['automat']['firstData']['value'] == 5)
        {
            $_SESSION['automat']['firstData']['succes'] = 5;
            return $this->redirect('/admin/automat/first-data');
        }
        else
        {
            return $this->render('judiciary-all', ['res' => $judiciaryData]);
        }
    }
}