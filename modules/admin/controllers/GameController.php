<?php
namespace app\modules\admin\controllers;

use app\libs\Parsing;
use app\models\GameJudiciarys;
use app\models\GameOtherStatistic;
use app\models\GameProtocolCommands;
use app\models\GameProtocolTeamComparisons;
use app\models\GameRootedStatistics;
use app\models\Games;
use app\models\PersonnelCommissars;
use app\models\PersonnelJudiciarys;
use app\models\PersonnelTrainers;
use app\models\Players;
use app\models\Seasons;
use app\models\SettingSeasonForGameProtocol;
use app\models\Teams;
use yii\helpers\Url;
use yii\web\Controller;
use phpQuery;

class GameController extends Controller
{
    public function actionIndex()
    {
        session_start();
        unset($_SESSION['game']);
        return $this->render('index');
    }

    public function actionParsAll()
    {
        session_start();
        $seasons = Seasons::find()->asArray()->all();
        if(count($seasons) > 0)
        {
            $tableElement = [];
            $tableElement['success'] = [];
            $tableElement['errors'] = [];
            if(!isset($_SESSION['game']['seasons']['now']))
            {
                $_SESSION['game']['seasons']['now'] = 1;
            }
            $tableElement['count_season'] = count($seasons);
            $tableElement['this_season'] = $_SESSION['game']['seasons']['now'];
            if($_SESSION['game']['seasons']['now'] > count($seasons))
            {
                unset($_SESSION['game']['seasons']['now']);
                return $this->render('pars-finish');
            }
            else
            {
                if(!isset($_SESSION['game']['parsing']['page']))
                {
                    $_SESSION['game']['parsing']['page'] = 1;
                    $_SESSION['game']['seasons']['now'] += 1;
                    $this->redirect(Url::to('pars-all'));
                }
                else
                {
                    $tableElement['this_page'] = $_SESSION['game']['parsing']['page'];
                    $parsing = new Parsing();
                    $urlArr[] = "http://widgets.baskethotel.com/widget-service/show?";
                    $urlArr[] = "&api=aaed6059969d182ff2a08e7d61458ce2d1a372c1";
                    $urlArr[] = "&lang=uk";
                    $urlArr[] = "&nnav=1";
                    $urlArr[] = "&nav_object=0";
                    $urlArr[] = "&flash=0";
                    $urlArr[] = "&request[0][container]=5-303-container";
                    $urlArr[] = "&request[0][widget]=303";
                    $urlArr[] = "&request[0][part]=schedule_and_results";
                    $urlArr[] = "&request[0][state]=MzkwMDcwMjU2NmE6MTk6e3M6MTc6InRlYW1fbGlua192aXNpYmxlIjtzOjE6IjEiO3M6MTc6ImdhbWVfbGlua192aXNpYmxlIjtzOjE6IjEiO3M6MTk6InBsYXllcl9saW5rX3Zpc2libGUiO3M6MToiMSI7czoxNDoidGVhbV9saW5rX3R5cGUiO3M6MToiMyI7czoxNDoiZ2FtZV9saW5rX3R5cGUiO3M6MToiMyI7czoxNjoicGxheWVyX2xpbmtfdHlwZSI7czoxOiIzIjtzOjE3OiJ0ZWFtX2xpbmtfaGFuZGxlciI7czoxMjoibmF2aWdhdGVUZWFtIjtzOjE3OiJnYW1lX2xpbmtfaGFuZGxlciI7czoxMjoibmF2aWdhdGVHYW1lIjtzOjE5OiJwbGF5ZXJfbGlua19oYW5kbGVyIjtzOjE0OiJuYXZpZ2F0ZVBsYXllciI7czoxODoiYXJlbmFfbGlua192aXNpYmxlIjtzOjE6IjEiO3M6MTU6ImFyZW5hX2xpbmtfdHlwZSI7czoxOiIzIjtzOjE4OiJhcmVuYV9saW5rX2hhbmRsZXIiO3M6MTM6Im5hdmlnYXRlQXJlbmEiO3M6OToibGVhZ3VlX2lkIjtzOjM6IjIyNCI7czoyNzoic2hvd19yZWNlbnRseV91cGRhdGVkX2dhbWVzIjtzOjE6IjAiO3M6MTI6InNob3dfaGVhZGVycyI7czoxOiIxIjtzOjE2OiJzaG93X2dhbWVfbnVtYmVyIjtzOjE6IjEiO3M6MTg6InNob3dfcGxheW9mZl9zdGFnZSI7czoxOiIxIjtzOjE1OiJzaG93X2dyb3VwX25hbWUiO3M6MToiMSI7czoyMDoic2hvd19zZWFzb25fc2VsZWN0b3IiO3M6MToiMSI7fQ==";
                    $urlArr[] = "&request[0][param][season_id]=".$seasons[$_SESSION['game']['seasons']['now']-1]['id'];
                    $urlArr[] = "&request[0][param][filter][month]=all";
                    $urlArr[] = "&request[0][param][filter][type]=results_only";
                    $urlArr[] = "&request[0][param][page]=".$_SESSION['game']['parsing']['page'];
                    $allGamesHtml = $parsing->request(implode($urlArr));
                    $table = phpQuery::newDocument($allGamesHtml);
                    $dataFromSave = [];
                    if(count($table->find('.mbt-table tr')) > 0)
                    {
                        foreach($table->find('.mbt-table tr') as $key => $tr) // проходим по всем играм определенной страницы
                        {
                            if($key%2 != 0)
                            {
                                $td = phpQuery::newDocument(pq($tr)->html());
                                foreach($td->find('td') as $key2 => $item) // проходим по всем колонкам конкретной игры
                                {
                                    $tableElement[$_SESSION['game']['parsing']['page']][$key][$parsing->tableCalendarNameColum[$key2]] = trim(pq($item)->html());
                                    switch ($parsing->tableCalendarNameColum[$key2])
                                    {
                                        case 'Дата та час':
                                            $dataDate = preg_replace( '/\"/','\'', $tableElement[$_SESSION['game']['parsing']['page']][$key][$parsing->tableCalendarNameColum[$key2]]);
                                            $date = phpQuery::newDocument($dataDate);
                                            $dateText = $date->find('a')->text();
                                            $dataFromSave['date'] = date('Y-m-d H:i:s', strtotime(str_replace('/', '-',$dateText)));
                                            $dataFromSave['game_id'] = $date->find('a')->attr('game_id');
                                            $dataFromSave['season_id'] = $date->find('a')->attr('season_id');
                                            $urlToGame = $date->find('a')->attr('href');
                                            $dataFromSave['league'] = explode('/', str_replace('http://fbu.ua/statistics/league-', '', $urlToGame))[0];
                                            break;
                                        case '№ гри':
                                            $dataFromSave['number_game'] = $tableElement[$_SESSION['game']['parsing']['page']][$key][$parsing->tableCalendarNameColum[$key2]];
                                            break;
                                        case 'Група':
                                            $dataFromSave['group'] = $tableElement[$_SESSION['game']['parsing']['page']][$key][$parsing->tableCalendarNameColum[$key2]];
                                            break;
                                        case 'Етап':
                                            $dataFromSave['step'] = $tableElement[$_SESSION['game']['parsing']['page']][$key][$parsing->tableCalendarNameColum[$key2]];
                                            break;
                                        case 'Господарі':
                                            $command1 = phpQuery::newDocument($tableElement[$_SESSION['game']['parsing']['page']][$key][$parsing->tableCalendarNameColum[$key2]]);
                                            $dataFromSave['command_1'] = $command1->find('a')->attr('team_id');
                                            $dataFromSave['command_1__season_id'] = $command1->find('a')->attr('season_id');
                                            $dataFromSave['command_1__name'] = trim($command1->find('a')->text());
                                            break;
                                        case 'Результат':
                                            $resultGame = phpQuery::newDocument($tableElement[$_SESSION['game']['parsing']['page']][$key][$parsing->tableCalendarNameColum[$key2]]);
                                            $resultGameArr = explode(':', $resultGame->find('a')->text()); // $result_command_1 result_command_2
                                            $dataFromSave['result_command_1'] = $resultGameArr[0];
                                            $dataFromSave['result_command_2'] = $resultGameArr[1];
                                            break;
                                        case 'Гості':
                                            $command2 = phpQuery::newDocument($tableElement[$_SESSION['game']['parsing']['page']][$key][$parsing->tableCalendarNameColum[$key2]]);
                                            $dataFromSave['command_2'] = $command2->find('a')->attr('team_id');
                                            $dataFromSave['command_2__season_id'] = $command2->find('a')->attr('season_id');
                                            $dataFromSave['command_2__name'] = trim($command2->find('a')->text());
                                            break;
                                        case 'Арена':
                                            $arena = phpQuery::newDocument($tableElement[$_SESSION['game']['parsing']['page']][$key][$parsing->tableCalendarNameColum[$key2]]);
                                            $dataFromSave['arena'] = $arena->find('a')->attr('arena_id');
                                            break;
                                    }
                                }
                                // записываем данные в базу, которые удалось получить с парсинга
                                $findThisGame = Games::find()->where(['id' => $dataFromSave['game_id']])->all();
                                if(count($findThisGame) > 0)
                                {
                                    $tableElement['errors'] = 'Игра уже существует!';
                                }
                                else
                                {
                                    $findSeason = Seasons::find()->where(['id' => $dataFromSave['command_1__season_id']])->one();
                                    if(!empty($findSeason))
                                    {
                                        $findCommand_1 = Teams::find()->where(['team_id' => $dataFromSave['command_1'], 'league_id' => $findSeason->league_id])->one();
                                        if(empty($findCommand_1))
                                        {
                                            $createCommand_1 = new Teams();
                                            $createCommand_1->team_id = $dataFromSave['command_1'];
                                            $createCommand_1->league_id = $findSeason->league_id;
                                            $createCommand_1->name = $dataFromSave['command_1__name'];
                                            $createCommand_1->save();
                                        }
                                        $findCommand_2 = Teams::find()->where(['team_id' => $dataFromSave['command_2'], 'league_id' => $findSeason->league_id])->one();
                                        if(empty($findCommand_2))
                                        {
                                            $createCommand_2 = new Teams();
                                            $createCommand_2->team_id = $dataFromSave['command_2'];
                                            $createCommand_2->league_id = $findSeason->league_id;
                                            $createCommand_2->name = $dataFromSave['command_2__name'];
                                            $createCommand_2->save();
                                        }
                                        $game = new Games();
                                        $game->id = $dataFromSave['game_id'];
                                        $game->season_id = $dataFromSave['season_id'];
                                        $game->date = $dataFromSave['date'];
                                        $game->number_game = $dataFromSave['number_game'];
                                        $game->group = $dataFromSave['group'];
                                        $game->step = $dataFromSave['step'];
                                        $game->command_1 = $dataFromSave['command_1'];
                                        $game->command_2 = $dataFromSave['command_2'];
                                        $game->result_command_1 = $dataFromSave['result_command_1'];
                                        $game->result_command_2 = $dataFromSave['result_command_2'];
                                        $game->arena = $dataFromSave['arena'];
                                        if($game->save())
                                        {
                                            $tableElement['success'] = 'Игра успешно сохранена';
                                        }
                                        else
                                        {
                                            $tableElement['errors'] = 'Не прошли валидацию при сохранении игры';
                                        }
                                        $tableElement['resultParsingOneGame'] = $dataFromSave;
                                    }
                                    else
                                    {
                                        $tableElement['errors'] = 'Не найден сезон в котором играют комады в даной игре';
                                    }
                                }
                            }
                        }
                        $_SESSION['game']['parsing']['page'] += 1;
                    }
                    else
                    {
                        unset($_SESSION['game']['parsing']['page']);
                    }
                    return $this->render('pars-all', ['res' => $tableElement]);
                }
            }
        }
        else
        {
            return $this->asJson(['error' => 'Создайте сезоны, чтобы приступить к парсингу игр']);
        }
    }

    public function actionParsOne()
    {
        session_start();
        $gameData = [];
        $gameData['url'] = '';
        $gameData['info'] = [];
        $gameData['errors'] = [];
        $parsing = new Parsing();
        $allGames = Games::find()->all();
        if(isset($_SESSION['game']['now']) && $_SESSION['game']['now'] == $_SESSION['game']['count'])
        {
            unset($_SESSION['game']['now']);
            return $this->render('pars-one-finish');
        }
        else
        {
            if(!isset($_SESSION['game']['now']) || $_SESSION['game']['now'] <=0)
            {
                $_SESSION['game']['count'] = count($allGames);
                $_SESSION['game']['now'] = 1;
            }
            else
            {
                $_SESSION['game']['now'] += 1;
            }
            $gameId = $allGames[$_SESSION['game']['now'] - 1]->id;
//            $gameId = 3974127;
            $seasonId = $allGames[$_SESSION['game']['now'] - 1]->season_id ;
//            $seasonId = 99965;
            $gameData['count'] = $_SESSION['game']['count'];
            $gameData['now'] = $_SESSION['game']['now'];
            $gameData['id'] = $gameId;
            $gameData['seasonId'] = $seasonId;
            $url = [];
            $url[] = 'http://widgets.baskethotel.com/widget-service/show?';
            $url[] = '&api=aaed6059969d182ff2a08e7d61458ce2d1a372c1';
            $url[] = '&lang=uk';
            $url[] = '&nnav=1';
            $url[] = '&nav_object=0';
            $url[] = '&request[0][container]=game';
            $url[] = '&request[0][widget]=400';
            $url[] = '&request[0][param][team_link_visible]=1';
            $url[] = '&request[0][param][game_link_visible]=1';
            $url[] = '&request[0][param][player_link_visible]=1';
            $url[] = '&request[0][param][team_link_type]=3';
            $url[] = '&request[0][param][game_link_type]=3';
            $url[] = '&request[0][param][player_link_type]=3';
            $url[] = '&request[0][param][team_link_handler]=navigateTeam';
            $url[] = '&request[0][param][game_link_handler]=navigateGame';
            $url[] = '&request[0][param][player_link_handler]=navigatePlayer';
            $url[] = '&request[0][param][arena_link_visible]=1';
            $url[] = '&request[0][param][arena_link_type]=3';
            $url[] = '&request[0][param][arena_link_handler]=navigateArena';
//            $url[] = '&request[0][param][league_id]='.$leagueId;
            $url[] = '&request[0][param][season_id]='.$seasonId;
            $url[] = '&request[0][param][game_id]='.$gameId;
            $allDataGameHtml = $parsing->request(implode($url));
            $gameData['url'] = $parsing->finishUrl;
//            return var_dump($allDataGameHtml);
            $pqGame = phpQuery::newDocument($allDataGameHtml);
            $arrFirstQuarterComand = explode(' - ', $pqGame->find('.mbt-gamecard-info div:eq(1) span:eq(0)')->text());
            $gameData[$seasonId]['first_quarter_comand_1'] = $arrFirstQuarterComand[0];
            $gameData[$seasonId]['first_quarter_comand_2'] = $arrFirstQuarterComand[1];
            $arrSeccondQuarterComand = explode(' - ', $pqGame->find('.mbt-gamecard-info div:eq(1) span:eq(1)')->text());
            $gameData[$seasonId]['second_quarter_comand_1'] = $arrSeccondQuarterComand[0];
            $gameData[$seasonId]['second_quarter_comand_2'] = $arrSeccondQuarterComand[1];
            $arrThirdQuarterComand = explode(' - ', $pqGame->find('.mbt-gamecard-info div:eq(1) span:eq(2)')->text());
            $gameData[$seasonId]['third_quarter_comand_1'] = $arrThirdQuarterComand[0];
            $gameData[$seasonId]['third_quarter_comand_2'] = $arrThirdQuarterComand[1];
            $arrFourQuarterComand = explode(' - ', $pqGame->find('.mbt-gamecard-info .mbt-gamecard-quarters span:eq(3)')->text());
            $gameData[$seasonId]['fourth_quarter_comand_1'] = $arrFourQuarterComand[0];
            $gameData[$seasonId]['fourth_quarter_comand_2'] = $arrFourQuarterComand[1];
            if(0 == (integer)$pqGame->find('.mbt-gamecard-info .mbt-gamecard-info strong:eq(2))')->text())
            {
                $gameData[$seasonId]['viewers'] = (integer)$pqGame->find('.mbt-gamecard-info .mbt-gamecard-info strong:eq(3))')->text();
            }
            else
            {
                $gameData[$seasonId]['viewers'] = (integer)$pqGame->find('.mbt-gamecard-info .mbt-gamecard-info strong:eq(2))')->text();
            }
            $gameData[$seasonId]['arena'] = $pqGame->find('.mbt-gamecard-info strong:eq(2) a')->attr('arena_id');
            $gameData[$seasonId]['the_result_of_the_five_comand_1'] = $pqGame->find('.mbt-holder-content1 .mbt-table tr:eq(1) td:eq(1)')->text();
            $gameData[$seasonId]['the_result_of_the_five_comand_2'] = $pqGame->find('.mbt-holder-content1 .mbt-table tr:eq(1) td:eq(2)')->text();
            $gameData[$seasonId]['beat_the_spare_comand_1'] = $pqGame->find('.mbt-holder-content1 .mbt-table tr:eq(2) td:eq(1)')->text();
            $gameData[$seasonId]['beat_the_spare_comand_2'] = $pqGame->find('.mbt-holder-content1 .mbt-table tr:eq(2) td:eq(2)')->text();
            $gameData[$seasonId]['the_hottest_jerk_comand_1'] = $pqGame->find('.mbt-holder-content1 .mbt-table tr:eq(3) td:eq(1)')->text();
            $gameData[$seasonId]['the_hottest_jerk_comand_2'] = $pqGame->find('.mbt-holder-content1 .mbt-table tr:eq(3) td:eq(2)')->text();
            $gameData[$seasonId]['the_biggest_gap_comand_1'] = $pqGame->find('.mbt-holder-content1 .mbt-table tr:eq(4) td:eq(1)')->text();
            $gameData[$seasonId]['the_biggest_gap_comand_2'] = $pqGame->find('.mbt-holder-content1 .mbt-table tr:eq(4) td:eq(2)')->text();
            $gameData[$seasonId]['points_from_the_zone_comand_1'] = $pqGame->find('.mbt-holder-content1 .mbt-table tr:eq(5) td:eq(1)')->text();
            $gameData[$seasonId]['points_from_the_zone_comand_2'] = $pqGame->find('.mbt-holder-content1 .mbt-table tr:eq(5) td:eq(2)')->text();
            $gameData[$seasonId]['commissar'] = explode(',', $pqGame->find('.mbt-gamecard-info strong:eq(4)')->text());
            if(count($gameData[$seasonId]['commissar']) > 1)
            {
                // тогда это тренеры
                foreach($gameData[$seasonId]['commissar'] as $key=>$judiciary)
                {
                    $judiciary_id = 0;
                    $judiciaryData = explode(" ", $judiciary);
                    $findJudiciary = PersonnelJudiciarys::find()->where([
                        'first_name' => $judiciaryData[0],
                        'last_name' => $judiciaryData[1],
                    ])->all()[0];
                    if(empty($findJudiciary))
                    {
                        $newJudiciary = new PersonnelJudiciarys();
                        $newJudiciary->first_name = $judiciaryData[0];
                        $newJudiciary->last_name = $judiciaryData[1];
                        $newJudiciary->save();
                        $judiciary_id = $newJudiciary->id;
                    }
                    else
                    {
                        $judiciary_id = $findJudiciary->id;
                    }
                    $findGameJudiciarys = GameJudiciarys::find()->where([
                        'game_id' => $gameId,
                        'judiciary_id' => $judiciary_id
                    ])->one();
                    if(empty($findGameJudiciarys))
                    {
                        $gameJudiciarys = new GameJudiciarys();
                        $gameJudiciarys->game_id = $gameId;
                        $gameJudiciarys->judiciary_id = $judiciary_id;
                        $gameJudiciarys->save();
                    }
                    $gameData[$seasonId]['commissar1'] = $pqGame->find('.mbt-gamecard-info strong:eq(5)')->text();
                    $commissar = explode(" ", $gameData[$seasonId]['commissar1']);
                    $findCommissar = PersonnelCommissars::find()->where([
                        'first_name' => $commissar[0],
                        'last_name' => $commissar[1],
                    ])->all()[0];
                    if(empty($findCommissar))
                    {
                        $newCommissar = new PersonnelCommissars();
                        $newCommissar->first_name = $commissar[0];
                        $newCommissar->last_name = $commissar[1];
                        $newCommissar->save();
                        $gameData[$seasonId]['commissar_id'] = $newCommissar->id;
                    }
                    else
                    {
                        $gameData[$seasonId]['commissar_id'] = $findCommissar->id;
                    }
                }
            }
            else
            {
                // это комирсар
                $commissar = explode(" ", $gameData[$seasonId]['commissar'][0]);
                $findCommissar = PersonnelCommissars::find()->where([
                    'first_name' => $commissar[0],
                    'last_name' => $commissar[1],
                ])->all()[0];
                if(empty($findCommissar))
                {
                    $newCommissar = new PersonnelCommissars();
                    $newCommissar->first_name = $commissar[0];
                    $newCommissar->last_name = $commissar[1];
                    $newCommissar->save();
                    $gameData[$seasonId]['commissar_id'] = $newCommissar->id;
                }
                else
                {
                    $gameData[$seasonId]['commissar_id'] = $findCommissar->id;
                }
            }

            $findGame = Games::find()->where(['id' => $gameId, 'season_id' => $seasonId])->all()[0];
            $findGame->first_quarter_comand_1 = $gameData[$seasonId]['first_quarter_comand_1'];
            $findGame->first_quarter_comand_2 = $gameData[$seasonId]['first_quarter_comand_2'];
            $findGame->second_quarter_comand_1 = $gameData[$seasonId]['second_quarter_comand_1'];
            $findGame->second_quarter_comand_2 = $gameData[$seasonId]['second_quarter_comand_2'];
            $findGame->third_quarter_comand_1 = $gameData[$seasonId]['third_quarter_comand_1'];
            $findGame->third_quarter_comand_2 = $gameData[$seasonId]['third_quarter_comand_2'];
            $findGame->fourth_quarter_comand_1 = $gameData[$seasonId]['fourth_quarter_comand_1'];
            $findGame->fourth_quarter_comand_2 = $gameData[$seasonId]['fourth_quarter_comand_2'];
            $findGame->viewers = $gameData[$seasonId]['viewers'].'';
            $findGame->arena = $gameData[$seasonId]['arena'];
            $findGame->commissar_id = $gameData[$seasonId]['commissar_id'];
            $findGame->the_result_of_the_five_comand_1 = $gameData[$seasonId]['the_result_of_the_five_comand_1'];
            $findGame->the_result_of_the_five_comand_2 = $gameData[$seasonId]['the_result_of_the_five_comand_2'];
            $findGame->beat_the_spare_comand_1 = $gameData[$seasonId]['beat_the_spare_comand_1'];
            $findGame->beat_the_spare_comand_2 = $gameData[$seasonId]['beat_the_spare_comand_2'];
            $findGame->the_hottest_jerk_comand_1 = $gameData[$seasonId]['the_hottest_jerk_comand_1'];
            $findGame->the_hottest_jerk_comand_2 = $gameData[$seasonId]['the_hottest_jerk_comand_2'];
            $findGame->the_biggest_gap_comand_1 = $gameData[$seasonId]['the_biggest_gap_comand_1'];
            $findGame->the_biggest_gap_comand_2 = $gameData[$seasonId]['the_biggest_gap_comand_2'];
            $findGame->points_from_the_zone_comand_1 = $gameData[$seasonId]['points_from_the_zone_comand_1'];
            $findGame->points_from_the_zone_comand_2 = $gameData[$seasonId]['points_from_the_zone_comand_2'];
            $findGame->save();
            // парсинг протокола игры
            $url[] = "http://widgets.baskethotel.com/widget-service/show?";
            $url[] = "&api=aaed6059969d182ff2a08e7d61458ce2d1a372c1";
            $url[] = "&lang=uk";
            $url[] = "&nnav=1";
            $url[] = "&nav_object=0";
            $url[] = "&request[0][container]=5-400-tab-container";
            $url[] = "&request[0][widget]=400";
            $url[] = "&request[0][part]=boxscore";
            $url[] = "&request[0][state]=MTU4OTM3NzAyNGE6MTU6e3M6MTc6InRlYW1fbGlua192aXNpYmxlIjtzOjE6IjEiO3M6MTc6ImdhbWVfbGlua192aXNpYmxlIjtzOjE6IjEiO3M6MTk6InBsYXllcl9saW5rX3Zpc2libGUiO3M6MToiMSI7czoxNDoidGVhbV9saW5rX3R5cGUiO3M6MToiMyI7czoxNDoiZ2FtZV9saW5rX3R5cGUiO3M6MToiMyI7czoxNjoicGxheWVyX2xpbmtfdHlwZSI7czoxOiIzIjtzOjE3OiJ0ZWFtX2xpbmtfaGFuZGxlciI7czoxMjoibmF2aWdhdGVUZWFtIjtzOjE3OiJnYW1lX2xpbmtfaGFuZGxlciI7czoxMjoibmF2aWdhdGVHYW1lIjtzOjE5OiJwbGF5ZXJfbGlua19oYW5kbGVyIjtzOjE0OiJuYXZpZ2F0ZVBsYXllciI7czoxODoiYXJlbmFfbGlua192aXNpYmxlIjtzOjE6IjEiO3M6MTU6ImFyZW5hX2xpbmtfdHlwZSI7czoxOiIzIjtzOjE4OiJhcmVuYV9saW5rX2hhbmRsZXIiO3M6MTM6Im5hdmlnYXRlQXJlbmEiO3M6OToibGVhZ3VlX2lkIjtzOjM6IjIyNCI7czo5OiJzZWFzb25faWQiO3M6NDoiODU0MyI7czo3OiJnYW1lX2lkIjtzOjY6IjI2NzEzNSI7fQ==";
            $url[] = "&request[0][param][team_id]=".$gameId;
            $gameProtocolHtml = $parsing->request(implode($url));
            $gameProtocolArr = [];
            $gameProtocolArr['error'] = '';
            $gameProtocolArr['success'] = '';
            $pqProtocol = phpQuery::newDocument($gameProtocolHtml);
            // парсинг таблицы команд
            function parsingTeamTable($gameId, $pqProtocol, $numberTable, $gameProtocolArr, $teamStatus, $nameCommand)
            {
                for($i = 0; $i < count($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr')); $i++)
                {
                    if($i == 0 ||
                        $i == 1 ||
                        $i == count($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr')) - 1 ||
                        $i == count($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr')) - 2)
                    {
                        continue;
                    }
                    $gameProtocolArr[$nameCommand][$i]['game_id'] = $gameId;
                    $gameProtocolArr[$nameCommand][$i]['team_status'] = $teamStatus;
                    $gameProtocolArr[$nameCommand][$i]['player_id'] = trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(1) a')->attr('player_id'));
                    $gameProtocolArr[$nameCommand][$i]['player_number'] = trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(0)')->text());
                    $gameProtocolArr[$nameCommand][$i]['time_in_game'] = trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(2)')->text());
                    $gameProtocolArr[$nameCommand][$i]['two_point_kiddies'] = trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(3) span:eq(1)')->text());
                    $gameProtocolArr[$nameCommand][$i]['two_point_kiddies_ok'] = trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(3) span:eq(0)')->text());
                    $gameProtocolArr[$nameCommand][$i]['two_point_kiddies_percent'] = str_replace("%", "", trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(4)')->text()));
                    $gameProtocolArr[$nameCommand][$i]['three_point_kiddies'] = trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(5) span:eq(1)')->text());
                    $gameProtocolArr[$nameCommand][$i]['three_point_kiddies_ok'] = trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(5) span:eq(0)')->text());
                    $gameProtocolArr[$nameCommand][$i]['three_point_kiddies_percent'] = str_replace("%", "", trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(6)')->text()));
                    $gameProtocolArr[$nameCommand][$i]['one_point_kiddies'] = trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(7) span:eq(1)')->text());
                    $gameProtocolArr[$nameCommand][$i]['one_point_kiddies_ok'] = trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(7) span:eq(0)')->text());
                    $gameProtocolArr[$nameCommand][$i]['one_point_kiddies_percent'] = str_replace("%", "", trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(8)')->text()));
                    $gameProtocolArr[$nameCommand][$i]['picking_up_in_an_attack'] = trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(9)')->text());
                    $gameProtocolArr[$nameCommand][$i]['picking_up_in_defense'] = trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(10)')->text());
                    $gameProtocolArr[$nameCommand][$i]['overall_picking_up'] = str_replace("%", "", trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(11)')->text()));
                    $gameProtocolArr[$nameCommand][$i]['transmissions'] = trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(12)')->text());
                    $gameProtocolArr[$nameCommand][$i]['personal_foul'] = trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(13)')->text());
                    $gameProtocolArr[$nameCommand][$i]['losses'] = trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(14)')->text());
                    $gameProtocolArr[$nameCommand][$i]['interception'] = trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(15)')->text());
                    $gameProtocolArr[$nameCommand][$i]['block_shots_1'] = trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(16)')->text());
                    $gameProtocolArr[$nameCommand][$i]['efficiency'] = trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(17)')->text());
                    $gameProtocolArr[$nameCommand][$i]['plus_minus'] = trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(18)')->text());
                    $gameProtocolArr[$nameCommand][$i]['gained_points'] = trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(19)')->text());
                    $findPlayer = Players::find()->where(['id' => $gameProtocolArr[$nameCommand][$i]['player_id']])->one();
                    if(empty($findPlayer))
                    {
                        $newPlayer = new Players();
                        $newPlayer->id =  $gameProtocolArr[$nameCommand][$i]['player_id'];
                        $palyerName = explode(" ", trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(1) a')->text()));
                        $newPlayer->first_name = $palyerName[0];
                        $newPlayer->last_name = $palyerName[1];
                        $newPlayer->save();
                    }
                    $findGameProtocolCommands = GameProtocolCommands::find()->where(['game_id' => $gameProtocolArr[$nameCommand][$i]['game_id'], 'player_id' => $gameProtocolArr[$nameCommand][$i]['player_id']])->one();
                    if(empty($findGameProtocolCommands))
                    {
                        $gameProtocolCommand = new GameProtocolCommands();
                        $gameProtocolCommand->game_id = $gameProtocolArr[$nameCommand][$i]['game_id'];
                        $gameProtocolCommand->team_status = $gameProtocolArr[$nameCommand][$i]['team_status'];
                        $gameProtocolCommand->player_id = $gameProtocolArr[$nameCommand][$i]['player_id'];
                        $gameProtocolCommand->player_number = $gameProtocolArr[$nameCommand][$i]['player_number'];
                        $gameProtocolCommand->time_in_game = $gameProtocolArr[$nameCommand][$i]['time_in_game'];
                        $gameProtocolCommand->two_point_kiddies = $gameProtocolArr[$nameCommand][$i]['two_point_kiddies'];
                        $gameProtocolCommand->two_point_kiddies_ok = $gameProtocolArr[$nameCommand][$i]['two_point_kiddies_ok'];
                        $gameProtocolCommand->two_point_kiddies_percent = $gameProtocolArr[$nameCommand][$i]['two_point_kiddies_percent'];
                        $gameProtocolCommand->three_point_kiddies = $gameProtocolArr[$nameCommand][$i]['three_point_kiddies'];
                        $gameProtocolCommand->three_point_kiddies_ok = $gameProtocolArr[$nameCommand][$i]['three_point_kiddies_ok'];
                        $gameProtocolCommand->three_point_kiddies_percent = $gameProtocolArr[$nameCommand][$i]['three_point_kiddies_percent'];
                        $gameProtocolCommand->one_point_kiddies = $gameProtocolArr[$nameCommand][$i]['one_point_kiddies'];
                        $gameProtocolCommand->one_point_kiddies_ok = $gameProtocolArr[$nameCommand][$i]['one_point_kiddies_ok'];
                        $gameProtocolCommand->one_point_kiddies_percent = $gameProtocolArr[$nameCommand][$i]['one_point_kiddies_percent'];
                        $gameProtocolCommand->picking_up_in_an_attack = $gameProtocolArr[$nameCommand][$i]['picking_up_in_an_attack'];
                        $gameProtocolCommand->picking_up_in_defense = $gameProtocolArr[$nameCommand][$i]['picking_up_in_defense'];
                        $gameProtocolCommand->overall_picking_up = $gameProtocolArr[$nameCommand][$i]['overall_picking_up'];
                        $gameProtocolCommand->transmissions = $gameProtocolArr[$nameCommand][$i]['transmissions'];
                        $gameProtocolCommand->personal_foul = $gameProtocolArr[$nameCommand][$i]['personal_foul'];
                        $gameProtocolCommand->losses = $gameProtocolArr[$nameCommand][$i]['losses'];
                        $gameProtocolCommand->interception = $gameProtocolArr[$nameCommand][$i]['interception'];
                        $gameProtocolCommand->block_shots_1 = $gameProtocolArr[$nameCommand][$i]['block_shots_1'];
                        $gameProtocolCommand->efficiency = $gameProtocolArr[$nameCommand][$i]['efficiency'];
                        $gameProtocolCommand->plus_minus = $gameProtocolArr[$nameCommand][$i]['plus_minus'];
                        $gameProtocolCommand->gained_points = $gameProtocolArr[$nameCommand][$i]['gained_points'];
                        if($gameProtocolCommand->save())
                        {
                            $gameProtocolArr['success'] = 'Данные сохранены успешно';
                        }
                        else
                        {
                            $gameProtocolArr['error'] = 'При сохранении записи об игре не прошли валидацию';
                        }
                    }
                }
                return $gameProtocolArr;
            }
            function parsingTeamTable2($gameId, $pqProtocol, $numberTable, $gameProtocolArr, $teamStatus, $nameCommand)
            {
                for($i = 0; $i < count($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr')); $i++)
                {
                    if($i == 0 ||
                        $i == 1 ||
                        $i == count($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr')) - 1 ||
                        $i == count($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr')) - 2)
                    {
                        continue;
                    }
                    $gameProtocolArr[$nameCommand][$i]['game_id'] = $gameId;
                    $gameProtocolArr[$nameCommand][$i]['team_status'] = $teamStatus;
                    $gameProtocolArr[$nameCommand][$i]['player_id'] = trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(1) a')->attr('player_id'));
                    $gameProtocolArr[$nameCommand][$i]['player_number'] = trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(0)')->text());
                    $gameProtocolArr[$nameCommand][$i]['time_in_game'] = trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(2)')->text());
                    $gameProtocolArr[$nameCommand][$i]['two_point_kiddies'] = trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(3) span:eq(1)')->text());
                    $gameProtocolArr[$nameCommand][$i]['two_point_kiddies_ok'] = trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(3) span:eq(0)')->text());
                    $gameProtocolArr[$nameCommand][$i]['two_point_kiddies_percent'] = str_replace("%", "", trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(4)')->text()));
                    $gameProtocolArr[$nameCommand][$i]['three_point_kiddies'] = trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(5) span:eq(1)')->text());
                    $gameProtocolArr[$nameCommand][$i]['three_point_kiddies_ok'] = trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(5) span:eq(0)')->text());
                    $gameProtocolArr[$nameCommand][$i]['three_point_kiddies_percent'] = str_replace("%", "", trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(6)')->text()));
                    $gameProtocolArr[$nameCommand][$i]['for_game_attempt'] = trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(7) span:eq(1)')->text());
                    $gameProtocolArr[$nameCommand][$i]['for_game_attempt_ok'] = trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(7) span:eq(0)')->text());
                    $gameProtocolArr[$nameCommand][$i]['for_game_attempt_percent'] = str_replace("%", "", trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(8)')->text()));
                    $gameProtocolArr[$nameCommand][$i]['one_point_kiddies'] = trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(9) span:eq(1)')->text());
                    $gameProtocolArr[$nameCommand][$i]['one_point_kiddies_ok'] = trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(9) span:eq(0)')->text());
                    $gameProtocolArr[$nameCommand][$i]['one_point_kiddies_percent'] = str_replace("%", "", trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(10)')->text()));
                    $gameProtocolArr[$nameCommand][$i]['picking_up_in_an_attack'] = trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(11)')->text());
                    $gameProtocolArr[$nameCommand][$i]['picking_up_in_defense'] = trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(12)')->text());
                    $gameProtocolArr[$nameCommand][$i]['overall_picking_up'] = trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(13)')->text());
                    $gameProtocolArr[$nameCommand][$i]['transmissions'] = trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(14)')->text());
                    $gameProtocolArr[$nameCommand][$i]['personal_foul'] = trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(15)')->text());
                    $gameProtocolArr[$nameCommand][$i]['losses'] = trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(16)')->text());
                    $gameProtocolArr[$nameCommand][$i]['interception'] = trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(17)')->text());
                    $gameProtocolArr[$nameCommand][$i]['block_shots_1'] = trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(18)')->text());
                    $gameProtocolArr[$nameCommand][$i]['block_shots_2'] = trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(19)')->text());
                    $gameProtocolArr[$nameCommand][$i]['efficiency'] = trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(20)')->text());
                    $gameProtocolArr[$nameCommand][$i]['plus_minus'] = trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(21)')->text());
                    $gameProtocolArr[$nameCommand][$i]['gained_points'] = trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(22)')->text());
                    $findPlayer = Players::find()->where(['id' => $gameProtocolArr[$nameCommand][$i]['player_id']])->one();
                    if(empty($findPlayer))
                    {
                        $newPlayer = new Players();
                        $newPlayer->id =  $gameProtocolArr[$nameCommand][$i]['player_id'];
                        $palyerName = explode(" ", trim($pqProtocol->find('.mbt-layout-top:eq('.$numberTable.') .mbt-holder-content1 tr:eq('.$i.') td:eq(1) a')->text()));
                        $newPlayer->first_name = $palyerName[0];
                        $newPlayer->last_name = $palyerName[1];
                        $newPlayer->save();
                    }
                    $findGameProtocolCommands = GameProtocolCommands::find()->where(['game_id' => $gameProtocolArr[$nameCommand][$i]['game_id'], 'player_id' => $gameProtocolArr[$nameCommand][$i]['player_id']])->one();
                    if(empty($findGameProtocolCommands))
                    {
                        $gameProtocolCommand = new GameProtocolCommands();
                        $gameProtocolCommand->game_id = $gameProtocolArr[$nameCommand][$i]['game_id'];
                        $gameProtocolCommand->team_status = $gameProtocolArr[$nameCommand][$i]['team_status'];
                        $gameProtocolCommand->player_id = $gameProtocolArr[$nameCommand][$i]['player_id'];
                        $gameProtocolCommand->player_number = $gameProtocolArr[$nameCommand][$i]['player_number'];
                        $gameProtocolCommand->time_in_game = $gameProtocolArr[$nameCommand][$i]['time_in_game'];
                        $gameProtocolCommand->two_point_kiddies = $gameProtocolArr[$nameCommand][$i]['two_point_kiddies'];
                        $gameProtocolCommand->two_point_kiddies_ok = $gameProtocolArr[$nameCommand][$i]['two_point_kiddies_ok'];
                        $gameProtocolCommand->two_point_kiddies_percent = $gameProtocolArr[$nameCommand][$i]['two_point_kiddies_percent'];
                        $gameProtocolCommand->three_point_kiddies = $gameProtocolArr[$nameCommand][$i]['three_point_kiddies'];
                        $gameProtocolCommand->three_point_kiddies_ok = $gameProtocolArr[$nameCommand][$i]['three_point_kiddies_ok'];
                        $gameProtocolCommand->three_point_kiddies_percent = $gameProtocolArr[$nameCommand][$i]['three_point_kiddies_percent'];
                        $gameProtocolCommand->for_game_attempt = $gameProtocolArr[$nameCommand][$i]['for_game_attempt'];
                        $gameProtocolCommand->for_game_attempt_ok = $gameProtocolArr[$nameCommand][$i]['for_game_attempt_ok'];
                        $gameProtocolCommand->for_game_attempt_percent = $gameProtocolArr[$nameCommand][$i]['for_game_attempt_percent'];
                        $gameProtocolCommand->one_point_kiddies = $gameProtocolArr[$nameCommand][$i]['one_point_kiddies'];
                        $gameProtocolCommand->one_point_kiddies_ok = $gameProtocolArr[$nameCommand][$i]['one_point_kiddies_ok'];
                        $gameProtocolCommand->one_point_kiddies_percent = $gameProtocolArr[$nameCommand][$i]['one_point_kiddies_percent'];
                        $gameProtocolCommand->picking_up_in_an_attack = $gameProtocolArr[$nameCommand][$i]['picking_up_in_an_attack'];
                        $gameProtocolCommand->picking_up_in_defense = $gameProtocolArr[$nameCommand][$i]['picking_up_in_defense'];
                        $gameProtocolCommand->overall_picking_up = $gameProtocolArr[$nameCommand][$i]['overall_picking_up'];
                        $gameProtocolCommand->transmissions = $gameProtocolArr[$nameCommand][$i]['transmissions'];
                        $gameProtocolCommand->personal_foul = $gameProtocolArr[$nameCommand][$i]['personal_foul'];
                        $gameProtocolCommand->losses = $gameProtocolArr[$nameCommand][$i]['losses'];
                        $gameProtocolCommand->interception = $gameProtocolArr[$nameCommand][$i]['interception'];
                        $gameProtocolCommand->block_shots_1 = $gameProtocolArr[$nameCommand][$i]['block_shots_1'];
                        $gameProtocolCommand->block_shots_2 = $gameProtocolArr[$nameCommand][$i]['block_shots_2'];
                        $gameProtocolCommand->efficiency = $gameProtocolArr[$nameCommand][$i]['efficiency'];
                        $gameProtocolCommand->plus_minus = $gameProtocolArr[$nameCommand][$i]['plus_minus'];
                        $gameProtocolCommand->gained_points = $gameProtocolArr[$nameCommand][$i]['gained_points'];
                        if($gameProtocolCommand->save())
                        {
                            $gameProtocolArr['success'] = 'Данные сохранены успешно';
                        }
                        else
                        {
                            $gameProtocolArr['error'] = 'При сохранении записи об игре не прошли валидацию';
                        }
                    }

                }
                return $gameProtocolArr;
            }
            $findSettingSeasonForGameProtocol = SettingSeasonForGameProtocol::find()->asArray()->all();
            if(count($findSettingSeasonForGameProtocol) > 0)
            {
                for($i = 0; $i < count($findSettingSeasonForGameProtocol); $i++)
                {
                    if($findSettingSeasonForGameProtocol[$i]['season_id'] == $seasonId)
                    {
                        $gameProtocolArr = parsingTeamTable($gameId, $pqProtocol, 0, $gameProtocolArr, 1, 'command_1');
                        $gameProtocolArr = parsingTeamTable($gameId, $pqProtocol, 2, $gameProtocolArr, 2, 'command_2');
                    }
                    else
                    {
                        $gameProtocolArr = parsingTeamTable2($gameId, $pqProtocol, 0, $gameProtocolArr, 1, 'command_1');
                        $gameProtocolArr = parsingTeamTable2($gameId, $pqProtocol, 2, $gameProtocolArr, 2, 'command_2');
                    }
                }
            }
            else
            {
                $gameProtocolArr = parsingTeamTable2($gameId, $pqProtocol, 0, $gameProtocolArr, 1, 'command_1');
                $gameProtocolArr = parsingTeamTable2($gameId, $pqProtocol, 2, $gameProtocolArr, 2, 'command_2');
            }
            // парсинг талбицы протокол/сравнения команд
            if(count($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr')) > 0)
            {
                if(count($findSettingSeasonForGameProtocol) > 0)
                {
                    for($i = 0; $i < count($findSettingSeasonForGameProtocol); $i++)
                    {
                        if($findSettingSeasonForGameProtocol[$i]['season_id'] == $seasonId)
                        {
                            $gameTeamComparisons['command_1']['game_id'] = $gameId;
                            $gameTeamComparisons['command_1']['team_status'] = 1;
                            $gameTeamComparisons['command_1']['team_id'] = $pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(0) a')->attr('team_id');
                            $gameTeamComparisons['command_1']['two_point_kiddies'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(1)')->text());
                            $gameTeamComparisons['command_1']['two_point_kiddies_ok'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(2)')->text());
                            $gameTeamComparisons['command_1']['two_point_kiddies_percent'] = explode('%', trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(3)')->text()))[0];
                            $gameTeamComparisons['command_1']['three_point_kiddies'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(4)')->text());
                            $gameTeamComparisons['command_1']['three_point_kiddies_ok'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(5)')->text());
                            $gameTeamComparisons['command_1']['three_point_kiddies_percent'] = explode('%', trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(6)')->text()))[0];

                            $gameTeamComparisons['command_1']['one_point_kiddies'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(8)')->text());
                            $gameTeamComparisons['command_1']['one_point_kiddies_ok'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(7)')->text());

                            $gameTeamComparisons['command_1']['one_point_kiddies_percent'] = explode('%', trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(9)')->text()))[0];
                            $gameTeamComparisons['command_1']['picking_up_in_an_attack'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(10)')->text());
                            $gameTeamComparisons['command_1']['picking_up_in_defense'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(11)')->text());
                            $gameTeamComparisons['command_1']['overall_picking_up'] = str_replace("%", "", trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(12)')->text()));
                            $gameTeamComparisons['command_1']['transmissions'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(13)')->text());
                            $gameTeamComparisons['command_1']['personal_foul'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(14)')->text());
                            $gameTeamComparisons['command_1']['losses'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(15)')->text());
                            $gameTeamComparisons['command_1']['interception'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(16)')->text());
                            $gameTeamComparisons['command_1']['block_shots_1'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(17)')->text());
                            $gameTeamComparisons['command_1']['efficiency'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(18)')->text());
                            $gameTeamComparisons['command_1']['gained_points'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(19)')->text());
                            $gameTeamComparisons['command_2']['game_id'] = $gameId;
                            $gameTeamComparisons['command_2']['team_status'] = 2;
                            $gameTeamComparisons['command_2']['team_id'] = $pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(0) a')->attr('team_id');
                            $gameTeamComparisons['command_2']['two_point_kiddies'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(1)')->text());
                            $gameTeamComparisons['command_2']['two_point_kiddies_ok'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(2)')->text());
                            $gameTeamComparisons['command_2']['two_point_kiddies_percent'] = explode('%', trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(3)')->text()))[0];
                            $gameTeamComparisons['command_2']['three_point_kiddies'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(4)')->text());
                            $gameTeamComparisons['command_2']['three_point_kiddies_ok'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(5)')->text());
                            $gameTeamComparisons['command_2']['three_point_kiddies_percent'] = explode('%', trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(6)')->text()))[0];

                            $gameTeamComparisons['command_2']['one_point_kiddies'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(8)')->text());
                            $gameTeamComparisons['command_2']['one_point_kiddies_ok'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(7)')->text());

                            $gameTeamComparisons['command_2']['one_point_kiddies_percent'] = explode('%', trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(9)')->text()))[0];
                            $gameTeamComparisons['command_2']['picking_up_in_an_attack'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(10)')->text());
                            $gameTeamComparisons['command_2']['picking_up_in_defense'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(11)')->text());
                            $gameTeamComparisons['command_2']['overall_picking_up'] = str_replace("%", "", trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(12)')->text()));
                            $gameTeamComparisons['command_2']['transmissions'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(13)')->text());
                            $gameTeamComparisons['command_2']['personal_foul'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(14)')->text());
                            $gameTeamComparisons['command_2']['losses'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(15)')->text());
                            $gameTeamComparisons['command_2']['interception'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(16)')->text());
                            $gameTeamComparisons['command_2']['block_shots_1'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(17)')->text());
                            $gameTeamComparisons['command_2']['efficiency'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(18)')->text());
                            $gameTeamComparisons['command_2']['gained_points'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(19)')->text());
                            $findGameTeamComparisonsCommand_1 = GameProtocolTeamComparisons::find()->where(['game_id' => $gameTeamComparisons['command_1']['game_id'], 'team_id' => $gameTeamComparisons['command_1']['team_id']])->one();
                            $findGameTeamComparisonsCommand_2 = GameProtocolTeamComparisons::find()->where(['game_id' => $gameTeamComparisons['command_2']['game_id'], 'team_id' => $gameTeamComparisons['command_2']['team_id']])->one();
                            if (empty($findGameTeamComparisonsCommand_1) &&
                                empty($findGameTeamComparisonsCommand_2))
                            {
                                $gameTeamComparisonsCommand_1 = new GameProtocolTeamComparisons();
                                $gameTeamComparisonsCommand_1->game_id = $gameTeamComparisons['command_1']['game_id'];
                                $gameTeamComparisonsCommand_1->team_status = $gameTeamComparisons['command_1']['team_status'];
                                $gameTeamComparisonsCommand_1->team_id = $gameTeamComparisons['command_1']['team_id'];
                                $gameTeamComparisonsCommand_1->two_point_kiddies = $gameTeamComparisons['command_1']['two_point_kiddies'];
                                $gameTeamComparisonsCommand_1->two_point_kiddies_ok = $gameTeamComparisons['command_1']['two_point_kiddies_ok'];
                                $gameTeamComparisonsCommand_1->two_point_kiddies_percent = $gameTeamComparisons['command_1']['two_point_kiddies_percent'];
                                $gameTeamComparisonsCommand_1->three_point_kiddies = $gameTeamComparisons['command_1']['three_point_kiddies'];
                                $gameTeamComparisonsCommand_1->three_point_kiddies_ok = $gameTeamComparisons['command_1']['three_point_kiddies_ok'];
                                $gameTeamComparisonsCommand_1->three_point_kiddies_percent = $gameTeamComparisons['command_1']['three_point_kiddies_percent'];
                                $gameTeamComparisonsCommand_1->one_point_kiddies = $gameTeamComparisons['command_1']['one_point_kiddies'];
                                $gameTeamComparisonsCommand_1->one_point_kiddies_ok = $gameTeamComparisons['command_1']['one_point_kiddies_ok'];
                                $gameTeamComparisonsCommand_1->one_point_kiddies_percent = $gameTeamComparisons['command_1']['one_point_kiddies_percent'];
                                $gameTeamComparisonsCommand_1->picking_up_in_an_attack = $gameTeamComparisons['command_1']['picking_up_in_an_attack'];
                                $gameTeamComparisonsCommand_1->picking_up_in_defense = $gameTeamComparisons['command_1']['picking_up_in_defense'];
                                $gameTeamComparisonsCommand_1->overall_picking_up = $gameTeamComparisons['command_1']['overall_picking_up'];
                                $gameTeamComparisonsCommand_1->transmissions = $gameTeamComparisons['command_1']['transmissions'];
                                $gameTeamComparisonsCommand_1->personal_foul = $gameTeamComparisons['command_1']['personal_foul'];
                                $gameTeamComparisonsCommand_1->losses = $gameTeamComparisons['command_1']['losses'];
                                $gameTeamComparisonsCommand_1->interception = $gameTeamComparisons['command_1']['interception'];
                                $gameTeamComparisonsCommand_1->block_shots_1 = $gameTeamComparisons['command_1']['block_shots_1'];
                                $gameTeamComparisonsCommand_1->efficiency = $gameTeamComparisons['command_1']['efficiency'];
                                $gameTeamComparisonsCommand_1->gained_points = $gameTeamComparisons['command_1']['gained_points'];
                                $gameTeamComparisonsCommand_1->save();
                                $gameTeamComparisonsCommand_2 = new GameProtocolTeamComparisons();
                                $gameTeamComparisonsCommand_2->game_id = $gameTeamComparisons['command_2']['game_id'];
                                $gameTeamComparisonsCommand_2->team_status = $gameTeamComparisons['command_2']['team_status'];
                                $gameTeamComparisonsCommand_2->team_id = $gameTeamComparisons['command_2']['team_id'];
                                $gameTeamComparisonsCommand_2->two_point_kiddies = $gameTeamComparisons['command_2']['two_point_kiddies'];
                                $gameTeamComparisonsCommand_2->two_point_kiddies_ok = $gameTeamComparisons['command_2']['two_point_kiddies_ok'];
                                $gameTeamComparisonsCommand_2->two_point_kiddies_percent = $gameTeamComparisons['command_2']['two_point_kiddies_percent'];
                                $gameTeamComparisonsCommand_2->three_point_kiddies = $gameTeamComparisons['command_2']['three_point_kiddies'];
                                $gameTeamComparisonsCommand_2->three_point_kiddies_ok = $gameTeamComparisons['command_2']['three_point_kiddies_ok'];
                                $gameTeamComparisonsCommand_2->three_point_kiddies_percent = $gameTeamComparisons['command_2']['three_point_kiddies_percent'];
                                $gameTeamComparisonsCommand_2->one_point_kiddies = $gameTeamComparisons['command_2']['one_point_kiddies'];
                                $gameTeamComparisonsCommand_2->one_point_kiddies_ok = $gameTeamComparisons['command_2']['one_point_kiddies_ok'];
                                $gameTeamComparisonsCommand_2->one_point_kiddies_percent = $gameTeamComparisons['command_2']['one_point_kiddies_percent'];
                                $gameTeamComparisonsCommand_2->picking_up_in_an_attack = $gameTeamComparisons['command_2']['picking_up_in_an_attack'];
                                $gameTeamComparisonsCommand_2->picking_up_in_defense = $gameTeamComparisons['command_2']['picking_up_in_defense'];
                                $gameTeamComparisonsCommand_2->overall_picking_up = $gameTeamComparisons['command_2']['overall_picking_up'];
                                $gameTeamComparisonsCommand_2->transmissions = $gameTeamComparisons['command_2']['transmissions'];
                                $gameTeamComparisonsCommand_2->personal_foul = $gameTeamComparisons['command_2']['personal_foul'];
                                $gameTeamComparisonsCommand_2->losses = $gameTeamComparisons['command_2']['losses'];
                                $gameTeamComparisonsCommand_2->interception = $gameTeamComparisons['command_2']['interception'];
                                $gameTeamComparisonsCommand_2->block_shots_1 = $gameTeamComparisons['command_2']['block_shots_1'];
                                $gameTeamComparisonsCommand_2->efficiency = $gameTeamComparisons['command_2']['efficiency'];
                                $gameTeamComparisonsCommand_2->gained_points = $gameTeamComparisons['command_2']['gained_points'];
                                $gameTeamComparisonsCommand_2->save();
                            }
                        }
                        else
                        {
                            $gameTeamComparisons['command_1']['game_id'] = $gameId;
                            $gameTeamComparisons['command_1']['team_status'] = 1;
                            $gameTeamComparisons['command_1']['team_id'] = $pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(0) a')->attr('team_id');
                            $gameTeamComparisons['command_1']['two_point_kiddies'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(1)')->text());
                            $gameTeamComparisons['command_1']['two_point_kiddies_ok'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(2)')->text());
                            $gameTeamComparisons['command_1']['two_point_kiddies_percent'] = explode('%', trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(3)')->text()))[0];
                            $gameTeamComparisons['command_1']['three_point_kiddies'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(4)')->text());
                            $gameTeamComparisons['command_1']['three_point_kiddies_ok'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(5)')->text());
                            $gameTeamComparisons['command_1']['three_point_kiddies_percent'] = explode('%', trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(6)')->text()))[0];
                            $gameTeamComparisons['command_1']['for_game_attempt'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(7)')->text());
                            $gameTeamComparisons['command_1']['for_game_attempt_ok'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(8)')->text());
                            $gameTeamComparisons['command_1']['for_game_attempt_percent'] =  explode('%', trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(9)')->text()))[0];
                            $gameTeamComparisons['command_1']['one_point_kiddies'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(10)')->text());
                            $gameTeamComparisons['command_1']['one_point_kiddies_ok'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(11)')->text());
                            $gameTeamComparisons['command_1']['one_point_kiddies_percent'] =  explode('%', trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(12)')->text()))[0];
                            $gameTeamComparisons['command_1']['picking_up_in_an_attack'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(13)')->text());
                            $gameTeamComparisons['command_1']['picking_up_in_defense'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(14)')->text());
                            $gameTeamComparisons['command_1']['overall_picking_up'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(15)')->text());
                            $gameTeamComparisons['command_1']['transmissions'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(16)')->text());
                            $gameTeamComparisons['command_1']['personal_foul'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(17)')->text());
                            $gameTeamComparisons['command_1']['losses'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(18)')->text());
                            $gameTeamComparisons['command_1']['interception'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(19)')->text());
                            $gameTeamComparisons['command_1']['block_shots_1'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(20)')->text());
                            $gameTeamComparisons['command_1']['block_shots_2'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(21)')->text());
                            $gameTeamComparisons['command_1']['efficiency'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(22)')->text());
                            $gameTeamComparisons['command_1']['gained_points'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(23)')->text());
                            $gameTeamComparisons['command_2']['game_id'] = $gameId;
                            $gameTeamComparisons['command_2']['team_status'] = 2;
                            $gameTeamComparisons['command_2']['team_id'] = $pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(0) a')->attr('team_id');
                            $gameTeamComparisons['command_2']['two_point_kiddies'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(1)')->text());
                            $gameTeamComparisons['command_2']['two_point_kiddies_ok'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(2)')->text());
                            $gameTeamComparisons['command_2']['two_point_kiddies_percent'] = explode('%', trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(3)')->text()))[0];
                            $gameTeamComparisons['command_2']['three_point_kiddies'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(4)')->text());
                            $gameTeamComparisons['command_2']['three_point_kiddies_ok'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(5)')->text());
                            $gameTeamComparisons['command_2']['three_point_kiddies_percent'] = explode('%', trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(6)')->text()))[0];
                            $gameTeamComparisons['command_2']['for_game_attempt'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(7)')->text());
                            $gameTeamComparisons['command_2']['for_game_attempt_ok'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(8)')->text());
                            $gameTeamComparisons['command_2']['for_game_attempt_percent'] =  explode('%', trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(9)')->text()))[0];
                            $gameTeamComparisons['command_2']['one_point_kiddies'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(10)')->text());
                            $gameTeamComparisons['command_2']['one_point_kiddies_ok'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(11)')->text());
                            $gameTeamComparisons['command_2']['one_point_kiddies_percent'] =  explode('%', trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(12)')->text()))[0];
                            $gameTeamComparisons['command_2']['picking_up_in_an_attack'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(13)')->text());
                            $gameTeamComparisons['command_2']['picking_up_in_defense'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(14)')->text());
                            $gameTeamComparisons['command_2']['overall_picking_up'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(15)')->text());
                            $gameTeamComparisons['command_2']['transmissions'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(16)')->text());
                            $gameTeamComparisons['command_2']['personal_foul'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(17)')->text());
                            $gameTeamComparisons['command_2']['losses'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(18)')->text());
                            $gameTeamComparisons['command_2']['interception'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(19)')->text());
                            $gameTeamComparisons['command_2']['block_shots_1'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(20)')->text());
                            $gameTeamComparisons['command_2']['block_shots_2'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(21)')->text());
                            $gameTeamComparisons['command_2']['efficiency'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(22)')->text());
                            $gameTeamComparisons['command_2']['gained_points'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(23)')->text());
                            $findGameTeamComparisonsCommand_1 = GameProtocolTeamComparisons::find()->where(['game_id' => $gameTeamComparisons['command_1']['game_id'], 'team_id' => $gameTeamComparisons['command_1']['team_id']])->one();
                            $findGameTeamComparisonsCommand_2 = GameProtocolTeamComparisons::find()->where(['game_id' => $gameTeamComparisons['command_2']['game_id'], 'team_id' => $gameTeamComparisons['command_2']['team_id']])->one();
                            if(empty($findGameTeamComparisonsCommand_1) &&
                                empty($findGameTeamComparisonsCommand_2))
                            {
                                $gameTeamComparisonsCommand_1 = new GameProtocolTeamComparisons();
                                $gameTeamComparisonsCommand_1->game_id = $gameTeamComparisons['command_1']['game_id'];
                                $gameTeamComparisonsCommand_1->team_status = $gameTeamComparisons['command_1']['team_status'];
                                $gameTeamComparisonsCommand_1->team_id = $gameTeamComparisons['command_1']['team_id'];
                                $gameTeamComparisonsCommand_1->two_point_kiddies = $gameTeamComparisons['command_1']['two_point_kiddies'];
                                $gameTeamComparisonsCommand_1->two_point_kiddies_ok = $gameTeamComparisons['command_1']['two_point_kiddies_ok'];
                                $gameTeamComparisonsCommand_1->two_point_kiddies_percent = $gameTeamComparisons['command_1']['two_point_kiddies_percent'];
                                $gameTeamComparisonsCommand_1->three_point_kiddies = $gameTeamComparisons['command_1']['three_point_kiddies'];
                                $gameTeamComparisonsCommand_1->three_point_kiddies_ok = $gameTeamComparisons['command_1']['three_point_kiddies_ok'];
                                $gameTeamComparisonsCommand_1->three_point_kiddies_percent = $gameTeamComparisons['command_1']['three_point_kiddies_percent'];
                                $gameTeamComparisonsCommand_1->for_game_attempt = $gameTeamComparisons['command_1']['for_game_attempt'];
                                $gameTeamComparisonsCommand_1->for_game_attempt_ok = $gameTeamComparisons['command_1']['for_game_attempt_ok'];
                                $gameTeamComparisonsCommand_1->for_game_attempt_percent = $gameTeamComparisons['command_1']['for_game_attempt_percent'];
                                $gameTeamComparisonsCommand_1->one_point_kiddies = $gameTeamComparisons['command_1']['one_point_kiddies'];
                                $gameTeamComparisonsCommand_1->one_point_kiddies_ok = $gameTeamComparisons['command_1']['one_point_kiddies_ok'];
                                $gameTeamComparisonsCommand_1->one_point_kiddies_percent = $gameTeamComparisons['command_1']['one_point_kiddies_percent'];
                                $gameTeamComparisonsCommand_1->picking_up_in_an_attack = $gameTeamComparisons['command_1']['picking_up_in_an_attack'];
                                $gameTeamComparisonsCommand_1->picking_up_in_defense = $gameTeamComparisons['command_1']['picking_up_in_defense'];
                                $gameTeamComparisonsCommand_1->overall_picking_up = $gameTeamComparisons['command_1']['overall_picking_up'];
                                $gameTeamComparisonsCommand_1->transmissions = $gameTeamComparisons['command_1']['transmissions'];
                                $gameTeamComparisonsCommand_1->personal_foul = $gameTeamComparisons['command_1']['personal_foul'];
                                $gameTeamComparisonsCommand_1->losses = $gameTeamComparisons['command_1']['losses'];
                                $gameTeamComparisonsCommand_1->interception = $gameTeamComparisons['command_1']['interception'];
                                $gameTeamComparisonsCommand_1->block_shots_1 = $gameTeamComparisons['command_1']['block_shots_1'];
                                $gameTeamComparisonsCommand_1->block_shots_2 = $gameTeamComparisons['command_1']['block_shots_2'];
                                $gameTeamComparisonsCommand_1->efficiency = $gameTeamComparisons['command_1']['efficiency'];
                                $gameTeamComparisonsCommand_1->gained_points = $gameTeamComparisons['command_1']['gained_points'];
                                $gameTeamComparisonsCommand_1->save();
                                $gameTeamComparisonsCommand_2 = new GameProtocolTeamComparisons();
                                $gameTeamComparisonsCommand_2->game_id = $gameTeamComparisons['command_2']['game_id'];
                                $gameTeamComparisonsCommand_2->team_status = $gameTeamComparisons['command_2']['team_status'];
                                $gameTeamComparisonsCommand_2->team_id = $gameTeamComparisons['command_2']['team_id'];
                                $gameTeamComparisonsCommand_2->two_point_kiddies = $gameTeamComparisons['command_2']['two_point_kiddies'];
                                $gameTeamComparisonsCommand_2->two_point_kiddies_ok = $gameTeamComparisons['command_2']['two_point_kiddies_ok'];
                                $gameTeamComparisonsCommand_2->two_point_kiddies_percent = $gameTeamComparisons['command_2']['two_point_kiddies_percent'];
                                $gameTeamComparisonsCommand_2->three_point_kiddies = $gameTeamComparisons['command_2']['three_point_kiddies'];
                                $gameTeamComparisonsCommand_2->three_point_kiddies_ok = $gameTeamComparisons['command_2']['three_point_kiddies_ok'];
                                $gameTeamComparisonsCommand_2->three_point_kiddies_percent = $gameTeamComparisons['command_2']['three_point_kiddies_percent'];
                                $gameTeamComparisonsCommand_2->for_game_attempt = $gameTeamComparisons['command_2']['for_game_attempt'];
                                $gameTeamComparisonsCommand_2->for_game_attempt_ok = $gameTeamComparisons['command_2']['for_game_attempt_ok'];
                                $gameTeamComparisonsCommand_2->for_game_attempt_percent = $gameTeamComparisons['command_2']['for_game_attempt_percent'];
                                $gameTeamComparisonsCommand_2->one_point_kiddies = $gameTeamComparisons['command_2']['one_point_kiddies'];
                                $gameTeamComparisonsCommand_2->one_point_kiddies_ok = $gameTeamComparisons['command_2']['one_point_kiddies_ok'];
                                $gameTeamComparisonsCommand_2->one_point_kiddies_percent = $gameTeamComparisons['command_2']['one_point_kiddies_percent'];
                                $gameTeamComparisonsCommand_2->picking_up_in_an_attack = $gameTeamComparisons['command_2']['picking_up_in_an_attack'];
                                $gameTeamComparisonsCommand_2->picking_up_in_defense = $gameTeamComparisons['command_2']['picking_up_in_defense'];
                                $gameTeamComparisonsCommand_2->overall_picking_up = $gameTeamComparisons['command_2']['overall_picking_up'];
                                $gameTeamComparisonsCommand_2->transmissions = $gameTeamComparisons['command_2']['transmissions'];
                                $gameTeamComparisonsCommand_2->personal_foul = $gameTeamComparisons['command_2']['personal_foul'];
                                $gameTeamComparisonsCommand_2->losses = $gameTeamComparisons['command_2']['losses'];
                                $gameTeamComparisonsCommand_2->interception = $gameTeamComparisons['command_2']['interception'];
                                $gameTeamComparisonsCommand_2->block_shots_1 = $gameTeamComparisons['command_2']['block_shots_1'];
                                $gameTeamComparisonsCommand_2->block_shots_2 = $gameTeamComparisons['command_2']['block_shots_2'];
                                $gameTeamComparisonsCommand_2->efficiency = $gameTeamComparisons['command_2']['efficiency'];
                                $gameTeamComparisonsCommand_2->gained_points = $gameTeamComparisons['command_2']['gained_points'];
                                $gameTeamComparisonsCommand_2->save();
                            }
                        }
                    }
                }
                else
                {
                    $gameTeamComparisons['command_1']['game_id'] = $gameId;
                    $gameTeamComparisons['command_1']['team_status'] = 1;
                    $gameTeamComparisons['command_1']['team_id'] = $pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(0) a')->attr('team_id');
                    $gameTeamComparisons['command_1']['two_point_kiddies'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(1)')->text());
                    $gameTeamComparisons['command_1']['two_point_kiddies_ok'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(2)')->text());
                    $gameTeamComparisons['command_1']['two_point_kiddies_percent'] = explode('%', trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(3)')->text()))[0];
                    $gameTeamComparisons['command_1']['three_point_kiddies'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(4)')->text());
                    $gameTeamComparisons['command_1']['three_point_kiddies_ok'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(5)')->text());
                    $gameTeamComparisons['command_1']['three_point_kiddies_percent'] = explode('%', trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(6)')->text()))[0];
                    $gameTeamComparisons['command_1']['for_game_attempt'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(7)')->text());
                    $gameTeamComparisons['command_1']['for_game_attempt_ok'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(8)')->text());
                    $gameTeamComparisons['command_1']['for_game_attempt_percent'] =  explode('%', trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(9)')->text()))[0];
                    $gameTeamComparisons['command_1']['one_point_kiddies'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(10)')->text());
                    $gameTeamComparisons['command_1']['one_point_kiddies_ok'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(11)')->text());
                    $gameTeamComparisons['command_1']['one_point_kiddies_percent'] =  explode('%', trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(12)')->text()))[0];
                    $gameTeamComparisons['command_1']['picking_up_in_an_attack'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(13)')->text());
                    $gameTeamComparisons['command_1']['picking_up_in_defense'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(14)')->text());
                    $gameTeamComparisons['command_1']['overall_picking_up'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(15)')->text());
                    $gameTeamComparisons['command_1']['transmissions'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(16)')->text());
                    $gameTeamComparisons['command_1']['personal_foul'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(17)')->text());
                    $gameTeamComparisons['command_1']['losses'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(18)')->text());
                    $gameTeamComparisons['command_1']['interception'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(19)')->text());
                    $gameTeamComparisons['command_1']['block_shots_1'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(20)')->text());
                    $gameTeamComparisons['command_1']['block_shots_2'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(21)')->text());
                    $gameTeamComparisons['command_1']['efficiency'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(22)')->text());
                    $gameTeamComparisons['command_1']['gained_points'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(2) td:eq(23)')->text());
                    $gameTeamComparisons['command_2']['game_id'] = $gameId;
                    $gameTeamComparisons['command_2']['team_status'] = 2;
                    $gameTeamComparisons['command_2']['team_id'] = $pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(0) a')->attr('team_id');
                    $gameTeamComparisons['command_2']['two_point_kiddies'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(1)')->text());
                    $gameTeamComparisons['command_2']['two_point_kiddies_ok'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(2)')->text());
                    $gameTeamComparisons['command_2']['two_point_kiddies_percent'] = explode('%', trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(3)')->text()))[0];
                    $gameTeamComparisons['command_2']['three_point_kiddies'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(4)')->text());
                    $gameTeamComparisons['command_2']['three_point_kiddies_ok'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(5)')->text());
                    $gameTeamComparisons['command_2']['three_point_kiddies_percent'] = explode('%', trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(6)')->text()))[0];
                    $gameTeamComparisons['command_2']['for_game_attempt'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(7)')->text());
                    $gameTeamComparisons['command_2']['for_game_attempt_ok'] = trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(8)')->text());
                    $gameTeamComparisons['command_2']['for_game_attempt_percent'] =  explode('%', trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(9)')->text()))[0];
                    $gameTeamComparisons['command_2']['one_point_kiddies'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(10)')->text());
                    $gameTeamComparisons['command_2']['one_point_kiddies_ok'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(11)')->text());
                    $gameTeamComparisons['command_2']['one_point_kiddies_percent'] =  explode('%', trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(12)')->text()))[0];
                    $gameTeamComparisons['command_2']['picking_up_in_an_attack'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(13)')->text());
                    $gameTeamComparisons['command_2']['picking_up_in_defense'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(14)')->text());
                    $gameTeamComparisons['command_2']['overall_picking_up'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(15)')->text());
                    $gameTeamComparisons['command_2']['transmissions'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(16)')->text());
                    $gameTeamComparisons['command_2']['personal_foul'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(17)')->text());
                    $gameTeamComparisons['command_2']['losses'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(18)')->text());
                    $gameTeamComparisons['command_2']['interception'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(19)')->text());
                    $gameTeamComparisons['command_2']['block_shots_1'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(20)')->text());
                    $gameTeamComparisons['command_2']['block_shots_2'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(21)')->text());
                    $gameTeamComparisons['command_2']['efficiency'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(22)')->text());
                    $gameTeamComparisons['command_2']['gained_points'] =  trim($pqProtocol->find('.mbt-layout-top:eq(1) .mbt-holder-content1 tr:eq(3) td:eq(23)')->text());
                    $findGameTeamComparisonsCommand_1 = GameProtocolTeamComparisons::find()->where(['game_id' => $gameTeamComparisons['command_1']['game_id'], 'team_id' => $gameTeamComparisons['command_1']['team_id']])->one();
                    $findGameTeamComparisonsCommand_2 = GameProtocolTeamComparisons::find()->where(['game_id' => $gameTeamComparisons['command_2']['game_id'], 'team_id' => $gameTeamComparisons['command_2']['team_id']])->one();
                    if(empty($findGameTeamComparisonsCommand_1) &&
                        empty($findGameTeamComparisonsCommand_2))
                    {
                        $gameTeamComparisonsCommand_1 = new GameProtocolTeamComparisons();
                        $gameTeamComparisonsCommand_1->game_id = $gameTeamComparisons['command_1']['game_id'];
                        $gameTeamComparisonsCommand_1->team_status = $gameTeamComparisons['command_1']['team_status'];
                        $gameTeamComparisonsCommand_1->team_id = $gameTeamComparisons['command_1']['team_id'];
                        $gameTeamComparisonsCommand_1->two_point_kiddies = $gameTeamComparisons['command_1']['two_point_kiddies'];
                        $gameTeamComparisonsCommand_1->two_point_kiddies_ok = $gameTeamComparisons['command_1']['two_point_kiddies_ok'];
                        $gameTeamComparisonsCommand_1->two_point_kiddies_percent = $gameTeamComparisons['command_1']['two_point_kiddies_percent'];
                        $gameTeamComparisonsCommand_1->three_point_kiddies = $gameTeamComparisons['command_1']['three_point_kiddies'];
                        $gameTeamComparisonsCommand_1->three_point_kiddies_ok = $gameTeamComparisons['command_1']['three_point_kiddies_ok'];
                        $gameTeamComparisonsCommand_1->three_point_kiddies_percent = $gameTeamComparisons['command_1']['three_point_kiddies_percent'];
                        $gameTeamComparisonsCommand_1->for_game_attempt = $gameTeamComparisons['command_1']['for_game_attempt'];
                        $gameTeamComparisonsCommand_1->for_game_attempt_ok = $gameTeamComparisons['command_1']['for_game_attempt_ok'];
                        $gameTeamComparisonsCommand_1->for_game_attempt_percent = $gameTeamComparisons['command_1']['for_game_attempt_percent'];
                        $gameTeamComparisonsCommand_1->one_point_kiddies = $gameTeamComparisons['command_1']['one_point_kiddies'];
                        $gameTeamComparisonsCommand_1->one_point_kiddies_ok = $gameTeamComparisons['command_1']['one_point_kiddies_ok'];
                        $gameTeamComparisonsCommand_1->one_point_kiddies_percent = $gameTeamComparisons['command_1']['one_point_kiddies_percent'];
                        $gameTeamComparisonsCommand_1->picking_up_in_an_attack = $gameTeamComparisons['command_1']['picking_up_in_an_attack'];
                        $gameTeamComparisonsCommand_1->picking_up_in_defense = $gameTeamComparisons['command_1']['picking_up_in_defense'];
                        $gameTeamComparisonsCommand_1->overall_picking_up = $gameTeamComparisons['command_1']['overall_picking_up'];
                        $gameTeamComparisonsCommand_1->transmissions = $gameTeamComparisons['command_1']['transmissions'];
                        $gameTeamComparisonsCommand_1->personal_foul = $gameTeamComparisons['command_1']['personal_foul'];
                        $gameTeamComparisonsCommand_1->losses = $gameTeamComparisons['command_1']['losses'];
                        $gameTeamComparisonsCommand_1->interception = $gameTeamComparisons['command_1']['interception'];
                        $gameTeamComparisonsCommand_1->block_shots_1 = $gameTeamComparisons['command_1']['block_shots_1'];
                        $gameTeamComparisonsCommand_1->block_shots_2 = $gameTeamComparisons['command_1']['block_shots_2'];
                        $gameTeamComparisonsCommand_1->efficiency = $gameTeamComparisons['command_1']['efficiency'];
                        $gameTeamComparisonsCommand_1->gained_points =$gameTeamComparisons['command_1']['gained_points'];
                        $gameTeamComparisonsCommand_1->save();
                        $gameTeamComparisonsCommand_2 = new GameProtocolTeamComparisons();
                        $gameTeamComparisonsCommand_2->game_id = $gameTeamComparisons['command_2']['game_id'];
                        $gameTeamComparisonsCommand_2->team_status = $gameTeamComparisons['command_2']['team_status'];
                        $gameTeamComparisonsCommand_2->team_id = $gameTeamComparisons['command_2']['team_id'];
                        $gameTeamComparisonsCommand_2->two_point_kiddies = $gameTeamComparisons['command_2']['two_point_kiddies'];
                        $gameTeamComparisonsCommand_2->two_point_kiddies_ok = $gameTeamComparisons['command_2']['two_point_kiddies_ok'];
                        $gameTeamComparisonsCommand_2->two_point_kiddies_percent = $gameTeamComparisons['command_2']['two_point_kiddies_percent'];
                        $gameTeamComparisonsCommand_2->three_point_kiddies = $gameTeamComparisons['command_2']['three_point_kiddies'];
                        $gameTeamComparisonsCommand_2->three_point_kiddies_ok = $gameTeamComparisons['command_2']['three_point_kiddies_ok'];
                        $gameTeamComparisonsCommand_2->three_point_kiddies_percent = $gameTeamComparisons['command_2']['three_point_kiddies_percent'];
                        $gameTeamComparisonsCommand_2->for_game_attempt = $gameTeamComparisons['command_2']['for_game_attempt'];
                        $gameTeamComparisonsCommand_2->for_game_attempt_ok = $gameTeamComparisons['command_2']['for_game_attempt_ok'];
                        $gameTeamComparisonsCommand_2->for_game_attempt_percent = $gameTeamComparisons['command_2']['for_game_attempt_percent'];
                        $gameTeamComparisonsCommand_2->one_point_kiddies = $gameTeamComparisons['command_2']['one_point_kiddies'];
                        $gameTeamComparisonsCommand_2->one_point_kiddies_ok = $gameTeamComparisons['command_2']['one_point_kiddies_ok'];
                        $gameTeamComparisonsCommand_2->one_point_kiddies_percent = $gameTeamComparisons['command_2']['one_point_kiddies_percent'];
                        $gameTeamComparisonsCommand_2->picking_up_in_an_attack = $gameTeamComparisons['command_2']['picking_up_in_an_attack'];
                        $gameTeamComparisonsCommand_2->picking_up_in_defense = $gameTeamComparisons['command_2']['picking_up_in_defense'];
                        $gameTeamComparisonsCommand_2->overall_picking_up = $gameTeamComparisons['command_2']['overall_picking_up'];
                        $gameTeamComparisonsCommand_2->transmissions = $gameTeamComparisons['command_2']['transmissions'];
                        $gameTeamComparisonsCommand_2->personal_foul = $gameTeamComparisons['command_2']['personal_foul'];
                        $gameTeamComparisonsCommand_2->losses = $gameTeamComparisons['command_2']['losses'];
                        $gameTeamComparisonsCommand_2->interception = $gameTeamComparisons['command_2']['interception'];
                        $gameTeamComparisonsCommand_2->block_shots_1 = $gameTeamComparisons['command_2']['block_shots_1'];
                        $gameTeamComparisonsCommand_2->block_shots_2 = $gameTeamComparisons['command_2']['block_shots_2'];
                        $gameTeamComparisonsCommand_2->efficiency = $gameTeamComparisons['command_2']['efficiency'];
                        $gameTeamComparisonsCommand_2->gained_points =$gameTeamComparisons['command_2']['gained_points'];
                        $gameTeamComparisonsCommand_2->save();
                    }
                }

                $url = [];
                $url[] = 'http://widgets.baskethotel.com/widget-service/show?';
                $url[] = '&api=aaed6059969d182ff2a08e7d61458ce2d1a372c1';
                $url[] = '&lang=uk';
                $url[] = '&nnav=1';
                $url[] = '&nav_object=0';
                $url[] = '&request[0][container]=5-400-tab-container&request[0][widget]=400';
                $url[] = '&request[0][part]=advanced_stats';
                $url[] = '&request[0][state]=MTA4NjQ3MDIyMGE6MTU6e3M6MTc6InRlYW1fbGlua192aXNpYmxlIjtzOjE6IjEiO3M6MTc6ImdhbWVfbGlua192aXNpYmxlIjtzOjE6IjEiO3M6MTk6InBsYXllcl9saW5rX3Zpc2libGUiO3M6MToiMSI7czoxNDoidGVhbV9saW5rX3R5cGUiO3M6MToiMyI7czoxNDoiZ2FtZV9saW5rX3R5cGUiO3M6MToiMyI7czoxNjoicGxheWVyX2xpbmtfdHlwZSI7czoxOiIzIjtzOjE3OiJ0ZWFtX2xpbmtfaGFuZGxlciI7czoxMjoibmF2aWdhdGVUZWFtIjtzOjE3OiJnYW1lX2xpbmtfaGFuZGxlciI7czoxMjoibmF2aWdhdGVHYW1lIjtzOjE5OiJwbGF5ZXJfbGlua19oYW5kbGVyIjtzOjE0OiJuYXZpZ2F0ZVBsYXllciI7czoxODoiYXJlbmFfbGlua192aXNpYmxlIjtzOjE6IjEiO3M6MTU6ImFyZW5hX2xpbmtfdHlwZSI7czoxOiIzIjtzOjE4OiJhcmVuYV9saW5rX2hhbmRsZXIiO3M6MTM6Im5hdmlnYXRlQXJlbmEiO3M6OToibGVhZ3VlX2lkIjtzOjM6IjIyNCI7czo5OiJzZWFzb25faWQiO3M6NToiOTg5NTciO3M6NzoiZ2FtZV9pZCI7czo3OiIzODgzNDkxIjt9';
                $url[] = '&request[0][param][team_id]='.$gameId;
                $gameRootedStatisticsArr = [];
                $gameRootedStatisticsHtml = $parsing->request(implode($url));
                $pqGameRootedStatistics = phpQuery::newDocument($gameRootedStatisticsHtml);
                function gameRootedStatisticsChange($gameRootedStatisticsArr, $gameId, $team_id, $pqGameRootedStatistics, $team, $type, $number)
                {
                    $gameRootedStatisticsArr['comparison_start_bench'][$team][$type]['game_id'] = $gameId;
                    $gameRootedStatisticsArr['comparison_start_bench'][$team][$type]['team_id'] = $team_id;
                    $gameRootedStatisticsArr['comparison_start_bench'][$team][$type]['team_status'] = $type;
                    $gameRootedStatisticsArr['comparison_start_bench'][$team][$type]['time_in_game'] = trim($pqGameRootedStatistics->find('.mbt-layout-top .mbt-holder-content1 .mbt-complex tr:eq('.$number.') td:eq(1)')->text());
                    $gameRootedStatisticsArr['comparison_start_bench'][$team][$type]['two_point_kiddies'] = trim($pqGameRootedStatistics->find('.mbt-layout-top .mbt-holder-content1 .mbt-complex tr:eq('.$number.') td:eq(3)')->text());
                    $gameRootedStatisticsArr['comparison_start_bench'][$team][$type]['two_point_kiddies_ok'] = trim($pqGameRootedStatistics->find('.mbt-layout-top .mbt-holder-content1 .mbt-complex tr:eq('.$number.') td:eq(2)')->text());
                    $gameRootedStatisticsArr['comparison_start_bench'][$team][$type]['three_point_kiddies'] = trim($pqGameRootedStatistics->find('.mbt-layout-top .mbt-holder-content1 .mbt-complex tr:eq('.$number.') td:eq(5)')->text());
                    $gameRootedStatisticsArr['comparison_start_bench'][$team][$type]['three_point_kiddies_ok'] = trim($pqGameRootedStatistics->find('.mbt-layout-top .mbt-holder-content1 .mbt-complex tr:eq('.$number.') td:eq(4)')->text());
                    $gameRootedStatisticsArr['comparison_start_bench'][$team][$type]['one_point_kiddies'] = trim($pqGameRootedStatistics->find('.mbt-layout-top .mbt-holder-content1 .mbt-complex tr:eq('.$number.') td:eq(7)')->text());
                    $gameRootedStatisticsArr['comparison_start_bench'][$team][$type]['one_point_kiddies_ok'] = trim($pqGameRootedStatistics->find('.mbt-layout-top .mbt-holder-content1 .mbt-complex tr:eq('.$number.') td:eq(6)')->text());
                    $gameRootedStatisticsArr['comparison_start_bench'][$team][$type]['picking_up_in_an_attack'] = trim($pqGameRootedStatistics->find('.mbt-layout-top .mbt-holder-content1 .mbt-complex tr:eq('.$number.') td:eq(8)')->text());
                    $gameRootedStatisticsArr['comparison_start_bench'][$team][$type]['picking_up_in_defense'] = trim($pqGameRootedStatistics->find('.mbt-layout-top .mbt-holder-content1 .mbt-complex tr:eq('.$number.') td:eq(9)')->text());
                    $gameRootedStatisticsArr['comparison_start_bench'][$team][$type]['overall_picking_up'] = trim($pqGameRootedStatistics->find('.mbt-layout-top .mbt-holder-content1 .mbt-complex tr:eq('.$number.') td:eq(10)')->text());
                    $gameRootedStatisticsArr['comparison_start_bench'][$team][$type]['transmissions'] = trim($pqGameRootedStatistics->find('.mbt-layout-top .mbt-holder-content1 .mbt-complex tr:eq('.$number.') td:eq(11)')->text());
                    $gameRootedStatisticsArr['comparison_start_bench'][$team][$type]['personal_foul'] = trim($pqGameRootedStatistics->find('.mbt-layout-top .mbt-holder-content1 .mbt-complex tr:eq('.$number.') td:eq(12)')->text());
                    $gameRootedStatisticsArr['comparison_start_bench'][$team][$type]['losses'] = trim($pqGameRootedStatistics->find('.mbt-layout-top .mbt-holder-content1 .mbt-complex tr:eq('.$number.') td:eq(13)')->text());
                    $gameRootedStatisticsArr['comparison_start_bench'][$team][$type]['interception'] = trim($pqGameRootedStatistics->find('.mbt-layout-top .mbt-holder-content1 .mbt-complex tr:eq('.$number.') td:eq(14)')->text());
                    $gameRootedStatisticsArr['comparison_start_bench'][$team][$type]['block_shots'] = trim($pqGameRootedStatistics->find('.mbt-layout-top .mbt-holder-content1 .mbt-complex tr:eq('.$number.') td:eq(15)')->text());
                    $gameRootedStatisticsArr['comparison_start_bench'][$team][$type]['efficiency'] = trim($pqGameRootedStatistics->find('.mbt-layout-top .mbt-holder-content1 .mbt-complex tr:eq('.$number.') td:eq(16)')->text());
                    $gameRootedStatisticsArr['comparison_start_bench'][$team][$type]['gained_points'] = trim($pqGameRootedStatistics->find('.mbt-layout-top .mbt-holder-content1 .mbt-complex tr:eq('.$number.') td:eq(17)')->text());
                    $findGameRootedStatistic = GameRootedStatistics::find()->where(['game_id' => $gameId, 'team_id' => $team_id, 'team_status' => $gameRootedStatisticsArr['comparison_start_bench'][$team][$type]['team_status']])->one();
                    if(empty($findGameRootedStatistic))
                    {
                        $newGameRootedStatistic = new GameRootedStatistics();
                        $newGameRootedStatistic->game_id = $gameRootedStatisticsArr['comparison_start_bench'][$team][$type]['game_id'];
                        $newGameRootedStatistic->team_id = $gameRootedStatisticsArr['comparison_start_bench'][$team][$type]['team_id'];
                        $newGameRootedStatistic->team_status = $gameRootedStatisticsArr['comparison_start_bench'][$team][$type]['team_status'];
                        $newGameRootedStatistic->time_in_game = $gameRootedStatisticsArr['comparison_start_bench'][$team][$type]['time_in_game'];
                        $newGameRootedStatistic->two_point_kiddies = $gameRootedStatisticsArr['comparison_start_bench'][$team][$type]['two_point_kiddies'];
                        $newGameRootedStatistic->two_point_kiddies_ok = $gameRootedStatisticsArr['comparison_start_bench'][$team][$type]['two_point_kiddies_ok'];
                        $newGameRootedStatistic->three_point_kiddies = $gameRootedStatisticsArr['comparison_start_bench'][$team][$type]['three_point_kiddies'];
                        $newGameRootedStatistic->three_point_kiddies_ok = $gameRootedStatisticsArr['comparison_start_bench'][$team][$type]['three_point_kiddies_ok'];
                        $newGameRootedStatistic->one_point_kiddies = $gameRootedStatisticsArr['comparison_start_bench'][$team][$type]['one_point_kiddies'];
                        $newGameRootedStatistic->one_point_kiddies_ok = $gameRootedStatisticsArr['comparison_start_bench'][$team][$type]['one_point_kiddies_ok'];
                        $newGameRootedStatistic->picking_up_in_an_attack = $gameRootedStatisticsArr['comparison_start_bench'][$team][$type]['picking_up_in_an_attack'];
                        $newGameRootedStatistic->picking_up_in_defense = $gameRootedStatisticsArr['comparison_start_bench'][$team][$type]['picking_up_in_defense'];
                        $newGameRootedStatistic->overall_picking_up = $gameRootedStatisticsArr['comparison_start_bench'][$team][$type]['overall_picking_up'];
                        $newGameRootedStatistic->transmissions = $gameRootedStatisticsArr['comparison_start_bench'][$team][$type]['transmissions'];
                        $newGameRootedStatistic->personal_foul = $gameRootedStatisticsArr['comparison_start_bench'][$team][$type]['personal_foul'];
                        $newGameRootedStatistic->losses = $gameRootedStatisticsArr['comparison_start_bench'][$team][$type]['losses'];
                        $newGameRootedStatistic->interception = $gameRootedStatisticsArr['comparison_start_bench'][$team][$type]['interception'];
                        $newGameRootedStatistic->block_shots = $gameRootedStatisticsArr['comparison_start_bench'][$team][$type]['block_shots'];
                        $newGameRootedStatistic->efficiency = $gameRootedStatisticsArr['comparison_start_bench'][$team][$type]['efficiency'];
                        $newGameRootedStatistic->gained_points = $gameRootedStatisticsArr['comparison_start_bench'][$team][$type]['gained_points'];
                        $newGameRootedStatistic->save();
                    }
                    return $gameRootedStatisticsArr;
                }
                $gameRootedStatisticsArr = gameRootedStatisticsChange($gameRootedStatisticsArr, $gameId, $gameTeamComparisons['command_1']['team_id'], $pqGameRootedStatistics, 'team_1', 'start', 2);
                $gameRootedStatisticsArr = gameRootedStatisticsChange($gameRootedStatisticsArr, $gameId, $gameTeamComparisons['command_1']['team_id'], $pqGameRootedStatistics, 'team_1', 'bench', 3);
                $gameRootedStatisticsArr = gameRootedStatisticsChange($gameRootedStatisticsArr, $gameId, $gameTeamComparisons['command_2']['team_id'], $pqGameRootedStatistics, 'team_2', 'start', 4);
                $gameRootedStatisticsArr = gameRootedStatisticsChange($gameRootedStatisticsArr, $gameId, $gameTeamComparisons['command_2']['team_id'], $pqGameRootedStatistics, 'team_2', 'bench', 5);
                $gameOtherStatistic = [];
                for($i = 0; $i < count($pqGameRootedStatistics->find('.mbt-holder-content1 table:eq(3) tr')); $i++)
                {
                    if($i == 0 ||
                        $i > 10 )
                    {
                        continue;
                    }
                    $gameOtherStatistic[$i]['name'] = trim($pqGameRootedStatistics->find('.mbt-holder-content1 table:eq(3) tr:eq('.$i.') td:eq(0)')->text());
                    $gameOtherStatistic[$i]['team_1'] = explode('%', $pqGameRootedStatistics->find('.mbt-holder-content1 table:eq(3) tr:eq('.$i.') td:eq(1)')->text())[0];
                    $gameOtherStatistic[$i]['team_2'] = explode('%',$pqGameRootedStatistics->find('.mbt-holder-content1 table:eq(3) tr:eq('.$i.') td:eq(2)')->text())[0];
                    $findGameOtherStatic = GameOtherStatistic::find()->where(['game_id' => $gameId, 'name' => $gameOtherStatistic[$i]['name']])->one();
                    if(empty($findGameOtherStatic))
                    {
                        $newGameOtherStatic = new GameOtherStatistic();
                        $newGameOtherStatic->game_id = $gameId;
                        $newGameOtherStatic->name = $gameOtherStatistic[$i]['name'];
                        $newGameOtherStatic->team_1 = $gameOtherStatistic[$i]['team_1'];
                        $newGameOtherStatic->team_2 = $gameOtherStatistic[$i]['team_2'];
                        $newGameOtherStatic->save();
                    }
                }
            }
        }
        return $this->render('pars-one', ['gameData' => $gameData]);
    }

}