<?php

namespace app\modules\admin;

/**
 * Admin module definition class
 */
class Admin extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\admin\controllers';
    public $layoutPath = '@app/modules/admin/views/layout/admin.php';
    public $layout = '/admin';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
