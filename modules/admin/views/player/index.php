<?php
use yii\helpers\Html;
?>

<div class="row">
    <div class="col-md-12">
        <?= Html::a("Парсинг игроков", ['player/pars-all'], ['class' => ["btn", "btn-success"]]) ?>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <?= Html::a("Парсинг игрока", ['player/pars-one'], ['class' => ["btn", "btn-success"]]) ?>
    </div>
</div>