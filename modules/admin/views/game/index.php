<?php
use yii\helpers\Html;
?>

<div class="row">
    <div class="col-md-12">
        <?= Html::a("Парсинг игр", ['game/pars-all'], ['class' => ["btn", "btn-success"]]) ?>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <?= Html::a("Парсинг каждой игры подробно", ['game/pars-one'], ['class' => ["btn", "btn-success"]]) ?>
    </div>
</div>