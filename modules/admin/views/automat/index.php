<?php
use yii\helpers\Html;
?>
<h2>Автоматический парсинг компонентов</h2>

<hr>
<h4>Парсинг всего сразу</h4>
<div class="row">
    <div class="col-md-12">

        <?= Html::a("Начать", ['automat/all'], ['class' => ["btn", "btn-success"]]) ?>
    </div>
</div>
<hr>
<h4>Парсинг персоначальных данных</h4>
<div class="row">
    <div class="col-md-12">

        <?= Html::a("Начать", ['automat/first-data'], ['class' => ["btn", "btn-success"]]) ?>
    </div>
</div>
