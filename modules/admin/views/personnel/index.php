<?php
use yii\helpers\Html;
?>
<div class="row">
    <div class="col-md-12">
        <?= Html::a("Парсинг тренеров", ['personnel/trainer-all'], ['class' => ["btn", "btn-success"]]) ?>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <?= Html::a("Парсинг комисаров", ['personnel/commissar-all'], ['class' => ["btn", "btn-success"]]) ?>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <?= Html::a("Парсинг арбитров", ['personnel/judiciary-all'], ['class' => ["btn", "btn-success"]]) ?>
    </div>
</div>