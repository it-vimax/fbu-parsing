<?php
namespace app\libs;
class Parsing
{
    public $finishUrl;
    public $playerMenu = [
        "home" => "&request[0][part]=home", // домашняя
        "game_by_game" => "&request[0][part]=game_by_game", // игра за игрой
        "highs" => "&request[0][part]=highs", // рекорды
        "advanced_stats" => "&request[0][part]=advanced_stats", // рекорды
        "career" => "&request[0][part]=career", // карера
    ];

    public $tableCalendarNameColum = [
        'Дата та час',
        '№ гри',
        'Група',
        'Етап',
        'Господарі',
        'Результат',
        'Гості',
        'Арена',
    ];

    public $playerRequestPart = [
        0 =>'home',
        1 =>'game_by_game',
        2 =>'highs', // рекорды
        3 =>'advanced_stats',
        4 =>'career', // карьера
    ];

    public function request($dataPath)
    {
        $this->finishUrl = $dataPath;
        $baskethotel = curl_init();
        curl_setopt($baskethotel, CURLOPT_URL, $dataPath);
        curl_setopt($baskethotel, CURLOPT_RETURNTRANSFER, 1);
        $resBaskethotel = curl_exec($baskethotel);
        curl_close($baskethotel);
        $t = '/\t/';
        $resBaskethotel = preg_replace($t,'', $resBaskethotel);
        $r = '/\\\r/';
        $resBaskethotel = preg_replace($r,'', $resBaskethotel);
        $r = '/\\r/';
        $resBaskethotel = preg_replace($r,'', $resBaskethotel);
        $n = '/\\\n/';
        $resBaskethotel = preg_replace($n,'', $resBaskethotel);
        $n = '/\\n/';
        $resBaskethotel = preg_replace($n,'', $resBaskethotel);
        $h = '/\\\+/';
        $resBaskethotel = preg_replace($h,'', $resBaskethotel);
        $h2 = '/\"/';
        $resBaskethotel = preg_replace($h2,'\'', $resBaskethotel);
        return $resBaskethotel;
    }
}