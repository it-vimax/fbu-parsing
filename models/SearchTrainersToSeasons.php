<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TrainersToSeasons;

/**
 * SearchTrainersToSeasons represents the model behind the search form about `app\models\TrainersToSeasons`.
 */
class SearchTrainersToSeasons extends TrainersToSeasons
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['trainer_id', 'season_id'], 'integer'],
            [['team_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TrainersToSeasons::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'trainer_id' => $this->trainer_id,
            'season_id' => $this->season_id,
        ]);

        $query->andFilterWhere(['like', 'team_name', $this->team_name]);

        return $dataProvider;
    }
}
