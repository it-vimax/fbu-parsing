<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "teams".
 *
 * @property integer $team_id
 * @property integer $league_id
 * @property string $name
 * @property string $city
 *
 * @property GameProtocolTeamComparisons[] $gameProtocolTeamComparisons
 * @property GameRootedStatistics[] $gameRootedStatistics
 * @property Games[] $games
 * @property Games[] $games0
 * @property PlayerCareers[] $playerCareers
 * @property PlayerToSeason[] $playerToSeasons
 * @property Leagues $league
 */
class Teams extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'teams';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['team_id', 'league_id'], 'required'],
            [['team_id', 'league_id'], 'integer'],
            [['name', 'city'], 'string', 'max' => 255],
            [['league_id'], 'exist', 'skipOnError' => true, 'targetClass' => Leagues::className(), 'targetAttribute' => ['league_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'team_id' => 'Team ID',
            'league_id' => 'League ID',
            'name' => 'Name',
            'city' => 'City',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGameProtocolTeamComparisons()
    {
        return $this->hasMany(GameProtocolTeamComparisons::className(), ['team_id' => 'team_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGameRootedStatistics()
    {
        return $this->hasMany(GameRootedStatistics::className(), ['team_id' => 'team_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGames()
    {
        return $this->hasMany(Games::className(), ['command_1' => 'team_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGames0()
    {
        return $this->hasMany(Games::className(), ['command_2' => 'team_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayerCareers()
    {
        return $this->hasMany(PlayerCareers::className(), ['club_id' => 'team_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayerToSeasons()
    {
        return $this->hasMany(PlayerToSeason::className(), ['club_id' => 'team_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLeague()
    {
        return $this->hasOne(Leagues::className(), ['id' => 'league_id']);
    }
}
