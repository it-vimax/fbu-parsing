<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PlayerCareers;

/**
 * SearchPlayerCareers represents the model behind the search form about `app\models\PlayerCareers`.
 */
class SearchPlayerCareers extends PlayerCareers
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'player_id', 'season_id', 'club_id', 'count_games'], 'integer'],
            [['game_time', 'two_point_kiddies', 'two_point_kiddies_ok', 'two_point_kiddies_percent', 'three_point_kiddies', 'three_point_kiddies_ok', 'three_point_kiddies_percent', 'one_point_kiddies', 'one_point_kiddies_ok', 'one_point_kiddies_percent', 'picking_up_in_an_attack', 'picking_up_in_defense', 'overall_picking_up', 'transmissions', 'staff_foul_1', 'staff_fouls_2', 'losses', 'interception', 'block_shots_1', 'block_shots_2', 'efficiency', 'gained_points'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PlayerCareers::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'player_id' => $this->player_id,
            'season_id' => $this->season_id,
            'club_id' => $this->club_id,
            'count_games' => $this->count_games,
        ]);

        $query->andFilterWhere(['like', 'game_time', $this->game_time])
            ->andFilterWhere(['like', 'two_point_kiddies', $this->two_point_kiddies])
            ->andFilterWhere(['like', 'two_point_kiddies_ok', $this->two_point_kiddies_ok])
            ->andFilterWhere(['like', 'two_point_kiddies_percent', $this->two_point_kiddies_percent])
            ->andFilterWhere(['like', 'three_point_kiddies', $this->three_point_kiddies])
            ->andFilterWhere(['like', 'three_point_kiddies_ok', $this->three_point_kiddies_ok])
            ->andFilterWhere(['like', 'three_point_kiddies_percent', $this->three_point_kiddies_percent])
            ->andFilterWhere(['like', 'one_point_kiddies', $this->one_point_kiddies])
            ->andFilterWhere(['like', 'one_point_kiddies_ok', $this->one_point_kiddies_ok])
            ->andFilterWhere(['like', 'one_point_kiddies_percent', $this->one_point_kiddies_percent])
            ->andFilterWhere(['like', 'picking_up_in_an_attack', $this->picking_up_in_an_attack])
            ->andFilterWhere(['like', 'picking_up_in_defense', $this->picking_up_in_defense])
            ->andFilterWhere(['like', 'overall_picking_up', $this->overall_picking_up])
            ->andFilterWhere(['like', 'transmissions', $this->transmissions])
            ->andFilterWhere(['like', 'staff_foul_1', $this->staff_foul_1])
            ->andFilterWhere(['like', 'staff_fouls_2', $this->staff_fouls_2])
            ->andFilterWhere(['like', 'losses', $this->losses])
            ->andFilterWhere(['like', 'interception', $this->interception])
            ->andFilterWhere(['like', 'block_shots_1', $this->block_shots_1])
            ->andFilterWhere(['like', 'block_shots_2', $this->block_shots_2])
            ->andFilterWhere(['like', 'efficiency', $this->efficiency])
            ->andFilterWhere(['like', 'gained_points', $this->gained_points]);

        return $dataProvider;
    }
}
