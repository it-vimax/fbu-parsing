<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PlayerToSeason;

/**
 * SearchPlayerToSeason represents the model behind the search form about `app\models\PlayerToSeason`.
 */
class SearchPlayerToSeason extends PlayerToSeason
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['player_id', 'season_id', 'club_id', 'count_game', 'points', 'pink_up', 'forwarding', 'efficiency', 'plus_minus'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PlayerToSeason::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'player_id' => $this->player_id,
            'season_id' => $this->season_id,
            'club_id' => $this->club_id,
            'count_game' => $this->count_game,
            'points' => $this->points,
            'pink_up' => $this->pink_up,
            'forwarding' => $this->forwarding,
            'efficiency' => $this->efficiency,
            'plus_minus' => $this->plus_minus,
        ]);

        return $dataProvider;
    }
}
