<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Games;

/**
 * SearchGames represents the model behind the search form about `app\models\Games`.
 */
class SearchGames extends Games
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'season_id', 'command_1', 'command_2', 'result_command_1', 'result_command_2', 'commissar_id', 'first_quarter_comand_1', 'first_quarter_comand_2', 'second_quarter_comand_1', 'second_quarter_comand_2', 'third_quarter_comand_1', 'third_quarter_comand_2', 'fourth_quarter_comand_1', 'fourth_quarter_comand_2', 'the_result_of_the_five_comand_1', 'the_result_of_the_five_comand_2', 'beat_the_spare_comand_1', 'beat_the_spare_comand_2', 'the_hottest_jerk_comand_1', 'the_hottest_jerk_comand_2', 'the_biggest_gap_comand_1', 'the_biggest_gap_comand_2', 'points_from_the_zone_comand_1', 'points_from_the_zone_comand_2'], 'integer'],
            [['date', 'number_game', 'group', 'step', 'arena', 'viewers'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Games::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'season_id' => $this->season_id,
            'date' => $this->date,
            'command_1' => $this->command_1,
            'command_2' => $this->command_2,
            'result_command_1' => $this->result_command_1,
            'result_command_2' => $this->result_command_2,
            'commissar_id' => $this->commissar_id,
            'first_quarter_comand_1' => $this->first_quarter_comand_1,
            'first_quarter_comand_2' => $this->first_quarter_comand_2,
            'second_quarter_comand_1' => $this->second_quarter_comand_1,
            'second_quarter_comand_2' => $this->second_quarter_comand_2,
            'third_quarter_comand_1' => $this->third_quarter_comand_1,
            'third_quarter_comand_2' => $this->third_quarter_comand_2,
            'fourth_quarter_comand_1' => $this->fourth_quarter_comand_1,
            'fourth_quarter_comand_2' => $this->fourth_quarter_comand_2,
            'the_result_of_the_five_comand_1' => $this->the_result_of_the_five_comand_1,
            'the_result_of_the_five_comand_2' => $this->the_result_of_the_five_comand_2,
            'beat_the_spare_comand_1' => $this->beat_the_spare_comand_1,
            'beat_the_spare_comand_2' => $this->beat_the_spare_comand_2,
            'the_hottest_jerk_comand_1' => $this->the_hottest_jerk_comand_1,
            'the_hottest_jerk_comand_2' => $this->the_hottest_jerk_comand_2,
            'the_biggest_gap_comand_1' => $this->the_biggest_gap_comand_1,
            'the_biggest_gap_comand_2' => $this->the_biggest_gap_comand_2,
            'points_from_the_zone_comand_1' => $this->points_from_the_zone_comand_1,
            'points_from_the_zone_comand_2' => $this->points_from_the_zone_comand_2,
        ]);

        $query->andFilterWhere(['like', 'number_game', $this->number_game])
            ->andFilterWhere(['like', 'group', $this->group])
            ->andFilterWhere(['like', 'step', $this->step])
            ->andFilterWhere(['like', 'arena', $this->arena])
            ->andFilterWhere(['like', 'viewers', $this->viewers]);

        return $dataProvider;
    }
}
