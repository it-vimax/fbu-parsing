<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\GameProtocolTeamComparisons;

/**
 * SearchGameProtocolTeamComparisons represents the model behind the search form about `app\models\GameProtocolTeamComparisons`.
 */
class SearchGameProtocolTeamComparisons extends GameProtocolTeamComparisons
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'game_id', 'team_status', 'team_id'], 'integer'],
            [['two_point_kiddies', 'two_point_kiddies_ok', 'two_point_kiddies_percent', 'three_point_kiddies', 'three_point_kiddies_ok', 'three_point_kiddies_percent', 'one_point_kiddies', 'one_point_kiddies_ok', 'one_point_kiddies_percent', 'picking_up_in_an_attack', 'picking_up_in_defense', 'overall_picking_up', 'transmissions', 'personal_foul', 'losses', 'interception', 'block_shots_1', 'efficiency', 'gained_points'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GameProtocolTeamComparisons::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'game_id' => $this->game_id,
            'team_status' => $this->team_status,
            'team_id' => $this->team_id,
        ]);

        $query->andFilterWhere(['like', 'two_point_kiddies', $this->two_point_kiddies])
            ->andFilterWhere(['like', 'two_point_kiddies_ok', $this->two_point_kiddies_ok])
            ->andFilterWhere(['like', 'two_point_kiddies_percent', $this->two_point_kiddies_percent])
            ->andFilterWhere(['like', 'three_point_kiddies', $this->three_point_kiddies])
            ->andFilterWhere(['like', 'three_point_kiddies_ok', $this->three_point_kiddies_ok])
            ->andFilterWhere(['like', 'three_point_kiddies_percent', $this->three_point_kiddies_percent])
            ->andFilterWhere(['like', 'one_point_kiddies', $this->one_point_kiddies])
            ->andFilterWhere(['like', 'one_point_kiddies_ok', $this->one_point_kiddies_ok])
            ->andFilterWhere(['like', 'one_point_kiddies_percent', $this->one_point_kiddies_percent])
            ->andFilterWhere(['like', 'picking_up_in_an_attack', $this->picking_up_in_an_attack])
            ->andFilterWhere(['like', 'picking_up_in_defense', $this->picking_up_in_defense])
            ->andFilterWhere(['like', 'overall_picking_up', $this->overall_picking_up])
            ->andFilterWhere(['like', 'transmissions', $this->transmissions])
            ->andFilterWhere(['like', 'personal_foul', $this->personal_foul])
            ->andFilterWhere(['like', 'losses', $this->losses])
            ->andFilterWhere(['like', 'interception', $this->interception])
            ->andFilterWhere(['like', 'block_shots_1', $this->block_shots_1])
            ->andFilterWhere(['like', 'efficiency', $this->efficiency])
            ->andFilterWhere(['like', 'gained_points', $this->gained_points]);

        return $dataProvider;
    }
}
