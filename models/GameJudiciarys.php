<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "game_judiciarys".
 *
 * @property integer $id
 * @property integer $game_id
 * @property integer $judiciary_id
 *
 * @property Games $game
 * @property PersonnelJudiciarys $judiciary
 */
class GameJudiciarys extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'game_judiciarys';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['game_id', 'judiciary_id'], 'integer'],
            [['game_id'], 'exist', 'skipOnError' => true, 'targetClass' => Games::className(), 'targetAttribute' => ['game_id' => 'id']],
            [['judiciary_id'], 'exist', 'skipOnError' => true, 'targetClass' => PersonnelJudiciarys::className(), 'targetAttribute' => ['judiciary_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'game_id' => 'Game ID',
            'judiciary_id' => 'Judiciary ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGame()
    {
        return $this->hasOne(Games::className(), ['id' => 'game_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJudiciary()
    {
        return $this->hasOne(PersonnelJudiciarys::className(), ['id' => 'judiciary_id']);
    }
}
