<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "personnel_judiciarys".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 *
 * @property GameJudiciarys[] $gameJudiciarys
 */
class PersonnelJudiciarys extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'personnel_judiciarys';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGameJudiciarys()
    {
        return $this->hasMany(GameJudiciarys::className(), ['judiciary_id' => 'id']);
    }
}
