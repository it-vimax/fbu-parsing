<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PlayerHighs;

/**
 * SearchPlayerHighs represents the model behind the search form about `app\models\PlayerHighs`.
 */
class SearchPlayerHighs extends PlayerHighs
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'player_id', 'season_id', 'position'], 'integer'],
            [['category_name', 'category_number', 'date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PlayerHighs::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'player_id' => $this->player_id,
            'season_id' => $this->season_id,
            'date' => $this->date,
            'position' => $this->position,
        ]);

        $query->andFilterWhere(['like', 'category_name', $this->category_name])
            ->andFilterWhere(['like', 'category_number', $this->category_number]);

        return $dataProvider;
    }
}
