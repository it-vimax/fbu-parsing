<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "player_careers".
 *
 * @property integer $id
 * @property integer $player_id
 * @property integer $season_id
 * @property integer $club_id
 * @property integer $count_games
 * @property string $game_time
 * @property string $two_point_kiddies
 * @property string $two_point_kiddies_ok
 * @property string $two_point_kiddies_percent
 * @property string $three_point_kiddies
 * @property string $three_point_kiddies_ok
 * @property string $three_point_kiddies_percent
 * @property string $one_point_kiddies
 * @property string $one_point_kiddies_ok
 * @property string $one_point_kiddies_percent
 * @property string $picking_up_in_an_attack
 * @property string $picking_up_in_defense
 * @property string $overall_picking_up
 * @property string $transmissions
 * @property string $staff_foul_1
 * @property string $staff_fouls_2
 * @property string $losses
 * @property string $interception
 * @property string $block_shots_1
 * @property string $block_shots_2
 * @property string $efficiency
 * @property string $gained_points
 *
 * @property Teams $club
 * @property Players $player
 * @property Seasons $season
 */
class PlayerCareers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'player_careers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['player_id', 'season_id', 'club_id', 'count_games'], 'integer'],
            [['game_time', 'two_point_kiddies', 'two_point_kiddies_ok', 'two_point_kiddies_percent', 'three_point_kiddies', 'three_point_kiddies_ok', 'three_point_kiddies_percent', 'one_point_kiddies', 'one_point_kiddies_ok', 'one_point_kiddies_percent', 'picking_up_in_an_attack', 'picking_up_in_defense', 'overall_picking_up', 'transmissions', 'staff_foul_1', 'staff_fouls_2', 'losses', 'interception', 'block_shots_1', 'block_shots_2', 'efficiency', 'gained_points'], 'string', 'max' => 255],
            [['club_id'], 'exist', 'skipOnError' => true, 'targetClass' => Teams::className(), 'targetAttribute' => ['club_id' => 'team_id']],
            [['player_id'], 'exist', 'skipOnError' => true, 'targetClass' => Players::className(), 'targetAttribute' => ['player_id' => 'id']],
            [['season_id'], 'exist', 'skipOnError' => true, 'targetClass' => Seasons::className(), 'targetAttribute' => ['season_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'player_id' => 'Player ID',
            'season_id' => 'Season ID',
            'club_id' => 'Club ID',
            'count_games' => 'Count Games',
            'game_time' => 'Game Time',
            'two_point_kiddies' => 'Two Point Kiddies',
            'two_point_kiddies_ok' => 'Two Point Kiddies Ok',
            'two_point_kiddies_percent' => 'Two Point Kiddies Percent',
            'three_point_kiddies' => 'Three Point Kiddies',
            'three_point_kiddies_ok' => 'Three Point Kiddies Ok',
            'three_point_kiddies_percent' => 'Three Point Kiddies Percent',
            'one_point_kiddies' => 'One Point Kiddies',
            'one_point_kiddies_ok' => 'One Point Kiddies Ok',
            'one_point_kiddies_percent' => 'One Point Kiddies Percent',
            'picking_up_in_an_attack' => 'Picking Up In An Attack',
            'picking_up_in_defense' => 'Picking Up In Defense',
            'overall_picking_up' => 'Overall Picking Up',
            'transmissions' => 'Transmissions',
            'staff_foul_1' => 'Staff Foul 1',
            'staff_fouls_2' => 'Staff Fouls 2',
            'losses' => 'Losses',
            'interception' => 'Interception',
            'block_shots_1' => 'Block Shots 1',
            'block_shots_2' => 'Block Shots 2',
            'efficiency' => 'Efficiency',
            'gained_points' => 'Gained Points',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClub()
    {
        return $this->hasOne(Teams::className(), ['team_id' => 'club_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayer()
    {
        return $this->hasOne(Players::className(), ['id' => 'player_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeason()
    {
        return $this->hasOne(Seasons::className(), ['id' => 'season_id']);
    }
}
