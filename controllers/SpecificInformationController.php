<?php
/**
 * Created by PhpStorm.
 * User: Maksim
 * Date: 20.12.2017
 * Time: 14:41
 */

namespace app\controllers;

use app\models\GameJudiciarys;
use app\models\GameOtherStatistic;
use app\models\GameProtocolCommands;
use app\models\GameProtocolTeamComparisons;
use app\models\GameRootedStatistics;
use app\models\Games;
use app\models\PersonnelCommissars;
use app\models\PersonnelJudiciarys;
use app\models\PersonnelTrainers;
use app\models\PlayerCareers;
use app\models\PlayerHighs;
use app\models\Players;
use app\models\PlayerToSeason;
use app\models\SearchGameProtocolTeamComparisons;
use app\models\Teams;
use app\models\TrainersToSeasons;
use Yii;
use yii\web\Controller;

class SpecificInformationController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionFirstData()
    {
        return $this->render('first-data');
    }

    public function actionPlayerData()
    {
        return $this->render('player-data');
    }

    public function actionTeamData()
    {
        return $this->render('team-data');
    }

    public function actionGameData()
    {
        return $this->render('game-data');
    }

    public function actionPlayer()
    {
        $id = Yii::$app->request->get('player_id');
        $player = Players::find()->where(['id' => $id])->asArray()->one();
        if(!empty($player))
        {
            $playerCareers = PlayerCareers::find()->where(['player_id' => $id])->with(['season', 'club'])->orderBy(['id' => SORT_DESC])->all();
            $playerHighs = PlayerHighs::find()->where(['player_id' => $id])->with('season')->all();
            $playerToSeason = PlayerToSeason::find()->where(['player_id' => $id])->with(['season', 'club'])->all();
            return $this->render('player', compact('player', 'playerCareers', 'playerHighs', 'playerToSeason'));
        }
        else
        {
            return $this->asJson('Игрок не найден');
        }
    }

    public function actionGame()
    {
        $id = Yii::$app->request->get('game_id');
        $game = Games::find()->where(['id' => $id])->with(['season', 'command1', 'command2', 'commissar'])->all()[0];
        if (!empty($game))
        {
            $gameOtherStatistic = GameOtherStatistic::find()->where(['game_id' => $id])->all();
            $gameProtocolCommands = GameProtocolCommands::find()->where(['game_id' => $id])->with(['player'])->all();
            $gameProtocolTeamComparisons = GameProtocolTeamComparisons::find()->where(['game_id' => $id])->all();
            $gameRootedStatistics = GameRootedStatistics::find()->where(['game_id' => $id])->with(['team'])->all();
            $gameJudiciarys = GameJudiciarys::find()->where(['game_id' => $id])->all();
            return $this->render('game', compact('game', 'gameOtherStatistic', 'gameProtocolCommands', 'gameProtocolTeamComparisons', 'gameRootedStatistics', 'gameJudiciarys'));
        }
        else
        {
            return $this->asJson('Игра не найдена');
        }
    }

    public function actionTeam()
    {
        $id = Yii::$app->request->get('team_id');
        $team = Teams::find()->where(['team_id' => $id])->all();
        if(!empty($team))
        {
            return $this->render('team', compact('team'));
        }
        else
        {
            return $this->asJson('Команда не найдена');
        }
    }
    public function actionTrainer()
    {
        $id = Yii::$app->request->get('trainer_id');
        $personnelTrainers = PersonnelTrainers::find()->where(['id' => $id])->all()[0];
        if(!empty($personnelTrainers))
        {
            $trainersToSeasons = TrainersToSeasons::find()->where(['trainer_id' => $personnelTrainers->id])->all();
            return $this->render('trainer', compact('personnelTrainers','trainersToSeasons'));
        }
        else
        {
            return $this->asJson('Тренер не найден');
        }
    }

    public function actionCommissar()
    {
        $id = Yii::$app->request->get('commissar_id');
        $commissar = PersonnelCommissars::find()->where(['id' => $id])->all()[0];
        if(!empty($commissar))
        {
            $gamesForCommissar = Games::find()->where(['commissar_id' => $id])->all();
            return $this->render('commissar', compact('commissar', 'gamesForCommissar'));
        }
        else
        {
            return $this->asJson('Комисар не найден');
        }
    }

    public function actionJudiciary()
    {
        $id = Yii::$app->request->get('judiciary_id');
        $judiciary = PersonnelJudiciarys::find()->where(['id' => $id])->all()[0];
        if(!empty($judiciary))
        {
            $gamesForJudiciary = GameJudiciarys::find()->where(['judiciary_id' => $id])->all();
            $gameJudiciarys = [];
            foreach($gamesForJudiciary as $key=>$value)
            {

                $gameJudiciarys[$key] = GameJudiciarys::find()->where(['game_id' => $value->game_id])->all();
            }
//            return $this->asJson($gameJudiciarys);
            return $this->render('judiciary', compact('judiciary', 'gamesForJudiciary', 'gameJudiciarys'));
        }
        else
        {
            return $this->asJson('Суддя не найден');
        }
    }
}