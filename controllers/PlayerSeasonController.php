<?php

namespace app\controllers;

use Yii;
use app\models\PlayerToSeason;
use app\models\SearchPlayerToSeason;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PlayerSeasonController implements the CRUD actions for PlayerToSeason model.
 */
class PlayerSeasonController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PlayerToSeason models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchPlayerToSeason();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PlayerToSeason model.
     * @param integer $player_id
     * @param integer $season_id
     * @return mixed
     */
    public function actionView($player_id, $season_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($player_id, $season_id),
        ]);
    }

    /**
     * Creates a new PlayerToSeason model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PlayerToSeason();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'player_id' => $model->player_id, 'season_id' => $model->season_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PlayerToSeason model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $player_id
     * @param integer $season_id
     * @return mixed
     */
    public function actionUpdate($player_id, $season_id)
    {
        $model = $this->findModel($player_id, $season_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'player_id' => $model->player_id, 'season_id' => $model->season_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PlayerToSeason model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $player_id
     * @param integer $season_id
     * @return mixed
     */
    public function actionDelete($player_id, $season_id)
    {
        $this->findModel($player_id, $season_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PlayerToSeason model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $player_id
     * @param integer $season_id
     * @return PlayerToSeason the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($player_id, $season_id)
    {
        if (($model = PlayerToSeason::findOne(['player_id' => $player_id, 'season_id' => $season_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
