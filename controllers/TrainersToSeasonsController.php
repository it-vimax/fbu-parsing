<?php

namespace app\controllers;

use Yii;
use app\models\TrainersToSeasons;
use app\models\SearchTrainersToSeasons;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TrainersToSeasonsController implements the CRUD actions for TrainersToSeasons model.
 */
class TrainersToSeasonsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TrainersToSeasons models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchTrainersToSeasons();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TrainersToSeasons model.
     * @param integer $trainer_id
     * @param integer $season_id
     * @return mixed
     */
    public function actionView($trainer_id, $season_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($trainer_id, $season_id),
        ]);
    }

    /**
     * Creates a new TrainersToSeasons model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TrainersToSeasons();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'trainer_id' => $model->trainer_id, 'season_id' => $model->season_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TrainersToSeasons model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $trainer_id
     * @param integer $season_id
     * @return mixed
     */
    public function actionUpdate($trainer_id, $season_id)
    {
        $model = $this->findModel($trainer_id, $season_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'trainer_id' => $model->trainer_id, 'season_id' => $model->season_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TrainersToSeasons model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $trainer_id
     * @param integer $season_id
     * @return mixed
     */
    public function actionDelete($trainer_id, $season_id)
    {
        $this->findModel($trainer_id, $season_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TrainersToSeasons model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $trainer_id
     * @param integer $season_id
     * @return TrainersToSeasons the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($trainer_id, $season_id)
    {
        if (($model = TrainersToSeasons::findOne(['trainer_id' => $trainer_id, 'season_id' => $season_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
