<?php

use yii\db\Migration;

/**
 * Class m171223_133133_personnelTrainerToSeason
 */
class m171223_133133_personnelTrainerToSeason extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('trainers_to_seasons', [
            'trainer_id' => $this->integer()->notNull(),
            'season_id' => $this->integer()->notNull(),
            'team_name' => $this->string()
        ]);
        $this->addPrimaryKey(
            'pk-trainers_to_seasons-trainer_id-season_id',
            'trainers_to_seasons',
            ['trainer_id', 'season_id']
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171223_133133_personnelTrainerToSeason cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171223_133133_personnelTrainerToSeason cannot be reverted.\n";

        return false;
    }
    */
}
