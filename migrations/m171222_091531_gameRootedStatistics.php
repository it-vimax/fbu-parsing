<?php

use yii\db\Migration;

/**
 * Class m171222_091531_gameRootedStatistics
 */
class m171222_091531_gameRootedStatistics extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('game_rooted_statistics', [
            'id' => $this->primaryKey(),
            'game_id' => $this->integer(),
            'team_id' => $this->integer(),
            'team_status' => $this->string(),
            'time_in_game' => $this->string(), // время в игре
            'two_point_kiddies' => $this->string(),// двохочковые броски
            'two_point_kiddies_ok' => $this->string(),// двохочковые броски попал
            'three_point_kiddies' => $this->string(),// трехочковые броски
            'three_point_kiddies_ok' => $this->string(),// трехочковые броски попал
            'one_point_kiddies' => $this->string(),// штрафные броски
            'one_point_kiddies_ok' => $this->string(),// штрафные броски попал
            'picking_up_in_an_attack' => $this->string(), // подбирание в нападении
            'picking_up_in_defense' => $this->string(),// подбирание в защите
            'overall_picking_up' => $this->string(),// подбирань в общем
            'transmissions' => $this->string(),// передачи
            'personal_foul' => $this->string(),// персональные фолы
            'losses' => $this->string(), // потери
            'interception' => $this->string(), // перехват
            'block_shots' => $this->string(), // бок шоты
            'efficiency' => $this->string(),// эфэктивность
            'gained_points' => $this->string(),// полученые очки
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171222_091531_gameRootedStatistics cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171222_091531_gameRootedStatistics cannot be reverted.\n";

        return false;
    }
    */
}
