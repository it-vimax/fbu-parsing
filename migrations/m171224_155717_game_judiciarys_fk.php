<?php

use yii\db\Migration;

/**
 * Class m171224_155717_game_judiciarys_fk
 */
class m171224_155717_game_judiciarys_fk extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-game_judiciarys-game_id',
            'game_judiciarys',
            'game_id',
            'games',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-game_judiciarys-judiciary_id',
            'game_judiciarys',
            'judiciary_id',
            'personnel_judiciarys',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171224_155717_game_judiciarys_fk cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171224_155717_game_judiciarys_fk cannot be reverted.\n";

        return false;
    }
    */
}
