<?php

use yii\db\Migration;

/**
 * Class m171220_160539_Season_fk
 */
class m171220_160539_Season_fk extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-seasons-league_id',
            'seasons',
            'league_id',
            'leagues',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171220_160539_Season_fk cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171220_160539_Season_fk cannot be reverted.\n";

        return false;
    }
    */
}
