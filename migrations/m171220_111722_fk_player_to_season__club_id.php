<?php

use yii\db\Migration;

/**
 * Class m171220_111722_fk_player_to_season__club_id
 */
class m171220_111722_fk_player_to_season__club_id extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-player_to_season-club_id',
            'player_to_season',
            'club_id',
            'teams',
            'team_id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171220_111722_fk_player_to_season__club_id cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171220_111722_fk_player_to_season__club_id cannot be reverted.\n";

        return false;
    }
    */
}
