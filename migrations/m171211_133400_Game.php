<?php

use yii\db\Migration;

/**
 * Class m171211_133400_Game
 */
class m171211_133400_Game extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('games', [
            'id' => $this->primaryKey(),
            'season_id' => $this->integer()->notNull(),
            'date' => $this->dateTime(),
            'number_game' => $this->string(),
            'group' => $this->string(),
            'step' => $this->string(),
            'command_1' => $this->integer(),
            'command_2' => $this->integer(),
            'result_command_1' => $this->integer(),
            'result_command_2' => $this->integer(),
            'arena' => $this->string(),
            'commissar_id' => $this->integer(),
            'viewers' => $this->string(),
            'first_quarter_comand_1' => $this->integer(), // первая четверть команда 1
            'first_quarter_comand_2' => $this->integer(), // первая четверть команда 2
            'second_quarter_comand_1' => $this->integer(), // вторая четверть команда 1
            'second_quarter_comand_2' => $this->integer(), // вторая четверть команда 2
            'third_quarter_comand_1' => $this->integer(), // третья четверть команда 1
            'third_quarter_comand_2' => $this->integer(), // третья четверть команда 2
            'fourth_quarter_comand_1' => $this->integer(), // четвертая четверть команда 1
            'fourth_quarter_comand_2' => $this->integer(), // четвертая четверть команда 2
            'the_result_of_the_five_comand_1' => $this->integer(), // Результат пятірки команда 1
            'the_result_of_the_five_comand_2' => $this->integer(), // Результат пятірки команда 2
            'beat_the_spare_comand_1' => $this->integer(), //забили запасні команда 1
            'beat_the_spare_comand_2' => $this->integer(), //забили запасні команда 2
            'the_hottest_jerk_comand_1' => $this->integer(), //найбілиший ривок команда 1
            'the_hottest_jerk_comand_2' => $this->integer(), //найбілиший ривок команда 2
            'the_biggest_gap_comand_1' => $this->integer(), //найбільший розрив команда 1
            'the_biggest_gap_comand_2' => $this->integer(), //найбільший розрив команда 2
            'points_from_the_zone_comand_1' => $this->integer(), //Очки з зони команда 1
            'points_from_the_zone_comand_2' => $this->integer(), //Очки з зони команда 2
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('games');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171211_133400_Game cannot be reverted.\n";

        return false;
    }
    */
}
