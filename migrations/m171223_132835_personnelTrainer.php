<?php

use yii\db\Migration;

/**
 * Class m171223_132835_personnelTrainer
 */
class m171223_132835_personnelTrainer extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('personnel_trainers', [
            'id' => $this->primaryKey(),
            'first_name' => $this->string(),
            'last_name' => $this->string()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171223_132835_personnelTrainer cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171223_132835_personnelTrainer cannot be reverted.\n";

        return false;
    }
    */
}
