<?php

use yii\db\Migration;

/**
 * Class m171224_155504_game_judiciarys
 */
class m171224_155504_game_judiciarys extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('game_judiciarys', [
            'id' => $this->primaryKey(),
            'game_id' => $this->integer(),
            'judiciary_id' => $this->integer()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171224_155504_game_judiciarys cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171224_155504_game_judiciarys cannot be reverted.\n";

        return false;
    }
    */
}
