<?php

use yii\db\Migration;

/**
 * Class m171223_131214_personnelJudiciarys
 */
class m171223_131214_personnelJudiciarys extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('personnel_judiciarys', [
            'id' => $this->primaryKey(),
            'first_name' => $this->string(),
            'last_name' => $this->string()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171223_131214_personnelJudiciarys cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171223_131214_personnelJudiciarys cannot be reverted.\n";

        return false;
    }
    */
}
