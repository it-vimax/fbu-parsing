<?php

use yii\db\Migration;

/**
 * Class m171222_135030_gameOtherStatistic_fk
 */
class m171222_135030_gameOtherStatistic_fk extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-game_other_statistic-game_id',
            'game_other_statistic',
            'game_id',
            'games',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171222_135030_gameOtherStatistic_fk cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171222_135030_gameOtherStatistic_fk cannot be reverted.\n";

        return false;
    }
    */
}
