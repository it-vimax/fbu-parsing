<?php

use yii\db\Migration;

/**
 * Class m171218_114735_Teams
 */
class m171218_114735_Teams extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('teams', [
//            'team_id' => $this->primaryKey(),
            'team_id' => $this->integer()->notNull(),
            'league_id' => $this->integer()->notNull(),
            'name' => $this->string(),
            'city' => $this->string(),
        ]);
        $this->addPrimaryKey('team_league_pk', 'teams', ['team_id', 'league_id']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('teams');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171218_114735_Teams cannot be reverted.\n";

        return false;
    }
    */
}
