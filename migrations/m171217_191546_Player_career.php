<?php

use yii\db\Migration;

/**
 * Class m171217_191546_Player_career
 */
class m171217_191546_Player_career extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('player_careers', [
            'id' => $this->primaryKey(),
            'player_id' => $this->integer(),
            'season_id' => $this->integer(),
            'club_id' => $this->integer(),
            'count_games' => $this->integer(),
            'game_time' => $this->string(),
            'two_point_kiddies' => $this->string(),// двохочковые броски
            'two_point_kiddies_ok' => $this->string(),// двохочковые броски попал
            'two_point_kiddies_percent' => $this->string(),// двохочковые броски процент
            'three_point_kiddies' => $this->string(),// трехочковые броски
            'three_point_kiddies_ok' => $this->string(),// трехочковые броски попал
            'three_point_kiddies_percent' => $this->string(),// трехочковые броски процент
            'one_point_kiddies' => $this->string(),// штрафные броски
            'one_point_kiddies_ok' => $this->string(),// штрафные броски попал
            'one_point_kiddies_percent' => $this->string(),// штрафные броски процент
            'picking_up_in_an_attack' => $this->string(), // подбирание в нападении
            'picking_up_in_defense' => $this->string(),// подбирание в защите
            'overall_picking_up' => $this->string(),// подбирань в общем
            'transmissions' => $this->string(),// передачи
            'staff_foul_1' => $this->string(),// персональные фолы
            'staff_fouls_2' => $this->string(),
            'losses' => $this->string(), // потери
            'interception' => $this->string(), // перехват
            'block_shots_1' => $this->string(), // бок шоты
            'block_shots_2' => $this->string(),// бок шоты
            'efficiency' => $this->string(),// эфэктивность
            'gained_points' => $this->string(),// полученые очки
        ]);
        $this->alterColumn('player_careers', 'id', $this->integer().' NOT NULL AUTO_INCREMENT');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('player_careers');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171217_191546_Player_career cannot be reverted.\n";

        return false;
    }
    */
}
