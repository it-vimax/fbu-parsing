<?php

use yii\db\Migration;

/**
 * Class m171207_162416_Season
 */
class m171207_162416_Season extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('seasons', [
            'id' => $this->primaryKey(),
            'league_id' => $this->integer(),
            'name' => $this->string(),
        ]);
//        $this->addPrimaryKey('id-league-pk', 'seasons', ['id', 'league_id']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('seasons');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171207_162416_Season cannot be reverted.\n";

        return false;
    }
    */
}
