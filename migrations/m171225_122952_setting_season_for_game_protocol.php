<?php

use yii\db\Migration;

/**
 * Class m171225_122952_settingSeasonforgameprotocol
 */
class m171225_122952_setting_season_for_game_protocol extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('setting_season_for_game_protocol', [
            'id' => $this->primaryKey(),
            'season_id' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171225_122952_settingSeasonforgameprotocol cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171225_122952_settingSeasonforgameprotocol cannot be reverted.\n";

        return false;
    }
    */
}
