<?php

use yii\db\Migration;

/**
 * Class m171220_160703_Player_to_season_fk
 */
class m171220_160703_Player_to_season_fk extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-player_to_season-season_id',
            'player_to_season',
            'season_id',
            'seasons',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171220_160703_Player_to_season_fk cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171220_160703_Player_to_season_fk cannot be reverted.\n";

        return false;
    }
    */
}
