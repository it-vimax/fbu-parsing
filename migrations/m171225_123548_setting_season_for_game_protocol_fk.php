<?php

use yii\db\Migration;

/**
 * Class m171225_123548_setting_season_for_game_protocol_fk
 */
class m171225_123548_setting_season_for_game_protocol_fk extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-setting_season_for_game_protocol-season_id',
            'setting_season_for_game_protocol',
            'season_id',
            'seasons',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171225_123548_setting_season_for_game_protocol_fk cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171225_123548_setting_season_for_game_protocol_fk cannot be reverted.\n";

        return false;
    }
    */
}
