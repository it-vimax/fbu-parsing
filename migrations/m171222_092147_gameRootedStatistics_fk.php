<?php

use yii\db\Migration;

/**
 * Class m171222_092147_gameRootedStatistics_fk
 */
class m171222_092147_gameRootedStatistics_fk extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-game_rooted_statistics-game_id',
            'game_rooted_statistics',
            'game_id',
            'games',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-game_rooted_statistics-team_id',
            'game_rooted_statistics',
            'team_id',
            'teams',
            'team_id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171222_092147_gameRootedStatistics_fk cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171222_092147_gameRootedStatistics_fk cannot be reverted.\n";

        return false;
    }
    */
}
