<?php

use yii\db\Migration;

/**
 * Class m171207_162435_Player
 */
class m171207_162435_Player extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('players', [
            'id' => $this->primaryKey(),
            'first_name' => $this->string(),
            'last_name' => $this->string(),
            'date_of_birth' => $this->date(),
            'position' => $this->string(),
            'height' => $this->integer(),
            'weight' => $this->integer(),
        ]);
        $this->alterColumn('players', 'id', $this->integer().' NOT NULL AUTO_INCREMENT');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('players');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171207_162435_Player cannot be reverted.\n";

        return false;
    }
    */
}
