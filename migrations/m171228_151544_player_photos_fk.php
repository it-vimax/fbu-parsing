<?php

use yii\db\Migration;

/**
 * Class m171228_151544_player_photos_fk
 */
class m171228_151544_player_photos_fk extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-player_photos-player_id',
            'player_photos',
            'player_id',
            'players',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-player_photos-season_id',
            'player_photos',
            'season_id',
            'seasons',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171228_151544_player_photos_fk cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171228_151544_player_photos_fk cannot be reverted.\n";

        return false;
    }
    */
}
