<?php

use yii\db\Migration;

/**
 * Class m171222_061556_game_protocol_team_comparison_fk
 */
class m171222_061556_game_protocol_team_comparison_fk extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-game_game_protocol_team_comparisons-game_id',
            'game_protocol_team_comparisons',
            'game_id',
            'games',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-game_protocol_team_comparisons-team_id',
            'game_protocol_team_comparisons',
            'team_id',
            'teams',
            'team_id',
            'CASCADE',
            'CASCADE'
        );

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171222_061556_game_protocol_team_comparison_fk cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171222_061556_game_protocol_team_comparison_fk cannot be reverted.\n";

        return false;
    }
    */
}
