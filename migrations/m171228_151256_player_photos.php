<?php

use yii\db\Migration;

/**
 * Class m171228_151256_player_photos
 */
class m171228_151256_player_photos extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('player_photos', [
            'id' => $this->primaryKey(),
            'player_id' => $this->integer(),
            'season_id' => $this->integer(),
            'name' => $this->string()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171228_151256_player_photos cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171228_151256_player_photos cannot be reverted.\n";

        return false;
    }
    */
}
