<?php

use yii\db\Migration;

/**
 * Class m171221_161445_game_protocol_commands_fk
 */
class m171221_161445_game_protocol_commands_fk extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-game_protocol_commands-game_id',
            'game_protocol_commands',
            'game_id',
            'games',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-game_protocol_commands-player_id',
            'game_protocol_commands',
            'player_id',
            'players',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171221_161445_game_protocol_commands_fk cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171221_161445_game_protocol_commands_fk cannot be reverted.\n";

        return false;
    }
    */
}
