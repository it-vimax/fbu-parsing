<?php

use yii\db\Migration;

/**
 * Class m171222_134839_gameOtherStatistic
 */
class m171222_134839_gameOtherStatistic extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('game_other_statistic', [
            'id' => $this->primaryKey(),
            'game_id' => $this->integer(),
            'name' => $this->string(),
            'team_1' => $this->string(),
            'team_2' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171222_134839_gameOtherStatistic cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171222_134839_gameOtherStatistic cannot be reverted.\n";

        return false;
    }
    */
}
