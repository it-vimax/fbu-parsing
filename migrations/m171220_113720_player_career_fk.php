<?php

use yii\db\Migration;

/**
 * Class m171220_113720_player_career_fk
 */
class m171220_113720_player_career_fk extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-player_careers-player_id',
            'player_careers',
            'player_id',
            'players',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-player_careers-club_id',
            'player_careers',
            'club_id',
            'teams',
            'team_id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-player_careers-season_id',
            'player_careers',
            'season_id',
            'seasons',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171220_113720_player_career_fk cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171220_113720_player_career_fk cannot be reverted.\n";

        return false;
    }
    */
}
