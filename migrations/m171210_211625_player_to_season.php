<?php

use yii\db\Migration;

/**
 * Class m171210_211625_player_to_season
 */
class m171210_211625_player_to_season extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('player_to_season', [
            'player_id' => $this->integer()->notNull(),
            'season_id' => $this->integer()->notNull(),
            'club_id' => $this->integer(),
//            'position' => $this->string(),
//            'height' => $this->integer(),
//            'weight' => $this->integer(),
            'count_game' => $this->integer(),
            'points' => $this->integer(),
            'pink_up' => $this->integer(),
            'forwarding' => $this->integer(),
            'efficiency' => $this->integer(),
            'plus_minus' => $this->integer(),
            ]);
        $this->addPrimaryKey('player_season_pk', 'player_to_season', ['player_id', 'season_id']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('player_to_season');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171210_211625_player_to_season cannot be reverted.\n";

        return false;
    }
    */
}
