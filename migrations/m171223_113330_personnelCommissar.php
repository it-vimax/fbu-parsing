<?php

use yii\db\Migration;

/**
 * Class m171223_113330_personnelCommissar
 */
class m171223_113330_personnelCommissar extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('personnel_commissars', [
            'id' => $this->primaryKey(),
            'first_name' => $this->string(),
            'last_name' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171223_113330_personnelCommissar cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171223_113330_personnelCommissar cannot be reverted.\n";

        return false;
    }
    */
}
