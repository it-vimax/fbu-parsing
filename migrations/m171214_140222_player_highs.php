<?php

use yii\db\Migration;

/**
 * Class m171214_140222_player_highs
 */
class m171214_140222_player_highs extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('player_highs', [
            'id' => $this->primaryKey(),
            'player_id'=> $this->integer()->notNull(),
            'season_id'=> $this->integer()->notNull(),
            'category_name'=> $this->string(),
            'category_number'=> $this->string(),
            'date'=> $this->dateTime(),
            'position'=> $this->integer(),
        ]);
        $this->alterColumn('player_highs', 'id', $this->integer(). ' NOT NULL AUTO_INCREMENT');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('player_highs');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171214_140222_player_highs cannot be reverted.\n";

        return false;
    }
    */
}
