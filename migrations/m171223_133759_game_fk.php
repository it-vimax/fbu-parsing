<?php

use yii\db\Migration;

/**
 * Class m171223_133759_game_fk
 */
class m171223_133759_game_fk extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-games-season_id',
            'games',
            'season_id',
            'seasons',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-games-command_1',
            'games',
            'command_1',
            'teams',
            'team_id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-games-command_2',
            'games',
            'command_2',
            'teams',
            'team_id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-games-commissar_id',
            'games',
            'commissar_id',
            'personnel_commissars',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171223_133759_game_fk cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171223_133759_game_fk cannot be reverted.\n";

        return false;
    }
    */
}
