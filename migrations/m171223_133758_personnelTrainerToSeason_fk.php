<?php

use yii\db\Migration;

/**
 * Class m171223_133758_personnelTrainerToSeason_fk
 */
class m171223_133758_personnelTrainerToSeason_fk extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-trainers_to_seasons-trainer_id',
            'trainers_to_seasons',
            'trainer_id',
            'personnel_trainers',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-trainers_to_seasons-season_id',
            'trainers_to_seasons',
            'season_id',
            'seasons',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171223_133758_personnelTrainerToSeason_fk cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171223_133758_personnelTrainerToSeason_fk cannot be reverted.\n";

        return false;
    }
    */
}
