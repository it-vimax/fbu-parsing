<?php

use yii\db\Migration;

/**
 * Class m171220_114838_player_highs_fk
 */
class m171220_114838_player_highs_fk extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-player_highs-player_id',
            'player_highs',
            'player_id',
            'players',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-player_highs-season_id',
            'player_highs',
            'season_id',
            'seasons',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171220_114838_player_highs_fk cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171220_114838_player_highs_fk cannot be reverted.\n";

        return false;
    }
    */
}
