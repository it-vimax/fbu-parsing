<?php

use yii\db\Migration;

/**
 * Class m171221_151907_game_protocol_commands
 */
class m171221_151907_game_protocol_commands extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('game_protocol_commands', [
            'id' => $this->primaryKey(),
            'game_id' => $this->integer(),
            'team_status' => $this->integer(),
            'player_id' => $this->integer(),
            'player_number' => $this->integer(),
            'time_in_game' => $this->string(),
            'two_point_kiddies' => $this->string(),// двохочковые броски
            'two_point_kiddies_ok' => $this->string(),// двохочковые броски попал
            'two_point_kiddies_percent' => $this->string(),// двохочковые броски процент
            'three_point_kiddies' => $this->string(),// трехочковые броски
            'three_point_kiddies_ok' => $this->string(),// трехочковые броски попал
            'three_point_kiddies_percent' => $this->string(),// трехочковые броски процент
            'for_game_attempt' => $this->string(), // с игры попытки
            'for_game_attempt_ok' => $this->string(), // с игры попытки получилось
            'for_game_attempt_percent' => $this->string(), // с игры попытки процент
            'one_point_kiddies' => $this->string(),// штрафные броски
            'one_point_kiddies_ok' => $this->string(),// штрафные броски попал
            'one_point_kiddies_percent' => $this->string(),// штрафные броски процент
            'picking_up_in_an_attack' => $this->string(), // подбирание в нападении
            'picking_up_in_defense' => $this->string(),// подбирание в защите
            'overall_picking_up' => $this->string(),// подбирань в общем
            'transmissions' => $this->string(),// передачи
            'personal_foul' => $this->string(),// персональные фолы
            'losses' => $this->string(), // потери
            'interception' => $this->string(), // перехват
            'block_shots_1' => $this->string(), // бок шоты
            'block_shots_2' => $this->string(),// бок шоты
            'efficiency' => $this->string(),// эфэктивность
            'plus_minus' => $this->string(), // */-
            'gained_points' => $this->string(),// полученые очки
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171221_151907_game_protocol_commands cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171221_151907_game_protocol_commands cannot be reverted.\n";

        return false;
    }
    */
}
