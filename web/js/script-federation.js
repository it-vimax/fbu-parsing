/**
 * Created by Maks on 07.12.2017.
 */
MBT.API.ApiId = "aaed6059969d182ff2a08e7d61458ce2d1a372c1";
MBT.API.History.enabled = true;
MBT.API.Lang = 'uk';
MBT.API.NewNav = 1;
MBT.API.stylesheetIndex = 1;
MBT.API.livePagesTypeIndex = 1;

MBT.Integration = {};
MBT.Integration.constant = {};
MBT.Integration.defaults = {};

MBT.Integration.leagueID = null;
MBT.Integration.seasonID = null;
MBT.Integration.defaults.leagueID = 8185;
MBT.Integration.constant.url = "http://fbu.ua/";
MBT.Integration.teamID = 313933;
MBT.Integration.playerID = 3845633;
MBT.Integration.arenaID = 20833;
MBT.Integration.gameID = 2168703;
MBT.Integration.mensLeagueId = 8185;
MBT.Integration.cupLeagueId = 8381;
MBT.Integration.womensTopLeagueId = 10843;

MBT.Integration.NTteamID = 491;
MBT.Integration.NTplayerID = 1658863;
MBT.Integration.NTgameID = 185691;
MBT.Integration.NTseasonID = 11311;
MBT.Integration.NTleagueID = 1651;

if(typeof(MBTExternalValues) === 'undefined')
    MBTExternalValues = {};

MBT.Integration.OverrideDefaultValues = function()
{
    for(key in MBTExternalValues)
    {
        MBT.Integration[key] = MBTExternalValues[key];
    }
}

if (MBT.API.URL.CurrentQueryString.get("season_id"))
    MBTExternalValues['seasonID'] = MBT.API.URL.CurrentQueryString.get("season_id");

if (MBT.API.URL.CurrentQueryString.get("game_id"))
    MBTExternalValues['gameID'] = MBT.API.URL.CurrentQueryString.get("game_id");

if (MBT.API.URL.CurrentQueryString.get("team_id"))
    MBTExternalValues['teamID'] = MBT.API.URL.CurrentQueryString.get("team_id");

if (MBT.API.URL.CurrentQueryString.get("player_id"))
    MBTExternalValues['playerID'] = MBT.API.URL.CurrentQueryString.get("player_id");

if (MBT.API.URL.CurrentQueryString.get("arena_id"))
    MBTExternalValues['arenaID'] = MBT.API.URL.CurrentQueryString.get("arena_id");

if (MBT.API.URL.CurrentQueryString.get("league_id"))
    MBTExternalValues['leagueID'] = MBT.API.URL.CurrentQueryString.get("league_id");

if ((typeof(MBTExternalValues['seasonID']) == "undefined") && (typeof(MBTExternalValues['leagueID']) == "undefined"))
    MBTExternalValues['leagueID'] = MBT.Integration.defaults.leagueID;

MBT.Integration.OverrideDefaultValues();

navigatePlayer = function (playerId, seasonId)
{
    return MBT.Integration.constant.url + "ua/widget/534.htm?player_id=" + playerId + "&season_id=" + seasonId;
}

navigateGame = function (gameId, seasonId)
{
    return MBT.Integration.constant.url + "ua/widget/533.htm?game_id=" + gameId +  "&season_id=" + seasonId;;
}


navigateTeam = function (teamId, seasonId)
{
    return MBT.Integration.constant.url + "ua/widget/1507.htm?team_id=" + teamId + "&season_id=" + seasonId;
}

handleSeasonClick = function (seasonId)
{
    var link = document.location.href;
    var new_link="";
    var search_result = link.indexOf('?');
    if( search_result == -1)
        new_link = document.location.href + "?season_id=" + seasonId;
    else
    {
        var parameters_url = link.substring(search_result+1);
        new_link = link.substring(0,search_result);
        var parameters = parameters_url.split("&");
        for(i=0; i<parameters.length; i++)
        {
            var value = 0;
            var param = parameters[i].split("=");
            if(param[0]=="season_id")
            {
                value = seasonId;
            }
            else
            {
                value = param[1];
            }
            value = param[1];
            if(i != 0)
                new_link += "&";
            else
                new_link += "?";
            new_link += param[0]+"="+value;

        }
    }

    if(new_link.indexOf('season_id') == -1){
        new_link += "&season_id="+seasonId;
    }

    document.location.href = new_link;
}

navigateLeague = function (leagueId) {
    return MBT.Integration.constant.url + "competitions-home/?league_id=" + leagueId;
}

navigateSeason = function(seasonId)
{
    return MBT.Integration.constant.url + 'competitions-home/?season_id=' + seasonId;
}

navigateSchedule = function(seasonId)
{
    if(MBT.Integration.leagueID == MBT.Integration.cupLeagueId)
        return MBT.Integration.constant.url + 'ua/tournamentCalendar/2.htm?season_id=' + seasonId;
    else
        return MBT.Integration.constant.url + 'ua/tournamentCalendar/1.htm?season_id=' + seasonId;

}

navigateLeaders = function(seasonId)
{
    return MBT.Integration.constant.url + 'ua/widget/447.htm?season_id=' + seasonId;
}

navigateStandings = function(leagueId, seasonId)
{
    return MBT.Integration.constant.url + 'ua/widget/537.htm?season_id=' + seasonId + '&league_id=' + leagueId;
}

navigateArena = function(arenaId)
{
    return MBT.Integration.constant.url + 'arena/?arena_id=' + arenaId;
}
navigateLiveGame = function(gameId)
{
    return "javascript:window.open('http://live.baskethotel.com/ubf/?&lang=1', '_blank', 'width=715,height=660,status=no,location=no,toolbar=no,menubar=no,scrollbars=no,resizable=yes,left=0,top=0');void(0);";
}

nt_navigateLeague = function(leagueID, seasonID)
{
    MBT.API.Application.set("nt_league_id", leagueID);
    MBT.API.Application.set("nt_season_id", seasonID);
    return MBT.Integration.constant.NTurl + '/nt-league/?nt_league_id=' + leagueID + '&nt_season_id=' + seasonID;
}

nt_navigatePlayer = function(playerId,seasonID)
{
    MBT.API.Application.set("player_id", playerId);
    MBT.API.Application.set("season_id", seasonID);
    return MBT.Integration.constant.NTurl + '/nt-player/?nt_season_id='+seasonID+'&nt_player_id='+playerId;
}

nt_navigateGame = function(gameId,seasonID)
{
    MBT.API.Application.set("game_id", gameId);
    MBT.API.Application.set("season_id", seasonID);
    return MBT.Integration.constant.NTurl + '/nt-game/?nt_game_id='+gameId+'&nt_season_id='+seasonID;
}

nt_navigateTeam = function(teamId,seasonID)
{
    MBT.API.Application.set("team_id", teamId);
    MBT.API.Application.set("season_id", seasonID);
    return MBT.Integration.constant.NTurl + '/nt-team?nt_team_id='+teamId+'&nt_season_id='+seasonID;
}

nt_gamesLink = MBT.Integration.constant.NTurl + '/nt-games/';
nt_teamsLink = MBT.Integration.constant.NTurl + '/nt-teams/';
nt_statsLink = MBT.Integration.constant.NTurl + '/nt-statistics-players/';

nt_navigateStats = function(gameId,seasonID)
{
    MBT.API.Application.set("game_id", gameId);
    MBT.API.Application.set("season_id", seasonID);
    return MBT.Integration.constant.NTurl + '/nt-statistics/?nt_game_id='+gameId+'&nt_season_id='+seasonID;
}
console.log(MBT);