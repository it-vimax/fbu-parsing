/**
 * Created by Maks on 07.12.2017.
 */
var request = new MBT.API.Widgets.RenderRequest();
var widget = new MBT.API.Widgets.Widget();
widget.setContainer('leaders');
widget.setWidgetId(MBT.API.Widgets.SEASON_LEADERS_SHORT_WIDGET);
widget.setParam('league_link_visible', 1);
widget.setParam('team_link_visible', 1);
widget.setParam('game_link_visible', 1);
widget.setParam('player_link_visible', 1);
widget.setParam('league_id', MBT.Integration.leagueID);
widget.setParam('season_id', MBT.Integration.seasonID);
//widget.setParam('stage_id', '178229');
widget.setParam('team_link_type', MBT.API.NAVIGATION_TYPE_CUSTOM);
widget.setParam('team_link_handler', 'navigateTeam');
widget.setParam('player_link_type', MBT.API.NAVIGATION_TYPE_CUSTOM);
widget.setParam('player_link_handler', 'navigatePlayer');
widget.setParam('layout', 'h');

request.addWidget(widget);
request.render();