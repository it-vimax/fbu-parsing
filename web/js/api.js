/**
 * Created by Maks on 07.12.2017.
 */
! function() {
    "use strict";

    function g(a) {
        a.fn.swiper = function(b) {
            var c;
            return a(this).each(function() {
                var a = new Swiper(this, b);
                c || (c = a)
            }), c
        }
    }
    var a, b = function(d, f) {
        function p(a) {
            return Math.floor(a)
        }

        function q() {
            var a = n.params.autoplay,
                b = n.slides.eq(n.activeIndex);
            b.attr("data-swiper-autoplay") && (a = b.attr("data-swiper-autoplay") || n.params.autoplay), n.autoplayTimeoutId = setTimeout(function() {
                n.params.loop ? (n.fixLoop(), n._slideNext(), n.emit("onAutoplay", n)) : n.isEnd ? f.autoplayStopOnLast ? n.stopAutoplay() : (n._slideTo(0), n.emit("onAutoplay", n)) : (n._slideNext(), n.emit("onAutoplay", n))
            }, a)
        }

        function r(b, c) {
            var d = a(b.target);
            if (!d.is(c))
                if ("string" == typeof c) d = d.parents(c);
                else if (c.nodeType) {
                    var e;
                    return d.parents().each(function(a, b) {
                        b === c && (e = c)
                    }), e ? c : void 0
                }
            if (0 !== d.length) return d[0]
        }

        function H(a, b) {
            b = b || {};
            var c = window.MutationObserver || window.WebkitMutationObserver,
                d = new c(function(a) {
                    a.forEach(function(a) {
                        n.onResize(!0), n.emit("onObserverUpdate", n, a)
                    })
                });
            d.observe(a, {
                attributes: "undefined" == typeof b.attributes || b.attributes,
                childList: "undefined" == typeof b.childList || b.childList,
                characterData: "undefined" == typeof b.characterData || b.characterData
            }), n.observers.push(d)
        }

        function I(a) {
            a.originalEvent && (a = a.originalEvent);
            var b = a.keyCode || a.charCode;
            if (!n.params.allowSwipeToNext && (n.isHorizontal() && 39 === b || !n.isHorizontal() && 40 === b)) return !1;
            if (!n.params.allowSwipeToPrev && (n.isHorizontal() && 37 === b || !n.isHorizontal() && 38 === b)) return !1;
            if (!(a.shiftKey || a.altKey || a.ctrlKey || a.metaKey || document.activeElement && document.activeElement.nodeName && ("input" === document.activeElement.nodeName.toLowerCase() || "textarea" === document.activeElement.nodeName.toLowerCase()))) {
                if (37 === b || 39 === b || 38 === b || 40 === b) {
                    var c = !1;
                    if (n.container.parents("." + n.params.slideClass).length > 0 && 0 === n.container.parents("." + n.params.slideActiveClass).length) return;
                    var d = {
                            left: window.pageXOffset,
                            top: window.pageYOffset
                        },
                        e = window.innerWidth,
                        f = window.innerHeight,
                        g = n.container.offset();
                    n.rtl && (g.left = g.left - n.container[0].scrollLeft);
                    for (var h = [
                        [g.left, g.top],
                        [g.left + n.width, g.top],
                        [g.left, g.top + n.height],
                        [g.left + n.width, g.top + n.height]
                    ], i = 0; i < h.length; i++) {
                        var j = h[i];
                        j[0] >= d.left && j[0] <= d.left + e && j[1] >= d.top && j[1] <= d.top + f && (c = !0)
                    }
                    if (!c) return
                }
                n.isHorizontal() ? (37 !== b && 39 !== b || (a.preventDefault ? a.preventDefault() : a.returnValue = !1), (39 === b && !n.rtl || 37 === b && n.rtl) && n.slideNext(), (37 === b && !n.rtl || 39 === b && n.rtl) && n.slidePrev()) : (38 !== b && 40 !== b || (a.preventDefault ? a.preventDefault() : a.returnValue = !1), 40 === b && n.slideNext(), 38 === b && n.slidePrev())
            }
        }

        function J() {
            var a = "onwheel",
                b = a in document;
            if (!b) {
                var c = document.createElement("div");
                c.setAttribute(a, "return;"), b = "function" == typeof c[a]
            }
            return !b && document.implementation && document.implementation.hasFeature && document.implementation.hasFeature("", "") !== !0 && (b = document.implementation.hasFeature("Events.wheel", "3.0")), b
        }

        function K(a) {
            a.originalEvent && (a = a.originalEvent);
            var b = 0,
                c = n.rtl ? -1 : 1,
                d = L(a);
            if (n.params.mousewheelForceToAxis)
                if (n.isHorizontal()) {
                    if (!(Math.abs(d.pixelX) > Math.abs(d.pixelY))) return;
                    b = d.pixelX * c
                } else {
                    if (!(Math.abs(d.pixelY) > Math.abs(d.pixelX))) return;
                    b = d.pixelY
                }
            else b = Math.abs(d.pixelX) > Math.abs(d.pixelY) ? -d.pixelX * c : -d.pixelY;
            if (0 !== b) {
                if (n.params.mousewheelInvert && (b = -b), n.params.freeMode) {
                    var e = n.getWrapperTranslate() + b * n.params.mousewheelSensitivity,
                        f = n.isBeginning,
                        g = n.isEnd;
                    if (e >= n.minTranslate() && (e = n.minTranslate()), e <= n.maxTranslate() && (e = n.maxTranslate()), n.setWrapperTransition(0), n.setWrapperTranslate(e), n.updateProgress(), n.updateActiveIndex(), (!f && n.isBeginning || !g && n.isEnd) && n.updateClasses(), n.params.freeModeSticky ? (clearTimeout(n.mousewheel.timeout), n.mousewheel.timeout = setTimeout(function() {
                            n.slideReset()
                        }, 300)) : n.params.lazyLoading && n.lazy && n.lazy.load(), n.emit("onScroll", n, a), n.params.autoplay && n.params.autoplayDisableOnInteraction && n.stopAutoplay(), 0 === e || e === n.maxTranslate()) return
                } else {
                    if ((new window.Date).getTime() - n.mousewheel.lastScrollTime > 60)
                        if (b < 0)
                            if (n.isEnd && !n.params.loop || n.animating) {
                                if (n.params.mousewheelReleaseOnEdges) return !0
                            } else n.slideNext(), n.emit("onScroll", n, a);
                        else if (n.isBeginning && !n.params.loop || n.animating) {
                            if (n.params.mousewheelReleaseOnEdges) return !0
                        } else n.slidePrev(), n.emit("onScroll", n, a);
                    n.mousewheel.lastScrollTime = (new window.Date).getTime()
                }
                return a.preventDefault ? a.preventDefault() : a.returnValue = !1, !1
            }
        }

        function L(a) {
            var b = 10,
                c = 40,
                d = 800,
                e = 0,
                f = 0,
                g = 0,
                h = 0;
            return "detail" in a && (f = a.detail), "wheelDelta" in a && (f = -a.wheelDelta / 120), "wheelDeltaY" in a && (f = -a.wheelDeltaY / 120), "wheelDeltaX" in a && (e = -a.wheelDeltaX / 120), "axis" in a && a.axis === a.HORIZONTAL_AXIS && (e = f, f = 0), g = e * b, h = f * b, "deltaY" in a && (h = a.deltaY), "deltaX" in a && (g = a.deltaX), (g || h) && a.deltaMode && (1 === a.deltaMode ? (g *= c, h *= c) : (g *= d, h *= d)), g && !e && (e = g < 1 ? -1 : 1), h && !f && (f = h < 1 ? -1 : 1), {
                spinX: e,
                spinY: f,
                pixelX: g,
                pixelY: h
            }
        }

        function M(b, c) {
            b = a(b);
            var d, e, f, g = n.rtl ? -1 : 1;
            d = b.attr("data-swiper-parallax") || "0", e = b.attr("data-swiper-parallax-x"), f = b.attr("data-swiper-parallax-y"), e || f ? (e = e || "0", f = f || "0") : n.isHorizontal() ? (e = d, f = "0") : (f = d, e = "0"), e = e.indexOf("%") >= 0 ? parseInt(e, 10) * c * g + "%" : e * c * g + "px", f = f.indexOf("%") >= 0 ? parseInt(f, 10) * c + "%" : f * c + "px", b.transform("translate3d(" + e + ", " + f + ",0px)")
        }

        function P(a) {
            return 0 !== a.indexOf("on") && (a = a[0] !== a[0].toUpperCase() ? "on" + a[0].toUpperCase() + a.substring(1) : "on" + a), a
        }
        if (!(this instanceof b)) return new b(d, f);
        var g = {
                direction: "horizontal",
                touchEventsTarget: "container",
                initialSlide: 0,
                speed: 300,
                autoplay: !1,
                autoplayDisableOnInteraction: !0,
                autoplayStopOnLast: !1,
                iOSEdgeSwipeDetection: !1,
                iOSEdgeSwipeThreshold: 20,
                freeMode: !1,
                freeModeMomentum: !0,
                freeModeMomentumRatio: 1,
                freeModeMomentumBounce: !0,
                freeModeMomentumBounceRatio: 1,
                freeModeMomentumVelocityRatio: 1,
                freeModeSticky: !1,
                freeModeMinimumVelocity: .02,
                autoHeight: !1,
                setWrapperSize: !1,
                virtualTranslate: !1,
                effect: "slide",
                coverflow: {
                    rotate: 50,
                    stretch: 0,
                    depth: 100,
                    modifier: 1,
                    slideShadows: !0
                },
                flip: {
                    slideShadows: !0,
                    limitRotation: !0
                },
                cube: {
                    slideShadows: !0,
                    shadow: !0,
                    shadowOffset: 20,
                    shadowScale: .94
                },
                fade: {
                    crossFade: !1
                },
                parallax: !1,
                zoom: !1,
                zoomMax: 3,
                zoomMin: 1,
                zoomToggle: !0,
                scrollbar: null,
                scrollbarHide: !0,
                scrollbarDraggable: !1,
                scrollbarSnapOnRelease: !1,
                keyboardControl: !1,
                mousewheelControl: !1,
                mousewheelReleaseOnEdges: !1,
                mousewheelInvert: !1,
                mousewheelForceToAxis: !1,
                mousewheelSensitivity: 1,
                mousewheelEventsTarged: "container",
                hashnav: !1,
                hashnavWatchState: !1,
                history: !1,
                replaceState: !1,
                breakpoints: void 0,
                spaceBetween: 0,
                slidesPerView: 1,
                slidesPerColumn: 1,
                slidesPerColumnFill: "column",
                slidesPerGroup: 1,
                centeredSlides: !1,
                slidesOffsetBefore: 0,
                slidesOffsetAfter: 0,
                roundLengths: !1,
                touchRatio: 1,
                touchAngle: 45,
                simulateTouch: !0,
                shortSwipes: !0,
                longSwipes: !0,
                longSwipesRatio: .5,
                longSwipesMs: 300,
                followFinger: !0,
                onlyExternal: !1,
                threshold: 0,
                touchMoveStopPropagation: !0,
                touchReleaseOnEdges: !1,
                uniqueNavElements: !0,
                pagination: null,
                paginationElement: "span",
                paginationClickable: !1,
                paginationHide: !1,
                paginationBulletRender: null,
                paginationProgressRender: null,
                paginationFractionRender: null,
                paginationCustomRender: null,
                paginationType: "bullets",
                resistance: !0,
                resistanceRatio: .85,
                nextButton: null,
                prevButton: null,
                watchSlidesProgress: !1,
                watchSlidesVisibility: !1,
                grabCursor: !1,
                preventClicks: !0,
                preventClicksPropagation: !0,
                slideToClickedSlide: !1,
                lazyLoading: !1,
                lazyLoadingInPrevNext: !1,
                lazyLoadingInPrevNextAmount: 1,
                lazyLoadingOnTransitionStart: !1,
                preloadImages: !0,
                updateOnImagesReady: !0,
                loop: !1,
                loopAdditionalSlides: 0,
                loopedSlides: null,
                control: void 0,
                controlInverse: !1,
                controlBy: "slide",
                normalizeSlideIndex: !0,
                allowSwipeToPrev: !0,
                allowSwipeToNext: !0,
                swipeHandler: null,
                noSwiping: !0,
                noSwipingClass: "mbt-v2-games-scroller-no-swiping",
                passiveListeners: !0,
                containerModifierClass: "mbt-v2-games-scroller-container-",
                slideClass: "mbt-v2-games-scroller-item",
                slideActiveClass: "mbt-v2-games-scroller-item-active",
                slideDuplicateActiveClass: "mbt-v2-games-scroller-item-duplicate-active",
                slideVisibleClass: "mbt-v2-games-scroller-item-visible",
                slideDuplicateClass: "mbt-v2-games-scroller-item-duplicate",
                slideNextClass: "mbt-v2-games-scroller-item-next",
                slideDuplicateNextClass: "mbt-v2-games-scroller-item-next-duplicate-next",
                slidePrevClass: "mbt-v2-games-scroller-item-prev",
                slideDuplicatePrevClass: "mbt-v2-games-scroller-item-next-duplicate-prev",
                wrapperClass: "mbt-v2-games-scroller-wrapper",
                bulletClass: "mbt-v2-games-scroller-pagination-bullet",
                bulletActiveClass: "mbt-v2-games-scroller-pagination-bullet-active",
                buttonDisabledClass: "mbt-v2-games-scroller-button-disabled",
                paginationCurrentClass: "mbt-v2-games-scroller-pagination-current",
                paginationTotalClass: "mbt-v2-games-scroller-pagination-total",
                paginationHiddenClass: "mbt-v2-games-scroller-pagination-hidden",
                paginationProgressbarClass: "mbt-v2-games-scroller-pagination-progressbar",
                paginationClickableClass: "mbt-v2-games-scroller-pagination-clickable",
                paginationModifierClass: "mbt-v2-games-scroller-pagination-",
                lazyLoadingClass: "mbt-v2-games-scroller-lazy",
                lazyStatusLoadingClass: "mbt-v2-games-scroller-lazy-loading",
                lazyStatusLoadedClass: "mbt-v2-games-scroller-lazy-loaded",
                lazyPreloaderClass: "mbt-v2-games-scroller-lazy-preloader",
                notificationClass: "mbt-v2-games-scroller-notification",
                preloaderClass: "mbt-v2-games-scroller-preloader",
                zoomContainerClass: "mbt-v2-games-scroller-zoom-container",
                observer: !1,
                observeParents: !1,
                a11y: !1,
                prevSlideMessage: "Previous slide",
                nextSlideMessage: "Next slide",
                firstSlideMessage: "This is the first slide",
                lastSlideMessage: "This is the last slide",
                paginationBulletMessage: "Go to slide {{index}}",
                runCallbacksOnInit: !0
            },
            h = f && f.virtualTranslate;
        f = f || {};
        var i = {};
        for (var j in f)
            if ("object" != typeof f[j] || null === f[j] || (f[j].nodeType || f[j] === window || f[j] === document || "undefined" != typeof c && f[j] instanceof c || "undefined" != typeof jQuery && f[j] instanceof jQuery)) i[j] = f[j];
            else {
                i[j] = {};
                for (var k in f[j]) i[j][k] = f[j][k]
            }
        for (var l in g)
            if ("undefined" == typeof f[l]) f[l] = g[l];
            else if ("object" == typeof f[l])
                for (var m in g[l]) "undefined" == typeof f[l][m] && (f[l][m] = g[l][m]);
        var n = this;
        if (n.params = f, n.originalParams = i, n.classNames = [], "undefined" != typeof a && "undefined" != typeof c && (a = c), ("undefined" != typeof a || (a = "undefined" == typeof c ? window.Dom7 || window.Zepto || window.jQuery : c)) && (n.$ = a, n.currentBreakpoint = void 0, n.getActiveBreakpoint = function() {
                if (!n.params.breakpoints) return !1;
                var c, a = !1,
                    b = [];
                for (c in n.params.breakpoints) n.params.breakpoints.hasOwnProperty(c) && b.push(c);
                b.sort(function(a, b) {
                    return parseInt(a, 10) > parseInt(b, 10)
                });
                for (var d = 0; d < b.length; d++) c = b[d], c >= window.innerWidth && !a && (a = c);
                return a || "max"
            }, n.setBreakpoint = function() {
                var a = n.getActiveBreakpoint();
                if (a && n.currentBreakpoint !== a) {
                    var b = a in n.params.breakpoints ? n.params.breakpoints[a] : n.originalParams,
                        c = n.params.loop && b.slidesPerView !== n.params.slidesPerView;
                    for (var d in b) n.params[d] = b[d];
                    n.currentBreakpoint = a, c && n.destroyLoop && n.reLoop(!0)
                }
            }, n.params.breakpoints && n.setBreakpoint(), n.container = a(d), 0 !== n.container.length)) {
            if (n.container.length > 1) {
                var o = [];
                return n.container.each(function() {
                    o.push(new Swiper(this, f))
                }), o
            }
            n.container[0].swiper = n, n.container.data("swiper", n), n.classNames.push(n.params.containerModifierClass + n.params.direction), n.params.freeMode && n.classNames.push(n.params.containerModifierClass + "free-mode"), n.support.flexbox || (n.classNames.push(n.params.containerModifierClass + "no-flexbox"), n.params.slidesPerColumn = 1), n.params.autoHeight && n.classNames.push(n.params.containerModifierClass + "autoheight"), (n.params.parallax || n.params.watchSlidesVisibility) && (n.params.watchSlidesProgress = !0), n.params.touchReleaseOnEdges && (n.params.resistanceRatio = 0), ["cube", "coverflow", "flip"].indexOf(n.params.effect) >= 0 && (n.support.transforms3d ? (n.params.watchSlidesProgress = !0, n.classNames.push(n.params.containerModifierClass + "3d")) : n.params.effect = "slide"), "slide" !== n.params.effect && n.classNames.push(n.params.containerModifierClass + n.params.effect), "cube" === n.params.effect && (n.params.resistanceRatio = 0, n.params.slidesPerView = 1, n.params.slidesPerColumn = 1, n.params.slidesPerGroup = 1, n.params.centeredSlides = !1, n.params.spaceBetween = 0, n.params.virtualTranslate = !0, n.params.setWrapperSize = !1), "fade" !== n.params.effect && "flip" !== n.params.effect || (n.params.slidesPerView = 1, n.params.slidesPerColumn = 1, n.params.slidesPerGroup = 1, n.params.watchSlidesProgress = !0, n.params.spaceBetween = 0, n.params.setWrapperSize = !1, "undefined" == typeof h && (n.params.virtualTranslate = !0)), n.params.grabCursor && n.support.touch && (n.params.grabCursor = !1), n.wrapper = n.container.children("." + n.params.wrapperClass), n.params.pagination && (n.paginationContainer = a(n.params.pagination), n.params.uniqueNavElements && "string" == typeof n.params.pagination && n.paginationContainer.length > 1 && 1 === n.container.find(n.params.pagination).length && (n.paginationContainer = n.container.find(n.params.pagination)), "bullets" === n.params.paginationType && n.params.paginationClickable ? n.paginationContainer.addClass(n.params.paginationModifierClass + "clickable") : n.params.paginationClickable = !1, n.paginationContainer.addClass(n.params.paginationModifierClass + n.params.paginationType)), (n.params.nextButton || n.params.prevButton) && (n.params.nextButton && (n.nextButton = a(n.params.nextButton), n.params.uniqueNavElements && "string" == typeof n.params.nextButton && n.nextButton.length > 1 && 1 === n.container.find(n.params.nextButton).length && (n.nextButton = n.container.find(n.params.nextButton))), n.params.prevButton && (n.prevButton = a(n.params.prevButton), n.params.uniqueNavElements && "string" == typeof n.params.prevButton && n.prevButton.length > 1 && 1 === n.container.find(n.params.prevButton).length && (n.prevButton = n.container.find(n.params.prevButton)))), n.isHorizontal = function() {
                return "horizontal" === n.params.direction
            }, n.rtl = n.isHorizontal() && ("rtl" === n.container[0].dir.toLowerCase() || "rtl" === n.container.css("direction")), n.rtl && n.classNames.push(n.params.containerModifierClass + "rtl"), n.rtl && (n.wrongRTL = "-webkit-box" === n.wrapper.css("display")), n.params.slidesPerColumn > 1 && n.classNames.push(n.params.containerModifierClass + "multirow"), n.device.android && n.classNames.push(n.params.containerModifierClass + "android"), n.container.addClass(n.classNames.join(" ")), n.translate = 0, n.progress = 0, n.velocity = 0, n.lockSwipeToNext = function() {
                n.params.allowSwipeToNext = !1, n.params.allowSwipeToPrev === !1 && n.params.grabCursor && n.unsetGrabCursor()
            }, n.lockSwipeToPrev = function() {
                n.params.allowSwipeToPrev = !1, n.params.allowSwipeToNext === !1 && n.params.grabCursor && n.unsetGrabCursor()
            }, n.lockSwipes = function() {
                n.params.allowSwipeToNext = n.params.allowSwipeToPrev = !1, n.params.grabCursor && n.unsetGrabCursor()
            }, n.unlockSwipeToNext = function() {
                n.params.allowSwipeToNext = !0, n.params.allowSwipeToPrev === !0 && n.params.grabCursor && n.setGrabCursor()
            }, n.unlockSwipeToPrev = function() {
                n.params.allowSwipeToPrev = !0, n.params.allowSwipeToNext === !0 && n.params.grabCursor && n.setGrabCursor()
            }, n.unlockSwipes = function() {
                n.params.allowSwipeToNext = n.params.allowSwipeToPrev = !0, n.params.grabCursor && n.setGrabCursor()
            }, n.setGrabCursor = function(a) {
                n.container[0].style.cursor = "move", n.container[0].style.cursor = a ? "-webkit-grabbing" : "-webkit-grab", n.container[0].style.cursor = a ? "-moz-grabbin" : "-moz-grab", n.container[0].style.cursor = a ? "grabbing" : "grab"
            }, n.unsetGrabCursor = function() {
                n.container[0].style.cursor = ""
            }, n.params.grabCursor && n.setGrabCursor(), n.imagesToLoad = [], n.imagesLoaded = 0, n.loadImage = function(a, b, c, d, e, f) {
                function h() {
                    f && f()
                }
                var g;
                a.complete && e ? h() : b ? (g = new window.Image, g.onload = h, g.onerror = h, d && (g.sizes = d), c && (g.srcset = c), b && (g.src = b)) : h()
            }, n.preloadImages = function() {
                function a() {
                    "undefined" != typeof n && null !== n && (void 0 !== n.imagesLoaded && n.imagesLoaded++, n.imagesLoaded === n.imagesToLoad.length && (n.params.updateOnImagesReady && n.update(), n.emit("onImagesReady", n)))
                }
                n.imagesToLoad = n.container.find("img");
                for (var b = 0; b < n.imagesToLoad.length; b++) n.loadImage(n.imagesToLoad[b], n.imagesToLoad[b].currentSrc || n.imagesToLoad[b].getAttribute("src"), n.imagesToLoad[b].srcset || n.imagesToLoad[b].getAttribute("srcset"), n.imagesToLoad[b].sizes || n.imagesToLoad[b].getAttribute("sizes"), !0, a)
            }, n.autoplayTimeoutId = void 0, n.autoplaying = !1, n.autoplayPaused = !1, n.startAutoplay = function() {
                return "undefined" == typeof n.autoplayTimeoutId && (!!n.params.autoplay && (!n.autoplaying && (n.autoplaying = !0, n.emit("onAutoplayStart", n), void q())))
            }, n.stopAutoplay = function(a) {
                n.autoplayTimeoutId && (n.autoplayTimeoutId && clearTimeout(n.autoplayTimeoutId), n.autoplaying = !1, n.autoplayTimeoutId = void 0, n.emit("onAutoplayStop", n))
            }, n.pauseAutoplay = function(a) {
                n.autoplayPaused || (n.autoplayTimeoutId && clearTimeout(n.autoplayTimeoutId), n.autoplayPaused = !0, 0 === a ? (n.autoplayPaused = !1, q()) : n.wrapper.transitionEnd(function() {
                    n && (n.autoplayPaused = !1, n.autoplaying ? q() : n.stopAutoplay())
                }))
            }, n.minTranslate = function() {
                return -n.snapGrid[0]
            }, n.maxTranslate = function() {
                return -n.snapGrid[n.snapGrid.length - 1]
            }, n.updateAutoHeight = function() {
                var a = [],
                    b = 0;
                if ("auto" !== n.params.slidesPerView && n.params.slidesPerView > 1)
                    for (e = 0; e < Math.ceil(n.params.slidesPerView); e++) {
                        var c = n.activeIndex + e;
                        if (c > n.slides.length) break;
                        a.push(n.slides.eq(c)[0])
                    } else a.push(n.slides.eq(n.activeIndex)[0]);
                for (e = 0; e < a.length; e++)
                    if ("undefined" != typeof a[e]) {
                        var d = a[e].offsetHeight;
                        b = d > b ? d : b
                    }
                b && n.wrapper.css("height", b + "px")
            }, n.updateContainerSize = function() {
                var a, b;
                a = "undefined" != typeof n.params.width ? n.params.width : n.container[0].clientWidth, b = "undefined" != typeof n.params.height ? n.params.height : n.container[0].clientHeight, 0 === a && n.isHorizontal() || 0 === b && !n.isHorizontal() || (a = a - parseInt(n.container.css("padding-left"), 10) - parseInt(n.container.css("padding-right"), 10), b = b - parseInt(n.container.css("padding-top"), 10) - parseInt(n.container.css("padding-bottom"), 10), n.width = a, n.height = b, n.size = n.isHorizontal() ? n.width : n.height)
            }, n.updateSlidesSize = function() {
                n.slides = n.wrapper.children("." + n.params.slideClass), n.snapGrid = [], n.slidesGrid = [], n.slidesSizesGrid = [];
                var c, a = n.params.spaceBetween,
                    b = -n.params.slidesOffsetBefore,
                    d = 0,
                    e = 0;
                if ("undefined" != typeof n.size) {
                    "string" == typeof a && a.indexOf("%") >= 0 && (a = parseFloat(a.replace("%", "")) / 100 * n.size), n.virtualSize = -a, n.rtl ? n.slides.css({
                        marginLeft: "",
                        marginTop: ""
                    }) : n.slides.css({
                        marginRight: "",
                        marginBottom: ""
                    });
                    var f;
                    n.params.slidesPerColumn > 1 && (f = Math.floor(n.slides.length / n.params.slidesPerColumn) === n.slides.length / n.params.slidesPerColumn ? n.slides.length : Math.ceil(n.slides.length / n.params.slidesPerColumn) * n.params.slidesPerColumn, "auto" !== n.params.slidesPerView && "row" === n.params.slidesPerColumnFill && (f = Math.max(f, n.params.slidesPerView * n.params.slidesPerColumn)));
                    var g, h = n.params.slidesPerColumn,
                        i = f / h,
                        j = i - (n.params.slidesPerColumn * i - n.slides.length);
                    for (c = 0; c < n.slides.length; c++) {
                        g = 0;
                        var k = n.slides.eq(c);
                        if (n.params.slidesPerColumn > 1) {
                            var l, m, o;
                            "column" === n.params.slidesPerColumnFill ? (m = Math.floor(c / h), o = c - m * h, (m > j || m === j && o === h - 1) && ++o >= h && (o = 0, m++), l = m + o * f / h, k.css({
                                "-webkit-box-ordinal-group": l,
                                "-moz-box-ordinal-group": l,
                                "-ms-flex-order": l,
                                "-webkit-order": l,
                                order: l
                            })) : (o = Math.floor(c / i), m = c - o * i), k.css("margin-" + (n.isHorizontal() ? "top" : "left"), 0 !== o && n.params.spaceBetween && n.params.spaceBetween + "px").attr("data-swiper-column", m).attr("data-swiper-row", o)
                        }
                        "none" !== k.css("display") && ("auto" === n.params.slidesPerView ? (g = n.isHorizontal() ? k.outerWidth(!0) : k.outerHeight(!0), n.params.roundLengths && (g = p(g))) : (g = (n.size - (n.params.slidesPerView - 1) * a) / n.params.slidesPerView, n.params.roundLengths && (g = p(g)), n.isHorizontal() ? n.slides[c].style.width = g + "px" : n.slides[c].style.height = g + "px"), n.slides[c].swiperSlideSize = g, n.slidesSizesGrid.push(g), n.params.centeredSlides ? (b = b + g / 2 + d / 2 + a, 0 === c && (b = b - n.size / 2 - a), Math.abs(b) < .001 && (b = 0), e % n.params.slidesPerGroup === 0 && n.snapGrid.push(b), n.slidesGrid.push(b)) : (e % n.params.slidesPerGroup === 0 && n.snapGrid.push(b), n.slidesGrid.push(b), b = b + g + a), n.virtualSize += g + a, d = g, e++)
                    }
                    n.virtualSize = Math.max(n.virtualSize, n.size) + n.params.slidesOffsetAfter;
                    var q;
                    if (n.rtl && n.wrongRTL && ("slide" === n.params.effect || "coverflow" === n.params.effect) && n.wrapper.css({
                            width: n.virtualSize + n.params.spaceBetween + "px"
                        }), n.support.flexbox && !n.params.setWrapperSize || (n.isHorizontal() ? n.wrapper.css({
                            width: n.virtualSize + n.params.spaceBetween + "px"
                        }) : n.wrapper.css({
                            height: n.virtualSize + n.params.spaceBetween + "px"
                        })), n.params.slidesPerColumn > 1 && (n.virtualSize = (g + n.params.spaceBetween) * f, n.virtualSize = Math.ceil(n.virtualSize / n.params.slidesPerColumn) - n.params.spaceBetween, n.isHorizontal() ? n.wrapper.css({
                            width: n.virtualSize + n.params.spaceBetween + "px"
                        }) : n.wrapper.css({
                            height: n.virtualSize + n.params.spaceBetween + "px"
                        }), n.params.centeredSlides)) {
                        for (q = [], c = 0; c < n.snapGrid.length; c++) n.snapGrid[c] < n.virtualSize + n.snapGrid[0] && q.push(n.snapGrid[c]);
                        n.snapGrid = q
                    }
                    if (!n.params.centeredSlides) {
                        for (q = [], c = 0; c < n.snapGrid.length; c++) n.snapGrid[c] <= n.virtualSize - n.size && q.push(n.snapGrid[c]);
                        n.snapGrid = q, Math.floor(n.virtualSize - n.size) - Math.floor(n.snapGrid[n.snapGrid.length - 1]) > 1 && n.snapGrid.push(n.virtualSize - n.size)
                    }
                    0 === n.snapGrid.length && (n.snapGrid = [0]), 0 !== n.params.spaceBetween && (n.isHorizontal() ? n.rtl ? n.slides.css({
                        marginLeft: a + "px"
                    }) : n.slides.css({
                        marginRight: a + "px"
                    }) : n.slides.css({
                        marginBottom: a + "px"
                    })), n.params.watchSlidesProgress && n.updateSlidesOffset()
                }
            }, n.updateSlidesOffset = function() {
                for (var a = 0; a < n.slides.length; a++) n.slides[a].swiperSlideOffset = n.isHorizontal() ? n.slides[a].offsetLeft : n.slides[a].offsetTop
            }, n.updateSlidesProgress = function(a) {
                if ("undefined" == typeof a && (a = n.translate || 0), 0 !== n.slides.length) {
                    "undefined" == typeof n.slides[0].swiperSlideOffset && n.updateSlidesOffset();
                    var b = -a;
                    n.rtl && (b = a), n.slides.removeClass(n.params.slideVisibleClass);
                    for (var c = 0; c < n.slides.length; c++) {
                        var d = n.slides[c],
                            e = (b + (n.params.centeredSlides ? n.minTranslate() : 0) - d.swiperSlideOffset) / (d.swiperSlideSize + n.params.spaceBetween);
                        if (n.params.watchSlidesVisibility) {
                            var f = -(b - d.swiperSlideOffset),
                                g = f + n.slidesSizesGrid[c],
                                h = f >= 0 && f < n.size || g > 0 && g <= n.size || f <= 0 && g >= n.size;
                            h && n.slides.eq(c).addClass(n.params.slideVisibleClass)
                        }
                        d.progress = n.rtl ? -e : e
                    }
                }
            }, n.updateProgress = function(a) {
                "undefined" == typeof a && (a = n.translate || 0);
                var b = n.maxTranslate() - n.minTranslate(),
                    c = n.isBeginning,
                    d = n.isEnd;
                0 === b ? (n.progress = 0, n.isBeginning = n.isEnd = !0) : (n.progress = (a - n.minTranslate()) / b, n.isBeginning = n.progress <= 0, n.isEnd = n.progress >= 1), n.isBeginning && !c && n.emit("onReachBeginning", n), n.isEnd && !d && n.emit("onReachEnd", n), n.params.watchSlidesProgress && n.updateSlidesProgress(a), n.emit("onProgress", n, n.progress)
            }, n.updateActiveIndex = function() {
                var b, c, d, a = n.rtl ? n.translate : -n.translate;
                for (c = 0; c < n.slidesGrid.length; c++) "undefined" != typeof n.slidesGrid[c + 1] ? a >= n.slidesGrid[c] && a < n.slidesGrid[c + 1] - (n.slidesGrid[c + 1] - n.slidesGrid[c]) / 2 ? b = c : a >= n.slidesGrid[c] && a < n.slidesGrid[c + 1] && (b = c + 1) : a >= n.slidesGrid[c] && (b = c);
                n.params.normalizeSlideIndex && (b < 0 || "undefined" == typeof b) && (b = 0), d = Math.floor(b / n.params.slidesPerGroup), d >= n.snapGrid.length && (d = n.snapGrid.length - 1), b !== n.activeIndex && (n.snapIndex = d, n.previousIndex = n.activeIndex, n.activeIndex = b, n.updateClasses(), n.updateRealIndex())
            }, n.updateRealIndex = function() {
                n.realIndex = n.slides.eq(n.activeIndex).attr("data-swiper-slide-index") || n.activeIndex
            }, n.updateClasses = function() {
                n.slides.removeClass(n.params.slideActiveClass + " " + n.params.slideNextClass + " " + n.params.slidePrevClass + " " + n.params.slideDuplicateActiveClass + " " + n.params.slideDuplicateNextClass + " " + n.params.slideDuplicatePrevClass);
                var b = n.slides.eq(n.activeIndex);
                b.addClass(n.params.slideActiveClass), f.loop && (b.hasClass(n.params.slideDuplicateClass) ? n.wrapper.children("." + n.params.slideClass + ":not(." + n.params.slideDuplicateClass + ')[data-swiper-slide-index="' + n.realIndex + '"]').addClass(n.params.slideDuplicateActiveClass) : n.wrapper.children("." + n.params.slideClass + "." + n.params.slideDuplicateClass + '[data-swiper-slide-index="' + n.realIndex + '"]').addClass(n.params.slideDuplicateActiveClass));
                var c = b.next("." + n.params.slideClass).addClass(n.params.slideNextClass);
                n.params.loop && 0 === c.length && (c = n.slides.eq(0), c.addClass(n.params.slideNextClass));
                var d = b.prev("." + n.params.slideClass).addClass(n.params.slidePrevClass);
                if (n.params.loop && 0 === d.length && (d = n.slides.eq(-1), d.addClass(n.params.slidePrevClass)), f.loop && (c.hasClass(n.params.slideDuplicateClass) ? n.wrapper.children("." + n.params.slideClass + ":not(." + n.params.slideDuplicateClass + ')[data-swiper-slide-index="' + c.attr("data-swiper-slide-index") + '"]').addClass(n.params.slideDuplicateNextClass) : n.wrapper.children("." + n.params.slideClass + "." + n.params.slideDuplicateClass + '[data-swiper-slide-index="' + c.attr("data-swiper-slide-index") + '"]').addClass(n.params.slideDuplicateNextClass), d.hasClass(n.params.slideDuplicateClass) ? n.wrapper.children("." + n.params.slideClass + ":not(." + n.params.slideDuplicateClass + ')[data-swiper-slide-index="' + d.attr("data-swiper-slide-index") + '"]').addClass(n.params.slideDuplicatePrevClass) : n.wrapper.children("." + n.params.slideClass + "." + n.params.slideDuplicateClass + '[data-swiper-slide-index="' + d.attr("data-swiper-slide-index") + '"]').addClass(n.params.slideDuplicatePrevClass)), n.paginationContainer && n.paginationContainer.length > 0) {
                    var e, g = n.params.loop ? Math.ceil((n.slides.length - 2 * n.loopedSlides) / n.params.slidesPerGroup) : n.snapGrid.length;
                    if (n.params.loop ? (e = Math.ceil((n.activeIndex - n.loopedSlides) / n.params.slidesPerGroup), e > n.slides.length - 1 - 2 * n.loopedSlides && (e -= n.slides.length - 2 * n.loopedSlides), e > g - 1 && (e -= g), e < 0 && "bullets" !== n.params.paginationType && (e = g + e)) : e = "undefined" != typeof n.snapIndex ? n.snapIndex : n.activeIndex || 0, "bullets" === n.params.paginationType && n.bullets && n.bullets.length > 0 && (n.bullets.removeClass(n.params.bulletActiveClass), n.paginationContainer.length > 1 ? n.bullets.each(function() {
                            a(this).index() === e && a(this).addClass(n.params.bulletActiveClass)
                        }) : n.bullets.eq(e).addClass(n.params.bulletActiveClass)), "fraction" === n.params.paginationType && (n.paginationContainer.find("." + n.params.paginationCurrentClass).text(e + 1), n.paginationContainer.find("." + n.params.paginationTotalClass).text(g)), "progress" === n.params.paginationType) {
                        var h = (e + 1) / g,
                            i = h,
                            j = 1;
                        n.isHorizontal() || (j = h, i = 1), n.paginationContainer.find("." + n.params.paginationProgressbarClass).transform("translate3d(0,0,0) scaleX(" + i + ") scaleY(" + j + ")").transition(n.params.speed)
                    }
                    "custom" === n.params.paginationType && n.params.paginationCustomRender && (n.paginationContainer.html(n.params.paginationCustomRender(n, e + 1, g)), n.emit("onPaginationRendered", n, n.paginationContainer[0]))
                }
                n.params.loop || (n.params.prevButton && n.prevButton && n.prevButton.length > 0 && (n.isBeginning ? (n.prevButton.addClass(n.params.buttonDisabledClass), n.params.a11y && n.a11y && n.a11y.disable(n.prevButton)) : (n.prevButton.removeClass(n.params.buttonDisabledClass), n.params.a11y && n.a11y && n.a11y.enable(n.prevButton))), n.params.nextButton && n.nextButton && n.nextButton.length > 0 && (n.isEnd ? (n.nextButton.addClass(n.params.buttonDisabledClass), n.params.a11y && n.a11y && n.a11y.disable(n.nextButton)) : (n.nextButton.removeClass(n.params.buttonDisabledClass), n.params.a11y && n.a11y && n.a11y.enable(n.nextButton))))
            }, n.updatePagination = function() {
                if (n.params.pagination && n.paginationContainer && n.paginationContainer.length > 0) {
                    var a = "";
                    if ("bullets" === n.params.paginationType) {
                        for (var b = n.params.loop ? Math.ceil((n.slides.length - 2 * n.loopedSlides) / n.params.slidesPerGroup) : n.snapGrid.length, c = 0; c < b; c++) a += n.params.paginationBulletRender ? n.params.paginationBulletRender(n, c, n.params.bulletClass) : "<" + n.params.paginationElement + ' class="' + n.params.bulletClass + '"></' + n.params.paginationElement + ">";
                        n.paginationContainer.html(a), n.bullets = n.paginationContainer.find("." + n.params.bulletClass), n.params.paginationClickable && n.params.a11y && n.a11y && n.a11y.initPagination()
                    }
                    "fraction" === n.params.paginationType && (a = n.params.paginationFractionRender ? n.params.paginationFractionRender(n, n.params.paginationCurrentClass, n.params.paginationTotalClass) : '<span class="' + n.params.paginationCurrentClass + '"></span> / <span class="' + n.params.paginationTotalClass + '"></span>', n.paginationContainer.html(a)), "progress" === n.params.paginationType && (a = n.params.paginationProgressRender ? n.params.paginationProgressRender(n, n.params.paginationProgressbarClass) : '<span class="' + n.params.paginationProgressbarClass + '"></span>', n.paginationContainer.html(a)), "custom" !== n.params.paginationType && n.emit("onPaginationRendered", n, n.paginationContainer[0])
                }
            }, n.update = function(a) {
                function b() {
                    n.rtl ? -n.translate : n.translate;
                    d = Math.min(Math.max(n.translate, n.maxTranslate()), n.minTranslate()), n.setWrapperTranslate(d), n.updateActiveIndex(), n.updateClasses()
                }
                if (n.updateContainerSize(), n.updateSlidesSize(), n.updateProgress(), n.updatePagination(), n.updateClasses(), n.params.scrollbar && n.scrollbar && n.scrollbar.set(), a) {
                    var c, d;
                    n.controller && n.controller.spline && (n.controller.spline = void 0), n.params.freeMode ? (b(), n.params.autoHeight && n.updateAutoHeight()) : (c = ("auto" === n.params.slidesPerView || n.params.slidesPerView > 1) && n.isEnd && !n.params.centeredSlides ? n.slideTo(n.slides.length - 1, 0, !1, !0) : n.slideTo(n.activeIndex, 0, !1, !0), c || b())
                } else n.params.autoHeight && n.updateAutoHeight()
            }, n.onResize = function(a) {
                n.params.breakpoints && n.setBreakpoint();
                var b = n.params.allowSwipeToPrev,
                    c = n.params.allowSwipeToNext;
                n.params.allowSwipeToPrev = n.params.allowSwipeToNext = !0, n.updateContainerSize(), n.updateSlidesSize(), ("auto" === n.params.slidesPerView || n.params.freeMode || a) && n.updatePagination(), n.params.scrollbar && n.scrollbar && n.scrollbar.set(), n.controller && n.controller.spline && (n.controller.spline = void 0);
                var d = !1;
                if (n.params.freeMode) {
                    var e = Math.min(Math.max(n.translate, n.maxTranslate()), n.minTranslate());
                    n.setWrapperTranslate(e), n.updateActiveIndex(), n.updateClasses(), n.params.autoHeight && n.updateAutoHeight()
                } else n.updateClasses(), d = ("auto" === n.params.slidesPerView || n.params.slidesPerView > 1) && n.isEnd && !n.params.centeredSlides ? n.slideTo(n.slides.length - 1, 0, !1, !0) : n.slideTo(n.activeIndex, 0, !1, !0);
                n.params.lazyLoading && !d && n.lazy && n.lazy.load(), n.params.allowSwipeToPrev = b, n.params.allowSwipeToNext = c
            }, n.touchEventsDesktop = {
                start: "mousedown",
                move: "mousemove",
                end: "mouseup"
            }, window.navigator.pointerEnabled ? n.touchEventsDesktop = {
                start: "pointerdown",
                move: "pointermove",
                end: "pointerup"
            } : window.navigator.msPointerEnabled && (n.touchEventsDesktop = {
                    start: "MSPointerDown",
                    move: "MSPointerMove",
                    end: "MSPointerUp"
                }), n.touchEvents = {
                start: n.support.touch || !n.params.simulateTouch ? "touchstart" : n.touchEventsDesktop.start,
                move: n.support.touch || !n.params.simulateTouch ? "touchmove" : n.touchEventsDesktop.move,
                end: n.support.touch || !n.params.simulateTouch ? "touchend" : n.touchEventsDesktop.end
            }, (window.navigator.pointerEnabled || window.navigator.msPointerEnabled) && ("container" === n.params.touchEventsTarget ? n.container : n.wrapper).addClass("swiper-wp8-" + n.params.direction), n.initEvents = function(a) {
                var b = a ? "off" : "on",
                    c = a ? "removeEventListener" : "addEventListener",
                    d = "container" === n.params.touchEventsTarget ? n.container[0] : n.wrapper[0],
                    e = n.support.touch ? d : document,
                    g = !!n.params.nested;
                if (n.browser.ie) d[c](n.touchEvents.start, n.onTouchStart, !1), e[c](n.touchEvents.move, n.onTouchMove, g), e[c](n.touchEvents.end, n.onTouchEnd, !1);
                else {
                    if (n.support.touch) {
                        var h = !("touchstart" !== n.touchEvents.start || !n.support.passiveListener || !n.params.passiveListeners) && {
                                passive: !0,
                                capture: !1
                            };
                        d[c](n.touchEvents.start, n.onTouchStart, h), d[c](n.touchEvents.move, n.onTouchMove, g), d[c](n.touchEvents.end, n.onTouchEnd, h)
                    }(f.simulateTouch && !n.device.ios && !n.device.android || f.simulateTouch && !n.support.touch && n.device.ios) && (d[c]("mousedown", n.onTouchStart, !1), document[c]("mousemove", n.onTouchMove, g), document[c]("mouseup", n.onTouchEnd, !1))
                }
                window[c]("resize", n.onResize), n.params.nextButton && n.nextButton && n.nextButton.length > 0 && (n.nextButton[b]("click", n.onClickNext), n.params.a11y && n.a11y && n.nextButton[b]("keydown", n.a11y.onEnterKey)), n.params.prevButton && n.prevButton && n.prevButton.length > 0 && (n.prevButton[b]("click", n.onClickPrev), n.params.a11y && n.a11y && n.prevButton[b]("keydown", n.a11y.onEnterKey)), n.params.pagination && n.params.paginationClickable && (n.paginationContainer[b]("click", "." + n.params.bulletClass, n.onClickIndex), n.params.a11y && n.a11y && n.paginationContainer[b]("keydown", "." + n.params.bulletClass, n.a11y.onEnterKey)), (n.params.preventClicks || n.params.preventClicksPropagation) && d[c]("click", n.preventClicks, !0)
            }, n.attachEvents = function() {
                n.initEvents();
            }, n.detachEvents = function() {
                n.initEvents(!0)
            }, n.allowClick = !0, n.preventClicks = function(a) {
                n.allowClick || (n.params.preventClicks && a.preventDefault(), n.params.preventClicksPropagation && n.animating && (a.stopPropagation(), a.stopImmediatePropagation()))
            }, n.onClickNext = function(a) {
                a.preventDefault(), n.isEnd && !n.params.loop || n.slideNext()
            }, n.onClickPrev = function(a) {
                a.preventDefault(), n.isBeginning && !n.params.loop || n.slidePrev()
            }, n.onClickIndex = function(b) {
                b.preventDefault();
                var c = a(this).index() * n.params.slidesPerGroup;
                n.params.loop && (c += n.loopedSlides), n.slideTo(c)
            }, n.updateClickedSlide = function(b) {
                var c = r(b, "." + n.params.slideClass),
                    d = !1;
                if (c)
                    for (var e = 0; e < n.slides.length; e++) n.slides[e] === c && (d = !0);
                if (!c || !d) return n.clickedSlide = void 0, void(n.clickedIndex = void 0);
                if (n.clickedSlide = c, n.clickedIndex = a(c).index(), n.params.slideToClickedSlide && void 0 !== n.clickedIndex && n.clickedIndex !== n.activeIndex) {
                    var g, f = n.clickedIndex;
                    if (n.params.loop) {
                        if (n.animating) return;
                        g = a(n.clickedSlide).attr("data-swiper-slide-index"), n.params.centeredSlides ? f < n.loopedSlides - n.params.slidesPerView / 2 || f > n.slides.length - n.loopedSlides + n.params.slidesPerView / 2 ? (n.fixLoop(), f = n.wrapper.children("." + n.params.slideClass + '[data-swiper-slide-index="' + g + '"]:not(.' + n.params.slideDuplicateClass + ")").eq(0).index(), setTimeout(function() {
                            n.slideTo(f)
                        }, 0)) : n.slideTo(f) : f > n.slides.length - n.params.slidesPerView ? (n.fixLoop(), f = n.wrapper.children("." + n.params.slideClass + '[data-swiper-slide-index="' + g + '"]:not(.' + n.params.slideDuplicateClass + ")").eq(0).index(), setTimeout(function() {
                            n.slideTo(f)
                        }, 0)) : n.slideTo(f)
                    } else n.slideTo(f)
                }
            };
            var s, t, u, v, w, x, y, z, C, E, A = "input, select, textarea, button, video",
                B = Date.now(),
                D = [];
            n.animating = !1, n.touches = {
                startX: 0,
                startY: 0,
                currentX: 0,
                currentY: 0,
                diff: 0
            };
            var F, G;
            n.onTouchStart = function(b) {
                if (b.originalEvent && (b = b.originalEvent), F = "touchstart" === b.type, F || !("which" in b) || 3 !== b.which) {
                    if (n.params.noSwiping && r(b, "." + n.params.noSwipingClass)) return void(n.allowClick = !0);
                    if (!n.params.swipeHandler || r(b, n.params.swipeHandler)) {
                        var c = n.touches.currentX = "touchstart" === b.type ? b.targetTouches[0].pageX : b.pageX,
                            d = n.touches.currentY = "touchstart" === b.type ? b.targetTouches[0].pageY : b.pageY;
                        if (!(n.device.ios && n.params.iOSEdgeSwipeDetection && c <= n.params.iOSEdgeSwipeThreshold)) {
                            if (s = !0, t = !1, u = !0, w = void 0, G = void 0, n.touches.startX = c, n.touches.startY = d, v = Date.now(), n.allowClick = !0, n.updateContainerSize(), n.swipeDirection = void 0, n.params.threshold > 0 && (z = !1), "touchstart" !== b.type) {
                                var e = !0;
                                a(b.target).is(A) && (e = !1), document.activeElement && a(document.activeElement).is(A) && document.activeElement.blur(), e && b.preventDefault()
                            }
                            n.emit("onTouchStart", n, b)
                        }
                    }
                }
            }, n.onTouchMove = function(b) {
                if (b.originalEvent && (b = b.originalEvent), !F || "mousemove" !== b.type) {
                    if (b.preventedByNestedSwiper) return n.touches.startX = "touchmove" === b.type ? b.targetTouches[0].pageX : b.pageX, void(n.touches.startY = "touchmove" === b.type ? b.targetTouches[0].pageY : b.pageY);
                    if (n.params.onlyExternal) return n.allowClick = !1, void(s && (n.touches.startX = n.touches.currentX = "touchmove" === b.type ? b.targetTouches[0].pageX : b.pageX, n.touches.startY = n.touches.currentY = "touchmove" === b.type ? b.targetTouches[0].pageY : b.pageY, v = Date.now()));
                    if (F && n.params.touchReleaseOnEdges && !n.params.loop)
                        if (n.isHorizontal()) {
                            if (n.touches.currentX < n.touches.startX && n.translate <= n.maxTranslate() || n.touches.currentX > n.touches.startX && n.translate >= n.minTranslate()) return
                        } else if (n.touches.currentY < n.touches.startY && n.translate <= n.maxTranslate() || n.touches.currentY > n.touches.startY && n.translate >= n.minTranslate()) return;
                    if (F && document.activeElement && b.target === document.activeElement && a(b.target).is(A)) return t = !0, void(n.allowClick = !1);
                    if (u && n.emit("onTouchMove", n, b), !(b.targetTouches && b.targetTouches.length > 1)) {
                        if (n.touches.currentX = "touchmove" === b.type ? b.targetTouches[0].pageX : b.pageX, n.touches.currentY = "touchmove" === b.type ? b.targetTouches[0].pageY : b.pageY, "undefined" == typeof w) {
                            var c;
                            n.isHorizontal() && n.touches.currentY === n.touches.startY || !n.isHorizontal() && n.touches.currentX !== n.touches.startX ? w = !1 : (c = 180 * Math.atan2(Math.abs(n.touches.currentY - n.touches.startY), Math.abs(n.touches.currentX - n.touches.startX)) / Math.PI, w = n.isHorizontal() ? c > n.params.touchAngle : 90 - c > n.params.touchAngle)
                        }
                        if (w && n.emit("onTouchMoveOpposite", n, b), "undefined" == typeof G && n.browser.ieTouch && (n.touches.currentX === n.touches.startX && n.touches.currentY === n.touches.startY || (G = !0)), s) {
                            if (w) return void(s = !1);
                            if (G || !n.browser.ieTouch) {
                                n.allowClick = !1, n.emit("onSliderMove", n, b), b.preventDefault(), n.params.touchMoveStopPropagation && !n.params.nested && b.stopPropagation(), t || (f.loop && n.fixLoop(), y = n.getWrapperTranslate(), n.setWrapperTransition(0), n.animating && n.wrapper.trigger("webkitTransitionEnd transitionend oTransitionEnd MSTransitionEnd msTransitionEnd"), n.params.autoplay && n.autoplaying && (n.params.autoplayDisableOnInteraction ? n.stopAutoplay() : n.pauseAutoplay()), E = !1, !n.params.grabCursor || n.params.allowSwipeToNext !== !0 && n.params.allowSwipeToPrev !== !0 || n.setGrabCursor(!0)), t = !0;
                                var d = n.touches.diff = n.isHorizontal() ? n.touches.currentX - n.touches.startX : n.touches.currentY - n.touches.startY;
                                d *= n.params.touchRatio, n.rtl && (d = -d), n.swipeDirection = d > 0 ? "prev" : "next", x = d + y;
                                var e = !0;
                                if (d > 0 && x > n.minTranslate() ? (e = !1, n.params.resistance && (x = n.minTranslate() - 1 + Math.pow(-n.minTranslate() + y + d, n.params.resistanceRatio))) : d < 0 && x < n.maxTranslate() && (e = !1, n.params.resistance && (x = n.maxTranslate() + 1 - Math.pow(n.maxTranslate() - y - d, n.params.resistanceRatio))), e && (b.preventedByNestedSwiper = !0), !n.params.allowSwipeToNext && "next" === n.swipeDirection && x < y && (x = y), !n.params.allowSwipeToPrev && "prev" === n.swipeDirection && x > y && (x = y), n.params.threshold > 0) {
                                    if (!(Math.abs(d) > n.params.threshold || z)) return void(x = y);
                                    if (!z) return z = !0, n.touches.startX = n.touches.currentX, n.touches.startY = n.touches.currentY, x = y, void(n.touches.diff = n.isHorizontal() ? n.touches.currentX - n.touches.startX : n.touches.currentY - n.touches.startY)
                                }
                                n.params.followFinger && ((n.params.freeMode || n.params.watchSlidesProgress) && n.updateActiveIndex(), n.params.freeMode && (0 === D.length && D.push({
                                    position: n.touches[n.isHorizontal() ? "startX" : "startY"],
                                    time: v
                                }), D.push({
                                    position: n.touches[n.isHorizontal() ? "currentX" : "currentY"],
                                    time: (new window.Date).getTime()
                                })), n.updateProgress(x), n.setWrapperTranslate(x))
                            }
                        }
                    }
                }
            }, n.onTouchEnd = function(b) {
                if (b.originalEvent && (b = b.originalEvent), u && n.emit("onTouchEnd", n, b), u = !1, s) {
                    n.params.grabCursor && t && s && (n.params.allowSwipeToNext === !0 || n.params.allowSwipeToPrev === !0) && n.setGrabCursor(!1);
                    var c = Date.now(),
                        d = c - v;
                    if (n.allowClick && (n.updateClickedSlide(b), n.emit("onTap", n, b), d < 300 && c - B > 300 && (C && clearTimeout(C), C = setTimeout(function() {
                            n && (n.params.paginationHide && n.paginationContainer.length > 0 && !a(b.target).hasClass(n.params.bulletClass) && n.paginationContainer.toggleClass(n.params.paginationHiddenClass), n.emit("onClick", n, b))
                        }, 300)), d < 300 && c - B < 300 && (C && clearTimeout(C), n.emit("onDoubleTap", n, b))), B = Date.now(), setTimeout(function() {
                            n && (n.allowClick = !0)
                        }, 0), !s || !t || !n.swipeDirection || 0 === n.touches.diff || x === y) return void(s = t = !1);
                    s = t = !1;
                    var e;
                    if (e = n.params.followFinger ? n.rtl ? n.translate : -n.translate : -x, n.params.freeMode) {
                        if (e < -n.minTranslate()) return void n.slideTo(n.activeIndex);
                        if (e > -n.maxTranslate()) return void(n.slides.length < n.snapGrid.length ? n.slideTo(n.snapGrid.length - 1) : n.slideTo(n.slides.length - 1));
                        if (n.params.freeModeMomentum) {
                            if (D.length > 1) {
                                var f = D.pop(),
                                    g = D.pop(),
                                    h = f.position - g.position,
                                    i = f.time - g.time;
                                n.velocity = h / i, n.velocity = n.velocity / 2, Math.abs(n.velocity) < n.params.freeModeMinimumVelocity && (n.velocity = 0), (i > 150 || (new window.Date).getTime() - f.time > 300) && (n.velocity = 0)
                            } else n.velocity = 0;
                            n.velocity = n.velocity * n.params.freeModeMomentumVelocityRatio, D.length = 0;
                            var j = 1e3 * n.params.freeModeMomentumRatio,
                                k = n.velocity * j,
                                l = n.translate + k;
                            n.rtl && (l = -l);
                            var o, m = !1,
                                p = 20 * Math.abs(n.velocity) * n.params.freeModeMomentumBounceRatio;
                            if (l < n.maxTranslate()) n.params.freeModeMomentumBounce ? (l + n.maxTranslate() < -p && (l = n.maxTranslate() - p), o = n.maxTranslate(), m = !0, E = !0) : l = n.maxTranslate();
                            else if (l > n.minTranslate()) n.params.freeModeMomentumBounce ? (l - n.minTranslate() > p && (l = n.minTranslate() + p), o = n.minTranslate(), m = !0, E = !0) : l = n.minTranslate();
                            else if (n.params.freeModeSticky) {
                                var r, q = 0;
                                for (q = 0; q < n.snapGrid.length; q += 1)
                                    if (n.snapGrid[q] > -l) {
                                        r = q;
                                        break
                                    }
                                l = Math.abs(n.snapGrid[r] - l) < Math.abs(n.snapGrid[r - 1] - l) || "next" === n.swipeDirection ? n.snapGrid[r] : n.snapGrid[r - 1], n.rtl || (l = -l)
                            }
                            if (0 !== n.velocity) j = n.rtl ? Math.abs((-l - n.translate) / n.velocity) : Math.abs((l - n.translate) / n.velocity);
                            else if (n.params.freeModeSticky) return void n.slideReset();
                            n.params.freeModeMomentumBounce && m ? (n.updateProgress(o), n.setWrapperTransition(j), n.setWrapperTranslate(l), n.onTransitionStart(), n.animating = !0, n.wrapper.transitionEnd(function() {
                                n && E && (n.emit("onMomentumBounce", n), n.setWrapperTransition(n.params.speed), n.setWrapperTranslate(o), n.wrapper.transitionEnd(function() {
                                    n && n.onTransitionEnd()
                                }))
                            })) : n.velocity ? (n.updateProgress(l), n.setWrapperTransition(j), n.setWrapperTranslate(l), n.onTransitionStart(), n.animating || (n.animating = !0, n.wrapper.transitionEnd(function() {
                                n && n.onTransitionEnd()
                            }))) : n.updateProgress(l), n.updateActiveIndex()
                        }
                        return void((!n.params.freeModeMomentum || d >= n.params.longSwipesMs) && (n.updateProgress(), n.updateActiveIndex()))
                    }
                    var w, z = 0,
                        A = n.slidesSizesGrid[0];
                    for (w = 0; w < n.slidesGrid.length; w += n.params.slidesPerGroup) "undefined" != typeof n.slidesGrid[w + n.params.slidesPerGroup] ? e >= n.slidesGrid[w] && e < n.slidesGrid[w + n.params.slidesPerGroup] && (z = w, A = n.slidesGrid[w + n.params.slidesPerGroup] - n.slidesGrid[w]) : e >= n.slidesGrid[w] && (z = w, A = n.slidesGrid[n.slidesGrid.length - 1] - n.slidesGrid[n.slidesGrid.length - 2]);
                    var F = (e - n.slidesGrid[z]) / A;
                    if (d > n.params.longSwipesMs) {
                        if (!n.params.longSwipes) return void n.slideTo(n.activeIndex);
                        "next" === n.swipeDirection && (F >= n.params.longSwipesRatio ? n.slideTo(z + n.params.slidesPerGroup) : n.slideTo(z)), "prev" === n.swipeDirection && (F > 1 - n.params.longSwipesRatio ? n.slideTo(z + n.params.slidesPerGroup) : n.slideTo(z))
                    } else {
                        if (!n.params.shortSwipes) return void n.slideTo(n.activeIndex);
                        "next" === n.swipeDirection && n.slideTo(z + n.params.slidesPerGroup), "prev" === n.swipeDirection && n.slideTo(z)
                    }
                }
            }, n._slideTo = function(a, b) {
                return n.slideTo(a, b, !0, !0)
            }, n.slideTo = function(a, b, c, d) {
                "undefined" == typeof c && (c = !0), "undefined" == typeof a && (a = 0), a < 0 && (a = 0), n.snapIndex = Math.floor(a / n.params.slidesPerGroup), n.snapIndex >= n.snapGrid.length && (n.snapIndex = n.snapGrid.length - 1);
                var e = -n.snapGrid[n.snapIndex];
                if (n.params.autoplay && n.autoplaying && (d || !n.params.autoplayDisableOnInteraction ? n.pauseAutoplay(b) : n.stopAutoplay()), n.updateProgress(e), n.params.normalizeSlideIndex)
                    for (var f = 0; f < n.slidesGrid.length; f++) - Math.floor(100 * e) >= Math.floor(100 * n.slidesGrid[f]) && (a = f);
                return !(!n.params.allowSwipeToNext && e < n.translate && e < n.minTranslate()) && (!(!n.params.allowSwipeToPrev && e > n.translate && e > n.maxTranslate() && (n.activeIndex || 0) !== a) && ("undefined" == typeof b && (b = n.params.speed), n.previousIndex = n.activeIndex || 0, n.activeIndex = a, n.updateRealIndex(), n.rtl && -e === n.translate || !n.rtl && e === n.translate ? (n.params.autoHeight && n.updateAutoHeight(), n.updateClasses(), "slide" !== n.params.effect && n.setWrapperTranslate(e), !1) : (n.updateClasses(), n.onTransitionStart(c), 0 === b || n.browser.lteIE9 ? (n.setWrapperTranslate(e), n.setWrapperTransition(0), n.onTransitionEnd(c)) : (n.setWrapperTranslate(e), n.setWrapperTransition(b), n.animating || (n.animating = !0, n.wrapper.transitionEnd(function() {
                        n && n.onTransitionEnd(c)
                    }))), !0)))
            }, n.onTransitionStart = function(a) {
                "undefined" == typeof a && (a = !0), n.params.autoHeight && n.updateAutoHeight(), n.lazy && n.lazy.onTransitionStart(), a && (n.emit("onTransitionStart", n), n.activeIndex !== n.previousIndex && (n.emit("onSlideChangeStart", n), n.activeIndex > n.previousIndex ? n.emit("onSlideNextStart", n) : n.emit("onSlidePrevStart", n)))
            }, n.onTransitionEnd = function(a) {
                n.animating = !1, n.setWrapperTransition(0), "undefined" == typeof a && (a = !0), n.lazy && n.lazy.onTransitionEnd(), a && (n.emit("onTransitionEnd", n), n.activeIndex !== n.previousIndex && (n.emit("onSlideChangeEnd", n), n.activeIndex > n.previousIndex ? n.emit("onSlideNextEnd", n) : n.emit("onSlidePrevEnd", n))), n.params.history && n.history && n.history.setHistory(n.params.history, n.activeIndex), n.params.hashnav && n.hashnav && n.hashnav.setHash()
            }, n.slideNext = function(a, b, c) {
                if (n.params.loop) {
                    if (n.animating) return !1;
                    n.fixLoop();
                    n.container[0].clientLeft;
                    return n.slideTo(n.activeIndex + n.params.slidesPerGroup, b, a, c)
                }
                return n.slideTo(n.activeIndex + n.params.slidesPerGroup, b, a, c)
            }, n._slideNext = function(a) {
                return n.slideNext(!0, a, !0)
            }, n.slidePrev = function(a, b, c) {
                if (n.params.loop) {
                    if (n.animating) return !1;
                    n.fixLoop();
                    n.container[0].clientLeft;
                    return n.slideTo(n.activeIndex - 1, b, a, c)
                }
                return n.slideTo(n.activeIndex - 1, b, a, c)
            }, n._slidePrev = function(a) {
                return n.slidePrev(!0, a, !0)
            }, n.slideReset = function(a, b, c) {
                return n.slideTo(n.activeIndex, b, a)
            }, n.disableTouchControl = function() {
                return n.params.onlyExternal = !0, !0
            }, n.enableTouchControl = function() {
                return n.params.onlyExternal = !1, !0
            }, n.setWrapperTransition = function(a, b) {
                n.wrapper.transition(a), "slide" !== n.params.effect && n.effects[n.params.effect] && n.effects[n.params.effect].setTransition(a), n.params.parallax && n.parallax && n.parallax.setTransition(a), n.params.scrollbar && n.scrollbar && n.scrollbar.setTransition(a), n.params.control && n.controller && n.controller.setTransition(a, b), n.emit("onSetTransition", n, a)
            }, n.setWrapperTranslate = function(a, b, c) {
                var d = 0,
                    e = 0,
                    f = 0;
                n.isHorizontal() ? d = n.rtl ? -a : a : e = a, n.params.roundLengths && (d = p(d), e = p(e)), n.params.virtualTranslate || (n.support.transforms3d ? n.wrapper.transform("translate3d(" + d + "px, " + e + "px, " + f + "px)") : n.wrapper.transform("translate(" + d + "px, " + e + "px)")), n.translate = n.isHorizontal() ? d : e;
                var g, h = n.maxTranslate() - n.minTranslate();
                g = 0 === h ? 0 : (a - n.minTranslate()) / h, g !== n.progress && n.updateProgress(a), b && n.updateActiveIndex(), "slide" !== n.params.effect && n.effects[n.params.effect] && n.effects[n.params.effect].setTranslate(n.translate), n.params.parallax && n.parallax && n.parallax.setTranslate(n.translate), n.params.scrollbar && n.scrollbar && n.scrollbar.setTranslate(n.translate), n.params.control && n.controller && n.controller.setTranslate(n.translate, c), n.emit("onSetTranslate", n, n.translate)
            }, n.getTranslate = function(a, b) {
                var c, d, e, f;
                return "undefined" == typeof b && (b = "x"), n.params.virtualTranslate ? n.rtl ? -n.translate : n.translate : (e = window.getComputedStyle(a, null), window.WebKitCSSMatrix ? (d = e.transform || e.webkitTransform, d.split(",").length > 6 && (d = d.split(", ").map(function(a) {
                    return a.replace(",", ".")
                }).join(", ")), f = new window.WebKitCSSMatrix("none" === d ? "" : d)) : (f = e.MozTransform || e.OTransform || e.MsTransform || e.msTransform || e.transform || e.getPropertyValue("transform").replace("translate(", "matrix(1, 0, 0, 1,"), c = f.toString().split(",")), "x" === b && (d = window.WebKitCSSMatrix ? f.m41 : 16 === c.length ? parseFloat(c[12]) : parseFloat(c[4])), "y" === b && (d = window.WebKitCSSMatrix ? f.m42 : 16 === c.length ? parseFloat(c[13]) : parseFloat(c[5])), n.rtl && d && (d = -d), d || 0)
            }, n.getWrapperTranslate = function(a) {
                return "undefined" == typeof a && (a = n.isHorizontal() ? "x" : "y"), n.getTranslate(n.wrapper[0], a)
            }, n.observers = [], n.initObservers = function() {
                if (n.params.observeParents)
                    for (var a = n.container.parents(), b = 0; b < a.length; b++) H(a[b]);
                H(n.container[0], {
                    childList: !1
                }), H(n.wrapper[0], {
                    attributes: !1
                })
            }, n.disconnectObservers = function() {
                for (var a = 0; a < n.observers.length; a++) n.observers[a].disconnect();
                n.observers = []
            }, n.createLoop = function() {
                n.wrapper.children("." + n.params.slideClass + "." + n.params.slideDuplicateClass).remove();
                var b = n.wrapper.children("." + n.params.slideClass);
                "auto" !== n.params.slidesPerView || n.params.loopedSlides || (n.params.loopedSlides = b.length), n.loopedSlides = parseInt(n.params.loopedSlides || n.params.slidesPerView, 10), n.loopedSlides = n.loopedSlides + n.params.loopAdditionalSlides, n.loopedSlides > b.length && (n.loopedSlides = b.length);
                var e, c = [],
                    d = [];
                for (b.each(function(e, f) {
                    var g = a(this);
                    e < n.loopedSlides && d.push(f), e < b.length && e >= b.length - n.loopedSlides && c.push(f), g.attr("data-swiper-slide-index", e)
                }), e = 0; e < d.length; e++) n.wrapper.append(a(d[e].cloneNode(!0)).addClass(n.params.slideDuplicateClass));
                for (e = c.length - 1; e >= 0; e--) n.wrapper.prepend(a(c[e].cloneNode(!0)).addClass(n.params.slideDuplicateClass))
            }, n.destroyLoop = function() {
                n.wrapper.children("." + n.params.slideClass + "." + n.params.slideDuplicateClass).remove(), n.slides.removeAttr("data-swiper-slide-index")
            }, n.reLoop = function(a) {
                var b = n.activeIndex - n.loopedSlides;
                n.destroyLoop(), n.createLoop(), n.updateSlidesSize(), a && n.slideTo(b + n.loopedSlides, 0, !1)
            }, n.fixLoop = function() {
                var a;
                n.activeIndex < n.loopedSlides ? (a = n.slides.length - 3 * n.loopedSlides + n.activeIndex, a += n.loopedSlides, n.slideTo(a, 0, !1, !0)) : ("auto" === n.params.slidesPerView && n.activeIndex >= 2 * n.loopedSlides || n.activeIndex > n.slides.length - 2 * n.params.slidesPerView) && (a = -n.slides.length + n.activeIndex + n.loopedSlides, a += n.loopedSlides, n.slideTo(a, 0, !1, !0))
            }, n.appendSlide = function(a) {
                if (n.params.loop && n.destroyLoop(), "object" == typeof a && a.length)
                    for (var b = 0; b < a.length; b++) a[b] && n.wrapper.append(a[b]);
                else n.wrapper.append(a);
                n.params.loop && n.createLoop(), n.params.observer && n.support.observer || n.update(!0)
            }, n.prependSlide = function(a) {
                n.params.loop && n.destroyLoop();
                var b = n.activeIndex + 1;
                if ("object" == typeof a && a.length) {
                    for (var c = 0; c < a.length; c++) a[c] && n.wrapper.prepend(a[c]);
                    b = n.activeIndex + a.length
                } else n.wrapper.prepend(a);
                n.params.loop && n.createLoop(), n.params.observer && n.support.observer || n.update(!0), n.slideTo(b, 0, !1)
            }, n.removeSlide = function(a) {
                n.params.loop && (n.destroyLoop(), n.slides = n.wrapper.children("." + n.params.slideClass));
                var c, b = n.activeIndex;
                if ("object" == typeof a && a.length) {
                    for (var d = 0; d < a.length; d++) c = a[d], n.slides[c] && n.slides.eq(c).remove(), c < b && b--;
                    b = Math.max(b, 0)
                } else c = a, n.slides[c] && n.slides.eq(c).remove(), c < b && b--, b = Math.max(b, 0);
                n.params.loop && n.createLoop(), n.params.observer && n.support.observer || n.update(!0), n.params.loop ? n.slideTo(b + n.loopedSlides, 0, !1) : n.slideTo(b, 0, !1)
            }, n.removeAllSlides = function() {
                for (var a = [], b = 0; b < n.slides.length; b++) a.push(b);
                n.removeSlide(a)
            }, n.effects = {
                fade: {
                    setTranslate: function() {
                        for (var a = 0; a < n.slides.length; a++) {
                            var b = n.slides.eq(a),
                                c = b[0].swiperSlideOffset,
                                d = -c;
                            n.params.virtualTranslate || (d -= n.translate);
                            var e = 0;
                            n.isHorizontal() || (e = d, d = 0);
                            var f = n.params.fade.crossFade ? Math.max(1 - Math.abs(b[0].progress), 0) : 1 + Math.min(Math.max(b[0].progress, -1), 0);
                            b.css({
                                opacity: f
                            }).transform("translate3d(" + d + "px, " + e + "px, 0px)")
                        }
                    },
                    setTransition: function(a) {
                        if (n.slides.transition(a), n.params.virtualTranslate && 0 !== a) {
                            var b = !1;
                            n.slides.transitionEnd(function() {
                                if (!b && n) {
                                    b = !0, n.animating = !1;
                                    for (var a = ["webkitTransitionEnd", "transitionend", "oTransitionEnd", "MSTransitionEnd", "msTransitionEnd"], c = 0; c < a.length; c++) n.wrapper.trigger(a[c])
                                }
                            })
                        }
                    }
                },
                flip: {
                    setTranslate: function() {
                        for (var b = 0; b < n.slides.length; b++) {
                            var c = n.slides.eq(b),
                                d = c[0].progress;
                            n.params.flip.limitRotation && (d = Math.max(Math.min(c[0].progress, 1), -1));
                            var e = c[0].swiperSlideOffset,
                                f = -180 * d,
                                g = f,
                                h = 0,
                                i = -e,
                                j = 0;
                            if (n.isHorizontal() ? n.rtl && (g = -g) : (j = i, i = 0, h = -g, g = 0), c[0].style.zIndex = -Math.abs(Math.round(d)) + n.slides.length, n.params.flip.slideShadows) {
                                var k = n.isHorizontal() ? c.find(".swiper-slide-shadow-left") : c.find(".swiper-slide-shadow-top"),
                                    l = n.isHorizontal() ? c.find(".swiper-slide-shadow-right") : c.find(".swiper-slide-shadow-bottom");
                                0 === k.length && (k = a('<div class="swiper-slide-shadow-' + (n.isHorizontal() ? "left" : "top") + '"></div>'), c.append(k)), 0 === l.length && (l = a('<div class="swiper-slide-shadow-' + (n.isHorizontal() ? "right" : "bottom") + '"></div>'), c.append(l)), k.length && (k[0].style.opacity = Math.max(-d, 0)), l.length && (l[0].style.opacity = Math.max(d, 0))
                            }
                            c.transform("translate3d(" + i + "px, " + j + "px, 0px) rotateX(" + h + "deg) rotateY(" + g + "deg)")
                        }
                    },
                    setTransition: function(b) {
                        if (n.slides.transition(b).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(b), n.params.virtualTranslate && 0 !== b) {
                            var c = !1;
                            n.slides.eq(n.activeIndex).transitionEnd(function() {
                                if (!c && n && a(this).hasClass(n.params.slideActiveClass)) {
                                    c = !0, n.animating = !1;
                                    for (var b = ["webkitTransitionEnd", "transitionend", "oTransitionEnd", "MSTransitionEnd", "msTransitionEnd"], d = 0; d < b.length; d++) n.wrapper.trigger(b[d])
                                }
                            })
                        }
                    }
                },
                cube: {
                    setTranslate: function() {
                        var c, b = 0;
                        n.params.cube.shadow && (n.isHorizontal() ? (c = n.wrapper.find(".swiper-cube-shadow"), 0 === c.length && (c = a('<div class="swiper-cube-shadow"></div>'), n.wrapper.append(c)), c.css({
                            height: n.width + "px"
                        })) : (c = n.container.find(".swiper-cube-shadow"), 0 === c.length && (c = a('<div class="swiper-cube-shadow"></div>'), n.container.append(c))));
                        for (var d = 0; d < n.slides.length; d++) {
                            var e = n.slides.eq(d),
                                f = 90 * d,
                                g = Math.floor(f / 360);
                            n.rtl && (f = -f, g = Math.floor(-f / 360));
                            var h = Math.max(Math.min(e[0].progress, 1), -1),
                                i = 0,
                                j = 0,
                                k = 0;
                            d % 4 === 0 ? (i = 4 * -g * n.size, k = 0) : (d - 1) % 4 === 0 ? (i = 0, k = 4 * -g * n.size) : (d - 2) % 4 === 0 ? (i = n.size + 4 * g * n.size, k = n.size) : (d - 3) % 4 === 0 && (i = -n.size, k = 3 * n.size + 4 * n.size * g), n.rtl && (i = -i), n.isHorizontal() || (j = i, i = 0);
                            var l = "rotateX(" + (n.isHorizontal() ? 0 : -f) + "deg) rotateY(" + (n.isHorizontal() ? f : 0) + "deg) translate3d(" + i + "px, " + j + "px, " + k + "px)";
                            if (h <= 1 && h > -1 && (b = 90 * d + 90 * h, n.rtl && (b = 90 * -d - 90 * h)), e.transform(l), n.params.cube.slideShadows) {
                                var m = n.isHorizontal() ? e.find(".swiper-slide-shadow-left") : e.find(".swiper-slide-shadow-top"),
                                    o = n.isHorizontal() ? e.find(".swiper-slide-shadow-right") : e.find(".swiper-slide-shadow-bottom");
                                0 === m.length && (m = a('<div class="swiper-slide-shadow-' + (n.isHorizontal() ? "left" : "top") + '"></div>'), e.append(m)), 0 === o.length && (o = a('<div class="swiper-slide-shadow-' + (n.isHorizontal() ? "right" : "bottom") + '"></div>'), e.append(o)), m.length && (m[0].style.opacity = Math.max(-h, 0)), o.length && (o[0].style.opacity = Math.max(h, 0))
                            }
                        }
                        if (n.wrapper.css({
                                "-webkit-transform-origin": "50% 50% -" + n.size / 2 + "px",
                                "-moz-transform-origin": "50% 50% -" + n.size / 2 + "px",
                                "-ms-transform-origin": "50% 50% -" + n.size / 2 + "px",
                                "transform-origin": "50% 50% -" + n.size / 2 + "px"
                            }), n.params.cube.shadow)
                            if (n.isHorizontal()) c.transform("translate3d(0px, " + (n.width / 2 + n.params.cube.shadowOffset) + "px, " + -n.width / 2 + "px) rotateX(90deg) rotateZ(0deg) scale(" + n.params.cube.shadowScale + ")");
                            else {
                                var p = Math.abs(b) - 90 * Math.floor(Math.abs(b) / 90),
                                    q = 1.5 - (Math.sin(2 * p * Math.PI / 360) / 2 + Math.cos(2 * p * Math.PI / 360) / 2),
                                    r = n.params.cube.shadowScale,
                                    s = n.params.cube.shadowScale / q,
                                    t = n.params.cube.shadowOffset;
                                c.transform("scale3d(" + r + ", 1, " + s + ") translate3d(0px, " + (n.height / 2 + t) + "px, " + -n.height / 2 / s + "px) rotateX(-90deg)")
                            }
                        var u = n.isSafari || n.isUiWebView ? -n.size / 2 : 0;
                        n.wrapper.transform("translate3d(0px,0," + u + "px) rotateX(" + (n.isHorizontal() ? 0 : b) + "deg) rotateY(" + (n.isHorizontal() ? -b : 0) + "deg)")
                    },
                    setTransition: function(a) {
                        n.slides.transition(a).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(a), n.params.cube.shadow && !n.isHorizontal() && n.container.find(".swiper-cube-shadow").transition(a)
                    }
                },
                coverflow: {
                    setTranslate: function() {
                        for (var b = n.translate, c = n.isHorizontal() ? -b + n.width / 2 : -b + n.height / 2, d = n.isHorizontal() ? n.params.coverflow.rotate : -n.params.coverflow.rotate, e = n.params.coverflow.depth, f = 0, g = n.slides.length; f < g; f++) {
                            var h = n.slides.eq(f),
                                i = n.slidesSizesGrid[f],
                                j = h[0].swiperSlideOffset,
                                k = (c - j - i / 2) / i * n.params.coverflow.modifier,
                                l = n.isHorizontal() ? d * k : 0,
                                m = n.isHorizontal() ? 0 : d * k,
                                o = -e * Math.abs(k),
                                p = n.isHorizontal() ? 0 : n.params.coverflow.stretch * k,
                                q = n.isHorizontal() ? n.params.coverflow.stretch * k : 0;
                            Math.abs(q) < .001 && (q = 0), Math.abs(p) < .001 && (p = 0), Math.abs(o) < .001 && (o = 0), Math.abs(l) < .001 && (l = 0), Math.abs(m) < .001 && (m = 0);
                            var r = "translate3d(" + q + "px," + p + "px," + o + "px)  rotateX(" + m + "deg) rotateY(" + l + "deg)";
                            if (h.transform(r), h[0].style.zIndex = -Math.abs(Math.round(k)) + 1, n.params.coverflow.slideShadows) {
                                var s = n.isHorizontal() ? h.find(".swiper-slide-shadow-left") : h.find(".swiper-slide-shadow-top"),
                                    t = n.isHorizontal() ? h.find(".swiper-slide-shadow-right") : h.find(".swiper-slide-shadow-bottom");
                                0 === s.length && (s = a('<div class="swiper-slide-shadow-' + (n.isHorizontal() ? "left" : "top") + '"></div>'), h.append(s)), 0 === t.length && (t = a('<div class="swiper-slide-shadow-' + (n.isHorizontal() ? "right" : "bottom") + '"></div>'), h.append(t)), s.length && (s[0].style.opacity = k > 0 ? k : 0), t.length && (t[0].style.opacity = -k > 0 ? -k : 0)
                            }
                        }
                        if (n.browser.ie) {
                            var u = n.wrapper[0].style;
                            u.perspectiveOrigin = c + "px 50%"
                        }
                    },
                    setTransition: function(a) {
                        n.slides.transition(a).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(a)
                    }
                }
            }, n.lazy = {
                initialImageLoaded: !1,
                loadImageInSlide: function(b, c) {
                    if ("undefined" != typeof b && ("undefined" == typeof c && (c = !0), 0 !== n.slides.length)) {
                        var d = n.slides.eq(b),
                            e = d.find("." + n.params.lazyLoadingClass + ":not(." + n.params.lazyStatusLoadedClass + "):not(." + n.params.lazyStatusLoadingClass + ")");
                        !d.hasClass(n.params.lazyLoadingClass) || d.hasClass(n.params.lazyStatusLoadedClass) || d.hasClass(n.params.lazyStatusLoadingClass) || (e = e.add(d[0])), 0 !== e.length && e.each(function() {
                            var b = a(this);
                            b.addClass(n.params.lazyStatusLoadingClass);
                            var e = b.attr("data-background"),
                                f = b.attr("data-src"),
                                g = b.attr("data-srcset"),
                                h = b.attr("data-sizes");
                            n.loadImage(b[0], f || e, g, h, !1, function() {
                                if (e ? (b.css("background-image", 'url("' + e + '")'), b.removeAttr("data-background")) : (g && (b.attr("srcset", g), b.removeAttr("data-srcset")), h && (b.attr("sizes", h), b.removeAttr("data-sizes")), f && (b.attr("src", f), b.removeAttr("data-src"))), b.addClass(n.params.lazyStatusLoadedClass).removeClass(n.params.lazyStatusLoadingClass), d.find("." + n.params.lazyPreloaderClass + ", ." + n.params.preloaderClass).remove(), n.params.loop && c) {
                                    var a = d.attr("data-swiper-slide-index");
                                    if (d.hasClass(n.params.slideDuplicateClass)) {
                                        var i = n.wrapper.children('[data-swiper-slide-index="' + a + '"]:not(.' + n.params.slideDuplicateClass + ")");
                                        n.lazy.loadImageInSlide(i.index(), !1)
                                    } else {
                                        var j = n.wrapper.children("." + n.params.slideDuplicateClass + '[data-swiper-slide-index="' + a + '"]');
                                        n.lazy.loadImageInSlide(j.index(), !1)
                                    }
                                }
                                n.emit("onLazyImageReady", n, d[0], b[0])
                            }), n.emit("onLazyImageLoad", n, d[0], b[0])
                        })
                    }
                },
                load: function() {
                    var b, c = n.params.slidesPerView;
                    if ("auto" === c && (c = 0), n.lazy.initialImageLoaded || (n.lazy.initialImageLoaded = !0), n.params.watchSlidesVisibility) n.wrapper.children("." + n.params.slideVisibleClass).each(function() {
                        n.lazy.loadImageInSlide(a(this).index())
                    });
                    else if (c > 1)
                        for (b = n.activeIndex; b < n.activeIndex + c; b++) n.slides[b] && n.lazy.loadImageInSlide(b);
                    else n.lazy.loadImageInSlide(n.activeIndex);
                    if (n.params.lazyLoadingInPrevNext)
                        if (c > 1 || n.params.lazyLoadingInPrevNextAmount && n.params.lazyLoadingInPrevNextAmount > 1) {
                            var d = n.params.lazyLoadingInPrevNextAmount,
                                e = c,
                                f = Math.min(n.activeIndex + e + Math.max(d, e), n.slides.length),
                                g = Math.max(n.activeIndex - Math.max(e, d), 0);
                            for (b = n.activeIndex + c; b < f; b++) n.slides[b] && n.lazy.loadImageInSlide(b);
                            for (b = g; b < n.activeIndex; b++) n.slides[b] && n.lazy.loadImageInSlide(b)
                        } else {
                            var h = n.wrapper.children("." + n.params.slideNextClass);
                            h.length > 0 && n.lazy.loadImageInSlide(h.index());
                            var i = n.wrapper.children("." + n.params.slidePrevClass);
                            i.length > 0 && n.lazy.loadImageInSlide(i.index())
                        }
                },
                onTransitionStart: function() {
                    n.params.lazyLoading && (n.params.lazyLoadingOnTransitionStart || !n.params.lazyLoadingOnTransitionStart && !n.lazy.initialImageLoaded) && n.lazy.load()
                },
                onTransitionEnd: function() {
                    n.params.lazyLoading && !n.params.lazyLoadingOnTransitionStart && n.lazy.load()
                }
            }, n.scrollbar = {
                isTouched: !1,
                setDragPosition: function(a) {
                    var b = n.scrollbar,
                        f = n.isHorizontal() ? "touchstart" === a.type || "touchmove" === a.type ? a.targetTouches[0].pageX : a.pageX || a.clientX : "touchstart" === a.type || "touchmove" === a.type ? a.targetTouches[0].pageY : a.pageY || a.clientY,
                        g = f - b.track.offset()[n.isHorizontal() ? "left" : "top"] - b.dragSize / 2,
                        h = -n.minTranslate() * b.moveDivider,
                        i = -n.maxTranslate() * b.moveDivider;
                    g < h ? g = h : g > i && (g = i), g = -g / b.moveDivider, n.updateProgress(g), n.setWrapperTranslate(g, !0)
                },
                dragStart: function(a) {
                    var b = n.scrollbar;
                    b.isTouched = !0, a.preventDefault(), a.stopPropagation(), b.setDragPosition(a), clearTimeout(b.dragTimeout), b.track.transition(0), n.params.scrollbarHide && b.track.css("opacity", 1), n.wrapper.transition(100), b.drag.transition(100), n.emit("onScrollbarDragStart", n)
                },
                dragMove: function(a) {
                    var b = n.scrollbar;
                    b.isTouched && (a.preventDefault ? a.preventDefault() : a.returnValue = !1, b.setDragPosition(a), n.wrapper.transition(0), b.track.transition(0), b.drag.transition(0), n.emit("onScrollbarDragMove", n))
                },
                dragEnd: function(a) {
                    var b = n.scrollbar;
                    b.isTouched && (b.isTouched = !1, n.params.scrollbarHide && (clearTimeout(b.dragTimeout), b.dragTimeout = setTimeout(function() {
                        b.track.css("opacity", 0), b.track.transition(400)
                    }, 1e3)), n.emit("onScrollbarDragEnd", n), n.params.scrollbarSnapOnRelease && n.slideReset())
                },
                draggableEvents: function() {
                    return n.params.simulateTouch !== !1 || n.support.touch ? n.touchEvents : n.touchEventsDesktop
                }(),
                enableDraggable: function() {
                    var b = n.scrollbar,
                        c = n.support.touch ? b.track : document;
                    a(b.track).on(b.draggableEvents.start, b.dragStart), a(c).on(b.draggableEvents.move, b.dragMove), a(c).on(b.draggableEvents.end, b.dragEnd)
                },
                disableDraggable: function() {
                    var b = n.scrollbar,
                        c = n.support.touch ? b.track : document;
                    a(b.track).off(n.draggableEvents.start, b.dragStart), a(c).off(n.draggableEvents.move, b.dragMove), a(c).off(n.draggableEvents.end, b.dragEnd)
                },
                set: function() {
                    if (n.params.scrollbar) {
                        var b = n.scrollbar;
                        b.track = a(n.params.scrollbar), n.params.uniqueNavElements && "string" == typeof n.params.scrollbar && b.track.length > 1 && 1 === n.container.find(n.params.scrollbar).length && (b.track = n.container.find(n.params.scrollbar)), b.drag = b.track.find(".swiper-scrollbar-drag"), 0 === b.drag.length && (b.drag = a('<div class="swiper-scrollbar-drag"></div>'), b.track.append(b.drag)), b.drag[0].style.width = "", b.drag[0].style.height = "", b.trackSize = n.isHorizontal() ? b.track[0].offsetWidth : b.track[0].offsetHeight, b.divider = n.size / n.virtualSize, b.moveDivider = b.divider * (b.trackSize / n.size), b.dragSize = b.trackSize * b.divider, n.isHorizontal() ? b.drag[0].style.width = b.dragSize + "px" : b.drag[0].style.height = b.dragSize + "px", b.divider >= 1 ? b.track[0].style.display = "none" : b.track[0].style.display = "", n.params.scrollbarHide && (b.track[0].style.opacity = 0)
                    }
                },
                setTranslate: function() {
                    if (n.params.scrollbar) {
                        var d, b = n.scrollbar,
                            e = (n.translate || 0, b.dragSize);
                        d = (b.trackSize - b.dragSize) * n.progress, n.rtl && n.isHorizontal() ? (d = -d, d > 0 ? (e = b.dragSize - d, d = 0) : -d + b.dragSize > b.trackSize && (e = b.trackSize + d)) : d < 0 ? (e = b.dragSize + d, d = 0) : d + b.dragSize > b.trackSize && (e = b.trackSize - d), n.isHorizontal() ? (n.support.transforms3d ? b.drag.transform("translate3d(" + d + "px, 0, 0)") : b.drag.transform("translateX(" + d + "px)"), b.drag[0].style.width = e + "px") : (n.support.transforms3d ? b.drag.transform("translate3d(0px, " + d + "px, 0)") : b.drag.transform("translateY(" + d + "px)"), b.drag[0].style.height = e + "px"), n.params.scrollbarHide && (clearTimeout(b.timeout), b.track[0].style.opacity = 1, b.timeout = setTimeout(function() {
                            b.track[0].style.opacity = 0, b.track.transition(400)
                        }, 1e3))
                    }
                },
                setTransition: function(a) {
                    n.params.scrollbar && n.scrollbar.drag.transition(a)
                }
            }, n.controller = {
                LinearSpline: function(a, b) {
                    this.x = a, this.y = b, this.lastIndex = a.length - 1;
                    var c, d;
                    this.x.length;
                    this.interpolate = function(a) {
                        return a ? (d = f(this.x, a), c = d - 1, (a - this.x[c]) * (this.y[d] - this.y[c]) / (this.x[d] - this.x[c]) + this.y[c]) : 0
                    };
                    var f = function() {
                        var a, b, c;
                        return function(d, e) {
                            for (b = -1, a = d.length; a - b > 1;) d[c = a + b >> 1] <= e ? b = c : a = c;
                            return a
                        }
                    }()
                },
                getInterpolateFunction: function(a) {
                    n.controller.spline || (n.controller.spline = n.params.loop ? new n.controller.LinearSpline(n.slidesGrid, a.slidesGrid) : new n.controller.LinearSpline(n.snapGrid, a.snapGrid))
                },
                setTranslate: function(a, b) {
                    function f(b) {
                        a = b.rtl && "horizontal" === b.params.direction ? -n.translate : n.translate, "slide" === n.params.controlBy && (n.controller.getInterpolateFunction(b), e = -n.controller.spline.interpolate(-a)), e && "container" !== n.params.controlBy || (d = (b.maxTranslate() - b.minTranslate()) / (n.maxTranslate() - n.minTranslate()), e = (a - n.minTranslate()) * d + b.minTranslate()), n.params.controlInverse && (e = b.maxTranslate() - e), b.updateProgress(e), b.setWrapperTranslate(e, !1, n), b.updateActiveIndex()
                    }
                    var d, e, c = n.params.control;
                    if (n.isArray(c))
                        for (var g = 0; g < c.length; g++) c[g] !== b && c[g] instanceof Swiper && f(c[g]);
                    else c instanceof Swiper && b !== c && f(c)
                },
                setTransition: function(a, b) {
                    function e(b) {
                        b.setWrapperTransition(a, n), 0 !== a && (b.onTransitionStart(),
                            b.wrapper.transitionEnd(function() {
                                c && (b.params.loop && "slide" === n.params.controlBy && b.fixLoop(), b.onTransitionEnd())
                            }))
                    }
                    var d, c = n.params.control;
                    if (n.isArray(c))
                        for (d = 0; d < c.length; d++) c[d] !== b && c[d] instanceof Swiper && e(c[d]);
                    else c instanceof Swiper && b !== c && e(c)
                }
            }, n.hashnav = {
                onHashCange: function(a, b) {
                    var c = document.location.hash.replace("#", ""),
                        d = n.slides.eq(n.activeIndex).attr("data-hash");
                    c !== d && n.slideTo(n.wrapper.children("." + n.params.slideClass + '[data-hash="' + c + '"]').index())
                },
                attachEvents: function(b) {
                    var c = b ? "off" : "on";
                    a(window)[c]("hashchange", n.hashnav.onHashCange)
                },
                setHash: function() {
                    if (n.hashnav.initialized && n.params.hashnav)
                        if (n.params.replaceState && window.history && window.history.replaceState) window.history.replaceState(null, null, "#" + n.slides.eq(n.activeIndex).attr("data-hash") || "");
                        else {
                            var a = n.slides.eq(n.activeIndex),
                                b = a.attr("data-hash") || a.attr("data-history");
                            document.location.hash = b || ""
                        }
                },
                init: function() {
                    if (n.params.hashnav && !n.params.history) {
                        n.hashnav.initialized = !0;
                        var a = document.location.hash.replace("#", "");
                        if (a) {
                            for (var b = 0, c = 0, d = n.slides.length; c < d; c++) {
                                var e = n.slides.eq(c),
                                    f = e.attr("data-hash") || e.attr("data-history");
                                if (f === a && !e.hasClass(n.params.slideDuplicateClass)) {
                                    var g = e.index();
                                    n.slideTo(g, b, n.params.runCallbacksOnInit, !0)
                                }
                            }
                            n.params.hashnavWatchState && n.hashnav.attachEvents()
                        }
                    }
                },
                destroy: function() {
                    n.params.hashnavWatchState && n.hashnav.attachEvents(!0)
                }
            }, n.history = {
                init: function() {
                    if (n.params.history) {
                        if (!window.history || !window.history.pushState) return n.params.history = !1, void(n.params.hashnav = !0);
                        n.history.initialized = !0, this.paths = this.getPathValues(), (this.paths.key || this.paths.value) && (this.scrollToSlide(0, this.paths.value, n.params.runCallbacksOnInit), n.params.replaceState || window.addEventListener("popstate", this.setHistoryPopState))
                    }
                },
                setHistoryPopState: function() {
                    n.history.paths = n.history.getPathValues(), n.history.scrollToSlide(n.params.speed, n.history.paths.value, !1)
                },
                getPathValues: function() {
                    var a = window.location.pathname.slice(1).split("/"),
                        b = a.length,
                        c = a[b - 2],
                        d = a[b - 1];
                    return {
                        key: c,
                        value: d
                    }
                },
                setHistory: function(a, b) {
                    if (n.history.initialized && n.params.history) {
                        var c = n.slides.eq(b),
                            d = this.slugify(c.attr("data-history"));
                        window.location.pathname.includes(a) || (d = a + "/" + d), n.params.replaceState ? window.history.replaceState(null, null, d) : window.history.pushState(null, null, d)
                    }
                },
                slugify: function(a) {
                    return a.toString().toLowerCase().replace(/\s+/g, "-").replace(/[^\w\-]+/g, "").replace(/\-\-+/g, "-").replace(/^-+/, "").replace(/-+$/, "")
                },
                scrollToSlide: function(a, b, c) {
                    if (b)
                        for (var d = 0, e = n.slides.length; d < e; d++) {
                            var f = n.slides.eq(d),
                                g = this.slugify(f.attr("data-history"));
                            if (g === b && !f.hasClass(n.params.slideDuplicateClass)) {
                                var h = f.index();
                                n.slideTo(h, a, c)
                            }
                        } else n.slideTo(0, a, c)
                }
            }, n.disableKeyboardControl = function() {
                n.params.keyboardControl = !1, a(document).off("keydown", I)
            }, n.enableKeyboardControl = function() {
                n.params.keyboardControl = !0, a(document).on("keydown", I)
            }, n.mousewheel = {
                event: !1,
                lastScrollTime: (new window.Date).getTime()
            }, n.params.mousewheelControl && (n.mousewheel.event = navigator.userAgent.indexOf("firefox") > -1 ? "DOMMouseScroll" : J() ? "wheel" : "mousewheel"), n.disableMousewheelControl = function() {
                if (!n.mousewheel.event) return !1;
                var b = n.container;
                return "container" !== n.params.mousewheelEventsTarged && (b = a(n.params.mousewheelEventsTarged)), b.off(n.mousewheel.event, K), !0
            }, n.enableMousewheelControl = function() {
                if (!n.mousewheel.event) return !1;
                var b = n.container;
                return "container" !== n.params.mousewheelEventsTarged && (b = a(n.params.mousewheelEventsTarged)), b.on(n.mousewheel.event, K), !0
            }, n.parallax = {
                setTranslate: function() {
                    n.container.children("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function() {
                        M(this, n.progress)
                    }), n.slides.each(function() {
                        var b = a(this);
                        b.find("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function() {
                            var a = Math.min(Math.max(b[0].progress, -1), 1);
                            M(this, a)
                        })
                    })
                },
                setTransition: function(b) {
                    "undefined" == typeof b && (b = n.params.speed), n.container.find("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function() {
                        var c = a(this),
                            d = parseInt(c.attr("data-swiper-parallax-duration"), 10) || b;
                        0 === b && (d = 0), c.transition(d)
                    })
                }
            }, n.zoom = {
                scale: 1,
                currentScale: 1,
                isScaling: !1,
                gesture: {
                    slide: void 0,
                    slideWidth: void 0,
                    slideHeight: void 0,
                    image: void 0,
                    imageWrap: void 0,
                    zoomMax: n.params.zoomMax
                },
                image: {
                    isTouched: void 0,
                    isMoved: void 0,
                    currentX: void 0,
                    currentY: void 0,
                    minX: void 0,
                    minY: void 0,
                    maxX: void 0,
                    maxY: void 0,
                    width: void 0,
                    height: void 0,
                    startX: void 0,
                    startY: void 0,
                    touchesStart: {},
                    touchesCurrent: {}
                },
                velocity: {
                    x: void 0,
                    y: void 0,
                    prevPositionX: void 0,
                    prevPositionY: void 0,
                    prevTime: void 0
                },
                getDistanceBetweenTouches: function(a) {
                    if (a.targetTouches.length < 2) return 1;
                    var b = a.targetTouches[0].pageX,
                        c = a.targetTouches[0].pageY,
                        d = a.targetTouches[1].pageX,
                        e = a.targetTouches[1].pageY,
                        f = Math.sqrt(Math.pow(d - b, 2) + Math.pow(e - c, 2));
                    return f
                },
                onGestureStart: function(b) {
                    var c = n.zoom;
                    if (!n.support.gestures) {
                        if ("touchstart" !== b.type || "touchstart" === b.type && b.targetTouches.length < 2) return;
                        c.gesture.scaleStart = c.getDistanceBetweenTouches(b)
                    }
                    return c.gesture.slide && c.gesture.slide.length || (c.gesture.slide = a(this), 0 === c.gesture.slide.length && (c.gesture.slide = n.slides.eq(n.activeIndex)), c.gesture.image = c.gesture.slide.find("img, svg, canvas"), c.gesture.imageWrap = c.gesture.image.parent("." + n.params.zoomContainerClass), c.gesture.zoomMax = c.gesture.imageWrap.attr("data-swiper-zoom") || n.params.zoomMax, 0 !== c.gesture.imageWrap.length) ? (c.gesture.image.transition(0), void(c.isScaling = !0)) : void(c.gesture.image = void 0)
                },
                onGestureChange: function(a) {
                    var b = n.zoom;
                    if (!n.support.gestures) {
                        if ("touchmove" !== a.type || "touchmove" === a.type && a.targetTouches.length < 2) return;
                        b.gesture.scaleMove = b.getDistanceBetweenTouches(a)
                    }
                    b.gesture.image && 0 !== b.gesture.image.length && (n.support.gestures ? b.scale = a.scale * b.currentScale : b.scale = b.gesture.scaleMove / b.gesture.scaleStart * b.currentScale, b.scale > b.gesture.zoomMax && (b.scale = b.gesture.zoomMax - 1 + Math.pow(b.scale - b.gesture.zoomMax + 1, .5)), b.scale < n.params.zoomMin && (b.scale = n.params.zoomMin + 1 - Math.pow(n.params.zoomMin - b.scale + 1, .5)), b.gesture.image.transform("translate3d(0,0,0) scale(" + b.scale + ")"))
                },
                onGestureEnd: function(a) {
                    var b = n.zoom;
                    !n.support.gestures && ("touchend" !== a.type || "touchend" === a.type && a.changedTouches.length < 2) || b.gesture.image && 0 !== b.gesture.image.length && (b.scale = Math.max(Math.min(b.scale, b.gesture.zoomMax), n.params.zoomMin), b.gesture.image.transition(n.params.speed).transform("translate3d(0,0,0) scale(" + b.scale + ")"), b.currentScale = b.scale, b.isScaling = !1, 1 === b.scale && (b.gesture.slide = void 0))
                },
                onTouchStart: function(a, b) {
                    var c = a.zoom;
                    c.gesture.image && 0 !== c.gesture.image.length && (c.image.isTouched || ("android" === a.device.os && b.preventDefault(), c.image.isTouched = !0, c.image.touchesStart.x = "touchstart" === b.type ? b.targetTouches[0].pageX : b.pageX, c.image.touchesStart.y = "touchstart" === b.type ? b.targetTouches[0].pageY : b.pageY))
                },
                onTouchMove: function(a) {
                    var b = n.zoom;
                    if (b.gesture.image && 0 !== b.gesture.image.length && (n.allowClick = !1, b.image.isTouched && b.gesture.slide)) {
                        b.image.isMoved || (b.image.width = b.gesture.image[0].offsetWidth, b.image.height = b.gesture.image[0].offsetHeight, b.image.startX = n.getTranslate(b.gesture.imageWrap[0], "x") || 0, b.image.startY = n.getTranslate(b.gesture.imageWrap[0], "y") || 0, b.gesture.slideWidth = b.gesture.slide[0].offsetWidth, b.gesture.slideHeight = b.gesture.slide[0].offsetHeight, b.gesture.imageWrap.transition(0));
                        var c = b.image.width * b.scale,
                            d = b.image.height * b.scale;
                        if (!(c < b.gesture.slideWidth && d < b.gesture.slideHeight)) {
                            if (b.image.minX = Math.min(b.gesture.slideWidth / 2 - c / 2, 0), b.image.maxX = -b.image.minX, b.image.minY = Math.min(b.gesture.slideHeight / 2 - d / 2, 0), b.image.maxY = -b.image.minY, b.image.touchesCurrent.x = "touchmove" === a.type ? a.targetTouches[0].pageX : a.pageX, b.image.touchesCurrent.y = "touchmove" === a.type ? a.targetTouches[0].pageY : a.pageY, !b.image.isMoved && !b.isScaling) {
                                if (n.isHorizontal() && Math.floor(b.image.minX) === Math.floor(b.image.startX) && b.image.touchesCurrent.x < b.image.touchesStart.x || Math.floor(b.image.maxX) === Math.floor(b.image.startX) && b.image.touchesCurrent.x > b.image.touchesStart.x) return void(b.image.isTouched = !1);
                                if (!n.isHorizontal() && Math.floor(b.image.minY) === Math.floor(b.image.startY) && b.image.touchesCurrent.y < b.image.touchesStart.y || Math.floor(b.image.maxY) === Math.floor(b.image.startY) && b.image.touchesCurrent.y > b.image.touchesStart.y) return void(b.image.isTouched = !1)
                            }
                            a.preventDefault(), a.stopPropagation(), b.image.isMoved = !0, b.image.currentX = b.image.touchesCurrent.x - b.image.touchesStart.x + b.image.startX, b.image.currentY = b.image.touchesCurrent.y - b.image.touchesStart.y + b.image.startY, b.image.currentX < b.image.minX && (b.image.currentX = b.image.minX + 1 - Math.pow(b.image.minX - b.image.currentX + 1, .8)), b.image.currentX > b.image.maxX && (b.image.currentX = b.image.maxX - 1 + Math.pow(b.image.currentX - b.image.maxX + 1, .8)), b.image.currentY < b.image.minY && (b.image.currentY = b.image.minY + 1 - Math.pow(b.image.minY - b.image.currentY + 1, .8)), b.image.currentY > b.image.maxY && (b.image.currentY = b.image.maxY - 1 + Math.pow(b.image.currentY - b.image.maxY + 1, .8)), b.velocity.prevPositionX || (b.velocity.prevPositionX = b.image.touchesCurrent.x), b.velocity.prevPositionY || (b.velocity.prevPositionY = b.image.touchesCurrent.y), b.velocity.prevTime || (b.velocity.prevTime = Date.now()), b.velocity.x = (b.image.touchesCurrent.x - b.velocity.prevPositionX) / (Date.now() - b.velocity.prevTime) / 2, b.velocity.y = (b.image.touchesCurrent.y - b.velocity.prevPositionY) / (Date.now() - b.velocity.prevTime) / 2, Math.abs(b.image.touchesCurrent.x - b.velocity.prevPositionX) < 2 && (b.velocity.x = 0), Math.abs(b.image.touchesCurrent.y - b.velocity.prevPositionY) < 2 && (b.velocity.y = 0), b.velocity.prevPositionX = b.image.touchesCurrent.x, b.velocity.prevPositionY = b.image.touchesCurrent.y, b.velocity.prevTime = Date.now(), b.gesture.imageWrap.transform("translate3d(" + b.image.currentX + "px, " + b.image.currentY + "px,0)")
                        }
                    }
                },
                onTouchEnd: function(a, b) {
                    var c = a.zoom;
                    if (c.gesture.image && 0 !== c.gesture.image.length) {
                        if (!c.image.isTouched || !c.image.isMoved) return c.image.isTouched = !1, void(c.image.isMoved = !1);
                        c.image.isTouched = !1, c.image.isMoved = !1;
                        var d = 300,
                            e = 300,
                            f = c.velocity.x * d,
                            g = c.image.currentX + f,
                            h = c.velocity.y * e,
                            i = c.image.currentY + h;
                        0 !== c.velocity.x && (d = Math.abs((g - c.image.currentX) / c.velocity.x)), 0 !== c.velocity.y && (e = Math.abs((i - c.image.currentY) / c.velocity.y));
                        var j = Math.max(d, e);
                        c.image.currentX = g, c.image.currentY = i;
                        var k = c.image.width * c.scale,
                            l = c.image.height * c.scale;
                        c.image.minX = Math.min(c.gesture.slideWidth / 2 - k / 2, 0), c.image.maxX = -c.image.minX, c.image.minY = Math.min(c.gesture.slideHeight / 2 - l / 2, 0), c.image.maxY = -c.image.minY, c.image.currentX = Math.max(Math.min(c.image.currentX, c.image.maxX), c.image.minX), c.image.currentY = Math.max(Math.min(c.image.currentY, c.image.maxY), c.image.minY), c.gesture.imageWrap.transition(j).transform("translate3d(" + c.image.currentX + "px, " + c.image.currentY + "px,0)")
                    }
                },
                onTransitionEnd: function(a) {
                    var b = a.zoom;
                    b.gesture.slide && a.previousIndex !== a.activeIndex && (b.gesture.image.transform("translate3d(0,0,0) scale(1)"), b.gesture.imageWrap.transform("translate3d(0,0,0)"), b.gesture.slide = b.gesture.image = b.gesture.imageWrap = void 0, b.scale = b.currentScale = 1)
                },
                toggleZoom: function(b, c) {
                    var d = b.zoom;
                    if (d.gesture.slide || (d.gesture.slide = b.clickedSlide ? a(b.clickedSlide) : b.slides.eq(b.activeIndex), d.gesture.image = d.gesture.slide.find("img, svg, canvas"), d.gesture.imageWrap = d.gesture.image.parent("." + b.params.zoomContainerClass)), d.gesture.image && 0 !== d.gesture.image.length) {
                        var e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v;
                        "undefined" == typeof d.image.touchesStart.x && c ? (e = "touchend" === c.type ? c.changedTouches[0].pageX : c.pageX, f = "touchend" === c.type ? c.changedTouches[0].pageY : c.pageY) : (e = d.image.touchesStart.x, f = d.image.touchesStart.y), d.scale && 1 !== d.scale ? (d.scale = d.currentScale = 1, d.gesture.imageWrap.transition(300).transform("translate3d(0,0,0)"), d.gesture.image.transition(300).transform("translate3d(0,0,0) scale(1)"), d.gesture.slide = void 0) : (d.scale = d.currentScale = d.gesture.imageWrap.attr("data-swiper-zoom") || b.params.zoomMax, c ? (u = d.gesture.slide[0].offsetWidth, v = d.gesture.slide[0].offsetHeight, g = d.gesture.slide.offset().left, h = d.gesture.slide.offset().top, i = g + u / 2 - e, j = h + v / 2 - f, m = d.gesture.image[0].offsetWidth, n = d.gesture.image[0].offsetHeight, o = m * d.scale, p = n * d.scale, q = Math.min(u / 2 - o / 2, 0), r = Math.min(v / 2 - p / 2, 0), s = -q, t = -r, k = i * d.scale, l = j * d.scale, k < q && (k = q), k > s && (k = s), l < r && (l = r), l > t && (l = t)) : (k = 0, l = 0), d.gesture.imageWrap.transition(300).transform("translate3d(" + k + "px, " + l + "px,0)"), d.gesture.image.transition(300).transform("translate3d(0,0,0) scale(" + d.scale + ")"))
                    }
                },
                attachEvents: function(b) {
                    var c = b ? "off" : "on";
                    if (n.params.zoom) {
                        var e = (n.slides, !("touchstart" !== n.touchEvents.start || !n.support.passiveListener || !n.params.passiveListeners) && {
                            passive: !0,
                            capture: !1
                        });
                        n.support.gestures ? (n.slides[c]("gesturestart", n.zoom.onGestureStart, e), n.slides[c]("gesturechange", n.zoom.onGestureChange, e), n.slides[c]("gestureend", n.zoom.onGestureEnd, e)) : "touchstart" === n.touchEvents.start && (n.slides[c](n.touchEvents.start, n.zoom.onGestureStart, e), n.slides[c](n.touchEvents.move, n.zoom.onGestureChange, e), n.slides[c](n.touchEvents.end, n.zoom.onGestureEnd, e)), n[c]("touchStart", n.zoom.onTouchStart), n.slides.each(function(b, d) {
                            a(d).find("." + n.params.zoomContainerClass).length > 0 && a(d)[c](n.touchEvents.move, n.zoom.onTouchMove)
                        }), n[c]("touchEnd", n.zoom.onTouchEnd), n[c]("transitionEnd", n.zoom.onTransitionEnd), n.params.zoomToggle && n.on("doubleTap", n.zoom.toggleZoom)
                    }
                },
                init: function() {
                    n.zoom.attachEvents()
                },
                destroy: function() {
                    n.zoom.attachEvents(!0)
                }
            }, n._plugins = [];
            for (var N in n.plugins) {
                var O = n.plugins[N](n, n.params[N]);
                O && n._plugins.push(O)
            }
            return n.callPlugins = function(a) {
                for (var b = 0; b < n._plugins.length; b++) a in n._plugins[b] && n._plugins[b][a](arguments[1], arguments[2], arguments[3], arguments[4], arguments[5])
            }, n.emitterEventListeners = {}, n.emit = function(a) {
                n.params[a] && n.params[a](arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]);
                var b;
                if (n.emitterEventListeners[a])
                    for (b = 0; b < n.emitterEventListeners[a].length; b++) n.emitterEventListeners[a][b](arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]);
                n.callPlugins && n.callPlugins(a, arguments[1], arguments[2], arguments[3], arguments[4], arguments[5])
            }, n.on = function(a, b) {
                return a = P(a), n.emitterEventListeners[a] || (n.emitterEventListeners[a] = []), n.emitterEventListeners[a].push(b), n
            }, n.off = function(a, b) {
                var c;
                if (a = P(a), "undefined" == typeof b) return n.emitterEventListeners[a] = [], n;
                if (n.emitterEventListeners[a] && 0 !== n.emitterEventListeners[a].length) {
                    for (c = 0; c < n.emitterEventListeners[a].length; c++) n.emitterEventListeners[a][c] === b && n.emitterEventListeners[a].splice(c, 1);
                    return n
                }
            }, n.once = function(a, b) {
                a = P(a);
                var c = function() {
                    b(arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]), n.off(a, c)
                };
                return n.on(a, c), n
            }, n.a11y = {
                makeFocusable: function(a) {
                    return a.attr("tabIndex", "0"), a
                },
                addRole: function(a, b) {
                    return a.attr("role", b), a
                },
                addLabel: function(a, b) {
                    return a.attr("aria-label", b), a
                },
                disable: function(a) {
                    return a.attr("aria-disabled", !0), a
                },
                enable: function(a) {
                    return a.attr("aria-disabled", !1), a
                },
                onEnterKey: function(b) {
                    13 === b.keyCode && (a(b.target).is(n.params.nextButton) ? (n.onClickNext(b), n.isEnd ? n.a11y.notify(n.params.lastSlideMessage) : n.a11y.notify(n.params.nextSlideMessage)) : a(b.target).is(n.params.prevButton) && (n.onClickPrev(b), n.isBeginning ? n.a11y.notify(n.params.firstSlideMessage) : n.a11y.notify(n.params.prevSlideMessage)), a(b.target).is("." + n.params.bulletClass) && a(b.target)[0].click())
                },
                liveRegion: a('<span class="' + n.params.notificationClass + '" aria-live="assertive" aria-atomic="true"></span>'),
                notify: function(a) {
                    var b = n.a11y.liveRegion;
                    0 !== b.length && (b.html(""), b.html(a))
                },
                init: function() {
                    n.params.nextButton && n.nextButton && n.nextButton.length > 0 && (n.a11y.makeFocusable(n.nextButton), n.a11y.addRole(n.nextButton, "button"), n.a11y.addLabel(n.nextButton, n.params.nextSlideMessage)), n.params.prevButton && n.prevButton && n.prevButton.length > 0 && (n.a11y.makeFocusable(n.prevButton), n.a11y.addRole(n.prevButton, "button"), n.a11y.addLabel(n.prevButton, n.params.prevSlideMessage)), a(n.container).append(n.a11y.liveRegion)
                },
                initPagination: function() {
                    n.params.pagination && n.params.paginationClickable && n.bullets && n.bullets.length && n.bullets.each(function() {
                        var b = a(this);
                        n.a11y.makeFocusable(b), n.a11y.addRole(b, "button"), n.a11y.addLabel(b, n.params.paginationBulletMessage.replace(/{{index}}/, b.index() + 1))
                    })
                },
                destroy: function() {
                    n.a11y.liveRegion && n.a11y.liveRegion.length > 0 && n.a11y.liveRegion.remove()
                }
            }, n.init = function() {
                n.params.loop && n.createLoop(), n.updateContainerSize(), n.updateSlidesSize(), n.updatePagination(), n.params.scrollbar && n.scrollbar && (n.scrollbar.set(), n.params.scrollbarDraggable && n.scrollbar.enableDraggable()), "slide" !== n.params.effect && n.effects[n.params.effect] && (n.params.loop || n.updateProgress(), n.effects[n.params.effect].setTranslate()), n.params.loop ? n.slideTo(n.params.initialSlide + n.loopedSlides, 0, n.params.runCallbacksOnInit) : (n.slideTo(n.params.initialSlide, 0, n.params.runCallbacksOnInit), 0 === n.params.initialSlide && (n.parallax && n.params.parallax && n.parallax.setTranslate(), n.lazy && n.params.lazyLoading && (n.lazy.load(), n.lazy.initialImageLoaded = !0))), n.attachEvents(), n.params.observer && n.support.observer && n.initObservers(), n.params.preloadImages && !n.params.lazyLoading && n.preloadImages(), n.params.zoom && n.zoom && n.zoom.init(), n.params.autoplay && n.startAutoplay(), n.params.keyboardControl && n.enableKeyboardControl && n.enableKeyboardControl(), n.params.mousewheelControl && n.enableMousewheelControl && n.enableMousewheelControl(), n.params.hashnavReplaceState && (n.params.replaceState = n.params.hashnavReplaceState), n.params.history && n.history && n.history.init(), n.params.hashnav && n.hashnav && n.hashnav.init(), n.params.a11y && n.a11y && n.a11y.init(), n.emit("onInit", n)
            }, n.cleanupStyles = function() {
                n.container.removeClass(n.classNames.join(" ")).removeAttr("style"), n.wrapper.removeAttr("style"), n.slides && n.slides.length && n.slides.removeClass([n.params.slideVisibleClass, n.params.slideActiveClass, n.params.slideNextClass, n.params.slidePrevClass].join(" ")).removeAttr("style").removeAttr("data-swiper-column").removeAttr("data-swiper-row"), n.paginationContainer && n.paginationContainer.length && n.paginationContainer.removeClass(n.params.paginationHiddenClass), n.bullets && n.bullets.length && n.bullets.removeClass(n.params.bulletActiveClass), n.params.prevButton && a(n.params.prevButton).removeClass(n.params.buttonDisabledClass), n.params.nextButton && a(n.params.nextButton).removeClass(n.params.buttonDisabledClass), n.params.scrollbar && n.scrollbar && (n.scrollbar.track && n.scrollbar.track.length && n.scrollbar.track.removeAttr("style"), n.scrollbar.drag && n.scrollbar.drag.length && n.scrollbar.drag.removeAttr("style"))
            }, n.destroy = function(a, b) {
                n.detachEvents(), n.stopAutoplay(), n.params.scrollbar && n.scrollbar && n.params.scrollbarDraggable && n.scrollbar.disableDraggable(), n.params.loop && n.destroyLoop(), b && n.cleanupStyles(), n.disconnectObservers(), n.params.zoom && n.zoom && n.zoom.destroy(), n.params.keyboardControl && n.disableKeyboardControl && n.disableKeyboardControl(), n.params.mousewheelControl && n.disableMousewheelControl && n.disableMousewheelControl(), n.params.a11y && n.a11y && n.a11y.destroy(), n.params.history && !n.params.replaceState && window.removeEventListener("popstate", n.history.setHistoryPopState), n.params.hashnav && n.hashnav && n.hashnav.destroy(), n.emit("onDestroy"), a !== !1 && (n = null)
            }, n.init(), n
        }
    };
    b.prototype = {
        isSafari: function() {
            var a = navigator.userAgent.toLowerCase();
            return a.indexOf("safari") >= 0 && a.indexOf("chrome") < 0 && a.indexOf("android") < 0
        }(),
        isUiWebView: /(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/i.test(navigator.userAgent),
        isArray: function(a) {
            return "[object Array]" === Object.prototype.toString.apply(a)
        },
        browser: {
            ie: window.navigator.pointerEnabled || window.navigator.msPointerEnabled,
            ieTouch: window.navigator.msPointerEnabled && window.navigator.msMaxTouchPoints > 1 || window.navigator.pointerEnabled && window.navigator.maxTouchPoints > 1,
            lteIE9: function() {
                var a = document.createElement("div");
                return a.innerHTML = "<!--[if lte IE 9]><i></i><![endif]-->", 1 === a.getElementsByTagName("i").length
            }()
        },
        device: function() {
            var a = navigator.userAgent,
                b = a.match(/(Android);?[\s\/]+([\d.]+)?/),
                c = a.match(/(iPad).*OS\s([\d_]+)/),
                d = a.match(/(iPod)(.*OS\s([\d_]+))?/),
                e = !c && a.match(/(iPhone\sOS)\s([\d_]+)/);
            return {
                ios: c || e || d,
                android: b
            }
        }(),
        support: {
            touch: window.Modernizr && Modernizr.touch === !0 || function() {
                return !!("ontouchstart" in window || window.DocumentTouch && document instanceof DocumentTouch)
            }(),
            transforms3d: window.Modernizr && Modernizr.csstransforms3d === !0 || function() {
                var a = document.createElement("div").style;
                return "webkitPerspective" in a || "MozPerspective" in a || "OPerspective" in a || "MsPerspective" in a || "perspective" in a
            }(),
            flexbox: function() {
                for (var a = document.createElement("div").style, b = "alignItems webkitAlignItems webkitBoxAlign msFlexAlign mozBoxAlign webkitFlexDirection msFlexDirection mozBoxDirection mozBoxOrient webkitBoxDirection webkitBoxOrient".split(" "), c = 0; c < b.length; c++)
                    if (b[c] in a) return !0
            }(),
            observer: function() {
                return "MutationObserver" in window || "WebkitMutationObserver" in window
            }(),
            passiveListener: function() {
                var a = !1;
                try {
                    var b = Object.defineProperty({}, "passive", {
                        get: function() {
                            a = !0
                        }
                    });
                    window.addEventListener("testPassiveListener", null, b)
                } catch (a) {}
                return a
            }(),
            gestures: function() {
                return "ongesturestart" in window
            }()
        },
        plugins: {}
    };
    for (var c = (function() {
        var a = function(a) {
                var b = this,
                    c = 0;
                for (c = 0; c < a.length; c++) b[c] = a[c];
                return b.length = a.length, this
            },
            b = function(b, c) {
                var d = [],
                    e = 0;
                if (b && !c && b instanceof a) return b;
                if (b)
                    if ("string" == typeof b) {
                        var f, g, h = b.trim();
                        if (h.indexOf("<") >= 0 && h.indexOf(">") >= 0) {
                            var i = "div";
                            for (0 === h.indexOf("<li") && (i = "ul"), 0 === h.indexOf("<tr") && (i = "tbody"), 0 !== h.indexOf("<td") && 0 !== h.indexOf("<th") || (i = "tr"), 0 === h.indexOf("<tbody") && (i = "table"), 0 === h.indexOf("<option") && (i = "select"), g = document.createElement(i), g.innerHTML = b, e = 0; e < g.childNodes.length; e++) d.push(g.childNodes[e])
                        } else
                            for (f = c || "#" !== b[0] || b.match(/[ .<>:~]/) ? (c || document).querySelectorAll(b) : [document.getElementById(b.split("#")[1])], e = 0; e < f.length; e++) f[e] && d.push(f[e])
                    } else if (b.nodeType || b === window || b === document) d.push(b);
                    else if (b.length > 0 && b[0].nodeType)
                        for (e = 0; e < b.length; e++) d.push(b[e]);
                return new a(d)
            };
        return a.prototype = {
            addClass: function(a) {
                if ("undefined" == typeof a) return this;
                for (var b = a.split(" "), c = 0; c < b.length; c++)
                    for (var d = 0; d < this.length; d++) this[d].classList.add(b[c]);
                return this
            },
            removeClass: function(a) {
                for (var b = a.split(" "), c = 0; c < b.length; c++)
                    for (var d = 0; d < this.length; d++) this[d].classList.remove(b[c]);
                return this
            },
            hasClass: function(a) {
                return !!this[0] && this[0].classList.contains(a)
            },
            toggleClass: function(a) {
                for (var b = a.split(" "), c = 0; c < b.length; c++)
                    for (var d = 0; d < this.length; d++) this[d].classList.toggle(b[c]);
                return this
            },
            attr: function(a, b) {
                if (1 === arguments.length && "string" == typeof a) return this[0] ? this[0].getAttribute(a) : void 0;
                for (var c = 0; c < this.length; c++)
                    if (2 === arguments.length) this[c].setAttribute(a, b);
                    else
                        for (var d in a) this[c][d] = a[d], this[c].setAttribute(d, a[d]);
                return this
            },
            removeAttr: function(a) {
                for (var b = 0; b < this.length; b++) this[b].removeAttribute(a);
                return this
            },
            data: function(a, b) {
                if ("undefined" != typeof b) {
                    for (var d = 0; d < this.length; d++) {
                        var e = this[d];
                        e.dom7ElementDataStorage || (e.dom7ElementDataStorage = {}), e.dom7ElementDataStorage[a] = b
                    }
                    return this
                }
                if (this[0]) {
                    var c = this[0].getAttribute("data-" + a);
                    return c ? c : this[0].dom7ElementDataStorage && a in this[0].dom7ElementDataStorage ? this[0].dom7ElementDataStorage[a] : void 0
                }
            },
            transform: function(a) {
                for (var b = 0; b < this.length; b++) {
                    var c = this[b].style;
                    c.webkitTransform = c.MsTransform = c.msTransform = c.MozTransform = c.OTransform = c.transform = a
                }
                return this
            },
            transition: function(a) {
                "string" != typeof a && (a += "ms");
                for (var b = 0; b < this.length; b++) {
                    var c = this[b].style;
                    c.webkitTransitionDuration = c.MsTransitionDuration = c.msTransitionDuration = c.MozTransitionDuration = c.OTransitionDuration = c.transitionDuration = a
                }
                return this
            },
            on: function(a, c, d, e) {
                function f(a) {
                    var e = a.target;
                    if (b(e).is(c)) d.call(e, a);
                    else
                        for (var f = b(e).parents(), g = 0; g < f.length; g++) b(f[g]).is(c) && d.call(f[g], a)
                }
                var h, i, g = a.split(" ");
                for (h = 0; h < this.length; h++)
                    if ("function" == typeof c || c === !1)
                        for ("function" == typeof c && (d = arguments[1], e = arguments[2] || !1), i = 0; i < g.length; i++) this[h].addEventListener(g[i], d, e);
                    else
                        for (i = 0; i < g.length; i++) this[h].dom7LiveListeners || (this[h].dom7LiveListeners = []), this[h].dom7LiveListeners.push({
                            listener: d,
                            liveListener: f
                        }), this[h].addEventListener(g[i], f, e);
                return this
            },
            off: function(a, b, c, d) {
                for (var e = a.split(" "), f = 0; f < e.length; f++)
                    for (var g = 0; g < this.length; g++)
                        if ("function" == typeof b || b === !1) "function" == typeof b && (c = arguments[1], d = arguments[2] || !1), this[g].removeEventListener(e[f], c, d);
                        else if (this[g].dom7LiveListeners)
                            for (var h = 0; h < this[g].dom7LiveListeners.length; h++) this[g].dom7LiveListeners[h].listener === c && this[g].removeEventListener(e[f], this[g].dom7LiveListeners[h].liveListener, d);
                return this
            },
            once: function(a, b, c, d) {
                function f(g) {
                    c(g), e.off(a, b, f, d)
                }
                var e = this;
                "function" == typeof b && (b = !1, c = arguments[1], d = arguments[2]), e.on(a, b, f, d)
            },
            trigger: function(a, b) {
                for (var c = 0; c < this.length; c++) {
                    var d;
                    try {
                        d = new window.CustomEvent(a, {
                            detail: b,
                            bubbles: !0,
                            cancelable: !0
                        })
                    } catch (c) {
                        d = document.createEvent("Event"), d.initEvent(a, !0, !0), d.detail = b
                    }
                    this[c].dispatchEvent(d)
                }
                return this
            },
            transitionEnd: function(a) {
                function f(d) {
                    if (d.target === this)
                        for (a.call(this, d), c = 0; c < b.length; c++) e.off(b[c], f)
                }
                var c, b = ["webkitTransitionEnd", "transitionend", "oTransitionEnd", "MSTransitionEnd", "msTransitionEnd"],
                    e = this;
                if (a)
                    for (c = 0; c < b.length; c++) e.on(b[c], f);
                return this
            },
            width: function() {
                return this[0] === window ? window.innerWidth : this.length > 0 ? parseFloat(this.css("width")) : null
            },
            outerWidth: function(a) {
                return this.length > 0 ? a ? this[0].offsetWidth + parseFloat(this.css("margin-right")) + parseFloat(this.css("margin-left")) : this[0].offsetWidth : null
            },
            height: function() {
                return this[0] === window ? window.innerHeight : this.length > 0 ? parseFloat(this.css("height")) : null
            },
            outerHeight: function(a) {
                return this.length > 0 ? a ? this[0].offsetHeight + parseFloat(this.css("margin-top")) + parseFloat(this.css("margin-bottom")) : this[0].offsetHeight : null
            },
            offset: function() {
                if (this.length > 0) {
                    var a = this[0],
                        b = a.getBoundingClientRect(),
                        c = document.body,
                        d = a.clientTop || c.clientTop || 0,
                        e = a.clientLeft || c.clientLeft || 0,
                        f = window.pageYOffset || a.scrollTop,
                        g = window.pageXOffset || a.scrollLeft;
                    return {
                        top: b.top + f - d,
                        left: b.left + g - e
                    }
                }
                return null
            },
            css: function(a, b) {
                var c;
                if (1 === arguments.length) {
                    if ("string" != typeof a) {
                        for (c = 0; c < this.length; c++)
                            for (var d in a) this[c].style[d] = a[d];
                        return this
                    }
                    if (this[0]) return window.getComputedStyle(this[0], null).getPropertyValue(a)
                }
                if (2 === arguments.length && "string" == typeof a) {
                    for (c = 0; c < this.length; c++) this[c].style[a] = b;
                    return this
                }
                return this
            },
            each: function(a) {
                for (var b = 0; b < this.length; b++) a.call(this[b], b, this[b]);
                return this
            },
            html: function(a) {
                if ("undefined" == typeof a) return this[0] ? this[0].innerHTML : void 0;
                for (var b = 0; b < this.length; b++) this[b].innerHTML = a;
                return this
            },
            text: function(a) {
                if ("undefined" == typeof a) return this[0] ? this[0].textContent.trim() : null;
                for (var b = 0; b < this.length; b++) this[b].textContent = a;
                return this
            },
            is: function(c) {
                if (!this[0]) return !1;
                var d, e;
                if ("string" == typeof c) {
                    var f = this[0];
                    if (f === document) return c === document;
                    if (f === window) return c === window;
                    if (f.matches) return f.matches(c);
                    if (f.webkitMatchesSelector) return f.webkitMatchesSelector(c);
                    if (f.mozMatchesSelector) return f.mozMatchesSelector(c);
                    if (f.msMatchesSelector) return f.msMatchesSelector(c);
                    for (d = b(c), e = 0; e < d.length; e++)
                        if (d[e] === this[0]) return !0;
                    return !1
                }
                if (c === document) return this[0] === document;
                if (c === window) return this[0] === window;
                if (c.nodeType || c instanceof a) {
                    for (d = c.nodeType ? [c] : c, e = 0; e < d.length; e++)
                        if (d[e] === this[0]) return !0;
                    return !1
                }
                return !1
            },
            index: function() {
                if (this[0]) {
                    for (var a = this[0], b = 0; null !== (a = a.previousSibling);) 1 === a.nodeType && b++;
                    return b
                }
            },
            eq: function(b) {
                if ("undefined" == typeof b) return this;
                var d, c = this.length;
                return b > c - 1 ? new a([]) : b < 0 ? (d = c + b, new a(d < 0 ? [] : [this[d]])) : new a([this[b]])
            },
            append: function(b) {
                var c, d;
                for (c = 0; c < this.length; c++)
                    if ("string" == typeof b) {
                        var e = document.createElement("div");
                        for (e.innerHTML = b; e.firstChild;) this[c].appendChild(e.firstChild)
                    } else if (b instanceof a)
                        for (d = 0; d < b.length; d++) this[c].appendChild(b[d]);
                    else this[c].appendChild(b);
                return this
            },
            prepend: function(b) {
                var c, d;
                for (c = 0; c < this.length; c++)
                    if ("string" == typeof b) {
                        var e = document.createElement("div");
                        for (e.innerHTML = b, d = e.childNodes.length - 1; d >= 0; d--) this[c].insertBefore(e.childNodes[d], this[c].childNodes[0])
                    } else if (b instanceof a)
                        for (d = 0; d < b.length; d++) this[c].insertBefore(b[d], this[c].childNodes[0]);
                    else this[c].insertBefore(b, this[c].childNodes[0]);
                return this
            },
            insertBefore: function(a) {
                for (var c = b(a), d = 0; d < this.length; d++)
                    if (1 === c.length) c[0].parentNode.insertBefore(this[d], c[0]);
                    else if (c.length > 1)
                        for (var e = 0; e < c.length; e++) c[e].parentNode.insertBefore(this[d].cloneNode(!0), c[e])
            },
            insertAfter: function(a) {
                for (var c = b(a), d = 0; d < this.length; d++)
                    if (1 === c.length) c[0].parentNode.insertBefore(this[d], c[0].nextSibling);
                    else if (c.length > 1)
                        for (var e = 0; e < c.length; e++) c[e].parentNode.insertBefore(this[d].cloneNode(!0), c[e].nextSibling)
            },
            next: function(c) {
                return new a(this.length > 0 ? c ? this[0].nextElementSibling && b(this[0].nextElementSibling).is(c) ? [this[0].nextElementSibling] : [] : this[0].nextElementSibling ? [this[0].nextElementSibling] : [] : [])
            },
            nextAll: function(c) {
                var d = [],
                    e = this[0];
                if (!e) return new a([]);
                for (; e.nextElementSibling;) {
                    var f = e.nextElementSibling;
                    c ? b(f).is(c) && d.push(f) : d.push(f), e = f
                }
                return new a(d)
            },
            prev: function(c) {
                return new a(this.length > 0 ? c ? this[0].previousElementSibling && b(this[0].previousElementSibling).is(c) ? [this[0].previousElementSibling] : [] : this[0].previousElementSibling ? [this[0].previousElementSibling] : [] : [])
            },
            prevAll: function(c) {
                var d = [],
                    e = this[0];
                if (!e) return new a([]);
                for (; e.previousElementSibling;) {
                    var f = e.previousElementSibling;
                    c ? b(f).is(c) && d.push(f) : d.push(f), e = f
                }
                return new a(d)
            },
            parent: function(a) {
                for (var c = [], d = 0; d < this.length; d++) a ? b(this[d].parentNode).is(a) && c.push(this[d].parentNode) : c.push(this[d].parentNode);
                return b(b.unique(c))
            },
            parents: function(a) {
                for (var c = [], d = 0; d < this.length; d++)
                    for (var e = this[d].parentNode; e;) a ? b(e).is(a) && c.push(e) : c.push(e), e = e.parentNode;
                return b(b.unique(c))
            },
            find: function(b) {
                for (var c = [], d = 0; d < this.length; d++)
                    for (var e = this[d].querySelectorAll(b), f = 0; f < e.length; f++) c.push(e[f]);
                return new a(c)
            },
            children: function(c) {
                for (var d = [], e = 0; e < this.length; e++)
                    for (var f = this[e].childNodes, g = 0; g < f.length; g++) c ? 1 === f[g].nodeType && b(f[g]).is(c) && d.push(f[g]) : 1 === f[g].nodeType && d.push(f[g]);
                return new a(b.unique(d))
            },
            remove: function() {
                for (var a = 0; a < this.length; a++) this[a].parentNode && this[a].parentNode.removeChild(this[a]);
                return this
            },
            add: function() {
                var c, d, a = this;
                for (c = 0; c < arguments.length; c++) {
                    var e = b(arguments[c]);
                    for (d = 0; d < e.length; d++) a[a.length] = e[d], a.length++
                }
                return a
            }
        }, b.fn = a.prototype, b.unique = function(a) {
            for (var b = [], c = 0; c < a.length; c++) b.indexOf(a[c]) === -1 && b.push(a[c]);
            return b
        }, b
    }()), d = ["jQuery", "Zepto", "Dom7"], e = 0; e < d.length; e++) window[d[e]] && g(window[d[e]]);
    var f;
    f = "undefined" == typeof c ? window.Dom7 || window.Zepto || window.jQuery : c, f && ("transitionEnd" in f.fn || (f.fn.transitionEnd = function(a) {
        function f(d) {
            if (d.target === this)
                for (a.call(this, d), c = 0; c < b.length; c++) e.off(b[c], f)
        }
        var c, b = ["webkitTransitionEnd", "transitionend", "oTransitionEnd", "MSTransitionEnd", "msTransitionEnd"],
            e = this;
        if (a)
            for (c = 0; c < b.length; c++) e.on(b[c], f);
        return this
    }), "transform" in f.fn || (f.fn.transform = function(a) {
        for (var b = 0; b < this.length; b++) {
            var c = this[b].style;
            c.webkitTransform = c.MsTransform = c.msTransform = c.MozTransform = c.OTransform = c.transform = a
        }
        return this
    }), "transition" in f.fn || (f.fn.transition = function(a) {
        "string" != typeof a && (a += "ms");
        for (var b = 0; b < this.length; b++) {
            var c = this[b].style;
            c.webkitTransitionDuration = c.MsTransitionDuration = c.msTransitionDuration = c.MozTransitionDuration = c.OTransitionDuration = c.transitionDuration = a
        }
        return this
    }), "outerWidth" in f.fn || (f.fn.outerWidth = function(a) {
        return this.length > 0 ? a ? this[0].offsetWidth + parseFloat(this.css("margin-right")) + parseFloat(this.css("margin-left")) : this[0].offsetWidth : null;
    })), window.MBTGamesScroller = b
}(), MBT = {}, MBT.API = {}, MBT.API.Widgets = {}, MBT.API.Utils = {}, MBT.API.URL = {}, MBT.API.History = {}, MBT.API.Function = {}, MBT.API.Event = {}, MBT.API.UI = {}, MBT.API.Base64 = {}, MBT.API.Storage = {}, MBT.API.Cookies = {}, MBT.API.livePagesTypeIndex = null, MBT.API.livePagesLangId = null, MBT.API.imageServiceUrl = null, MBT.API.livePagesTypeIndexConfig = null, MBT.API.translationsLang = null, MBT.API.translationsSetId = null, MBT.API.addCSS = !0, MBT.API.getLivePagesTypeIndex = function(a) {
    return "undefined" != typeof a && "function" == typeof MBT.API.livePagesTypeIndexConfig ? MBT.API.livePagesTypeIndexConfig(a) : MBT.API.livePagesTypeIndex
}, MBT.API.getLivePagesLangId = function() {
    return MBT.API.livePagesLangId
}, "undefined" != typeof customServiceUrl && "" != customServiceUrl ? MBT.API.ServiceUrl = customServiceUrl : MBT.API.ServiceUrl = "http://widgets.baskethotel.com/", MBT.API.WidgetUrl = MBT.API.ServiceUrl + "widget-service/show?", MBT.API.StatisticsUrl = "http://185.38.167.137/stats.php", MBT.API.stylesheetIndex = 0, MBT.API.ApiCSSUrl = MBT.API.ServiceUrl + "css.php", MBT.API.ShowSurnameFirst = !1, MBT.API.FirstNamePriority = !1, MBT.API.NAVIGATION_TYPE_INNER = 1, MBT.API.NAVIGATION_TYPE_EXTERNAL = 2, MBT.API.NAVIGATION_TYPE_CUSTOM = 3, MBT.API.Widgets.TEST_WIDGET = 1, MBT.API.Widgets.PLAYER_FULL_VIEW_WIDGET = 100, MBT.API.Widgets.PLAYER_SMALL_WIDGET = 101, MBT.API.Widgets.PLAYER_ABC_LIST_WIDGET = 105, MBT.API.Widgets.PLAYER_COMPARISON_WIDGET = 120, MBT.API.Widgets.PLAYER_BIRTHDAY_WIDGET = 106, MBT.API.Widgets.TEAM_FULL_VIEW_WIDGET = 200, MBT.API.Widgets.TEAM_LOGO_LIST_WIDGET = 201, MBT.API.Widgets.TEAM_SCHEDULE_WIDGET = 203, MBT.API.Widgets.TEAM_RESULTS_WIDGET = 204, MBT.API.Widgets.TEAM_IN_LEAGUES_SCHEDULE_WIDGET = 205, MBT.API.Widgets.TEAM_IN_LEAGUES_LEADERS = 206, MBT.API.Widgets.TEAM_COMPARISON_WIDGET = 220, MBT.API.Widgets.TEAM_GAMES_SCROLLER = 225, MBT.API.Widgets.SEASON_STANDINGS_SHORT_WIDGET = 300, MBT.API.Widgets.SEASON_STANDINGS_LONG_WIDGET = 301, MBT.API.Widgets.SEASON_SCHEDULE_SHORT_WIDGET = 302, MBT.API.Widgets.SEASON_SCHEDULE_LONG_WIDGET = 303, MBT.API.Widgets.SEASON_FINAL_STANDINGS_WIDGET = 304, MBT.API.Widgets.SEASON_LEADERS_SHORT_WIDGET = 310, MBT.API.Widgets.SEASON_PLAYERS_TOP_5_WIDGET = 311, MBT.API.Widgets.SEASON_RESULTS_BY_TEAMS_WIDGET = 330, MBT.API.Widgets.SEASON_SELECTOR_WIDGET = 320, MBT.API.Widgets.GAME_FULL_VIEW_WIDGET = 400, MBT.API.Widgets.GAME_LIVE_BANNER_WIDGET = 401, MBT.API.Widgets.GAME_QUOTES_WIDGET = 402, MBT.API.Widgets.GAME_MEDIA_WIDGET = 403, MBT.API.Widgets.GAME_PLAY_BY_PLAY_WIDGET = 404, MBT.API.Widgets.LEAGUE_LIST_WITH_REGIONS_WIDGET = 500, MBT.API.Widgets.LEAGUE_RESULTS_VIEW_WIDGET = 501, MBT.API.Widgets.LEAGUE_TRANSFER_LIST_WIDGET = 502, MBT.API.Widgets.ALL_LEAGUES_SCHEDULE_WIDGET = 510, MBT.API.Widgets.TODAYS_GAMES_WIDGET = 511, MBT.API.Widgets.CALENDAR_SCHEDULE_WIDGET = 520, MBT.API.Widgets.LEAGUE_SELECTOR_WIDGET = 530, MBT.API.Widgets.COMPETITION_HISTORY_WIDGET = 550, MBT.API.Widgets.STATISTICS_PLAYERS_WIDGET = 600, MBT.API.Widgets.STATISTICS_TEAMS_WIDGET = 601, MBT.API.Widgets.STATISTICS_COACHES_WIDGET = 602, MBT.API.Widgets.STATISTICS_SCORING_WIDGET = 603, MBT.API.Widgets.STATISTICS_EFF_LEADERS_WIDGET = 604, MBT.API.Widgets.SELECTED_LEADERS_WIDGET = 605, MBT.API.Widgets.RECORDS_PLAYERS_WIDGET = 700, MBT.API.Widgets.RECORDS_TEAMS_WIDGET = 701, MBT.API.Widgets.CLUB_FULL_VIEW_WIDGET = 800, MBT.API.Widgets.ARENA_FULL_WIDGET = 810, MBT.API.Widgets.SEARCH_SIMPLE_WIDGET = 900, MBT.API.Widgets.PLAYOFF_SMALL_WIDGET = 1e3, MBT.API.Widgets.PLAYOFF_LARGE_WIDGET = 1001, MBT.API.Widgets.ENROLLMENT_LIST_WIDGET = 2001, MBT.API.Widgets.DISTRIBUTION_CLUB_WIDGET = 3e3, MBT.API.Widgets.NT_TEAM_FULL_WIDGET = 4e3, MBT.API.Widgets.NT_FRONTPAGE_FULL_WIDGET = 4010, MBT.API.Widgets.NT_FRONTPAGE_COMPACT_WIDGET = 4011, MBT.API.Widgets.NT_STATISTICS_PLAYER_WIDGET = 4100, MBT.API.Widgets.NT_TOURNAMENT_WIDGET = 4200, MBT.API.Widgets.NT_GAMES_OVERVIEW_WIDGET = 4300, MBT.API.Widgets.NT_GAME_CARD_WIDGET = 4400, MBT.API.Widgets.NT_GAMES_AND_TOURNAMENTS_WIDGET = 4500, MBT.API.Widgets.NT_PLAYER_WIDGET = 4600, MBT.API.Widgets.NT_PLAYER_LIST_WIDGET = 4650, MBT.API.Widgets.GAMES_REFEREES_AND_HOST_TEAMS_WIDGET = 5e3, MBT.API.Widgets.GAMES_SCROLLER_WIDGET = 5100, MBT.API.Widgets.WINNING_PERCENTAGE = 6e3, MBT.API.Widgets.ATTENDANCE_FULL = 6200, MBT.API.Widgets.ATTENDANCE_TOP5 = 6201, MBT.API.Widgets.GraphsLoaded = !1, MBT.API.Widgets.HtmlLiveLoaded = !1, MBT.API.Widgets.LogStats = !0, MBT.API.Widgets.ChartJsLoaded = !1, MBT.API.Cookies.NAMESPACE = "mbt-widget-app", MBT.API.Utils.addCSS = function(a) {
    var b = {
            rel: "stylesheet",
            type: "text/css",
            media: "all"
        },
        c = document.createElement("link");
    c.setAttribute("rel", b.rel), c.setAttribute("type", b.type), c.setAttribute("media", b.media), c.setAttribute("href", a + "&v=143"), document.getElementsByTagName("head")[0].appendChild(c)
}, MBT.API.Utils.addScript = function(a, b) {
    "undefined" != typeof MBT.API.debug && (a += "&d=" + Math.random());
    var c = document.createElement("script");
    c.setAttribute("type", "text/javascript"), c.setAttribute("language", "javascript"), "undefined" != typeof b && (c.onreadystatechange = function() {
        "loaded" == c.readyState && b()
    }, c.onload = b), c.setAttribute("src", a);
    var d = document.getElementsByTagName("head")[0].appendChild(c);
    MBT.API.Event.observe(d, "load", MBT.API.Function.createDelegate(function(a, b) {
        try {
            document.getElementsByTagName("head")[0].removeChild(b)
        } catch (a) {}
    }, null, [d], 1))
}, MBT.API.Utils.loadCharts = function(a) {
    MBT.API.Widgets.GraphsLoaded ? a() : (MBT.API.Utils.addScript(MBT.API.ServiceUrl + "static/scripts/amcharts/flash/swfobject.js?v=2", function() {
        MBT.API.Utils.addScript(MBT.API.ServiceUrl + "static/scripts/amcharts/amcharts.js?v=2", function() {
            MBT.API.Utils.addScript(MBT.API.ServiceUrl + "static/scripts/amcharts/amfallback.js?v=2", function() {
                MBT.API.Utils.addScript(MBT.API.ServiceUrl + "static/scripts/amcharts/raphael.js?v=2", a)
            })
        })
    }), MBT.API.Widgets.GraphsLoaded = !0)
}, MBT.API.Utils.loadChartsV2 = function(a) {
    MBT.API.Widgets.GraphsLoaded ? a() : (MBT.API.Utils.addScript(MBT.API.ServiceUrl + "static/scripts/amcharts/amcharts.js?v=2", function() {
        MBT.API.Utils.addScript(MBT.API.ServiceUrl + "static/scripts/amcharts/raphael.js?v=2", a)
    }), MBT.API.Widgets.GraphsLoaded = !0)
}, MBT.API.Utils.loadChartJs = function(a) {
    MBT.API.Widgets.ChartJsLoaded ? a() : (MBT.API.Utils.addScript(MBT.API.ServiceUrl + "static/scripts/external/ChartJs/dist/Chart.min.js", function() {
        a()
    }), MBT.API.Widgets.ChartJsLoaded = !0)
}, MBT.API.Utils.loadHtmlLive = function(a) {
    var b = 2;
    MBT.API.Widgets.HtmlLiveLoaded ? a() : (MBT.API.Utils.addCSS("http://beta.mbt.lt/~oskaras/live/static/styles/style_default.css?v=" + b), MBT.API.Utils.addScript("http://mbt.baskethotel.com/live/static/scripts/json2.js?v=" + b, function() {
        MBT.API.Utils.addScript("http://mbt.baskethotel.com/live/static/scripts/mbtjs.js?v=" + b, function() {
            MBT.API.Utils.addScript("http://mbt.baskethotel.com/live/static/scripts/live.js?v=" + b, a)
        })
    }), MBT.API.Widgets.HtmlLiveLoaded = !0)
}, MBT.API.Utils.getObjectKeys = function(a) {
    var b = [];
    for (var c in a) b.push(c);
    return b
}, MBT.API.Utils.getHtmlSelectValues = function(a) {
    var b = [],
        c = document.getElementById(a).options;
    if ("undefined" == typeof c) return [];
    for (i = 0; i < c.length; i++) b.push(c[i].value);
    return b
}, MBT.API.Utils.valueExistsInHtmlSelect = function(a, b) {
    var c = MBT.API.Utils.getHtmlSelectValues(a);
    for (i = 0; i < c.length; i++)
        if (c[i] == b) return !0;
    return !1
}, MBT.API.logStat = function(a, b) {
    if (MBT.API.Widgets.LogStats) {
        var c = MBT.API.URL.mergeParams(MBT.API.StatisticsUrl, {
                api: MBT.API.ApiId,
                w: a
            }),
            d = document.createElement("iframe");
        d.setAttribute("src", c), d.style.display = "none", document.getElementById(b) && document.getElementById(b).appendChild(d)
    }
}, MBT.API.get = function(a) {
    return "object" == typeof a ? a : document.getElementById(a)
}, MBT.API.isElement = function(a) {
    return "object" == typeof a && 1 == a.nodeType
}, MBT.API.isUndefined = function(a) {
    return "undefined" == typeof a
}, MBT.API.isArray = function(a) {
    return null != a && "object" == typeof a && "splice" in a && "join" in a
}, MBT.API.Browser = {
    IE: !(!window.attachEvent || navigator.userAgent.indexOf("Opera") !== -1),
    Opera: navigator.userAgent.indexOf("Opera") > -1,
    WebKit: navigator.userAgent.indexOf("AppleWebKit/") > -1,
    Gecko: navigator.userAgent.indexOf("Gecko") > -1 && navigator.userAgent.indexOf("KHTML") === -1,
    MobileSafari: !!navigator.userAgent.match(/Apple.*Mobile.*Safari/)
}, MBT.API.toJSON = function(a) {
    var b = typeof a;
    switch (b) {
        case "undefined":
        case "function":
        case "unknown":
            return;
        case "boolean":
            return a ? 1 : 0
    }
    if (null === a) return "null";
    if (a.toJSON) return a.toJSON();
    if (!MBT.API.isElement(a)) {
        var c = [];
        for (var d in a) {
            var e = MBT.API.toJSON(a[d]);
            MBT.API.isUndefined(e) || c.push(d.toJSON() + ": " + e)
        }
        return "{" + c.join(", ") + "}"
    }
}, MBT.API.extend = function(a, b) {
    for (var c in b.prototype) a.prototype[c] = b[c];
    return a
}, MBT.API.update = function(a, b) {
    var c = MBT.API.get(a);
    if (null == c) throw "Container element is empty " + b;
    b.indexOf("<form") > -1 && MBT.API.isInForm(c) && (b = b.replace(/<form[^>]*>/g, "").replace(/<\/form>/g, ""));
    var d = new RegExp("^<option.*/option>", "gi");
    if (d.test(b.replace(/^\s+|\s+$/g, ""))) {
        var d = new RegExp("<option.*/option>", "gi");
        for (c.options.length = 0; o = d.exec(b);) {
            var e = o[0],
                f = new RegExp('value="([0-9]*)"', "gi"),
                g = f.exec(e);
            if (g) var h = g[1];
            else var h = "";
            var i = new RegExp("<option.*>(.*)</option>", "gi"),
                j = i.exec(e);
            if (j) var k = j[1];
            else var k = "";
            var l = document.createElement("option");
            l.text = k, l.value = h, c.options.add(l)
        }
    } else c.innerHTML = b;
    b.evalScripts()
}, MBT.API.isInForm = function(a) {
    return !!a && ("FORM" == a.tagName || "BODY" != a.tagName && MBT.API.isInForm(a.parentNode))
}, MBT.API.triggerError = function(a) {
    "object" == typeof console && console.log(a)
}, MBT.API.addApiCSS = function() {
    if (("undefined" == typeof MBT.API.apiCSSLoaded || !MBT.API.apiCSSLoaded) && MBT.API.addCSS) {
        for (var a = MBT.API.getApiCSSUrl(), b = document.getElementsByTagName("link"), c = !1, d = 0; d < b.length; d++)
            if (b[d].href == a) {
                c = !0;
                break
            }
        c || (MBT.API.Utils.addCSS(a), MBT.API.apiCSSLoaded = !0)
    }
}, MBT.API.getApiCSSUrl = function() {
    var a = MBT.API.ApiId;
    if ("undefined" != typeof MBT.API.Widgets.CssApi) var a = MBT.API.Widgets.CssApi;
    var b = MBT.API.ApiCSSUrl + "?api=" + a + "&index=" + MBT.API.stylesheetIndex;
    return "undefined" != typeof mbtBasketHotelVersion && (b += "&ver=" + mbtBasketHotelVersion), b
}, MBT.API.Widgets.LOADING_IMG_LARGE = MBT.API.ServiceUrl + "static/images/loading-large.gif", MBT.API.Widgets.STATE_URL_PREFIX = "mbt:", "undefined" == typeof MBT.API.Lang && (MBT.API.Lang = "en"), "undefined" == typeof MBT.API.NewNav && (MBT.API.NewNav = 0), "undefined" == typeof MBT.API.NavObject && (MBT.API.NavObject = 0), MBT.API.Widgets.Widget = function(a) {
    if (this.container = null, this.width = null, this.height = null, this.widgetId = null, this.state = null, this.partial = null, this.callback = null, this.showLoadingIndicator = !0, this.params = {}, "object" == typeof a)
        for (var b in a) "undefined" != typeof this[b] && (this[b] = a[b]);
    return this
}, MBT.API.Widgets.Widget.prototype.setContainer = function(a) {
    this.container = a, this.__init()
}, MBT.API.Widgets.Widget.prototype.setCallback = function(a) {
    this.callback = a
}, MBT.API.Widgets.Widget.prototype.setWidgetId = function(a) {
    this.widgetId = a, this.__init()
}, MBT.API.Widgets.Widget.prototype.setParam = function(a, b) {
    null != b && "undefined" != typeof b && ("string" != typeof b || "undefined" != b && "null" != b) && (this.params[a] = b)
}, MBT.API.Widgets.Widget.prototype.__init = function() {
    var a = MBT.API.get(this.container);
    if (null != a) {
        var b = this.widgetId;
        null != b && a.setAttribute("mbt_widget", b)
    }
}, MBT.API.Widgets.Widget.prototype.setPartial = function(a) {
    this.partial = a
}, MBT.API.Widgets.Widget.prototype.render = function() {
    var a = new MBT.API.Widgets.RenderRequest;
    a.addWidget(this), a.render()
}, MBT.API.Widgets.Widget.prototype.renderPartial = function(a) {
    this.setPartial(a);
    var b = new MBT.API.Widgets.RenderRequest;
    b.addWidget(this), b.render()
}, MBT.API.Widgets.RenderRequest = function() {
    this.widgets = []
}, MBT.API.Widgets.RenderRequest.prototype.addWidget = function(a) {
    MBT.API.translationsLang && a.setParam("translations_lang", MBT.API.translationsLang), MBT.API.translationsSetId && a.setParam("translations_set_id", MBT.API.translationsSetId), this.widgets.push(a)
}, MBT.API.Widgets.RenderRequest.prototype.setOutputFormat = function(a) {
    this.outputFormat = a
}, MBT.API.Widgets.RenderRequest.prototype.serialize = function() {
    var a = "";
    this.outputFormat && (a += "&output_format=" + this.outputFormat);
    for (var b = 0; b < this.widgets.length; b++) {
        widget = this.widgets[b];
        var c = MBT.API.get(widget.container);
        a += "&request[" + b + "][container]=" + c.id, a += "&request[" + b + "][widget]=" + widget.widgetId, widget.partial && (a += "&request[" + b + "][part]=" + widget.partial), widget.state && (a += "&request[" + b + "][state]=" + widget.state), widget.callback && (a += "&request[" + b + "][callback=" + widget.callback), widget.params && (a += MBT.API.URL.serialize("request[" + b + "][param]", widget.params))
    }
    return MBT.API.Base64.encode(a)
}, MBT.API.Widgets.RenderRequest.prototype.render = function() {
    if (!MBT.API.Widgets.RenderRequest.disableRender) {
        if ("undefined" == typeof MBT.API.ApiId) throw "MBT.API.ApiId could not be empty";
        if ("" == MBT.API.Lang) throw "MBT.API.Lang could not be empty";
        this.widgets[0].params.api && (MBT.API.ApiId = this.widgets[0].params.api);
        var a = MBT.API.WidgetUrl;
        a = MBT.API.URL.mergeParams(a, {}), this.outputFormat && (a += "&output_format=" + this.outputFormat);
        for (var b = !1, c = 0; c < this.widgets.length; c++) {
            widget = this.widgets[c];
            var d = MBT.API.get(widget.container);
            if (null == d) throw "Assign output container using setContainer(...)";
            widget.partial && (b = !0), widget.showLoadingIndicator && MBT.API.Widgets.showLoading(d), a += "&request[" + c + "][container]=" + d.id, a += "&request[" + c + "][widget]=" + widget.widgetId, widget.partial && (a += "&request[" + c + "][part]=" + widget.partial), widget.state && (a += "&request[" + c + "][state]=" + widget.state), widget.callback && (a += "&request[" + c + "][callback]=" + widget.callback), null != MBT.API.imageServiceUrl && (widget.params.image_service_url = MBT.API.imageServiceUrl), widget.params && (a += MBT.API.URL.serialize("request[" + c + "][param]", widget.params))
        }
        "undefined" == typeof MBT.API.Widgets.RenderRequest.initialRequestState && (MBT.API.Widgets.RenderRequest.initialRequestState = this.serialize()), MBT.API.Utils.addScript(a), MBT.API.logStat(this.widgets.length, this.widgets[0].container)
    }
}, MBT.API.Widgets.showLoading = function(a) {
    a = MBT.API.get(a), "IFRAME" != a.tagName && (a.innerHTML = '<img class="mbt-loader-center" src="' + MBT.API.Widgets.LOADING_IMG_LARGE + '" />')
}, MBT.API.Widgets.RenderRequest.renderFromState = function(a) {
    var b = MBT.API.Base64.decode(a),
        c = MBT.API.WidgetUrl;
    c = MBT.API.URL.mergeParams(c, {}), c += b, MBT.API.Utils.addScript(c)
}, MBT.API.Widgets.findWidgetContainer = function(a) {
    a = MBT.API.get(a);
    do {
        if (null != a.getAttribute("mbt_widget")) return a;
        a = a.parentNode
    } while (null != a && a.getAttribute);
    return a
}, MBT.API.Widgets.Graph = function(a) {
    if (this.container = null, this.width = null, this.height = null, this.widgetId = null, this.partial = "graph", this.settingsPartial = null, this.params = {}, "object" == typeof a)
        for (var b in a) "undefined" != typeof this[b] && (this[b] = a[b]);
    return this
}, MBT.API.Widgets.Graph.prototype.setContainer = function(a) {
    this.container = a
}, MBT.API.Widgets.Graph.prototype.setWidgetId = function(a) {
    this.widgetId = a
}, MBT.API.Widgets.Graph.prototype.setParam = function(a, b) {
    this.params[a] = b
}, MBT.API.Widgets.Graph.prototype.setWidgetId = function(a) {
    this.widgetId = a
}, MBT.API.Widgets.Graph.prototype.setPartial = function(a) {
    this.partial = a
}, MBT.API.Widgets.Graph.prototype.setSettingsPartial = function(a) {
    this.settingsPartial = a
}, MBT.API.Widgets.Graph.prototype.render = function() {
    if ("undefined" == typeof MBT.API.ApiId) throw "MBT.API.ApiId could not be empty";
    if ("" == MBT.API.Lang) throw "MBT.API.Lang could not be empty";
    "undefined" == typeof this.params.width && (this.params.width = "400"), "undefined" == typeof this.params.height && (this.params.height = "250");
    var a = MBT.API.WidgetUrl;
    a = MBT.API.URL.mergeParams(a, {
        output_format: "graph"
    });
    var b = MBT.API.get(this.container);
    if (null == b) throw "Assign output container using setContainer(...)";
    MBT.API.Widgets.showLoading(b), a += "&request[0][container]=" + b.id, a += "&request[0][widget]=" + this.widgetId, this.partial && (a += "&request[0][part]=" + this.partial), this.params && (a += MBT.API.URL.serialize("request[0][param]", this.params)), b.style.border = "0px", b.width = this.params.width + "px", b.height = this.params.height + "px", b.scrolling = "no", b.src = a, MBT.API.logStat(1)
}, MBT.API.URL.openExternal = function(a) {
    document.location.href = a
}, MBT.API.URL.loadContainer = function(a, b) {
    a = MBT.API.URL.mergeParams(a, {
        container: b
    }), MBT.API.Utils.addScript(a)
}, MBT.API.URL.mergeParams = function(a, b) {
    a.indexOf("?") == -1 && (a += "?"), a += "&api=" + MBT.API.ApiId, a += "&lang=" + MBT.API.Lang, a += "&nnav=" + MBT.API.NewNav, a += "&nav_object=" + MBT.API.NavObject, MBT.API.ShowSurnameFirst && (a += "&surname_first=1"), MBT.API.FirstNamePriority && (a += "&first_name_priority=1"), MBT.API.haveFlash() || (a += "&flash=0");
    for (var c in b) {
        typeof b[c];
        a += "&" + c + "=" + b[c]
    }
    return a
}, MBT.API.URL.serialize = function(a, b) {
    var c = "",
        d = typeof b;
    if ("object" == d)
        for (var e in b) "function" != typeof b[e] && (c += MBT.API.URL.serialize(a + "[" + e + "]", b[e]));
    else if ("array" == d)
        for (var e = 0; e < b.length; e++) c += "&" + a + "[" + e + "]=" + b;
    else c = "&" + a + "=" + b;
    return c
}, MBT.API.URL.QueryString = function(a) {
    if (this.params = {}, null == a && (a = location.search.substring(1, location.search.length)), 0 != a.length) {
        a = a.replace(/\+/g, " ");
        for (var b = a.split("&"), c = 0; c < b.length; c++) {
            var d = b[c].split("="),
                e = decodeURIComponent(d[0]),
                f = 2 == d.length ? decodeURIComponent(d[1]) : e;
            this.params[e] = f
        }
    }
}, MBT.API.URL.QueryString.prototype.get = function(a, b) {
    var c = this.params[a];
    return null != c ? c : b
}, MBT.API.URL.QueryString.prototype.contains = function(a) {
    var b = this.params[a];
    return null != b
}, MBT.API.Function.createDelegate = function(a, b, c, d) {
    var e = a;
    return function() {
        var a = c || arguments;
        if (d !== !0) {
            if ("number" == typeof d) {
                a = Array.prototype.slice.call(arguments, 0);
                var f = [d, 0].concat(c);
                Array.prototype.splice.apply(a, f)
            }
            return e.apply(b || window, a)
        }
        a = Array.prototype.slice.call(arguments, 0), a = a.concat(c)
    }
}, MBT.API.Event.observe = function(a, b, c) {
    return element = "object" == typeof a ? a : MBT.API.get(a), element && (element.addEventListener ? element.addEventListener(b, c, !1) : element.attachEvent("on" + b, c)), element
}, MBT.API.Event.stopObserve = function(a, b, c) {
    element = "object" == typeof a ? a : MBT.API.get(a), a.removeEventListener ? a.removeEventListener(b, c, !1) : a.detachEvent && a.detachEvent("on" + b, c)
}, MBT.API.Event.pointer = function(a) {
    var b = document.documentElement,
        c = document.body || {
                scrollLeft: 0,
                scrollTop: 0
            };
    return {
        x: a.pageX || a.clientX + (b.scrollLeft || c.scrollLeft) - (b.clientLeft || 0),
        y: a.pageY || a.clientY + (b.scrollTop || c.scrollTop) - (b.clientTop || 0)
    }
}, MBT.API.Event.pointerX = function(a) {
    return MBT.API.Event.pointer(a).x
}, MBT.API.Event.pointerY = function(a) {
    return MBT.API.Event.pointer(a).y
}, MBT.API.Event.getEvent = function(a) {
    return a || window.event
}, MBT.API.Event.getTargetObject = function(a) {
    return a.target ? a.target : a.srcElement ? a.srcElement : window.event.srcElement
}, MBT.API.UI.TabPanel = function(a) {
    if (this.parentInstanceId = a.parentInstanceId ? a.parentInstanceId : null, this.container = a.container ? a.container : null, this.activeTabClass = a.activeTabClass ? a.activeTabClass : "active", this.tabClass = a.tabClass ? a.tabClass : "", this.hoverTabClass = a.hoverTabClass ? a.hoverTabClass : "", this.noCache = a.noCache ? a.noCache : null, this.rotatorHandler = 0, this.rotatorMiliseconds = a.rotatorMiliseconds ? a.rotatorMiliseconds : 0, this.cancelActiveTabEvent = !!a.cancelActiveTabEvent && a.cancelActiveTabEvent, this.activeTab = -1, activeTab = void 0 != typeof a.activeTab ? a.activeTab : null, this.tabs = a.tabs ? a.tabs : [], this.indexInController = a.indexInController ? a.indexInController : 0, this.tabs)
        for (var b = 0; b < this.tabs.length; b++) {
            var c = MBT.API.Function.createDelegate(this.setActiveTab, this, [b], 0);
            if (MBT.API.Event.observe(this.tabs[b].tabId, "click", c), this.hoverTabClass) {
                var c = MBT.API.Function.createDelegate(this.mouseOverTab, this, [b], 0);
                MBT.API.Event.observe(this.tabs[b].tabId, "mouseover", c);
                var c = MBT.API.Function.createDelegate(this.mouseOutTab, this, [b], 0);
                MBT.API.Event.observe(this.tabs[b].tabId, "mouseout", c)
            }
        }
    this.loadedCache = {}, this.tabs && null != activeTab && this.setActiveTab(activeTab), this.rotatorMiliseconds > 0 && this.startRotator(this.rotatorMiliseconds)
}, MBT.API.UI.TabPanel.prototype.setActiveTab = function(a, b, c, d) {
    if (this.stopRotator(), this.cancelActiveTabEvent && b && b.preventDefault(), "undefined" == typeof c && (c = !1), "undefined" != typeof this.tabs[a]) {
        if (!d && this.activeTab == a) return;
        var e = this.tabs[a];
        if (this.activeTab != -1) {
            var f = this.tabs[this.activeTab],
                g = MBT.API.get(this.container).innerHTML;
            this.loadedCache[this.activeTab] = g, MBT.API.get(this.container).innerHTML = "", MBT.API.get(f.tabId).className = this.tabClass
        }
        MBT.API.get(e.tabId).className = this.activeTabClass, MBT.API.get(e.tabId).setAttribute("prevClassName", this.activeTabClass), this.activeTab = a, this.noCache || "undefined" == typeof this.loadedCache[a] ? e.needLoad(this, a) : MBT.API.update(this.container, this.loadedCache[a]), null == this.parentInstanceId || c || (MBT.API.History.setParam(this.parentInstanceId, MBT.API.History.tabIndex, this.indexInController, a), MBT.API.History.manualHashChange = !0)
    }
}, MBT.API.UI.TabPanel.prototype.clearCache = function() {
    for (key in this.loadedCache) delete this.loadedCache[key]
}, MBT.API.UI.TabPanel.prototype.mouseOverTab = function(a) {
    if ("undefined" != typeof this.tabs[a]) {
        var b = MBT.API.get(this.tabs[a].tabId);
        b.setAttribute("prevClassName", b.className), b.className = this.hoverTabClass
    }
}, MBT.API.UI.TabPanel.prototype.mouseOutTab = function(a) {
    if ("undefined" != typeof this.tabs[a]) {
        var b = MBT.API.get(this.tabs[a].tabId);
        b.getAttribute("prevClassName") && (b.className = b.getAttribute("prevClassName"))
    }
}, MBT.API.UI.TabPanel.prototype.startRotator = function(a) {
    this.rotatorMiliseconds = a, this.rotatorHandler = setTimeout(MBT.API.Function.createDelegate(this.rotateTab, this), a)
}, MBT.API.UI.TabPanel.prototype.stopRotator = function() {
    this.rotatorHandler > 0 && clearTimeout(this.rotatorHandler)
}, MBT.API.UI.TabPanel.prototype.rotateTab = function() {
    var a = this.activeTab + 1;
    a >= this.tabs.length && (a = 0), this.setActiveTab(a), this.rotatorHandler = setTimeout(MBT.API.Function.createDelegate(this.rotateTab, this), this.rotatorMiliseconds)
}, MBT.API.UI.Select = {}, MBT.API.UI.Select.clearOptions = function(a) {
    MBT.API.get(a).innerHTML = ""
}, MBT.API.UI.Select.addOptions = function(a, b) {
    var a = MBT.API.get(a);
    if (a && b)
        for (var c = 0; c < b.length; c++) {
            var d = document.createElement("OPTION");
            d.innerHTML = b[c].text, d.value = b[c].id, a.appendChild(d), b[c].selected && (a.selectedIndex = c + 1)
        }
}, "undefined" == typeof String.prototype.evalScripts && (String.prototype.extractScripts = function() {
    for (var a = "<script[^>]*>([\\S\\s]*?)</script>", b = new RegExp(a, "img"), c = new RegExp(a, "im"), d = this.match(b) || [], e = [], f = 0; f < d.length; f++) {
        var g = (d[f].match(c) || ["", ""])[1];
        g && e.push(g)
    }
    return e
}, String.prototype.evalScripts = function() {
    for (var scripts = this.extractScripts(), i = 0; i < scripts.length; i++) eval(scripts[i])
}), MBT.API.UI.getFormElementValue = function(a) {
    var b = typeof a;
    if ("undefined" == b) return null;
    if ("function" != b || 0 != a.length) {
        if (MBT.API.isElement(a) && a.tagName) return a.value;
        if ("object" != b && "function" != b || !a.length) return "string" == b ? MBT.API.UI.getFormElementValue(MBT.API.get(a)) : null;
        for (var c = [], d = 0; d < a.length; d++) {
            var e = MBT.API.UI.getFormInputElementValue(a[d]);
            null != e && c.push(e)
        }
        return c
    }
}, MBT.API.UI.getFormInputElementValue = function(a) {
    switch (a.tagName) {
        case "INPUT":
            return "checkbox" == a.type || "radio" == a.type ? a.checked ? a.value : null : a.value;
        case "SELECT":
            return a.value;
        case "TEXTAREAN":
            return a.innerHTML
    }
}, MBT.API.UI.show = function(a) {
    MBT.API.get(a).style.display = ""
}, MBT.API.UI.hide = function(a) {
    MBT.API.get(a).style.display = "none"
}, MBT.API.UI.createWidgetContainer = function(a) {
    if ("undefined" == typeof a)
        do a = "mbt-widget-container-" + Math.round(1e4 * Math.random()); while (null != MBT.API.get(a));
    var b = document.createElement("div");
    return b.id = a, b.setAttribute("mbt_widget", 1), document.body.appendChild(b), b
}, MBT.API.SafeNames = {}, MBT.API.SafeNames.Values = {}, MBT.API.SafeNames.Container = function(a) {
    return "undefined" == typeof MBT.API.SafeNames.Values[a] && (MBT.API.SafeNames.Values[a] = {}), MBT.API.SafeNames.Values[a]
}, MBT.API.History.enabled = !0, MBT.API.History.hashPathSeparator = "/", MBT.API.History.hashTypeSeparator = ":", MBT.API.History.hashTypePrefix = "$", MBT.API.History.hashParamSeparator = "&", MBT.API.History.tabIndex = "t", MBT.API.History.filterIndex = "f", MBT.API.History.pageIndex = "p", MBT.API.History.manualHashChange = !1, MBT.API.History.lastState = {};
MBT.API.History.notFound = {};
MBT.API.History.init = function() {
    this.hiddenElementName = "mbt-widget-history-container";
    var a = this.getHashParams(),
        b = null,
        c = null;
    for (var e in a) b = e.split(this.hashPathSeparator), b[1] == this.tabIndex ? (c = MBT.API.SafeNames.Container(b[0]), c.initConfig = c.initConfig || {}, c.initConfig.tabs = c.initConfig.tabs || [], c.initConfig.tabs[b[2]] = {
        activeTab: a[e]
    }) : b[1] == this.filterIndex ? (c = MBT.API.SafeNames.Container(b[0]), c.initConfig = c.initConfig || {}, c.initConfig.filters = c.initConfig.filters || [], c.initConfig.filters[b[2]] = {
        value: a[e]
    }) : b[1] == this.pageIndex && (c = MBT.API.SafeNames.Container(b[0]), c.initConfig = c.initConfig || {}, c.initConfig.pages = c.initConfig.pages || [], c.initConfig.pages[b[2]] = {
            page: a[e]
        });
    this.lastState = a
}, MBT.API.History.onDocumentLoad = function() {
    MBT.API.Browser.IE ? (this.iframe = document.createElement("iframe"), this.iframe.name = this.hiddenElementName, this.iframe.id = this.hiddenElementName, this.iframe.src = "", this.iframe.width = "0", this.iframe.height = "0", this.iframe.style.visibility = "hidden", document.body.appendChild(this.iframe)) : (this.hiddenField = document.createElement("hidden"), this.hiddenField.id = this.hiddenElementName, document.body.appendChild(this.hiddenField)), this.tickObserver()
}, MBT.API.History.stateRestoreParams = {}, MBT.API.History.handleStateChange = function(a) {
    this.stateRestoreParams = this.getChangesInHashParams(this.getHashParams(), this.lastState), this.restoreWidgetsState()
}, MBT.API.History.getChangesInHashParams = function(a, b) {
    var d = {};
    for (c in a) "undefined" != typeof b[c] ? a[c] == b[c] || (d[c] = a[c], delete b[c]) : d[c] = a[c];
    for (l in b) "undefined" == typeof a[l] ? d[l] = "0" : d[l] = b[l];
    return d
}, MBT.API.History.restoreWidgetsState = function() {
    var a = MBT.API.History.stateRestoreParams,
        b = null,
        c = null,
        d = !1;
    for (p in a)
        if (b = p.split(this.hashPathSeparator), b[1] == this.tabIndex) {
            if ("undefined" == typeof MBT.API.SafeNames.Values[b[0]]) {
                setTimeout("MBT.API.History.restoreWidgetsState()", 1e3);
                continue
            }
            if (c = MBT.API.SafeNames.Container(b[0]), "undefined" == typeof c.tabController) {
                setTimeout("MBT.API.History.restoreWidgetsState()", 1e3);
                continue
            }
            "object" == typeof c.tabController[b[2]] ? (c.tabController[b[2]].setActiveTab(a[p]), delete this.lastState[p], delete MBT.API.History.stateRestoreParams[p]) : setTimeout("MBT.API.History.restoreWidgetsState()", 1e3)
        } else if (b[1] == this.filterIndex) {
            if ("undefined" == typeof MBT.API.SafeNames.Values[b[0]]) {
                setTimeout("MBT.API.History.restoreWidgetsState()", 1e3);
                continue
            }
            c = MBT.API.SafeNames.Container(b[0]);
            var e = b[0] + "-filter-" + b[2];
            if (document.getElementById(e))("SELECT" != document.getElementById(e).tagName || MBT.API.Utils.valueExistsInHtmlSelect(e, a[p])) && (document.getElementById(b[0] + "-filter-" + b[2]).value = a[p], delete this.lastState[p], delete MBT.API.History.stateRestoreParams[p], d = !0);
            else if (document.getElementsByName(e) && document.getElementsByName(e).length > 0) {
                var f = document.getElementsByName(e);
                for (i = 0; i < f.length; i++) f[i].value == a[p] ? f[i].checked = !0 : f[i].checked = !1
            } else setTimeout("MBT.API.History.restoreWidgetsState()", 1e3)
        } else if (b[1] == this.pageIndex) {
            if ("undefined" == typeof MBT.API.SafeNames.Values[b[0]]) {
                setTimeout("MBT.API.History.restoreWidgetsState()", 1e3);
                continue
            }
            c = MBT.API.SafeNames.Container(b[0]), "undefined" != typeof c.setPage && (c.setPage(null, a[p]), delete this.lastState[p], delete MBT.API.History.stateRestoreParams[p])
        }
    d && "undefined" != typeof c.applyFilter && c.applyFilter(null, !1)
}, MBT.API.History.getNotAppliedNavigation = function(a) {
    var b = {};
    return b = a == this.tabIndex ? this.notFound[this.tabIndex] : a == this.filterIndex ? this.notFound[this.filterIndex] : a == this.pageIndex ? this.notFound[this.pageIndex] : this.notFound
}, MBT.API.History.removeFromNotAppliedNavigation = function(a, b) {
    try {
        delete this.notFound[a][b]
    } catch (a) {}
}, MBT.API.History.setParam = function(a, b, c, d) {
    var c = a + this.hashPathSeparator + b + this.hashPathSeparator + c,
        e = this.getHashParams();
    e[c] = d, hash = MBT.API.Widgets.STATE_URL_PREFIX + this.implodeHashParams(e), hash == MBT.API.Widgets.STATE_URL_PREFIX && (hash = ""), (window.location.hash || "" !== hash.replace("#", "")) && ("" == hash ? window.location.hash = "stf" : window.location.hash = hash), this.manualHashChange = !0, this.previousHash = this.currentHash, MBT.API.Browser.IE ? hash != window.location.hash.substring(1) && this.setHashOnIframe(hash) : this.setHashOnHidden(hash)
}, MBT.API.History.getHashParams = function() {
    var a = window.location.hash.substring(1),
        b = MBT.API.Widgets.STATE_URL_PREFIX.length,
        c = null;
    return a.substring(0, b) != MBT.API.Widgets.STATE_URL_PREFIX || b >= a.length ? c = {} : (a = a.substring(b), c = this.explodeHashParams(a)), c
}, MBT.API.History.explodeHashParams = function(a) {
    for (var b = a.split(this.hashTypeSeparator), c = "", d = "", e = {}, f = 0, g = b.length; f < g; f++) {
        var h = b[f].split(this.hashParamSeparator);
        if (h[0].substring(0, 1) == this.hashTypePrefix) d = h[0].substring(1);
        else {
            var i = h[0].split(this.hashTypePrefix);
            c = i[0], d = i[1]
        }
        for (var j = 0, k = h.length; j < k; j++) {
            var l = h[j].split("=");
            "undefined" != unescape(l[1]) && (e[c + this.hashPathSeparator + d + this.hashPathSeparator + unescape(l[0])] = unescape(l[1]))
        }
    }
    return e
}, MBT.API.History.implodeHashParams = function(a) {
    for (var b = "", c = MBT.API.Utils.getObjectKeys(a).sort(), d = "", e = "", f = "", g = 0, h = c.length; g < h; g++) {
        var i = !1;
        null != a[c[g]] && "" != a[c[g]] && (d = c[g].split(this.hashPathSeparator), 3 == d.length && (e != d[0] && (b += this.hashTypeSeparator + d[0], e = d[0], i = !0, f = ""), f != d[1] && (i || (b += this.hashTypeSeparator), b += this.hashTypePrefix + d[1], f = d[1]), b += this.hashParamSeparator + escape(d[2]) + "=" + escape(a[c[g]])))
    }
    return b = b.substring(1)
}, MBT.API.History.update = function() {
    this.previousHash = this.currentHash;
    var a = window.location.hash.substring(1);
    this.currentHash = a
}, MBT.API.History.setHashOnIframe = function(a) {
    try {
        if (null == MBT.API.History.iframe || null == MBT.API.History.iframe.contentWindow || null == MBT.API.History.iframe.contentWindow.document || null == MBT.API.History.iframe.contentWindow.document.body) return void window.setTimeout("MBT.API.History.setHashOnIframe('" + a + "')", 200);
        var b = MBT.API.History.getHashOnIframe(),
            c = MBT.API.History.iframe.contentWindow.document;
        b != a && (c.open(), c.write('<html><body id="history">' + a + "</body></html>"), c.close())
    } catch (a) {
        throw a
    }
}, MBT.API.History.getHashOnIframe = function() {
    var a = MBT.API.History.iframe.contentWindow.document;
    return a && "history" == a.body.id ? a.body.innerText : ""
}, MBT.API.History.setHashOnHidden = function(a) {
    try {
        MBT.API.History.hiddenField.value = a
    } catch (a) {}
}, MBT.API.History.getHashOnHidden = function() {
    try {
        return MBT.API.History.hiddenField.value
    } catch (a) {}
}, MBT.API.History.isAltered = function() {
    var a = MBT.API.History.currentHash,
        b = MBT.API.History.previousHash;
    if (1 == this.manualHashChange) return this.manualHashChange = !1, !1;
    if ("undefined" == typeof this.previousHash) return !1;
    var c = a != b;
    return c && (this.lastState = this.explodeHashParams(b)), c
}, MBT.API.History.tickObserver = function() {
    setTimeout("MBT.API.History.observeChanges()", 50)
}, MBT.API.History.observeChanges = function() {
    MBT.API.History.update(), MBT.API.History.isAltered() && MBT.API.History.handleStateChange(MBT.API.History.currentHash), MBT.API.History.tickObserver()
}, MBT.API.History.handleStartup = function() {
    if (location.hash.length > 1) {
        var a = location.hash.substring(1);
        MBT.API.History.findWidgetState(a) !== !1 && MBT.API.Event.observe(window, "load", MBT.API.Function.createDelegate(function(a) {
            MBT.API.History.handleStateChange(a), MBT.API.Widgets.RenderRequest.initialRequestState = a, MBT.API.History.firstLoad = !1, MBT.API.Widgets.RenderRequest.disableRender = !1
        }, null, [a]))
    }
}, MBT.API.History.findWidgetState = function(a) {
    var b = MBT.API.Widgets.STATE_URL_PREFIX;
    return 0 == a.indexOf(b) && a.substring(b.length)
}, MBT.API.Storage.AppState = function() {}, MBT.API.Storage.AppState.prototype.get = function(a) {
    var b = MBT.API.URL.CurrentQueryString.get(a);
    return "string" == typeof b && "null" == b && (b = null), b
}, MBT.API.Storage.AppState.prototype.set = function(a, b) {
    MBT.API.Cookies.set(MBT.API.Cookies.NAMESPACE + a, b)
}, MBT.API.Storage.AppState.prototype.attachAppStateToExternalLinks = function(a, b, c) {
    var d = MBT.API.get(b);
    if (null != d) {
        for (var e = d.getElementsByTagName("a"), h = (MBT.API.Application.get("league_id"), MBT.API.Application.get("season_id"), []), i = 0; i < a.length; i++) {

            var j = MBT.API.Application.get(a[i]);
            null == j && null != c && c.length - 1 >= i && (j = c[i]), null != j && ("string" != typeof type || "undefined" != type && "null" != type) && h.push(a[i] + "=" + j);
        }
        if (h = h.join("&"), 0 != h.length)
            for (var i = 0; i < e.length; i++) {
                var k = e[i].href;
                k += k.indexOf("?") == -1 ? "?" : "&", k += h, e[i].href = k
            }
    }
}, MBT.API.Cookies.set = function(a, b, c) {
    if (c) {
        var d = new Date;
        d.setTime(d.getTime() + 1e3 * c);
        var e = "; expires=" + d.toGMTString()
    } else var e = "";
    document.cookie = a + "=" + b + e + "; path=/"
}, MBT.API.Cookies.get = function(a) {
    for (var b = a + "=", c = document.cookie.split(";"), d = 0; d < c.length; d++) {
        for (var e = c[d];
             " " == e.charAt(0);) e = e.substring(1, e.length);
        if (0 == e.indexOf(b)) return e.substring(b.length, e.length)
    }
    return null
}, MBT.API.Cookies.unset = function(a) {
    MBT.API.Cookies.set(a, "", -1)
}, MBT.API.Base64.keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=", MBT.API.Base64.encode = function(a) {
    a = escape(a);
    var c, d, f, g, h, b = "",
        e = "",
        i = "",
        j = 0;
    do c = a.charCodeAt(j++), d = a.charCodeAt(j++), e = a.charCodeAt(j++), f = c >> 2, g = (3 & c) << 4 | d >> 4, h = (15 & d) << 2 | e >> 6, i = 63 & e, isNaN(d) ? h = i = 64 : isNaN(e) && (i = 64), b = b + MBT.API.Base64.keyStr.charAt(f) + MBT.API.Base64.keyStr.charAt(g) + MBT.API.Base64.keyStr.charAt(h) + MBT.API.Base64.keyStr.charAt(i), c = d = e = "", f = g = h = i = ""; while (j < a.length);
    return b
}, MBT.API.Base64.decode = function(a) {
    var c, d, f, g, h, b = "",
        e = "",
        i = "",
        j = 0,
        k = /[^A-Za-z0-9\+\/\=]/g;
    if (k.exec(a)) throw "There were invalid base64 characters in the input text.\n";
    a = a.replace(/[^A-Za-z0-9\+\/\=]/g, "");
    do f = MBT.API.Base64.keyStr.indexOf(a.charAt(j++)), g = MBT.API.Base64.keyStr.indexOf(a.charAt(j++)), h = MBT.API.Base64.keyStr.indexOf(a.charAt(j++)), i = MBT.API.Base64.keyStr.indexOf(a.charAt(j++)), c = f << 2 | g >> 4, d = (15 & g) << 4 | h >> 2, e = (3 & h) << 6 | i, b += String.fromCharCode(c), 64 != h && (b += String.fromCharCode(d)), 64 != i && (b += String.fromCharCode(e)), c = d = e = "", f = g = h = i = ""; while (j < a.length);
    return unescape(b)
}, MBT.API.History.firstLoad = !0, MBT.API.History.handleStartup(), MBT.API.URL.CurrentQueryString = new MBT.API.URL.QueryString, MBT.API.Application = new MBT.API.Storage.AppState, MBT.API.History.init(), MBT.API.Event.observe(window, "load", function() {
    MBT.API.History.enabled && MBT.API.History.onDocumentLoad()
}), MBT.API.UI.Calendar = function(a, b) {
    this.value = "", this.format = "%Y-%m-%d", this.start_week_day = 1, this.iframe_obj = null, this.lib_path = "rich_calendar/", this.targetObj = a, this.showTime = b, this.userOnChangeHandler = null, this.userOncloseHandler = null, this.userOnAutocloseHandler = null, this.default_language = "en", this.language = "en", this.date = new Date, this.skin = "", this.auto_close = !0, this.value_el = null, this.position = null
}, MBT.API.UI.Calendar.attachEvents = function(a) {
    MBT.API.Event.observe(a, "click", MBT.API.UI.Calendar.onclick), MBT.API.Event.observe(a, "mouseover", MBT.API.UI.Calendar.onmouseover), MBT.API.Event.observe(a, "mouseout", MBT.API.UI.Calendar.onmouseout)
}, MBT.API.UI.Calendar.detach_events = function(a) {
    MBT.API.Event.stopObserve(a, "click", MBT.API.UI.Calendar.onclick), MBT.API.Event.stopObserve(a, "mouseover", MBT.API.UI.Calendar.onmouseover), MBT.API.Event.stopObserve(a, "mouseout", MBT.API.UI.Calendar.onmouseout)
}, MBT.API.UI.Calendar.onclick = function(a) {
    var c = (MBT.API.Event.getEvent(a), MBT.API.Event.getTargetObject(a));
    if (c) {
        var d = c.calendar,
            e = d.date.getFullYear(),
            f = d.date.getMonth(),
            g = d.date.getDate();
        switch (c.objectCode) {
            case "day":
                d.date.setDate(c.day_num);
                break;
            case "prev_year":
                d.date.setDate(1), d.date.setFullYear(e - 1);
                var h = MBT.API.UI.Calendar.getMonthDays(d.date);
                g > h ? d.date.setDate(h) : d.date.setDate(g), d.showDate();
                break;
            case "prev_month":
                d.date.setDate(1), d.date.setMonth(f - 1);
                var h = MBT.API.UI.Calendar.getMonthDays(d.date);
                g > h ? d.date.setDate(h) : d.date.setDate(g), d.showDate();
                break;
            case "next_month":
                d.date.setDate(1), d.date.setMonth(f + 1);
                var h = MBT.API.UI.Calendar.getMonthDays(d.date);
                g > h ? d.date.setDate(h) : d.date.setDate(g), d.showDate();
                break;
            case "next_year":
                d.date.setDate(1), d.date.setFullYear(e + 1);
                var h = MBT.API.UI.Calendar.getMonthDays(d.date);
                g > h ? d.date.setDate(h) : d.date.setDate(g), d.showDate();
                break;
            case "today":
                var i = new Date;
                i.setHours(d.date.getHours()), i.setMinutes(d.date.getMinutes()), i.setSeconds(d.date.getSeconds()), d.date = i, d.showDate();
                break;
            case "clear":
                d.value_el && (d.value_el.value = "");
                break;
            case "close":
                d.onCloseHandler();
                break;
            case "week_day":
                d.start_week_day = c.week_day_num, d.showDate()
        }
        "week_day" != c.objectCode && d.onChangeHandler(c.objectCode), MBT.API.UI.Calendar.hideAutoClose(d)
    }
}, MBT.API.UI.Calendar.onmouseover = function(a) {
    var c = (MBT.API.Event.getEvent(a), MBT.API.Event.getTargetObject(a));
    if (c) {
        var d = c.calendar;
        d.date.getFullYear(), d.date.getMonth(), d.date.getDate();
        switch (c.objectCode) {
            case "day":
                var h = new Date(d.date);
                h.setDate(c.day_num), d.setFooterText(d.getFormattedDate(d.text("footerDateFormat"), h)), MBT.API.UI.Calendar.addClass(c, "mbt-calendar-highlight"), MBT.API.UI.Calendar.addClass(c.parentNode, "mbt-calendar-highlight");
                break;
            case "clear":
            case "today":
            case "close":
            case "prev_year":
            case "prev_month":
            case "next_month":
            case "next_year":
                d.setFooterText(d.text(c.objectCode));
                break;
            case "week_day":
                if (c.week_day_num != d.start_week_day) {
                    var i = d.text("dayNames"),
                        j = i[c.week_day_num],
                        k = d.text("make_first");
                    k = k.replace("%s", j)
                } else var k = d.text("footerDefaultText");
                d.setFooterText(k);
                break;
            default:
                d.setFooterText(d.text("footerDefaultText"))
        }
    }
}, MBT.API.UI.Calendar.onmouseout = function(a) {
    var c = (MBT.API.Event.getEvent(a), MBT.API.Event.getTargetObject(a));
    if (c) {
        var d = c.calendar;
        d.setFooterText(d.text("footerDefaultText")), MBT.API.UI.Calendar.removeClass(c, "mbt-calendar-highlight"), MBT.API.UI.Calendar.removeClass(c.parentNode, "mbt-calendar-highlight")
    }
}, MBT.API.UI.Calendar.documentOnMouseDown = function(a) {
    var c = (MBT.API.Event.getEvent(a), MBT.API.Event.getTargetObject(a));
    if (c) {
        for (var d = c, e = null; d;) {
            if (d.className && d.className.match(/^mbt\-calendar\-iframe\-body/) && "BODY" == d.tagName.toUpperCase()) {
                e = d.calendar;
                break
            }
            d = d.parentNode
        }
        MBT.API.UI.Calendar.hideAutoClose(e)
    }
}, MBT.API.UI.Calendar.hideAutoClose = function(a) {
    var c, b = [];
    for (c = 0; c < MBT.API.UI.Calendar.activeCalendars.length; c++) {
        var d = MBT.API.UI.Calendar.activeCalendars[c];
        d.auto_close && d != a ? (d.hide(), d.userOnAutocloseHandler && d.userOnAutocloseHandler(this)) : b[b.length] = d
    }
    MBT.API.UI.Calendar.activeCalendars = b
}, MBT.API.UI.Calendar.makeInactive = function(a) {
    var c, b = [];
    for (c = 0; c < MBT.API.UI.Calendar.activeCalendars.length; c++) {
        var d = MBT.API.UI.Calendar.activeCalendars[c];
        d != a && (b[b.length] = d)
    }
    MBT.API.UI.Calendar.activeCalendars = b
}, MBT.API.UI.Calendar.getMonthDays = function(a, b) {
    var c = a.getFullYear();
    return b && (b = parseInt(b), (b <= 0 || b > 11) && (b = null)), b || (b = a.getMonth()), 1 == b && MBT.API.UI.Calendar.isLeapYear(c) ? 29 : MBT.API.UI.Calendar.monthDays[b]
}, MBT.API.UI.Calendar.isLeapYear = function(a) {
    return a % 4 == 0 && a % 100 != 0 || a % 400 == 0
}, MBT.API.UI.Calendar.getDayOfYear = function(a) {
    var b = new Date(a.getFullYear(), a.getMonth(), a.getDate(), 0, 0, 0),
        c = new Date(a.getFullYear(), 0, 0, 0, 0, 0),
        d = 864e5;
    return Math.floor((b - c) / d)
}, MBT.API.UI.Calendar.addClass = function(a, b) {
    MBT.API.UI.Calendar.removeClass(a, b), a.className += " " + b
}, MBT.API.UI.Calendar.removeClass = function(a, b) {
    if (a && a.className) {
        var e, c = [],
            d = String(a.className).split(" ");
        for (e = 0; e < d.length; e++) "" != d[e] && d[e] != b && (c[c.length] = d[e]);
        a.className = c.join(" ")
    }
}, MBT.API.UI.Calendar.getObjPos = function(a, b) {
    var c = Array(0, 0);
    if (!a) return c;
    for (; a && b != a;) {
        if (a.currentStyle) {
            if ("absolute" == a.currentStyle.position) break
        } else if ("absolute" == document.defaultView.getComputedStyle(a, "").getPropertyValue("position")) break;
        var d = "DIV" == a.tagName.toUpperCase();
        c[0] += a.offsetLeft - (d ? a.scrollLeft : 0), c[1] += a.offsetTop - (d ? a.scrollTop : 0), a = a.offsetParent
    }
    return c
}, MBT.API.UI.Calendar.langData = [], MBT.API.UI.Calendar.monthDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31], MBT.API.UI.Calendar.activeCalendars = [], MBT.API.UI.Calendar.handlert_set = !1, MBT.API.UI.Calendar.prototype.show = function(a, b, c, d) {
    if (this.value_el || (this.value_el = c), this.position = d, this.iframe_obj = document.createElement("IFRAME"), this.iframe_obj.className = "mbt-calendar", this.iframe_obj.setAttribute("scrolling", "no"), this.iframe_obj.calendar = this, this.isRelativePosition(d)) switch (d) {
        case "before":
            c.parentNode && c.parentNode.insertBefore(this.iframe_obj, c);
            break;
        case "after":
            c.parentNode && c.parentNode.insertBefore(this.iframe_obj, c.nextSibling);
            break;
        case "child":
        default:
            c.appendChild(this.iframe_obj), this.position = "child"
    } else {
        this.iframe_obj.style.position = "absolute";
        parseInt(a), parseInt(b);
        "number" == typeof a && "number" == typeof b && (this.iframe_obj.style.left = a + "px", this.iframe_obj.style.top = b + "px"), this.iframe_obj.style.border = "1px solid #000000", this.iframe_obj.value = this.value, document.body.appendChild(this.iframe_obj)
    }
    var g = '<html><head><link rel="stylesheet" type="text/css" href="' + MBT.API.getApiCSSUrl() + '?v=1237"></head><body class="mbt-calendar-iframe-body mbt-calendar" id="mbt_body"></body></html>';
    this.iframe_doc = this.iframe_obj.contentWindow.document, this.iframe_doc.open(), this.iframe_doc.write(g), this.iframe_doc.close(), MBT.API.Event.observe(this.iframe_doc, "mousedown", MBT.API.UI.Calendar.documentOnMouseDown), this.body_obj = this.iframe_doc.getElementById("mbt_body"), this.body_obj.calendar = this, this.table_obj = this.iframe_doc.createElement("TABLE"), this.table_obj.className = "mbt-calendar-table", this.table_obj.setAttribute("id", "mbt-calendar-iframe-table"), this.table_obj.cellSpacing = 1, this.table_obj.cellPadding = 0, this.table_obj.calendar = this, this.head_tr = this.table_obj.insertRow(0), this.head_tr.className = "mbt-calendar-head-tr", this.clear_td = this.head_tr.insertCell(0), this.clear_td.innerHTML = "c", this.clear_td.objectCode = "clear", this.clear_td.calendar = this, MBT.API.UI.Calendar.attachEvents(this.clear_td), this.head_td = this.head_tr.insertCell(1), this.head_td.colSpan = 5, this.close_td = this.head_tr.insertCell(2), this.close_td.innerHTML = "x", this.close_td.objectCode = "close", this.close_td.calendar = this, MBT.API.UI.Calendar.attachEvents(this.close_td), this.nav_tr = this.table_obj.insertRow(1), this.nav_tr.className = "mbt-calendar-nav-tr", this.prev_year_td = this.nav_tr.insertCell(0), this.prev_year_td.innerHTML = "&#x00ab;", this.prev_year_td.objectCode = "prev_year", this.prev_year_td.calendar = this, MBT.API.UI.Calendar.attachEvents(this.prev_year_td), this.prev_month_td = this.nav_tr.insertCell(1), this.prev_month_td.innerHTML = "&#x2039;", this.prev_month_td.objectCode = "prev_month", this.prev_month_td.calendar = this, MBT.API.UI.Calendar.attachEvents(this.prev_month_td), this.today_td = this.nav_tr.insertCell(2), this.today_td.colSpan = 3, this.today_td.innerHTML = this.text("today"), this.today_td.objectCode = "today", this.today_td.calendar = this, MBT.API.UI.Calendar.attachEvents(this.today_td), this.next_month_td = this.nav_tr.insertCell(3), this.next_month_td.innerHTML = "&#x203a;", this.next_month_td.objectCode = "next_month", this.next_month_td.calendar = this, MBT.API.UI.Calendar.attachEvents(this.next_month_td), this.next_year_td = this.nav_tr.insertCell(4), this.next_year_td.innerHTML = "&#x00bb;", this.next_year_td.objectCode = "next_year", this.next_year_td.calendar = this, MBT.API.UI.Calendar.attachEvents(this.next_year_td), this.wd_tr = this.table_obj.insertRow(2), this.wd_tr.className = "mbt-calendar-wd-tr";
    for (var h = 0; h < 7; h++) {
        var i = this.wd_tr.insertCell(h);
        i.objectCode = "week_day", i.calendar = this, MBT.API.UI.Calendar.attachEvents(i)
    }
    var k, j = 4;
    for (this.cal_tr = [], k = 0; k < j; k++) this.createCalRow(k);
    if (this.showTime) {
        this.time_tr = this.table_obj.insertRow(j + 3), this.time_tr.className = "mbt-calendar-time-tr";
        var i = this.time_tr.insertCell(0);
        i.colSpan = 2, i.innerHTML = this.text("time") + ":";
        var i = this.time_tr.insertCell(1);
        i.colSpan = 3, this.hours_obj = this.createElement("INPUT", i), this.hours_obj.className = "mbt-calendar-hours", this.hours_obj.setAttribute("size", 1), this.hours_obj.setAttribute("maxlength", 2), this.colon_span = this.createElement("SPAN", i), this.colon_span.className = "mbt-calendar-colon-span", this.colon_span.innerHTML = "&nbsp;:&nbsp;", this.mins_obj = this.createElement("INPUT", i), this.mins_obj.className = "mbt-calendar-mins", this.mins_obj.setAttribute("size", 1), this.mins_obj.setAttribute("maxlength", 2);
        var i = this.time_tr.insertCell(2);
        i.colSpan = 2, i.innerHTML = "&nbsp;"
    }
    this.footer_tr = this.table_obj.insertRow(j + 3 + (this.showTime ? 1 : 0)), this.footer_tr.className = "mbt-calendar-footer-tr", this.footer_td = this.footer_tr.insertCell(0), this.footer_td.colSpan = 7, this.footer_td.innerHTML = this.text("footerDefaultText"), this.body_obj.appendChild(this.table_obj), this.size_div = document.createElement("DIV"), this.size_div.className = this.body_obj.className, this.size_div.style.position = "absolute", this.size_div.style.left = "-1000px", this.size_div.style.top = "-1000px", document.body.appendChild(this.size_div), this.showDate(), MBT.API.UI.Calendar.handlersSet || (MBT.API.Event.observe(document, "mousedown", MBT.API.UI.Calendar.documentOnMouseDown), MBT.API.UI.Calendar.handlersSet = !0), MBT.API.UI.Calendar.activeCalendars[MBT.API.UI.Calendar.activeCalendars.length] = this
}, MBT.API.UI.Calendar.prototype.hide = function() {
    this.iframe_obj && (this.iframe_obj.parentNode.removeChild(this.iframe_obj), this.iframe_obj = null), MBT.API.UI.Calendar.makeInactive(this)
}, MBT.API.UI.Calendar.prototype.showAtElement = function(a, b) {
    if ("object" == typeof a && a) {
        if (this.isRelativePosition(b)) return void this.show(null, null, a, b);
        var d = (MBT.API.UI.Calendar.getObjPos(a), -1e3),
            e = -1e3;
        this.show(d, e, a, b)
    }
}, MBT.API.UI.Calendar.prototype.fixPosition = function(a) {
    var b = this.position;
    if (!this.isRelativePosition(b)) {
        a || (a = this.value_el);
        var c = String(b).split("-");
        if (2 == c.length) {
            var d = MBT.API.UI.Calendar.getObjPos(a),
                e = d[0],
                f = d[1] + a.offsetHeight,
                g = parseInt(this.iframe_obj.style.borderWidth),
                h = parseInt(this.iframe_obj.width) + 2 * g,
                i = parseInt(this.iframe_obj.height) + 2 * g;
            switch (c[0]) {
                case "left":
                    e -= h;
                    break;
                case "center":
                    e += (a.offsetWidth - h) / 2;
                    break;
                case "right":
                    e += a.offsetWidth;
                    break;
                case "adj_right":
                    e += a.offsetWidth - h
            }
            switch (c[1]) {
                case "top":
                    f -= a.offsetHeight + i;
                    break;
                case "center":
                    f += (a.offsetHeight - i) / 2 - a.offsetHeight;
                    break;
                case "bottom":
                    break;
                case "adj_bottom":
                    f -= i
            }
            this.iframe_obj.style.left = e + "px", this.iframe_obj.style.top = f + "px", this.iframe_obj.style.visibility = "visible"
        }
    }
}, MBT.API.UI.Calendar.prototype.isRelativePosition = function(a) {
    switch (a) {
        case "before":
        case "after":
        case "child":
            return !0;
        default:
            return !1
    }
}, MBT.API.UI.Calendar.prototype.createElement = function(a, b) {
    var c = this.iframe_doc.createElement(a);
    return b && b.appendChild(c), c
}, MBT.API.UI.Calendar.prototype.text = function(a, b) {
    return "undefined" == typeof b && (b = MBT.API.Lang), "undefined" != typeof MBT.API.UI.Calendar.langData[b] ? "undefined" != typeof MBT.API.UI.Calendar.langData[b][a] ? MBT.API.UI.Calendar.langData[b][a] : "" : "undefined" != typeof MBT.API.UI.Calendar.langData[this.default_language][a] ? MBT.API.UI.Calendar.langData[this.default_language][a] : ""
}, MBT.API.UI.Calendar.prototype.showDate = function() {
    var b, a = this.getWeekendDays(),
        c = this.text("dayNamesShort");
    for (b = 0; b < 7; b++) {
        var d = (b + this.start_week_day) % 7,
            e = this.wd_tr.cells[b];
        e.innerHTML = c[d], "undefined" != typeof a[d] ? e.className = "mbt-calendar-weekend-head" : e.className = "", e.week_day_num = d
    }
    var f = MBT.API.UI.Calendar.getMonthDays(this.date),
        g = new Date(this.date);
    g.setDate(1);
    var h = (g.getDay() + 7 - this.start_week_day) % 7 + 1,
        i = this.date.getFullYear(),
        j = this.date.getMonth(),
        k = this.date.getDate(),
        l = new Date,
        m = l.getFullYear(),
        n = l.getMonth(),
        o = l.getDate(),
        p = this.text("monthNames");
    this.head_td.innerHTML = p[j] + ", " + i;
    var q, r, t, s = 0;
    for (q = 0; q < 6; q++)
        if (s != f)
            for (r = 0; r < 7; r++) {
                this.cal_tr[q] || this.createCalRow(q);
                var u = this.cal_tr[q],
                    v = u.cells[r];
                if (v.className = "", 0 == q && r + 1 < h || s == f) var w = "&nbsp;";
                else {
                    var x = s + 1,
                        w = x;
                    s++, v.objectCode = "day", v.day_num = x, v.calendar = this, MBT.API.UI.Calendar.attachEvents(v), k == x && MBT.API.UI.Calendar.addClass(v, "mbt-calendar-current"), x == o && j == n && i == m && MBT.API.UI.Calendar.addClass(v, "mbt-calendar-today");
                    var d = (r + this.start_week_day) % 7;
                    "undefined" != typeof a[d] ? MBT.API.UI.Calendar.addClass(v, "mbt-calendar-weekend-day") : MBT.API.UI.Calendar.removeClass(v, "mbt-calendar-weekend-day")
                }
                v.innerHTML = w, s == f && (t = q)
            } else this.cal_tr[t + 1] && (this.cal_tr[t + 1].parentNode.removeChild(this.cal_tr[t + 1]), this.cal_tr[q] = null);
    if (this.showTime && this.hours_obj && this.mins_obj) {
        var y = this.date.getHours();
        y < 10 && (y = "0" + y);
        var z = this.date.getMinutes();
        z < 10 && (z = "0" + z), this.hours_obj.value = y, this.mins_obj.value = z
    }
    var A = this;
    window.setTimeout(function() {
        A.fitToContent()
    }, 1), window.setTimeout(function() {
        A.fixPosition()
    }, 5)
}, MBT.API.UI.Calendar.prototype.fitToContent = function() {
    try {
        var a = this.iframe_doc.getElementById("mbt-calendar-iframe-table");
        this.iframe_obj.width = a.offsetWidth, this.iframe_obj.height = a.offsetHeight, parseInt(this.iframe_obj.width) && parseInt(this.iframe_obj.height) || (this.size_div.innerHTML = this.body_obj.innerHTML, this.iframe_obj.width = this.size_div.offsetWidth, this.iframe_obj.height = this.size_div.offsetHeight)
    } catch (a) {}
}, MBT.API.UI.Calendar.prototype.createCalRow = function(a) {
    var b = this.table_obj.insertRow(3 + a);
    b.className = "mbt-calendar-cal-tr";
    var c;
    for (c = 0; c < 7; c++) {
        b.insertCell(c)
    }
    return this.cal_tr[a] = b, b
}, MBT.API.UI.Calendar.prototype.getFormattedDate = function(a, b) {
    if (b || (b = this.date), a || (a = this.getDateFormat()), this.showTime && this.hours_obj && this.mins_obj) {
        this.date.setHours(this.hours_obj.value);
        var c = this.date.setMinutes(this.mins_obj.value)
    }
    var d = b.getFullYear(),
        e = b.getMonth(),
        f = b.getDate(),
        g = b.getDay(),
        h = b.getHours(),
        c = b.getMinutes(),
        i = b.getSeconds(),
        j = this.text("monthNamesShort"),
        k = this.text("monthNames"),
        l = this.text("dayNamesShort"),
        m = this.text("dayNames"),
        n = h < 12,
        o = h > 12 ? h - 12 : 0 == h ? 12 : h,
        p = [];
    p["%a"] = n ? "am" : "pm", p["%A"] = n ? "AM" : "PM", p["%d"] = f < 10 ? "0" + f : f, p["%D"] = l[g], p["%F"] = k[e], p["%h"] = o < 10 ? "0" + o : o, p["%H"] = h < 10 ? "0" + h : h, p["%g"] = o, p["%G"] = h, p["%i"] = c < 10 ? "0" + c : c, p["%j"] = f, p["%l"] = m[g], p["%L"] = MBT.API.UI.Calendar.isLeapYear(d) ? 1 : 0, p["%m"] = e < 9 ? "0" + (e + 1) : e + 1, p["%n"] = e + 1, p["%M"] = j[e], p["%s"] = i < 10 ? "0" + i : i, p["%t"] = MBT.API.UI.Calendar.getMonthDays(b), p["%w"] = g, p["%Y"] = d, p["%y"] = String(d).substr(2, 2), p["%z"] = MBT.API.UI.Calendar.getDayOfYear(b);
    var r, q = String(a).match(/%./g),
        s = a;
    for (r = 0; r < q.length; r++) {
        var t = p[q[r]];
        if ("undefined" != typeof t) {
            var u = new RegExp(q[r], "g");
            s = s.replace(u, t)
        }
    }
    return s
}, MBT.API.UI.Calendar.prototype.setFooterText = function(a) {
    this.footer_td && (this.footer_td.innerHTML = a)
}, MBT.API.UI.Calendar.prototype.getWeekendDays = function() {
    var c, a = this.text("weekend"),
        b = a.split(","),
        d = [];
    for (c = 0; c < b.length; c++) d[b[c]] = !0;
    return d
}, MBT.API.UI.Calendar.prototype.onCloseHandler = function() {
    this.userOnCloseHandler ? this.userOnCloseHandler(this) : this.hide()
}, MBT.API.UI.Calendar.prototype.onChangeHandler = function(a) {
    this.userOnChangeHandler ? this.userOnChangeHandler(this, a) : "day" == a && (this.value_el && (this.value_el.value = this.getFormattedDate()), this.auto_close && this.hide())
}, MBT.API.UI.Calendar.prototype.getDateFormat = function() {
    var a = this.text("dateFormat"),
        b = a ? a : this.format;
    return this.showTime && (b += " %H:%i"), b
}, MBT.API.UI.Calendar.prototype.parseDate = function(a, b) {
    if ("undefined" != typeof a) {
        b || (b = this.getDateFormat());
        var c = new Date,
            d = 0,
            e = -1,
            f = 0,
            g = 0,
            h = 0,
            i = 0,
            j = this.text("monthNames"),
            k = this.text("monthNamesShort"),
            l = this.text("monthNames", "en"),
            m = this.text("monthNamesShort", "en");
        for (r = 0; r < j.length; r++) {
            var n = new RegExp(j[r], "gi");
            a = a.replace(n, l[r])
        }
        for (r = 0; r < k.length; r++) {
            var n = new RegExp(k[r], "gi");
            a = a.replace(n, m[r])
        }
        var q, r, o = String(a).split(/\W+/g),
            p = String(b).match(/%./g);
        for (q = 0; q < p.length; q++)
            if (o[q]) switch (p[q]) {
                case "%a":
                case "%A":
                    /am/i.test(o[q]) && g >= 12 ? g -= 12 : /pm/i.test(o[q]) && g < 12 && (g += 12);
                    break;
                case "%d":
                case "%j":
                    f = parseInt(Number(o[q]));
                    break;
                case "%F":
                    for (r = 0; r < l.length; r++)
                        if (l[r].toLowerCase() == o[q].toLowerCase()) {
                            e = r;
                            break
                        }
                    break;
                case "%h":
                case "%H":
                case "%g":
                case "%G":
                    g = parseInt(Number(o[q])), /am/i.test(o[q]) && g >= 12 ? g -= 12 : /pm/i.test(o[q]) && g < 12 && (g += 12);
                    break;
                case "%i":
                    h = parseInt(Number(o[q]));
                    break;
                case "%m":
                case "%n":
                    e = parseInt(Number(o[q])) - 1;
                    break;
                case "%M":
                    for (r = 0; r < m.length; r++)
                        if (m[r].toLowerCase() == o[q].toLowerCase()) {
                            e = r;
                            break
                        }
                    break;
                case "%s":
                    i = parseInt(Number(o[q]));
                    break;
                case "%Y":
                    d = parseInt(Number(o[q]));
                    break;
                case "%y":
                    d = parseInt(o[q]), d < 100 && (d += d + (d > 29 ? 1900 : 2e3))
            }(isNaN(d) || d <= 0) && (d = c.getFullYear()), (isNaN(e) || e < 0 || e > 11) && (e = c.getMonth()), (isNaN(f) || f <= 0 || f > 31) && (f = c.getDate()), (isNaN(g) || g < 0 || g > 23) && (g = c.getHours()), (isNaN(h) || h < 0 || h > 59) && (h = c.getMinutes()), (isNaN(i) || i < 0 || i > 59) && (i = c.getSeconds()), this.date = new Date(d, e, f, g, h, i)
    }
}, MBT.API.UI.Calendar.langData.en = {
    today: "Today",
    time: "Time",
    dayNamesShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
    dayNames: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
    monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
    monthNames: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
    footerDateFormat: "%D, %F %j %Y",
    dateFormat: "%Y-%m-%d",
    footerDefaultText: "Select date",
    clear: "Clear Date",
    prev_year: "Previous year",
    prev_month: "Previous month",
    next_month: "Next month",
    next_year: "Next year",
    close: "Close",
    weekend: "0,6",
    make_first: "Start with %s"
}, MBT.API.UI.Calendar.langData.fi = {
    today: "TÐ£ÂƒÐ¢Ð„nÐ£ÂƒÐ¢Ð„Ð£ÂƒÐ¢Ð„n",
    time: "Aika",
    dayNamesShort: ["Su", "Ma", "Ti", "Ke", "To", "Pe", "La"],
    dayNames: ["Sunnuntai", "Maanantai", "Tiistai", "Keskiviikko", "Torstai", "Perjantai", "Lauantai"],
    monthNamesShort: ["Tammi", "Helmi", "Maalis", "Huhti", "Touko", "KesÐ£ÂƒÐ¢Ð„", "HeinÐ£ÂƒÐ¢Ð„", "Elo", "Syys", "Loka", "Marras", "Joulu"],
    monthNames: ["Tammikuu", "Helmikuu", "Maaliskuu", "Huhtikuu", "Toukokuu", "KesÐ£ÂƒÐ¢Ð„kuu", "HeinÐ£ÂƒÐ¢Ð„kuu", "Elokuu", "Syyskuu", "Lokakuu", "Marraskuu", "Joulukuu"],
    footerDateFormat: "%D, %F %j %Y",
    dateFormat: "%Y-%m-%d",
    footerDefaultText: "Valitse pÐ£ÂƒÐ¢Ð„ivÐ£ÂƒÐ¢Ð„",
    clear: "TyhjennÐ£ÂƒÐ¢Ð„ valinta",
    prev_year: "Edellinen vuosi",
    prev_month: " Kitas",
    next_month: " Seuraava kuukausi",
    next_year: "Seuraava vuosi",
    close: "Sulje",
    weekend: "1,7",
    make_first: "Viikon 1. pÐ£ÂƒÐ¢Ð„ivÐ£ÂƒÐ¢Ð„ %s"
}, MBT.API.UI.Calendar.langData.lt = {
    today: "Ð¥ iandien",
    time: "Laikas",
    dayNamesShort: ["S", "P", "A", "T", "K", "P", "Ð¥ "],
    dayNames: ["Sekmadienis", "Pirmadienis", "Antradienis", "TreÐ¤Âiadienis", "Ketvirtadienis", "Penktadienis", "Ð¥ eÐ¥Ðtadienis"],
    monthNamesShort: ["Sausis", "Vasaris", "Kovas", "Balandis", "GeguÐ¥ÐžÐ¤Â—", "BirÐ¥Ðželis", "Liepa", "RugpjÐ¥Ð‹tis", "RugsÐ¤Â—jis", "Spalis", "Lapkritis", "Grudis"],
    monthNames: ["Sausis", "Vasaris", "Kovas", "Balandis", "GeguÐ¥ÐžÐ¤Â—", "BirÐ¥Ðželis", "Liepa", "RugpjÐ¥Ð‹tis", "RugsÐ¤Â—jis", "Spalis", "Lapkritis", "Grudis"],
    footerDateFormat: "%D, %F %j %Y",
    dateFormat: "%Y-%m-%d",
    footerDefaultText: "Pasirinkite datÐ¤Â…",
    clear: "IÐ¥Ðvalyti",
    prev_year: "PraÐ¤Â—jÐ¤Â™ metai",
    prev_month: " PraÐ¤Â—jÐ¤Â™s mÐ¤Â—nuo mÐ¤Â—nuo",
    next_month: " Kitas mÐ¤Â—nuo",
    next_year: "Kiti metai",
    close: "UÐ¥Ðždaryti",
    weekend: "0,6",
    make_first: "SavaitÐ¤Â—s pradÐ¥Ðžia"
}, MBT.API.formatDate = function(a, b) {
    for (var c = "", d = MBT.API.replaceChars, e = 0; e < b.length; e++) {
        var f = b.charAt(e);
        c += d[f] ? d[f].call(a) : f
    }
    return c
}, MBT.API.replaceChars = {
    shortMonths: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
    longMonths: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
    shortDays: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
    longDays: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
    d: function() {
        return (this.getDate() < 10 ? "0" : "") + this.getDate()
    },
    D: function() {
        return Date.replaceChars.shortDays[this.getDay()]
    },
    j: function() {
        return this.getDate()
    },
    l: function() {
        return Date.replaceChars.longDays[this.getDay()]
    },
    N: function() {
        return this.getDay() + 1
    },
    S: function() {
        return this.getDate() % 10 == 1 && 11 != this.getDate() ? "st" : this.getDate() % 10 == 2 && 12 != this.getDate() ? "nd" : this.getDate() % 10 == 3 && 13 != this.getDate() ? "rd" : "th"
    },
    w: function() {
        return this.getDay()
    },
    z: function() {
        return "Not Yet Supported"
    },
    W: function() {
        return "Not Yet Supported"
    },
    F: function() {
        return Date.replaceChars.longMonths[this.getMonth()]
    },
    m: function() {
        return (this.getMonth() < 9 ? "0" : "") + (this.getMonth() + 1)
    },
    M: function() {
        return Date.replaceChars.shortMonths[this.getMonth()]
    },
    n: function() {
        return this.getMonth() + 1
    },
    t: function() {
        return "Not Yet Supported"
    },
    L: function() {
        return "Not Yet Supported"
    },
    o: function() {
        return "Not Supported"
    },
    Y: function() {
        return this.getFullYear()
    },
    y: function() {
        return ("" + this.getFullYear()).substr(2)
    },
    a: function() {
        return this.getHours() < 12 ? "am" : "pm"
    },
    A: function() {
        return this.getHours() < 12 ? "AM" : "PM"
    },
    B: function() {
        return "Not Yet Supported"
    },
    g: function() {
        return this.getHours() % 12 || 12
    },
    G: function() {
        return this.getHours()
    },
    h: function() {
        return ((this.getHours() % 12 || 12) < 10 ? "0" : "") + (this.getHours() % 12 || 12)
    },
    H: function() {
        return (this.getHours() < 10 ? "0" : "") + this.getHours()
    },
    i: function() {
        return (this.getMinutes() < 10 ? "0" : "") + this.getMinutes()
    },
    s: function() {
        return (this.getSeconds() < 10 ? "0" : "") + this.getSeconds()
    },
    e: function() {
        return "Not Yet Supported"
    },
    I: function() {
        return "Not Supported"
    },
    O: function() {
        return (-this.getTimezoneOffset() < 0 ? "-" : "+") + (Math.abs(this.getTimezoneOffset() / 60) < 10 ? "0" : "") + Math.abs(this.getTimezoneOffset() / 60) + "00"
    },
    T: function() {
        var a = this.getMonth();
        this.setMonth(0);
        var b = this.toTimeString().replace(/^.+ \(?([^\)]+)\)?$/, "$1");
        return this.setMonth(a), b
    },
    Z: function() {
        return 60 * -this.getTimezoneOffset()
    },
    c: function() {
        return "Not Yet Supported"
    },
    r: function() {
        return this.toString()
    },
    U: function() {
        return this.getTime() / 1e3
    }
}, MBT.API.parseDate = function(a) {
    var b = a.split("-");
    if (3 != b.length) return null;
    var c = new Date(parseInt(b[0], 10), parseInt(b[1], 10) - 1, parseInt(b[2], 10));
    return c
}, MBT.API.haveFlash = function() {
    var a = !1;
    try {
        var b = new ActiveXObject("ShockwaveFlash.ShockwaveFlash");
        b && (a = !0)
    } catch (b) {
        void 0 != navigator.mimeTypes["application/x-shockwave-flash"] && (a = !0)
    }
    return a
}, MBT.API.formatLivePagesLink = function(a) {
    var b = a,
        c = MBT.API.getLivePagesTypeIndex(),
        d = MBT.API.getLivePagesLangId();
    return c > 0 && (b += "?type=" + c), d > 0 && (b += "&lang=" + d), b
}, MBT.API.convertDecToHex = function(a) {
    return a < 0 && (a = 4294967295 + a + 1), parseInt(a, 10).toString(16)
}, MBT.API.convertHexToDex = function(a) {
    var b = (a + "").replace(/[^a-f0-9]/gi, "");
    b = parseInt(a, 16);
    var c = Math.pow(2, 4 * a.length + a.length % 2),
        d = c - b;
    return b > d ? -d : b
}, MBT.API.UI.animate = function(a, b, c, d, e) {
    var f = d / 1e3 * 60,
        g = f,
        h = 1e3 / 60,
        i = Math.floor(parseInt(c) / f),
        j = function() {
            var d = parseInt(a.style[b]);
            1 == g ? a.style[b] = d + i + (c - i * f) + "px" : a.style[b] = d + i + "px", g--, g > 0 ? setTimeout(j, h) : e && e()
        };
    setTimeout(j, h)
}, MBT.API.UI.Scroller = function(a) {
    for (this.type = a.type || "vertical", this.previousButton = MBT.API.get(a.previousButton), MBT.API.Event.observe(this.previousButton, "click", this.scrollUp.bind(this)), this.nextButton = MBT.API.get(a.nextButton), MBT.API.Event.observe(this.nextButton, "click", this.scrollDown.bind(this)), this.itemTagName = a.itemTagName || "a", this.animationLength = a.animationLength || 500, this.container = MBT.API.get(a.container), "vertical" == this.type ? (this.styleKey = "top", this.offsetKey = "offsetHeight") : "horizontal" == this.type && (this.styleKey = "left", this.offsetKey = "offsetWidth"), this.container.style = this.container.style || {}, this.container.style.position = "relative", this.container.style[this.styleKey] = "0px", this.locked = !1, this.items = this.container.getElementsByTagName(this.itemTagName), i = 0; i < this.items.length; i++) MBT.API.Event.observe(this.items[i], "click", a.itemHandler);
    a.afterInit && setTimeout(a.afterInit, 1)
}, MBT.API.UI.Scroller.prototype.scrollUp = function(a, b) {
    if (b = b || 1, !this.locked) {
        var c = this.container.parentNode[this.offsetKey];
        parseInt(this.container.style[this.styleKey]) + c * b > 0 || (this.locked = !0, MBT.API.UI.animate(this.container, this.styleKey, c * b, this.animationLength * b, this.unlock.bind(this)))
    }
}, MBT.API.UI.Scroller.prototype.scrollDown = function(a, b) {
    if (b = b || 1, !this.locked) {
        var c = this.container.parentNode[this.offsetKey];
        parseInt(this.container.style[this.styleKey]) - c * b <= -this.container[this.offsetKey] || (this.locked = !0, MBT.API.UI.animate(this.container, this.styleKey, -c * b, this.animationLength * b, this.unlock.bind(this)))
    }
}, MBT.API.UI.Scroller.prototype.unlock = function() {
    this.locked = !1
}, MBT.API.UI.Scroller.prototype.scrollTo = function(a) {
    var b = this.container.parentNode.getBoundingClientRect(),
        c = this.items[a].getBoundingClientRect();
    if (c.left < b.left) {
        var d = Math.ceil(Math.abs(b.left - c.left) / b.width);
        return void this.scrollUp(null, d)
    }
    if (c.right > b.right) {
        var d = Math.ceil(Math.abs(c.right - b.right) / b.width);
        return void this.scrollDown(null, d)
    }
}, MBT.API.UI.Scroller.prototype.itemVisible = function(a) {
    var b = this.container.parentNode.getBoundingClientRect(),
        c = this.items[a].getBoundingClientRect();
    return "horizontal" == this.type ? c.left >= b.left && c.right <= b.right : "vertical" == this.type && (c.top >= b.top && c.bottom <= b.bottom)
}, MBT.API.extendParams = function(a, b) {
    for (var c in b) b.hasOwnProperty(c) && (a[c] = b[c]);
    return a
}, MBT.API.wrapInner = function(a, b, c, d) {
    "string" == typeof b && (b = document.createElement(b));
    for (a.appendChild(b).setAttribute(c, d); a.firstChild !== b;) b.appendChild(a.firstChild)
}, MBT.API.createResponsiveNavigation = function(a) {
    this.menuContainer = MBT.API.get(a.menuContainer), this.mobileMenuContainer = MBT.API.get(a.mobileMenuContainer), this.activeMenuItemText = a.activeMenuItemText, this.options = MBT.API.extendParams(this.getDefaults(), a), MBT.API.Event.observe(this.mobileMenuContainer, "click", this.init.bind(this))
}, MBT.API.createResponsiveNavigation.prototype = {
    getDefaults: function() {
        return {
            filtersMode: !1,
            mobileMenuCloseText: "Select",
            mobileMenuOpenedClass: "mbt-v2-mobile-menu-opened"
        }
    },
    init: function() {
        "none" == this.getMenuContainerVisibility() ? (this.showMenuContainer(), this.addOpendClassToMobileMenu(), this.options.filtersMode || this.setMobileMenuCloseText()) : (this.hideMenuContainer(), this.removeOpendClassToMobileMenu(), this.options.filtersMode || this.setMobileMenuTitle());
        for (var a = ["resize", "redraw"], b = 0; b < a.length; b++) window.addEventListener(a[b], this.checkVisibility.bind(this), !1)
    },
    getMenuContainerVisibility: function() {
        return window.getComputedStyle(this.menuContainer, null).getPropertyValue("display")
    },
    showMenuContainer: function() {
        this.menuContainer.style.display = "block"
    },
    hideMenuContainer: function() {
        this.menuContainer.style.display = "none"
    },
    addOpendClassToMobileMenu: function() {
        this.mobileMenuContainer.classList.add(this.options.mobileMenuOpenedClass)
    },
    removeOpendClassToMobileMenu: function() {
        this.mobileMenuContainer.classList.remove(this.options.mobileMenuOpenedClass)
    },
    getActiveMenuItemText: function() {
        for (var a = 0; a < this.menuContainer.childNodes.length; a++)
            if ("mbt-v2-navigation-tab-active" == this.menuContainer.childNodes[a].className) {
                item = this.menuContainer.childNodes[a], this.activeMenuItemText = item.innerHTML;
                break
            }
    },
    setMobileMenuTitle: function() {
        this.getActiveMenuItemText(), this.mobileMenuContainer.innerHTML = this.activeMenuItemText
    },
    setMobileMenuCloseText: function() {
        this.mobileMenuContainer.innerHTML = this.options.mobileMenuCloseText
    },
    checkVisibility: function() {
        var a = window.getComputedStyle(this.menuContainer, null).getPropertyValue("display"),
            b = window.getComputedStyle(this.menuContainer, null).getPropertyValue("display");
        "none" == a && "none" == b && (this.menuContainer.style.display = "block")
    }
}, MBT.API.responsiveTable = function(a) {
    this.table = MBT.API.get(a.table), this.tablesContainer = MBT.API.get(a.tablesContainer), this.columnsToPin = a.columnsToPin, this.splitted = !1
}, MBT.API.responsiveTable.prototype = {
    watchTable: function() {
        if (this.table) {
            var a = this;
            setTimeout(function() {
                a.checkTableWidth()
            }, 300);
            for (var b = ["resize", "redraw"], c = 0; c < b.length; c++) window.addEventListener(b[c], this.checkTableWidth.bind(this), !1)
        }
    },
    checkTableWidth: function() {
        this.splitted && this.scroller.resizeScrollContent(), this.table.offsetWidth > this.tablesContainer.offsetWidth && !this.splitted ? this.splitTable() : this.table.offsetWidth < this.tablesContainer.offsetWidth && this.splitted && this.unsplitTable()
    },
    splitTable: function() {
        var a = this.createPinnedTable();
        this.table.hasAttribute("class") && a.setAttribute("class", this.table.getAttribute("class"));
        var b = this.createScrollableContainer();
        b.appendChild(this.table), this.tablesContainer.appendChild(b), this.hideTableColumns();
        var c = this.createPinnedContainer();
        c.appendChild(a), this.tablesContainer.appendChild(c), "rtl" == window.getComputedStyle(this.table, null).getPropertyValue("direction") ? b.style.marginRight = a.offsetWidth + "px" : b.style.marginLeft = a.offsetWidth + "px", b.style.height = a.offsetHeight + "px", this.scroller = new MBT.API.Scrollbar(b, {
            scrollDirection: "horiz"
        }), this.splitted = !0
    },
    unsplitTable: function() {
        this.showTableColumns(), this.tablesContainer.innerHTML = null, this.tablesContainer.appendChild(this.table), this.splitted = !1
    },
    hideTableColumns: function() {
        for (var a = this.getTableTr(), b = 0; b < a.length; b++)
            for (var c = 0; c < this.columnsToPin && (a[b].getElementsByTagName("td")[c].style.display = "none", a[b].getElementsByTagName("td")[c].getAttribute("colspan") != this.columnsToPin); c++);
        var d = this.getTableThead();
        if (d)
            for (var b = 0; b < this.columnsToPin; b++) d.getElementsByTagName("th")[b].style.display = "none"
    },
    showTableColumns: function() {
        for (var a = this.getTableTr(), b = 0; b < a.length; b++)
            for (var c = 0; c < this.columnsToPin; c++) a[b].getElementsByTagName("td")[c].removeAttribute("style");
        var d = this.getTableThead();
        if (d)
            for (var b = 0; b < this.columnsToPin; b++) d.getElementsByTagName("th")[b].removeAttribute("style")
    },
    getTableTr: function() {
        var a = this.table.getElementsByTagName("tbody")[0],
            b = a.getElementsByTagName("tr");
        return b
    },
    getTableThead: function() {
        var a = this.table.getElementsByTagName("thead")[0];
        return !!a && a.getElementsByTagName("tr")[0]
    },
    createPinnedContainer: function() {
        var a = document.createElement("div");
        return a.className += " mbt-v2-pinned-table-container", a
    },
    createScrollableContainer: function() {
        var a = document.createElement("div");
        return a.className += " mbt-v2-scrollable-table-container", a
    },
    createPinnedTable: function() {
        var a = document.createElement("table");
        a.className += " mbt-v2-table";
        var b = this.getTableThead();
        if (b)
            for (var c = document.createElement("thead"), d = document.createElement("tr"), e = 0; e < this.columnsToPin; e++) {
                var f = document.createElement("th");
                f.innerHTML = b.getElementsByTagName("th")[e].innerHTML, d.style.height = b.getElementsByTagName("th")[e].offsetHeight + "px", d.appendChild(f), c.appendChild(d), a.appendChild(c)
            }
        for (var g = document.createElement("tbody"), h = this.getTableTr(), e = 0; e < h.length; e++) {
            var i = document.createElement("tr");
            h[e].hasAttribute("class") && i.setAttribute("class", h[e].getAttribute("class"));
            for (var j = 0; j < this.columnsToPin; j++) {
                var k = document.createElement("td");
                if (k.innerHTML = h[e].getElementsByTagName("td")[j].innerHTML, h[e].getElementsByTagName("td")[j].hasAttribute("colspan") && k.setAttribute("colspan", h[e].getElementsByTagName("td")[j].getAttribute("colspan")), i.appendChild(k), g.appendChild(i), h[e].getElementsByTagName("td")[j].getAttribute("colspan") == this.columnsToPin) break
            }
            var l = Math.max(i.offsetHeight, h[e].offsetHeight);
            h[e].style.height = l + "px", i.style.height = l + "px"
        }
        return a.appendChild(g), a
    }
}, MBT.API.Scrollbar = function(a, b) {
    this.el = MBT.API.get(a), this.track, this.scrollbar, this.dragOffset, this.flashTimeout, this.contentEl = this.el, this.scrollContentEl = this.el, this.scrollOffsetAttr = "scrollTop", this.sizeAttr = "height", this.scrollSizeAttr = "scrollHeight", this.offsetAttr = "top", this.options = MBT.API.extendParams(MBT.API.Scrollbar.DEFAULTS, b), this.scrollDirection = this.options.scrollDirection, this.theme = this.options.css, this.init()
}, MBT.API.Scrollbar.SCROLLBAR_WIDTH, MBT.API.Scrollbar.IS_WEBKIT = "WebkitAppearance" in document.documentElement.style, MBT.API.Scrollbar.DEFAULTS = {
    scrollDirection: "vert",
    wrapContent: !0,
    autoHide: !0,
    css: {
        container: "mbt-v2-bar",
        content: "mbt-v2-bar-content",
        scrollContent: "mbt-v2-bar-scroll-content",
        scrollbar: "mbt-v2-scrollbar",
        scrollbarTrack: "mbt-v2-scrollbar-track"
    }
}, MBT.API.Scrollbar.prototype = {
    init: function() {
        if ("undefined" == typeof MBT.API.Scrollbar.SCROLLBAR_WIDTH && (MBT.API.Scrollbar.SCROLLBAR_WIDTH = this.scrollbarWidth()), 0 === MBT.API.Scrollbar.SCROLLBAR_WIDTH) return void(this.el.style.overflow = 0);
        if ("horiz" === this.scrollDirection && (this.scrollOffsetAttr = "scrollLeft", this.sizeAttr = "offsetWidth", this.scrollSizeAttr = "scrollWidth", this.offsetAttr = "offsetLeft"), this.options.wrapContent) {
            var a = document.createElement("div"),
                b = document.createElement("div");
            MBT.API.wrapInner(this.el, b, "class", this.theme.content), MBT.API.wrapInner(this.el, a, "class", this.theme.scrollContent)
        }
        this.contentEl = b;
        var c = document.createElement("div");
        c.setAttribute("class", this.theme.scrollbarTrack);
        var d = document.createElement("div");
        d.setAttribute("class", this.theme.scrollbar), c.appendChild(d), this.el.insertBefore(c, this.el.firstChild), this.track = c, this.scrollbar = d, this.scrollContentEl = a, "horiz" === this.scrollDirection && (this.el.className += " mbt-v2-scrollbar-horizontal"), this.resizeScrollContent(), this.options.autoHide && this.el.addEventListener("mouseenter", this.flashScrollbar.bind(this), !1), this.scrollbar.addEventListener("mousedown", this.startDrag.bind(this), !1), this.scrollContentEl.addEventListener("scroll", this.startScroll.bind(this), !1), this.resizeScrollbar(), this.options.autoHide || this.showScrollbar()
    },
    startDrag: function(a) {
        a.preventDefault();
        var b = a.pageY;
        "horiz" === this.scrollDirection && (b = a.pageX), "top" === this.offsetAttr ? this.dragOffset = b - this.scrollbar.offsetTop : this.dragOffset = b - this.scrollbar.offsetLeft, document.addEventListener("mousemove", this, !1), document.addEventListener("mouseup", this, !1), this.resizeScrollbar()
    },
    startScroll: function(a) {
        this.flashScrollbar()
    },
    drag: function(a) {
        a.preventDefault();
        var b = a.pageY,
            c = null,
            d = null,
            e = null;
        "horiz" === this.scrollDirection && (b = a.pageX);
        var f = 0,
            g = 0;
        "top" === this.offsetAttr ? (f = this.track.offsetTop, g = this.track.offsetHeight) : (f = this.track.offsetLeft, g = this.track.offsetWidth), c = b - f - this.dragOffset, d = c / g, e = d * this.contentEl[this.scrollSizeAttr], this.scrollContentEl[this.scrollOffsetAttr] = e
    },
    endDrag: function() {
        document.removeEventListener("mousemove", this, !1), document.removeEventListener("mouseup", this, !1)
    },
    flashScrollbar: function() {
        this.resizeScrollbar(), this.showScrollbar()
    },
    resizeScrollbar: function() {
        if (0 !== MBT.API.Scrollbar) {
            var a = this.contentEl[this.scrollSizeAttr],
                b = this.scrollContentEl[this.scrollOffsetAttr],
                c = this.track[this.sizeAttr],
                d = c / a,
                e = Math.round(d * b) + 2,
                f = Math.floor(d * (c - 2)) - 2;
            c < a ? ("vert" === this.scrollDirection ? (this.scrollbar.style.top = e, this.scrollbar.style.height = f) : (this.scrollbar.style.left = e + "px", this.scrollbar.style.width = f + "px"), this.track.style.display = "block") : this.track.style.display = "none"
        }
    },
    resizeScrollContent: function() {
        MBT.API.Scrollbar.IS_WEBKIT || ("vert" === this.scrollDirection ? (this.scrollContentEl.style.width = this.el.offsetWidth + MBT.API.Scrollbar.SCROLLBAR_WIDTH + "px", this.scrollContentEl.style.height = this.el.offsetHeight + "px") : (this.scrollContentEl.style.width = this.el.offsetWidth + "px", this.scrollContentEl.style.height = this.el.offsetHeight + MBT.API.Scrollbar.SCROLLBAR_WIDTH + "px"))
    },
    showScrollbar: function() {
        this.scrollbar.className = this.theme.scrollbar + " visible", this.options.autoHide && ("number" == typeof this.flashTimeout && window.clearTimeout(this.flashTimeout), this.flashTimeout = window.setTimeout(this.hideScrollbar.bind(this), 1e3))
    },
    hideScrollbar: function() {
        this.scrollbar.className = this.theme.scrollbar, "number" == typeof this.flashTimeout && window.clearTimeout(this.flashTimeout)
    },
    resizeScrollContent: function() {
        MBT.API.Scrollbar.IS_WEBKIT || ("vert" === this.scrollDirection ? (this.scrollContentEl.style.width = this.el.offsetWidth + MBT.API.Scrollbar.SCROLLBAR_WIDTH + "px", this.scrollContentEl.style.height = this.el.offsetHeight + "px") : (this.scrollContentEl.style.width = this.el.offsetWidth + "px", this.scrollContentEl.style.height = this.el.offsetHeight + MBT.API.Scrollbar.SCROLLBAR_WIDTH + "px"))
    },
    scrollbarWidth: function() {
        var a = document.createElement("div");
        a.style.width = "50px", a.style.height = "50px", a.style.overflowY = "scroll", a.style.top = "-200px", a.style.left = "-200px";
        var b = document.createElement("div");
        return b.style.height = "100px", a.appendChild(b), document.body.appendChild(a), width = 0, widthMinusScrollbars = 0, width = a.offsetWidth, widthMinusScrollbars = b.offsetWidth, document.body.removeChild(a), width - widthMinusScrollbars
    },
    handleEvent: function(a) {
        switch (a.type) {
            case "mousemove":
                this.drag(a);
                break;
            case "mouseup":
                this.endDrag(a)
        }
    }
};