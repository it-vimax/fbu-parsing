/**
 * Created by Maks on 07.12.2017.
 */
var request = new MBT.API.Widgets.RenderRequest();
var widget = new MBT.API.Widgets.Widget();
widget.setContainer('schedule-short');
widget.setWidgetId(MBT.API.Widgets.SEASON_SCHEDULE_SHORT_WIDGET);
widget.setParam('league_link_visible', 1);
widget.setParam('team_link_visible', 1);
widget.setParam('game_link_visible', 1);
widget.setParam('hide_games_wo_time', 1);
widget.setParam('player_link_visible', 1);
widget.setParam('all_games_link_visible', 1);
widget.setParam('league_id', MBT.Integration.leagueID);
widget.setParam('season_id', MBT.Integration.seasonID);
widget.setParam('game_link_type', MBT.API.NAVIGATION_TYPE_CUSTOM);
widget.setParam('game_link_handler', 'navigateGame');
widget.setParam('all_games_link_type', MBT.API.NAVIGATION_TYPE_CUSTOM);
widget.setParam('all_games_link_handler', 'navigateSchedule');
widget.setParam('team_link_type', MBT.API.NAVIGATION_TYPE_CUSTOM);
widget.setParam('team_link_handler', 'navigateTeam');
widget.setParam('show_recently_updated_games', 0);
widget.setParam('show_game_number', 1);
widget.setParam('show_playoff_stage', 1);
widget.setParam('show_group_name', 1);
request.addWidget(widget);
request.render();