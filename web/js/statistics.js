/**
 * Created by Maks on 07.12.2017.
 */
var fbu_root_url = "http://fbu.ua/";
navigatePlayer = function(playerId, seasonId) {
    return fbu_root_url + "statistics/league-" + MBT.Integration.leagueID + "/player-" + playerId + "?season_id=" + seasonId
};
navigateGame = function(gameId, seasonId) {
    return fbu_root_url + "statistics/league-" + MBT.Integration.leagueID + "/game-" + gameId + "?season_id=" + seasonId
};
navigateTeam = function(teamId, seasonId) {
    return fbu_root_url + "statistics/league-" + MBT.Integration.leagueID + "/team-" + teamId + "?season_id=" + seasonId
};
navigateLeague = function(leagueId) {
    return fbu_root_url + "statistics/league-" + leagueId + "/"
};
navigateSeason = function(seasonId) {
    return fbu_root_url + "statistics/season-" + seasonId + "/"
};
navigateArena = function(arenaId) {
    return fbu_root_url + "statistics/arena-" + arenaId + "/"
};
navigateSchedule = function(seasonId) {
    return fbu_root_url + "statistics/league-" + MBT.Integration.leagueID + "/calendar"
};
navigateStandings = function(leagueId, seasonId) {
    return fbu_root_url + "statistics/league-" + leagueId + "/standings"
};
navigatePlayerStatsWithCategory = function(leagueId, seasonId, tabNo, categoryId) {
    return "#"
};

if(MBT.Integration==undefined){
    //window.location=window.location;
    setTimeout(function () {
        window.location=window.location;
    }, 500);
}
MBT.Integration.sessionID = null;
if (statistics.arenaID != null) {
    MBT.Integration.arenaID = statistics.arenaID
}
if (statistics.leagueID != null) {
    MBT.Integration.leagueID = statistics.leagueID
}
if (statistics.gameID != null) {
    MBT.Integration.gameID = statistics.gameID
}
if (statistics.teamID != null) {
    MBT.Integration.teamID = statistics.teamID
}
if (statistics.playerID != null) {
    MBT.Integration.playerID = statistics.playerID
}
nt_navigateTeam = function(teamId, seasonId) {
    return fbu_root_url + "national-team/team-" + teamId + "/" + (seasonId != null ? "season-" + seasonId : "")
};
nt_navigatePlayer = function(playerId, seasonId) {
    return fbu_root_url + "national-team/player-" + playerId + "/" + (seasonId != null ? "season-" + seasonId : "")
};
nt_navigateGame = function(gameId, seasonId) {
    return fbu_root_url + "national-team/game-" + gameId + "/" + (seasonId != null ? "season-" + seasonId : "")
};
nt_navigateLeague = function(leagueId, seasonId) {
    return fbu_root_url + "national-team/league-" + leagueId + "/" + (seasonId != null ? "season-" + seasonId : "")
};
if (statistics.NTteamID != null) {
    MBT.Integration.NTteamID = statistics.NTteamID
}
if (statistics.NTplayerID != null) {
    MBT.Integration.NTplayerID = statistics.NTplayerID
}
if (statistics.NTgameID != null) {
    MBT.Integration.NTgameID = statistics.NTgameID
}
if (statistics.NTleagueID != null) {
    MBT.Integration.NTleagueID = statistics.NTleagueID
}
if (MBT.API.URL.CurrentQueryString.get("season_id")) {
    MBT.Integration.NTseasonID = MBT.API.URL.CurrentQueryString.get("season_id")
} else {
    MBT.Integration.NTseasonID = null
}