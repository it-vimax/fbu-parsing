/**
 * Created by Maks on 07.12.2017.
 */
var mbtWidgetsLoadBlock = false;

function mbtWidgetsLoad(scripts) {

    for (i = 0, len = scripts.length; i < len; i++) {
        var scriptSrc = scripts[i] + '?v=' + mbtBasketHotelVersion;
        mbtWidgetsLoadScript(scriptSrc);
    }

}

function mbtWidgetsLoadScript(scriptSrc) {

    if (mbtWidgetsLoadBlock) {
        setTimeout('mbtWidgetsLoadScript(\'' + scriptSrc + '\')', 100);
        return;
    }

    mbtWidgetsLoadBlock = true;

    var fileref=document.createElement('script');
    fileref.setAttribute("type","text/javascript");
    fileref.setAttribute("src", scriptSrc);
    /* IE stuff */
    fileref.onreadystatechange = function () {
        if (this.readyState == 'loaded' || this.readyState == 'complete') {
            mbtWidgetsLoadReleaseBlock();
        }
    }
    /* browsers */
    fileref.onload = mbtWidgetsLoadReleaseBlock;
    if (typeof fileref != "undefined") {
        document.getElementsByTagName("head")[0].appendChild(fileref)
    }

}

function mbtWidgetsLoadReleaseBlock() {
    mbtWidgetsLoadBlock = false;
}