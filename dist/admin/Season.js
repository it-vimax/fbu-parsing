"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by Maks on 10.12.2017.
 */
var Season = (function () {
    function Season() {
    }
    //    public constructor(){}
    Season.prototype.getAll = function () {
        var getTableTimeInterval = setInterval(function () {
            this.allSeasonFromHtml = [];
            var local = this;
            if ($('#5-105-list-container .mbt-holder-content1 a').length > 0) {
                clearInterval(getTableTimeInterval);
                console.log('Таблица загрузилась');
                $('#5-105-filter-season option').each(function (key, value) {
                    local.allSeasonFromHtml.push({
                        id: $(this).val(),
                        name: $(this).text(),
                    });
                });
                console.log(local.allSeasonFromHtml);
                $.ajax({
                    url: '/admin/season/all-save',
                    //headers: {"Authorization": "Basic " + USERNAME + ":" + PASSWORD},	//	отправка заголовка запроса
                    dataType: "json",
                    data: {
                        param: $('meta[name=csrf-param]').attr("content"),
                        token: $('meta[name=csrf-token]').attr("content"),
                        data: local.allSeasonFromHtml,
                    },
                    async: true,
                    cache: true,
                    contentType: "application/x-www-form-urlencoded",
                    type: "post",
                    success: function (data) {
                        if (!data) {
                            console.warn('false');
                            return;
                        }
                        console.log(data);
                    },
                    error: function (error) {
                        console.log(error);
                    },
                    beforeSend: function () { },
                    complete: function () { } //	срабатывает по окончанию запроса
                });
            }
            else {
                console.log('таблица еще не загрузилась');
            }
        }, 300);
    };
    return Season;
}());
exports.Season = Season;
//# sourceMappingURL=Season.js.map