<?php
/**
 * Created by PhpStorm.
 * User: Viktor
 * Date: 21.06.2017
 * Time: 17:12
 */

namespace app\components;
use yii\base\Widget;
use yii\db\Query;
use Yii;

class StatisticsNewsWidget  extends  Widget
{
    public $id;
    public function init()
    {
        parent::init();
        if ($this->id === null) {
            $this->id = 224;
        }

    }
    public function run()
    {
        switch ($this->id){
            case 224:
                $codename = 'superleague';
                break;
            case 491:
                $codename = 'national-men-team';
                break;
            case 591:
                $codename = 'national-women-team';
                break;
            case 621:
                $codename = 'u20-men';
                break;
            case 611:
                $codename = 'u20-women';
                break;
            case 631:
                $codename = 'u18-men';
                break;
            case 601:
                $codename = 'u18-women';
                break;
            case 651:
                $codename = 'u16-men';
                break;
            case 641:
                $codename = 'u16-women';
                break;
            case 32079:
                $codename = 'women-league';
                break;
            case 8185:
                $codename = 'top-men-league';
                break;
            case 8381:
                $codename = 'cup-of-ukraine';
                break;
            case 31503:
                $codename = 'cup-of-ukraine';
                break;
                 case 10843:
                $codename = 'top-women-league';
                break;
            case 31497:
                $codename = 'student-association';
                break;
            case 31833:
                $codename = 'student-women-team';
                break;
            case 11343:
                $codename = 'first-men-league';
                break;
            default:
                $codename = 'superleague';
                break;
        }
        $news = Yii::$app->cache->get('news'.$this->id);
        if(!$news) {

            $query = new Query();
            $query->select([
                'news.*',
                'newstopics.topicid as topic',
                'topic.name as tname',
                'topic.codename as topicurl',
                'topic.topicid as topicid',

            ])
                ->from('news')

                ->join('LEFT JOIN', 'newstopics',
                    'newstopics.newsid =news.newsid')
                ->join('LEFT JOIN', 'topic',
                    'topic.topicid =newstopics.topicid')
                ->where(['news.isactive' => 1, 'news.isremoved' => 0])
                ->andWhere(['<', 'news.date', date("Y-m-d H:i:s")])
                ->andWhere(['topic.codename'=>$codename])
                ->orderBy(['news.date' => SORT_DESC])

                ->limit(20);

            $news = $query->all();
            Yii::$app->cache->set('news'.$this->id, $news, 60*60*24);
        }
        return $this->render('statisticnews', [
            'news'=>$news
        ]);
    }

}