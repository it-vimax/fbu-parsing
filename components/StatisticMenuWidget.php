<?php
/**
 * Created by PhpStorm.
 * User: Viktor
 * Date: 21.06.2017
 * Time: 14:16
 */

namespace app\components;
use yii\base\Widget;

class StatisticMenuWidget  extends  Widget
{
    public $id;
    public $type;
    public function init()
    {
        parent::init();
        if ($this->id === null) {
            $this->id = 224;
        }

    }
    public function run()
    {
        return $this->render('statistic', [
            'id'=>$this->id,
            'type'=>$this->type
        ]);
    }
}