<?php
/**
 * Created by PhpStorm.
 * User: Viktor
 * Date: 03.08.2017
 * Time: 12:24
 */

namespace app\components;
use yii\base\Widget;


class RegistrationMenuWidget extends  Widget
{
    public $url;
    public function init()
    {
        parent::init();
        if ($this->url === null) {
            $this->url = 'coaches/create';
        }

    }
    public function run()
    {
        return $this->render('registration', ['url'=>$this->url]);
    }
}