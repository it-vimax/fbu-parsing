<?php
/**
 * Created by PhpStorm.
 * User: Viktor
 * Date: 19.06.2017
 * Time: 15:46
 */

namespace app\components;
use yii\base\Widget;

class MediaLeftWidget extends  Widget
{
    public $id;
    public function init()
    {
        parent::init();
        if ($this->id === null) {
            $this->id = 0;
        }

    }
    public function run()
    {


        return $this->render('menumedia', [
            'id'=>$this->id,
            ]);
    }
}