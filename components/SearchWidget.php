<?php
/**
 * Created by PhpStorm.
 * User: Viktor
 * Date: 19.06.2017
 * Time: 15:46
 */

namespace app\components;
use yii\base\Widget;
use app\models\SearchForm;


class SearchWidget extends  Widget
{
    public function init()
    {
        parent::init();


    }
    public function run()
    {

    	$model = new SearchForm();
        return $this->render('searchform', ['model'=>$model]);
    }
}