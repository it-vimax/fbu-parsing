<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 06.08.2017
 * Time: 16:59
 */

namespace app\components;
use yii\base\Widget;

class RegionsMenuLeftWidget extends Widget
{
    public $type;
    public $url;
    public $amatour_link;
    public function init()
    {
        parent::init();
        if ($this->type === null) {
            $this->type = null;
        }

        if ($this->url === null) {
            $this->url = 'cherkaska-oblasna-federaciya-basketbolu';
        }


    }
    public function run()
    {


        return $this->render('menuregions', ['url'=>$this->url, 'type'=>$this->type, 'amatour_link' =>$this->amatour_link ]);
    }
}