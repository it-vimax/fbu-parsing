<?php
/**
 * Created by PhpStorm.
 * User: Viktor
 * Date: 21.06.2017
 * Time: 14:16
 */

namespace app\components;
use yii\base\Widget;

class NationalteamMenuWidget  extends  Widget
{
    public $id;
    public $type;
    public function init()
    {
        parent::init();
        if ($this->id === null) {
            $this->id = 491;
        }

    }
    public function run()
    {
        return $this->render('nt-team', [
            'id'=>$this->id,
        ]);
    }
}