<?php
/**
 * Created by PhpStorm.
 * User: Viktor
 * Date: 19.06.2017
 * Time: 15:46
 */

namespace app\components;
use yii\base\Widget;

class MenuLeftWidget extends  Widget
{
    public function init()
    {
        parent::init();

    }
    public function run()
    {


        return $this->render('menuleft');
    }
}