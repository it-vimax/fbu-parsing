<?php
use yii\helpers\Html;
use app\components\CropImageWidget;
setlocale(LC_ALL, 'uk_UA.UTF-8');
?>
<div class="slider_games">
    <div class="container">
        <div class="owl_carousel games_top">
            <?php if(!!$games) { ?>
                <?php foreach($games as $game) { ?>
                    <div class="item cl-<?=$game['league_type']?> <?php if(time() < strtotime($game['date_game'])) { ?>first<?php } ?> ">
                        <div id="<?php echo $game['xssport_link'];?>" class="annons_item <?php  if(time() >= strtotime($game['date_game']) && time() < (strtotime($game['date_game']) + 60*120)) { ?>new <?php } ?>">
                            <?php  if(time() >= strtotime($game['date_game']) && time() < (strtotime($game['date_game']) + 60*120)) { ?>
                                <div class="date_time_place">
                                    <span class="online">ОН-ЛАЙН</span>
                                </div>
                            <?php } else { ?>

                                <div class="date_time_place">

                                    <div class="date"><?php echo strftime("%e %m %Y",strtotime($game['date_game'])); ?></div>

                                    <div class="place_time"><?php echo date("H:i",strtotime($game['date_game']));?>  <?php echo $game['place_game'];?></div>

                                </div>
                            <?php } ?>
                            <?php if(!!$game['league_type']) { ?>
                                <div class="rang_play">
                                    <?php $league_type = !!preg_match('/league/', $game['league_type'])?1:(!!preg_match('/team/', $game['league_type'])?2:false);
                                    switch ($league_type){
                                        case 1:
                                            $league_url = '/statistics/'.$game['league_type'];
                                            break;
                                        case 2:
                                            $league_url = '/national-team/'.$game['league_type'];
                                            break;
                                        default:
                                            $league_url = false;
                                            break;
                                    } ?>
                                    <a href="<?=$league_url?>">
                                        <?php echo $game['ranggame'];?> <?=!!$game['tourn']?'Тур №'.$game['tourn']:''?>
                                    </a>
                                </div>
                            <?php }  else {?>
                                <div class="rang_play"><?php echo $game['ranggame'];?> <?=!!$game['tourn']?'Тур №'.$game['tourn']:''?></div>
                            <?php } ?>

                            <div class="games_contr">
                                <div class="team">
                                    <?php if(!!$game['teama_link']){?>
                                        <a href="<?=$game['teama_link']?>" target="_blank">
                                            <img src="<?= CropImageWidget::widget(['url'=>'http://i.fbu.kiev.ua/1/gameswidget/'.$game['teama_image'], 'type'=>'calendar', 'height'=>79, 'width'=>79])?>" alt="<?=$game['teama_name']?>">
                                            <span><?php echo $game['teama_name'];?></span>
                                        </a>
                                    <?php } else { ?>
                                        <img src="<?= CropImageWidget::widget(['url'=>'http://i.fbu.kiev.ua/1/gameswidget/'.$game['teama_image'], 'type'=>'calendar', 'height'=>79, 'width'=>79])?>" alt="<?=$game['teama_name']?>">
                                        <span><?php echo $game['teama_name'];?></span>
                                    <?php } ?>


                                </div>
                                <div class="score_video">
                                    <?php if(time() >= strtotime($game['date_game']) && time() < (strtotime($game['date_game']) + 60*120)) { ?>
                                        <?php if((!!$game['youtube_link'] || !!$game['xssport_link']) && !$game['teama_score']) { ?>

                                            <div class="look">Дивіться:</div>
                                            <?php if(!!$game['youtube_link']) { ?><a href="<?=$game['youtube_link']?>" target="_blank"><img src="/web/img/youtube.png" alt=""></a><?php } ?>
                                            <?php if(!!$game['xssport_link']) { ?><a href="<?=$game['xssport_link']?>" target="_blank"><img src="/web/img/xsport.png" alt=""></a><?php } ?>
                                        <?php } elseif ($game['teama_score']){ ?>
                                            <div class="<?php echo ($game['teama_score'] > $game['teamb_score'])?'score_win':'score_loose'; ?> "><?=$game['teama_score']?><span class="two_dots"></span></div>
                                            <span>:</span>
                                            <div class="<?php echo ($game['teamb_score'] > $game['teama_score'])?'score_win':'score_loose'; ?> "><?=$game['teamb_score']?></div>
                                        <?php } ?>
                                    <?php }  elseif (strtotime($game['date_game']) > time() && (strtotime($game['date_game']) < (time() + 60*60*3))) { ?>
                                        <div class="soon">Скоро почнеться</div>
                                        <?php if(!!$game['youtube_link'] || !!$game['xssport_link']) { ?>
                                            <?php if(!!$game['youtube_link']) { ?><a href="<?=$game['youtube_link']?>" target="_blank"><img src="/web/img/youtube.png" alt=""></a><?php } ?>
                                            <?php if(!!$game['xssport_link']) { ?><a href="<?=$game['xssport_link']?>" target="_blank"><img src="/web/img/xsport.png" alt=""></a><?php } ?>
                                        <?php } ?>
                                    <?php }  elseif ( time() < strtotime($game['date_game']) +  60*60*3  && !$game['teama_score']) { ?>
                                        <?php if(!!$game['xssport_link']) { ?><a href="<?=$game['xssport_link']?>" target="_blank"><img src="/web/img/xsport.png" alt=""></a><?php } ?>
                                        <?php if(!!$game['youtube_link']) { ?><a href="<?=$game['youtube_link']?>" target="_blank"><img src="/web/img/youtube.png" alt=""></a><?php } ?>
                                    <?php }  else { ?>
                                        <?php if(!!$game['teama_score'] && !!$game['teamb_score']) { ?>
                                            <div class="<?php echo ($game['teama_score'] > $game['teamb_score'])?'score_win':'score_loose'; ?> "><?=$game['teama_score']?><span class="two_dots"></span></div>
                                            <span>:</span>
                                            <div class="<?php echo ($game['teamb_score'] > $game['teama_score'])?'score_win':'score_loose'; ?> "><?=$game['teamb_score']?></div>
                                        <?php } ?>
                                    <?php } ?>
                                    <?php if(strtotime($game['date_game'])< time()) { ?>
                                        <div class="block_detalis_match">
                                            <?php if(!!$game['foto_link']) { ?><span class="foto"><a href="<?=$game['foto_link']?>"><i class="fa fa-camera" aria-hidden="true"></i></a></span><?php } ?>
                                            <?php if(!!$game['video_link']) { ?><span class="video"><a href="<?=$game['video_link']?>"><i class="fa fa-video-camera" aria-hidden="true"></i></a></span><?php } ?>
                                            <?php if(!!$game['post_link']) { ?><span class="news"><a href="<?=$game['post_link']?>"><i class="fa fa-file-o" aria-hidden="true"></i></a></span><?php } ?>
                                            <?php if(!!$game['stats_link']) { ?><span class="statistic"><a href="<?=$game['stats_link']?>"><i class="fa fa-bar-chart" aria-hidden="true"></i></a></span><?php } ?>
                                        </div>
                                    <?php } ?>
                                </div>
                                <div class="team">
                                    <?php if(!!$game['teamb_link']){?>
                                        <a href="<?=$game['teamb_link']?>" target="_blank">
                                            <img src="<?= CropImageWidget::widget(['url'=>'http://i.fbu.kiev.ua/1/gameswidget/'.$game['teamb_image'], 'type'=>'calendar', 'height'=>79, 'width'=>79])?>" alt="<?=$game['teamb_name']?>">
                                            <span><?php echo $game['teamb_name'];?></span>
                                        </a>
                                    <?php } else { ?>
                                        <img src="<?= CropImageWidget::widget(['url'=>'http://i.fbu.kiev.ua/1/gameswidget/'.$game['teamb_image'], 'type'=>'calendar', 'height'=>79, 'width'=>79])?>" alt="<?=$game['teamb_name']?>">
                                        <span><?php echo $game['teamb_name'];?></span>

                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>
            <!--<iframe frameborder="0" width="100%" height="100%" src="http://www.fiba.basketball/lsheader/index.html#12105" scrolling="no" border="0" ></iframe>-->

        </div>
    </div>
    <a href="https://esport.in.ua/uk" target="_blank" class="bilet_one_sec">Купити квиток за 55 сек</a>
    <a href="https://esport.in.ua/uk" target="_blank" class="bilet_one_sec">Купити квиток за 55 сек</a>
</div>