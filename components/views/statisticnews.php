<?php
use yii\helpers\Url;
use app\components\CropImageWidget;
?>
<div class="statisctic_block_news">
    <?php foreach ($news as $new) { ?>
    <?php $video = strpos($new['title'], 'відео'); ?>
    <div class="item_news_one <?php echo ($video || !!$new['videotype'])?'video':''; ?>">
        <div class="img_block">
         <a href="<?=Url::to(['news/news', 'nurl' => $new['nurl']])?>">
             <?php if(!!$new['imagehorizontal']){?>
                 <img src="<?= CropImageWidget::widget(['url'=>'http://i.fbu.kiev.ua/1/'.$new['newsid'].'/'.$new['imagehorizontal'], 'height'=>200, 'width'=>250])?>" alt="<?=$new['title']?>">
             <?php } ?>
         </a>
        </div>
        <div class="smal_info_block">
            <div class="date_and_cat">
                <?php if(!!$new['tname']) { ?>
                    <span class="cat"><a href="<?=!!$video?Url::to(['media/index']):Url::to(['topic/topic', 'codename'=>$new['topicurl']])?>"><?=!!$video?'Відео':$new['tname']?></a></span>
                <?php } ?>
                <span class="date"><?php echo date("d.m.Y",strtotime($new['date']));?></span>
            </div>
            <a href="<?=Url::to(['news/news', 'nurl' => $new['nurl']])?>" class="title"><?=$new['title']?></a>
            <div class="short_text"><?=$new['description']?></div>
        </div>
    </div>
    <?php } ?>

</div>
<div class="load_more" data-offset="20" data-section="0" data-topic="<?=$new['topicid']?>">
    Показати ще
</div>
