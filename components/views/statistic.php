<?php
use yii\helpers\Html;
use app\assets\AppAsset;
use yii\helpers\Url;
$vubl = array(27841, 27911, 27851, 27921, 29733, 29757, 27811, 27881, 31435, 31437, 32103, 32105, 27821, 27891, 27831, 27901 );
$amatour = array(33819, 34221,  34211, 34223, 34247, 34329, 34317, 34331, 34507);
?>
<div class="sub_header">
    <div class="container">

        <div class="drop_down_list_leages">
            <div class="liga_title">Суперліга Парі-Матч</div>

            <ul class="list_leages">
                <li class="<?php echo $id == 224?'active':'';?>" ><a href="<?=Url::to(['statistics/league', 'id'=>224, 'type'=>$type])?>">(мущени - пасинг) Суперліга Парі-Матч</a></li>
                <li class="<?php echo $id == 32079?'active':'';?>"><a href="<?=Url::to(['statistics/league', 'id'=>32079, 'type'=>$type])?>">Суперліга (жінки)</a></li>
                <li class="<?php echo $id == 8185?'active':'';?>"><a href="<?=Url::to(['statistics/league', 'id'=>8185, 'type'=>$type])?>">(мущени - пасинг) Вища лiга</a></li>
                <li class="<?php echo $id == 10843?'active':'';?>"><a href="<?=Url::to(['statistics/league', 'id'=>10843, 'type'=>$type])?>">Вища лiга (жінки)</a></li>
                <li class="<?php echo $id == 11343?'active':'';?>"><a href="<?=Url::to(['statistics/league', 'id'=>11343, 'type'=>$type])?>">(мущени - пасинг) Перша ліга</a></li>
                <li class="<?php echo $id == 8381?'active':'';?>"><a href="<?=Url::to(['statistics/league', 'id'=>8381, 'type'=>$type])?>">Кубок України (чоловіки)</a></li>
                <li class="<?php echo $id == 31503?'active':'';?>"><a href="<?=Url::to(['statistics/league', 'id'=>31503, 'type'=>$type])?>">Кубок України (жінки)</a></li>
                <li class="<?php echo $id == 31497?'active':'';?>"><a href="<?=Url::to(['statistics/league', 'id'=>31497, 'type'=>$type])?>">Студентська ліга чоловіки</a></li>
                <li class="<?php echo $id == 31833?'active':'';?>"><a href="<?=Url::to(['statistics/league', 'id'=>31833, 'type'=>$type])?>">Студентська ліга жінки</a></li>
                <li class="<?php echo in_array($id, $vubl)?'active':'';?>"><a href="<?=Url::to(['statistics/league', 'id'=>27851, 'type'=>$type])?>">Юнацька ліга</a></li>
                <li class="<?php echo in_array($id, $amatour)?'active':'';?>"><a href="<?=Url::to(['statistics/league', 'id'=>34223, 'type'=>$type])?>">Аматорська ліга</a></li>
            </ul>
        </div>
        <div class="statistic_switsher">
            <i class="fa fa-bars" aria-hidden="true"></i>
        </div>
        <div class="statisctic_controllers">
            <ul>
                <li class=""><a href="<?=Url::to(['statistics/league', 'id'=>$id])?>" class="">Статистика</a></li>
                <li class=""><a href="<?=Url::to(['statistics/league', 'id'=>$id, 'type'=>'standings'])?>" class="">Турнірна таблиця</a></li>
                <li class=""><a href="<?=Url::to(['statistics/league', 'id'=>$id, 'type'=>'leaders'])?>" class="">Лідери</a></li>
                <li class=""><a href="<?=Url::to(['statistics/league', 'id'=>$id, 'type'=>'calendar'])?>" class="">Календар</a></li>
                <li class=""><a href="<?=Url::to(['statistics/league', 'id'=>$id, 'type'=>'teams'])?>" class="">Команди</a></li>
                <li class=""><a href="<?=Url::to(['statistics/league', 'id'=>$id, 'type'=>'players'])?>" class="">Гравці</a></li>
                <li class=""><a href="<?=Url::to(['statistics/league', 'id'=>$id, 'type'=>'records'])?>" class="">Рекорди</a></li>
                <li class=""><a href="<?=Url::to(['statistics/league', 'id'=>$id, 'type'=>'coaches'])?>" class="">Тренери</a></li>
                <li class=""><a href="<?=Url::to(['statistics/league', 'id'=>$id, 'type'=>'referees'])?>" class="">Судді</a></li>
                <li class=""><a href="<?=Url::to(['statistics/league', 'id'=>$id, 'type'=>'commissars'])?>" class="">Комісари</a></li>
            </ul>
        </div>
    </div>
</div>
