<?php
use yii\helpers\Html;
use app\assets\AppAsset;
use yii\helpers\Url;
?>
<div class="sub_header">
    <div class="container">

        <div class="drop_down_list_leages">
            <div class="liga_title">Національна чоловіча збірна</div>

            <ul class="list_leages">
                <li class="<?php echo $id == 491?'active':'';?>" ><a href="<?=Url::to(['national-team/team', 'id'=>491])?>">Національна чоловіча збірна</a></li>
                <li class="<?php echo $id == 591?'active':'';?>"><a href="<?=Url::to(['national-team/team', 'id'=>591])?>">Національна жіноча збірна</a></li>
                <li class="<?php echo $id == 621?'active':'';?>"><a href="<?=Url::to(['national-team/team', 'id'=>621])?>">Молодша чоловіча збірна (U20)</a></li>
                <li class="<?php echo $id == 611?'active':'';?>"><a href="<?=Url::to(['national-team/team', 'id'=>611])?>">Молодша жіноча збірна (U20)</a></li>
                <li class="<?php echo $id == 631?'active':'';?>"><a href="<?=Url::to(['national-team/team', 'id'=>631])?>">Юніорська чоловіча збірна (U18)</a></li>
                <li class="<?php echo $id == 601?'active':'';?>"><a href="<?=Url::to(['national-team/team', 'id'=>601])?>">Юніорська жіноча збірна (U18)</a></li>
                <li class="<?php echo $id == 651?'active':'';?>"><a href="<?=Url::to(['national-team/team', 'id'=>651])?>">Кадетська чоловіча збірна (U16)</a></li>
                <li class="<?php echo $id == 641?'active':'';?>"><a href="<?=Url::to(['national-team/team', 'id'=>641])?>">Кадетська жіноча збірна (U16)</a></li>

            </ul>
        </div>

    </div>
</div>
