<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SearchForm */
/* @var $form ActiveForm */
?>
<div class="search">
<div class="container">

    <?php $form = ActiveForm::begin(['id'=>'search', 'action'=>'/search']); ?>

        <?= $form->field($model, 'q')->label(false) ?>
    
        <div class="form-group">
            <?= Html::submitButton('<i class="fa fa-search" aria-hidden="true"></i>', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>
</div>