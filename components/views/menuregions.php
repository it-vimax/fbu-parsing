<?php
use yii\helpers\Url;
?>
<div class="menu_news">
    <ul>
        <?php if($amatour_link != null) { ?>
        <li class=""><a href="<?=$amatour_link?>">Аматорський чемпіонат</a></li>
        <?php } ?>
        <li class="<?php echo $type=='news'?'active':''; ?>"><a href="<?=Url::to(['regions/one', 'url'=>$url, 'type'=>'news'])?>">Новини</a></li>
        <li class="<?php echo $type=='contact'?'active':''; ?>"><a href="<?=Url::to(['regions/one', 'url'=>$url, 'type'=>'contact'])?>">Контакти</a></li>
        <li class="<?php echo $type=='clubs'?'active':''; ?>"><a href="<?=Url::to(['regions/one', 'url'=>$url, 'type'=>'clubs'])?>">Клуби</a></li>
        <li class="<?php echo $type=='schools'?'active':''; ?>"><a href="<?=Url::to(['regions/one', 'url'=>$url, 'type'=>'schools'])?>">Баскетбольні школи</a></li>
        <li class="<?php echo $type=='leaders'?'active':''; ?>"><a href="<?=Url::to(['regions/one', 'url'=>$url, 'type'=>'leaders'])?>">Керівництво</a></li>
    </ul>
</div>