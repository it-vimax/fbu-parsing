<?php
use yii\helpers\Url;
?>
<div class="menu_registration">
    <ul>
        <li class="<?=($url=='reffere/index')?'active':''?>"><a href="<?=Url::to(['reffere/index'])?>">Судді та комісари</a></li>
        <li class="<?=($url=='reffere/news')?'active':''?>"><a href="<?=Url::to(['reffere/news'])?>">Новини</a></li>
        <li class="<?=($url=='reffere/document-commicar')?'active':''?>"><a href="<?=Url::to(['reffere/document-commicar'])?>">Документи для комісарів</a></li>
        <li class="<?=($url=='reffere/test')?'active':''?>"><a href="<?=Url::to(['reffere/test'])?>">Тести</a></li>
        <li class="<?=($url=='reffere/document-reffere')?'active':''?>"><a href="<?=Url::to(['reffere/document-reffere'])?>">Документи для суддів</a></li>
        <li class="<?=($url=='reffere/tk-fbu')?'active':''?>"><a href="<?=Url::to(['reffere/tk-fbu'])?>">ТК ФБУ</a></li>
        <li class="<?=($url=='reffere/question')?'active':''?>"><a href="<?=Url::to(['reffere/question'])?>">Запитання до арбітрів</a></li>

    </ul>
</div>