<?php
use yii\helpers\Url;
use app\components\CropImageWidget;
?>
<div class="left_column_news">




    <div class="main-block_title"><a href="<?=Url::to(['news/mizhnarodninovyny'])?>">Міжнародні новини</a></div>
    <?php $i=0; foreach ($news as $new) { ?>

        <?php $video = strpos($new['title'], 'відео'); ?>
        <div class="one_new_small <?php echo ($video || !!$new['videotype'])?'video':''; ?>">
            <div class="img_block">
                <a href="<?=Url::to(['news/news', 'nurl' => $new['nurl']])?>">
                    <?php if(!!$new['imagehorizontal']){?>
                        <img src="<?= CropImageWidget::widget(['url'=>'http://i.fbu.kiev.ua/1/'.$new['newsid'].'/'.$new['imagehorizontal'], 'height'=>75, 'width'=>75])?>" alt="<?=$new['title']?>">
                    <?php } ?>
                </a>
            </div>
            <div class="smal_info_block">
                <div class="date_and_cat">
                    <?php if(!!$new['topic']) { ?>
                        <span class="cat"><a href="<?=!!$video?Url::to(['media/index']):Url::to(['topic/topic', 'codename'=>$topics[$new['topic']]['codename']])?>"><?=!!$video?'Відео':$topics[$new['topic']]['name']?></a></span>
                    <?php } ?>
                    <span class="date"><?php echo date("d.m.Y",strtotime($new['date']));?></span>
                </div>
                <a href="<?=Url::to(['news/news', 'nurl' => $new['nurl']])?>" class="title"><?=$new['title']?></a>
            </div>
        </div>
        <?php $i++; } ?>
</div>
