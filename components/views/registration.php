<?php
use yii\helpers\Url;
?>
<div class="menu_registration">
    <ul>
        <li class="<?=($url=='amator/create')?'active':''?>"><a href="<?=Url::to(['amator/create'])?>">Аматорські чемпіонати України</a></li>
        <li class="<?=($url=='amator-player/create')?'active':''?>"><a href="<?=Url::to(['amator-player/create'])?>">Гравці аматорських чемпіонатів України</a></li>
        <li class="<?=($url=='player-vubl/create')?'active':''?>"><a href="<?=Url::to(['player-vubl/create'])?>">Гравці ВЮБЛ</a></li>
        <li class="<?=($url=='coaches/create')?'active':''?>"><a href="<?=Url::to(['coaches/create'])?>">Тренери</a></li>
        <li class="<?=($url=='teamreg/create')?'active':''?>"><a href="<?=Url::to(['teamreg/create'])?>">Реєстрація команди</a></li>
        

    </ul>
</div>