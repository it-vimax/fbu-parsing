<?php
use yii\helpers\Url;
?>
<div class="left_menu_switcher">
    Категорії відео
</div>
<div class="menu_news">
    <ul>
        <li><a href="<?=Url::to(['media/index'])?>"> Всі відеоновини</a></li>
        <li><a href="<?=Url::to(['media/type', 'id'=>1])?>"> Топ-5 моментів</a></li>
        <li><a href="<?=Url::to(['media/type', 'id'=>2])?>">Матч тижня</a></li>
        <li><a href="<?=Url::to(['media/type', 'id'=>3])?>">Інтерв’ю</a></li>

    </ul>
</div>