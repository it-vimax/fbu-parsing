<?php
use yii\helpers\Url;
use app\components\CropImageWidget;
?>
<div class="left_column_news">

<?php if(!empty($banner) && !is_string($banner)) { ?>

<a href="<?php echo $banner['link'];?>" style="background-image:url(<?= CropImageWidget::widget(['url'=>'http://i.fbu.kiev.ua/1/gameswidget/'.$banner['foto'], 'height'=>854, 'width'=>607])?>" class="afisha_bg" target="_blank"></a>

<?php }  else {  ?>
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <!-- fbu-336x280-1 -->
    <ins class="adsbygoogle"
         style="display:inline-block;width:336px;height:280px"
         data-ad-client="ca-pub-9416686036368899"
         data-ad-slot="5617890568"></ins>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({});
    </script>

<?php } ?>

<div id="SinoptikInformer" style="width:336px;" class="SinoptikInformer type5"><div class="siHeader"><div class="siLh"><div class="siMh"><a onmousedown="siClickCount();" class="siLogo" href="https://ua.sinoptik.ua/" target="_blank" rel="nofollow" title="Погода"> </a>Погода <span id="siHeader"></span></div></div></div><div class="siBody"><a onmousedown="siClickCount();" href="https://ua.sinoptik.ua/погода-київ" title="Погода у Києві" target="_blank" rel="nofollow"><div class="siCity"><div class="siCityName">Погода у <span>Києві</span></div><div id="siCont0" class="siBodyContent"><div class="siLeft"><div class="siTerm"></div><div class="siT" id="siT0"></div><div id="weatherIco0"></div></div><div class="siInf"><p>вологість: <span id="vl0"></span></p><p>тиск: <span id="dav0"></span></p><p>вітер: <span id="wind0"></span></p></div></div></div></a><div class="siLinks">Погода на <a href="https://ua.sinoptik.ua/погода-вінниця/" title="Погода у Вінниці" target="_blank" rel="nofollow" onmousedown="siClickCount();">sinoptik.ua</a>  у Вінниці</div></div><div class="siFooter"><div class="siLf"><div class="siMf"></div></div></div></div><script type="text/javascript" charset="UTF-8" src="//sinoptik.ua/informers_js.php?title=4&amp;wind=2&amp;cities=303010783&amp;lang=ua"></script>
    

<link rel="stylesheet" type="text/css" href="http://web1.mbt.lt/prod/snakesilver-client/liveGamesBanner-small.css" />
       
        <div style="padding-bottom: 30px">
            <div id="live-games-banner-container-superleague" class="live-games-banner"></div>
        </div>
         
        <div id="live-games-banner-container" class="live-games-banner"></div>
        
       

    <div class="main-block_title"><a href="<?=Url::to(['news/news'])?>">Всі новини</a></div>
    <?php $i=0; foreach ($news as $new) { ?>
    <?php if($i > $limit - 1) break; ?>
        <?php $video = strpos($new['title'], 'відео'); ?>
    <div class="one_new_small <?php echo ($video || !!$new['videotype'])?'video':''; ?>">
        <div class="img_block">
            <a href="<?=Url::to(['news/news', 'nurl' => $new['nurl']])?>">
                <?php if(!!$new['imagehorizontal']){?>
                    <img src="<?= CropImageWidget::widget(['url'=>'http://i.fbu.kiev.ua/1/'.$new['newsid'].'/'.$new['imagehorizontal'], 'height'=>75, 'width'=>75])?>" alt="<?=$new['title']?>">
                <?php } ?>
            </a>
        </div>
        <div class="smal_info_block">
            <div class="date_and_cat">
                <?php if(!!$new['topic']) { ?>
                    <span class="cat"><a href="<?=!!$video?Url::to(['media/index']):Url::to(['topic/topic', 'codename'=>$topics[$new['topic']]['codename']])?>"><?=!!$video?'Відео':$topics[$new['topic']]['name']?></a></span>
                <?php } ?>
                <span class="date"><?php echo date("d.m.Y",strtotime($new['date']));?></span>
            </div>
            <a href="<?=Url::to(['news/news', 'nurl' => $new['nurl']])?>" class="title"><?=$new['title']?></a>
        </div>
    </div>
    <?php $i++; } ?>

    <?php if(!$main){?>
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <!-- fbu-336x280-2 -->
    <ins class="adsbygoogle"
         style="display:inline-block;width:336px;height:280px"
         data-ad-client="ca-pub-9416686036368899"
         data-ad-slot="7094623768"></ins>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({});
    </script>
    <?php }?>
</div>
