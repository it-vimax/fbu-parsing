<?php
use yii\helpers\Url;
?>
<div class="left_menu_switcher">
    Всі категорії новин
</div>
<div class="menu_news">
    <ul>
        <li><a href="<?=Url::to(['news/index'])?>"> Усі новини</a></li>
        <li><a href="<?=Url::to(['basketball/index'])?>">Новини баскетболу</a></li>
        <li><a href="<?=Url::to(['fbu/index'])?>">Новини ФБУ</a></li>
        <li><a href="<?=Url::to(['topic/topic', 'codename'=>'3x3'])?>">Баскетбол 3Х3</a></li>

        <li><a href="<?=Url::to(['topic/topic', 'codename'=>'superleague'])?>">Суперліга Парі-Матч</a></li>
        <li><a href="<?=Url::to(['topic/topic', 'codename'=>'women-league'])?>">Суперліга (жінки)</a></li>
        <li><a href="<?=Url::to(['topic/topic', 'codename'=>'top-men-league'])?>">Вища лiга</a></li>
        <li><a href="<?=Url::to(['topic/topic', 'codename'=>'first-men-league'])?>">Перша ліга</a></li>

        <li><a href="<?=Url::to(['topic/topic', 'codename'=>'cup-of-ukraine'])?>">Кубок України</a></li>
        <li><a href="<?=Url::to(['topic/topic', 'codename'=>'eurobasket-2017'])?>">Євробаскет 2017</a></li>
        <li><a href="<?=Url::to(['topic/topic', 'codename'=>'liga-chempioniv-fiba'])?>">Єврокубки</a></li>
        <li><a href="<?=Url::to(['topic/topic', 'codename'=>'student-association'])?>">Студентська ліга</a></li>
        <li><a href="<?=Url::to(['topic/topic', 'codename'=>'vubl'])?>">Юнацька ліга</a></li>
        <li><a href="<?=Url::to(['topic/topic', 'codename'=>'veterani'])?>">Ветерани</a></li>
    </ul>
</div>