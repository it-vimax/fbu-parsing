<?php
/**
 * Created by PhpStorm.
 * User: Viktor
 * Date: 04.07.2017
 * Time: 16:24
 */

namespace app\components;

use yii\base\Widget;
use yii\db\Query;
use Yii;


class GamesCalendarWidget extends  Widget
{
    public function init()
    {

        parent::init();

    }
    public function run()
    {
        $games = Yii::$app->cache->get('gameswidget');
        if(!$games){
            $query = new Query();
            $query->select([])
                ->from('gameswidget')
                ->orderBy(['date_game'=>SORT_DESC])
                ->limit(20);
            $games = $query->all();
            Yii::$app->cache->set('gameswidget', $games, 60*60*24);
        }

        $games=array_reverse($games);

        return $this->render('gamescalendar', ['games'=>$games]);
    }

}