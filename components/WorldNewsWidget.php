<?php


namespace app\components;
use yii\base\Widget;
use yii\db\Query;
use Yii;

class WorldNewsWidget extends Widget
{


    public function init()
    {
        parent::init();

    }

    public function run()
    {
        $topics = Yii::$app->cache->get('topics');
        if(!$topics) {
            $query = new Query();
            $query->select([
                'topicid',
                'name',
                'codename'
            ])
                ->from('topic')
                ->where(['siteid' => 1]);

            $topics = $query->all();
            $topic_result = array();
            foreach ($topics as $topic){
                $topic_result[$topic['topicid']] = $topic;
            }
            Yii::$app->cache->set('topics', $topic_result, 60*60*24);
        }


        $news = Yii::$app->cache->get('worldnews');
        if(!$news) {

            $query = new Query();
            $subquery = new Query();
            $subquery ->select(['newstopics.topicid as as'])
                ->from('newstopics' )
                ->where('newstopics.newsid = news.newsid')->limit(1);

            $query ->select(['news.*', 'topic' =>$subquery])
                ->from('news')
                ->where(['news.isactive' => 1, 'news.isremoved' => 0, 'news.sectionid'=>14])
                ->andWhere(['<', 'news.date', date("Y-m-d H:i:s")])
                ->orderBy(['news.date' => SORT_DESC])
                ->limit(4);
            $news = $query->all();
            Yii::$app->cache->set('worldnews', $news, 60*60*24);
        }

        $topics= Yii::$app->cache->get('topics');

        return $this->render('world', ['news'=>$news, 'topics'=>$topics]);
    }
}