<?php
/**
 * Created by PhpStorm.
 * User: Viktor
 * Date: 22.06.2017
 * Time: 12:49
 */

namespace app\components;
use yii\base\Widget;
use yii\imagine\Image;
use Yii;
use Imagine\Image\Point;
use Imagine\Image\Box;
use Imagine\Image\ImageInterface;

class CropImageWidget extends Widget
{
    public $url;
    public $height;
    public $width;
    public $type;
    public function init()
    {
        parent::init();
        if($this->height === null){
            $this->height = 200;
        }
        if($this->width === null){
            $this->width = 200;
        }

    }
    public function run()
    {
        


        $url = str_replace('http://admin.fb-site.dev/', '', $this->url);
        $url = str_replace('http://i.fbu.kiev.ua/', '', $url);
        $image = explode('/', $url);
        $file = '@webroot/img';
        for ($i = 0; $i < count($image) - 1; $i++) {
            $file .= '/' . $image[$i];
            if (!is_dir(Yii::getAlias($file))) {
                @mkdir(Yii::getAlias($file));
            }
        }

        $image = $image[count($image) - 1];

        $image = str_replace(' ', '', $image);
        $image_name = explode('.', $image);
        $url = str_replace(' ', '', $url);
        $path = str_replace($image, '', $url);
        $path = '@webroot/img/' . $path . $image_name[0] . '_' . $this->width . '_' . $this->height . '.' . $image_name[1];
        $headers = get_headers($this->url);

        if (preg_match("/404/", $headers[0])) {


            return $this->url;
        }
         
        if ($this->type == "fb") {

            if (!file_exists(Yii::getAlias($path))) {
                $img_fb = Image::getImagine()->open($this->url);
                $height = $img_fb->getSize()->getHeight();
                $width = $img_fb->getSize()->getWidth();
                $koef = $width / $height;
                if ($height > $width) {
                	
                    $img_fb->resize(new Box(440, 440 * $koef))->crop(new Point(0, 0), new Box(440, 440))->save(Yii::getAlias($path));
                } else {
                	$x = (440 * $koef - 400) / 2;
                    $img_fb->resize(new Box(440* $koef, 440))->crop(new Point($x, 0), new Box(440, 440))->save(Yii::getAlias($path));
                }

                $path_img_fb = str_replace('@webroot', 'http://fbu.ua/web', $path);
                $collage = Image::getImagine()->create(new Box(660, 440));
                $photo = Image::getImagine()->open('http://fbu.ua/web/img/watermark.jpg');
                $collage->paste($photo, new Point(0, 0));
                $img_fb = Image::getImagine()->open($path_img_fb);
                $collage->paste($img_fb, new Point(220, 0));
                $collage->save(Yii::getAlias($path));
            }
            $path = str_replace('@webroot', '/web', $path);
            return $path;

            } else if ($this->type == "calendar") {
                if (!file_exists(Yii::getAlias($path))) {
                        $img_fb = Image::getImagine()->open($this->url);
                        $height = $img_fb->getSize()->getHeight();
                        $width = $img_fb->getSize()->getWidth();
                        $koef = $width / $height;
                        $koefcrop = $this->width/$this->height;
                        $img_fb->resize(new Box($this->width * $koef, $this->height))->save(Yii::getAlias($path));
                       /* if ($height > $width) {
                            
                            
                        } else {
                           
                            $img_fb->resize(new Box($this->width, $this->height* $koef))->save(Yii::getAlias($path));
                        }*/
                }

         } else {
                if (!file_exists(Yii::getAlias($path))) {
                    //$mode    = ImageInterface::THUMBNAIL_INSET;

                    $mode    = ImageInterface::THUMBNAIL_OUTBOUND;
                Image::thumbnail($this->url, $this->width, $this->height, $mode)->save(Yii::getAlias($path), ['quality' => 85]);
                }

        }
        $path = str_replace('@webroot', '/web', $path);
        return $path;
    }

}