<?php
/**
 * Created by PhpStorm.
 * User: Viktor
 * Date: 03.08.2017
 * Time: 16:45
 */

namespace app\components;
use yii\base\Widget;

class ReffereMenuWidget extends  Widget
{

    public $url;
    public function init()
    {
        parent::init();
        if ($this->url === null) {
            $this->url = 'reffere/index';
        }

    }
    public function run()
    {
        return $this->render('reffere', ['url'=>$this->url]);
    }

}