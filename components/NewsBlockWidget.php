<?php


namespace app\components;
use yii\base\Widget;
use yii\db\Query;
use Yii;

class NewsBlockWidget extends Widget
{

    public $limit;
    public $main;
    public function init()
    {
        parent::init();
        if ($this->limit === null) {
            $this->limit = 20;
        }
        if ($this->main === null) {
            $this->main =false;
        }

    }

    public function run()
    {
        $topics = Yii::$app->cache->get('topics');
        if(!$topics) {
            $query = new Query();
            $query->select([
                'topicid',
                'name',
                'codename'
            ])
                ->from('topic')
                ->where(['siteid' => 1]);

            $topics = $query->all();
            $topic_result = array();
            foreach ($topics as $topic){
                $topic_result[$topic['topicid']] = $topic;
            }
            Yii::$app->cache->set('topics', $topic_result, 60*60*24);
        }

         $banner = Yii::$app->cache->get('banner'); 
        if(!$banner){   
            $banner_q = new Query();
            $banner_q->select(['foto', 'link'])
            ->from('banners')
            ->orderBy(['date_added'=>SORT_ASC]);
            $banner = $banner_q->one();
            if(!empty($banner)){
                Yii::$app->cache->set('banner', $banner, 60*60*24);
            } else {
                Yii::$app->cache->set('banner', 'banner', 60*60*24);
            }

        }
        

        $news = Yii::$app->cache->get('allnews');
        if(!$news) {

            $query = new Query();
            $subquery = new Query();
            $subquery ->select(['newstopics.topicid as as'])
                ->from('newstopics' )
                ->where('newstopics.newsid = news.newsid')->limit(1);

            $query ->select(['news.*', 'topic' =>$subquery])
                ->from('news')
                ->where(['news.isactive' => 1, 'news.isremoved' => 0])
                ->andWhere(['<', 'news.date', date("Y-m-d H:i:s")])
                ->orderBy(['news.date' => SORT_DESC])
                ->limit(20);
            $news = $query->all();
            Yii::$app->cache->set('allnews', $news, 60*60*24);
        }

        $topics= Yii::$app->cache->get('topics');

        return $this->render('newsblock', ['news'=>$news, 'topics'=>$topics, 'limit'=>$this->limit, 'main'=>$this->main, 'banner'=>$banner]);
    }
}