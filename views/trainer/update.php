<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PersonnelTrainers */

$this->title = 'Update Personnel Trainers: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Personnel Trainers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="personnel-trainers-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
