<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PersonnelTrainers */

$this->title = 'Create Personnel Trainers';
$this->params['breadcrumbs'][] = ['label' => 'Personnel Trainers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="personnel-trainers-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
