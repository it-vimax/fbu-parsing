<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PlayerCareers */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Player Careers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="player-careers-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'player_id',
            'season_id',
            'club_id',
            'count_games',
            'game_time',
            'two_point_kiddies',
            'two_point_kiddies_ok',
            'two_point_kiddies_percent',
            'three_point_kiddies',
            'three_point_kiddies_ok',
            'three_point_kiddies_percent',
            'one_point_kiddies',
            'one_point_kiddies_ok',
            'one_point_kiddies_percent',
            'picking_up_in_an_attack',
            'picking_up_in_defense',
            'overall_picking_up',
            'transmissions',
            'staff_foul_1',
            'staff_fouls_2',
            'losses',
            'interception',
            'block_shots_1',
            'block_shots_2',
            'efficiency',
            'gained_points',
        ],
    ]) ?>

</div>
