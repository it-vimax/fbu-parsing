<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PlayerCareers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="player-careers-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'player_id')->textInput() ?>

    <?= $form->field($model, 'season_id')->textInput() ?>

    <?= $form->field($model, 'club_id')->textInput() ?>

    <?= $form->field($model, 'count_games')->textInput() ?>

    <?= $form->field($model, 'game_time')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'two_point_kiddies')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'two_point_kiddies_ok')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'two_point_kiddies_percent')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'three_point_kiddies')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'three_point_kiddies_ok')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'three_point_kiddies_percent')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'one_point_kiddies')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'one_point_kiddies_ok')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'one_point_kiddies_percent')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'picking_up_in_an_attack')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'picking_up_in_defense')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'overall_picking_up')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'transmissions')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'staff_foul_1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'staff_fouls_2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'losses')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'interception')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'block_shots_1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'block_shots_2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'efficiency')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gained_points')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
