<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PlayerCareers */

$this->title = 'Create Player Careers';
$this->params['breadcrumbs'][] = ['label' => 'Player Careers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="player-careers-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
