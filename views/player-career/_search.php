<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SearchPlayerCareers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="player-careers-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'player_id') ?>

    <?= $form->field($model, 'season_id') ?>

    <?= $form->field($model, 'club_id') ?>

    <?= $form->field($model, 'count_games') ?>

    <?php  echo $form->field($model, 'game_time') ?>
<!---->
    <?php  echo $form->field($model, 'two_point_kiddies') ?>
<!---->
    <?php  echo $form->field($model, 'two_point_kiddies_ok') ?>
<!---->
    <?php  echo $form->field($model, 'two_point_kiddies_percent') ?>
<!---->
    <?php  echo $form->field($model, 'three_point_kiddies') ?>
<!---->
    <?php  echo $form->field($model, 'three_point_kiddies_ok') ?>
<!---->
    <?php  echo $form->field($model, 'three_point_kiddies_percent') ?>
<!---->
    <?php  echo $form->field($model, 'one_point_kiddies') ?>
<!---->
    <?php  echo $form->field($model, 'one_point_kiddies_ok') ?>
<!---->
    <?php  echo $form->field($model, 'one_point_kiddies_percent') ?>
<!---->
    <?php  echo $form->field($model, 'picking_up_in_an_attack') ?>
<!---->
    <?php  echo $form->field($model, 'picking_up_in_defense') ?>
<!---->
    <?php  echo $form->field($model, 'overall_picking_up') ?>
<!---->
    <?php  echo $form->field($model, 'transmissions') ?>
<!---->
    <?php  echo $form->field($model, 'staff_foul_1') ?>
<!---->
    <?php  echo $form->field($model, 'staff_fouls_2') ?>
<!---->
    <?php  echo $form->field($model, 'losses') ?>
<!---->
    <?php  echo $form->field($model, 'interception') ?>
<!---->
    <?php  echo $form->field($model, 'block_shots_1') ?>
<!---->
    <?php  echo $form->field($model, 'block_shots_2') ?>
<!---->
    <?php  echo $form->field($model, 'efficiency') ?>
<!---->
    <?php  echo $form->field($model, 'gained_points') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
