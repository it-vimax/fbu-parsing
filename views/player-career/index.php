<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SearchPlayerCareers */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Player Careers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="player-careers-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Player Careers', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'player_id',
//            'season_id',
            [
                'attribute' => 'season_id',
                'value' => 'season.name'
            ],
//            'club_id',
            [
                'attribute' => 'club_id',
                'value' => 'club.name'
            ],
            'count_games',
             'game_time',
             'two_point_kiddies',
             'two_point_kiddies_ok',
             'two_point_kiddies_percent',
             'three_point_kiddies',
             'three_point_kiddies_ok',
             'three_point_kiddies_percent',
             'one_point_kiddies',
             'one_point_kiddies_ok',
             'one_point_kiddies_percent',
             'picking_up_in_an_attack',
             'picking_up_in_defense',
             'overall_picking_up',
             'transmissions',
             'staff_foul_1',
             'staff_fouls_2',
             'losses',
             'interception',
             'block_shots_1',
             'block_shots_2',
             'efficiency',
             'gained_points',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
