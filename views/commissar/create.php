<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PersonnelCommissars */

$this->title = 'Create Personner Commissars';
$this->params['breadcrumbs'][] = ['label' => 'Personner Commissars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="personner-commissars-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
