<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PersonnelJudiciarys */

$this->title = 'Update Personnel Judiciarys: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Personnel Judiciarys', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="personnel-judiciarys-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
