<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PersonnelJudiciarys */

$this->title = 'Create Personnel Judiciarys';
$this->params['breadcrumbs'][] = ['label' => 'Personnel Judiciarys', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="personnel-judiciarys-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
