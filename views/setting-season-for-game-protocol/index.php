<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SearchSettingSeasonForGameProtocol */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Setting Season For Game Protocols';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="setting-season-for-game-protocol-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Setting Season For Game Protocol', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'season_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
