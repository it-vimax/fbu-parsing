<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SettingSeasonForGameProtocol */

$this->title = 'Create Setting Season For Game Protocol';
$this->params['breadcrumbs'][] = ['label' => 'Setting Season For Game Protocols', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="setting-season-for-game-protocol-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
