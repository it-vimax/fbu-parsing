<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\GameProtocolTeamComparisons */

$this->title = 'Create Game Protocol Team Comparisons';
$this->params['breadcrumbs'][] = ['label' => 'Game Protocol Team Comparisons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="game-protocol-team-comparisons-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
