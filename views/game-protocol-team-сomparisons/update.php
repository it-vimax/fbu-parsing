<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\GameProtocolTeamComparisons */

$this->title = 'Update Game Protocol Team Comparisons: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Game Protocol Team Comparisons', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="game-protocol-team-comparisons-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
