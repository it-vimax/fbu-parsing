<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SearchGameProtocolTeamComparisons */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Game Protocol Team Comparisons';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="game-protocol-team-comparisons-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Game Protocol Team Comparisons', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'game_id',
            'team_status',
            'team_id',
            'two_point_kiddies',
            // 'two_point_kiddies_ok',
            // 'two_point_kiddies_percent',
            // 'three_point_kiddies',
            // 'three_point_kiddies_ok',
            // 'three_point_kiddies_percent',
            // 'one_point_kiddies',
            // 'one_point_kiddies_ok',
            // 'one_point_kiddies_percent',
            // 'picking_up_in_an_attack',
            // 'picking_up_in_defense',
            // 'overall_picking_up',
            // 'transmissions',
            // 'personal_foul',
            // 'losses',
            // 'interception',
            // 'block_shots_1',
            // 'efficiency',
            // 'gained_points',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
