<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\datecontrol\DateControl;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SearchPlayers */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Players';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="players-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Players', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'first_name',
            'last_name',
//            'date_of_birth:date',
            [
               'attribute' => 'date_of_birth',
               'value' => 'date_of_birth',
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => [
                    'pluginOptions' => [
                        'format' => 'dd-mm-yyyy',
                        'autoclose' => true,
                        'todayHighlight' => true,
                    ]
                ],
                'width' => '160px',
                'hAlign' => 'center',
            ],
            /*[
                'attribute'=>'date_of_birth',
                'format'=>['date_of_birth', 'd-m-Y'],
                //'type'=>GridView::INPUT_WIDGET, // enables you to use any widget
                'widgetOptions'=>[
                    'class'=>DateControl::classname(),
                    'type'=>DateControl::FORMAT_DATE
                ]
            ],*/
            /*[
                'attribute' => 'date_of_birth',
                'value' => 'date_of_birth',
                'filter' => \yii\jui\DatePicker::widget([
                    'model'=>$searchModel,
                    'attribute'=>'date_of_birth',
                    'language' => 'ru',
                    'dateFormat' => 'yyyy-MM-dd',
                ]),
                'format' => 'html',
            ],*/
            /*[
                'attribute' => 'date_of_birth',
                'value' => 'date_of_birth',
                'filter' => \yii\jui\DatePicker::widget(['language' => 'ru', 'dateFormat' => 'yyyy-mm-dd']),
                'format' => 'html',
            ],*/
            'position',
             'height',
             'weight',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
