<?php
use yii\helpers\Html;
?>
<h2>Выберете вариан</h2>
<div class="row">
    <div class="col-md-12">
        <?= Html::a("Тренера", ['/trainer/index'], ['class' => ["btn", "btn-success"]]) ?>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <?= Html::a("Дополнительная информацио об тренере", ['/trainers-to-seasons/index'], ['class' => ["btn", "btn-success"]]) ?>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <?= Html::a("Комисары", ['/commissar/index'], ['class' => ["btn", "btn-success"]]) ?>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <?= Html::a("Арбитры", ['/judiciary/index'], ['class' => ["btn", "btn-success"]]) ?>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <?= Html::a("Арбитры в играх", ['/game-judiciary/index'], ['class' => ["btn", "btn-success"]]) ?>
    </div>
</div>