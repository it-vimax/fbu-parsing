<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PlayerToSeason */

$this->title = 'Update Player To Season: ' . $model->player_id;
$this->params['breadcrumbs'][] = ['label' => 'Player To Seasons', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->player_id, 'url' => ['view', 'player_id' => $model->player_id, 'season_id' => $model->season_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="player-to-season-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
