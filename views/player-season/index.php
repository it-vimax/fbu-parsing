<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SearchPlayerToSeason */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Player To Seasons';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="player-to-season-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Player To Season', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'player_id',
//            'season_id',
            [
                'attribute' => 'season_id',
                'value' => 'season.name'
            ],
//            'club_id',
            [
                'attribute' => 'club_id',
                'value' => 'club.name'
            ],
            'count_game',
            'points',
             'pink_up',
             'forwarding',
             'efficiency',
             'plus_minus',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
