<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PlayerToSeason */

$this->title = 'Create Player To Season';
$this->params['breadcrumbs'][] = ['label' => 'Player To Seasons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="player-to-season-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
