<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SearchPlayerToSeason */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="player-to-season-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'player_id') ?>

    <?= $form->field($model, 'season_id') ?>

    <?= $form->field($model, 'club_id') ?>

    <?= $form->field($model, 'count_game') ?>

    <?= $form->field($model, 'points') ?>

    <?php  echo $form->field($model, 'pink_up') ?>
<!---->
    <?php  echo $form->field($model, 'forwarding') ?>
<!---->
    <?php  echo $form->field($model, 'efficiency') ?>
<!---->
    <?php  echo $form->field($model, 'plus_minus') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
