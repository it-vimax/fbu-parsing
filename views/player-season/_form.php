<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PlayerToSeason */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="player-to-season-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'player_id')->textInput() ?>

    <?= $form->field($model, 'season_id')->textInput() ?>

    <?= $form->field($model, 'club_id')->textInput() ?>

    <?= $form->field($model, 'count_game')->textInput() ?>

    <?= $form->field($model, 'points')->textInput() ?>

    <?= $form->field($model, 'pink_up')->textInput() ?>

    <?= $form->field($model, 'forwarding')->textInput() ?>

    <?= $form->field($model, 'efficiency')->textInput() ?>

    <?= $form->field($model, 'plus_minus')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
