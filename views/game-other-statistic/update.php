<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\GameOtherStatistic */

$this->title = 'Update Game Other Statistic: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Game Other Statistics', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="game-other-statistic-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
