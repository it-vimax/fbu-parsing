<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\GameOtherStatistic */

$this->title = 'Create Game Other Statistic';
$this->params['breadcrumbs'][] = ['label' => 'Game Other Statistics', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="game-other-statistic-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
