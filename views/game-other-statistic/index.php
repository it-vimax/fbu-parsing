<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SearchGameOtherStatistic */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Game Other Statistics';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="game-other-statistic-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Game Other Statistic', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'game_id',
            'name',
            'team_1',
            'team_2',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
