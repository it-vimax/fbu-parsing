<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PlayerHighs */

$this->title = 'Create Player Highs';
$this->params['breadcrumbs'][] = ['label' => 'Player Highs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="player-highs-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
