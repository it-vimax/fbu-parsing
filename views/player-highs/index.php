<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SearchPlayerHighs */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Player Highs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="player-highs-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Player Highs', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'player_id',
//            'season_id',
            [
                'attribute' => 'season_id',
                'value' => 'season.name'
            ],
            'category_name',
            'category_number',
             'date',
             'position',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
