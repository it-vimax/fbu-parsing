<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Games */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="games-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'season_id')->textInput() ?>

    <?= $form->field($model, 'date')->textInput() ?>

    <?= $form->field($model, 'number_game')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'group')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'step')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'command_1')->textInput() ?>

    <?= $form->field($model, 'command_2')->textInput() ?>

    <?= $form->field($model, 'result_command_1')->textInput() ?>

    <?= $form->field($model, 'result_command_2')->textInput() ?>

    <?= $form->field($model, 'arena')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'commissar_id')->textInput() ?>

    <?= $form->field($model, 'viewers')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'first_quarter_comand_1')->textInput() ?>

    <?= $form->field($model, 'first_quarter_comand_2')->textInput() ?>

    <?= $form->field($model, 'second_quarter_comand_1')->textInput() ?>

    <?= $form->field($model, 'second_quarter_comand_2')->textInput() ?>

    <?= $form->field($model, 'third_quarter_comand_1')->textInput() ?>

    <?= $form->field($model, 'third_quarter_comand_2')->textInput() ?>

    <?= $form->field($model, 'fourth_quarter_comand_1')->textInput() ?>

    <?= $form->field($model, 'fourth_quarter_comand_2')->textInput() ?>

    <?= $form->field($model, 'the_result_of_the_five_comand_1')->textInput() ?>

    <?= $form->field($model, 'the_result_of_the_five_comand_2')->textInput() ?>

    <?= $form->field($model, 'beat_the_spare_comand_1')->textInput() ?>

    <?= $form->field($model, 'beat_the_spare_comand_2')->textInput() ?>

    <?= $form->field($model, 'the_hottest_jerk_comand_1')->textInput() ?>

    <?= $form->field($model, 'the_hottest_jerk_comand_2')->textInput() ?>

    <?= $form->field($model, 'the_biggest_gap_comand_1')->textInput() ?>

    <?= $form->field($model, 'the_biggest_gap_comand_2')->textInput() ?>

    <?= $form->field($model, 'points_from_the_zone_comand_1')->textInput() ?>

    <?= $form->field($model, 'points_from_the_zone_comand_2')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
