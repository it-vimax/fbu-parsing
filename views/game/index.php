<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SearchGames */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Games';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="games-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Games', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'season_id',
            'date',
            'number_game',
            'group',
             'step',
             'command_1',
             'command_2',
             'result_command_1',
             'result_command_2',
             'arena',
             'commissar_id',
             'viewers',
             'first_quarter_comand_1',
             'first_quarter_comand_2',
             'second_quarter_comand_1',
             'second_quarter_comand_2',
             'third_quarter_comand_1',
             'third_quarter_comand_2',
             'fourth_quarter_comand_1',
             'fourth_quarter_comand_2',
             'the_result_of_the_five_comand_1',
             'the_result_of_the_five_comand_2',
             'beat_the_spare_comand_1',
             'beat_the_spare_comand_2',
             'the_hottest_jerk_comand_1',
             'the_hottest_jerk_comand_2',
             'the_biggest_gap_comand_1',
             'the_biggest_gap_comand_2',
             'points_from_the_zone_comand_1',
             'points_from_the_zone_comand_2',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
