<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SearchGames */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="games-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'season_id') ?>

    <?= $form->field($model, 'date') ?>

    <?= $form->field($model, 'number_game') ?>

    <?= $form->field($model, 'group') ?>

    <?php  echo $form->field($model, 'step') ?>
<!---->
    <?php  echo $form->field($model, 'command_1') ?>
<!---->
    <?php  echo $form->field($model, 'command_2') ?>
<!---->
    <?php  echo $form->field($model, 'result_command_1') ?>
<!---->
    <?php  echo $form->field($model, 'result_command_2') ?>
<!---->
    <?php  echo $form->field($model, 'arena') ?>
<!---->
    <?php  echo $form->field($model, 'commissar_id') ?>
<!---->
    <?php  echo $form->field($model, 'viewers') ?>
<!---->
    <?php  echo $form->field($model, 'first_quarter_comand_1') ?>
<!---->
    <?php  echo $form->field($model, 'first_quarter_comand_2') ?>
<!---->
    <?php  echo $form->field($model, 'second_quarter_comand_1') ?>
<!---->
    <?php  echo $form->field($model, 'second_quarter_comand_2') ?>
<!---->
    <?php  echo $form->field($model, 'third_quarter_comand_1') ?>
<!---->
    <?php  echo $form->field($model, 'third_quarter_comand_2') ?>
<!---->
    <?php  echo $form->field($model, 'fourth_quarter_comand_1') ?>
<!---->
    <?php  echo $form->field($model, 'fourth_quarter_comand_2') ?>
<!---->
    <?php  echo $form->field($model, 'the_result_of_the_five_comand_1') ?>
<!---->
    <?php  echo $form->field($model, 'the_result_of_the_five_comand_2') ?>
<!---->
    <?php  echo $form->field($model, 'beat_the_spare_comand_1') ?>
<!---->
    <?php  echo $form->field($model, 'beat_the_spare_comand_2') ?>
<!---->
    <?php  echo $form->field($model, 'the_hottest_jerk_comand_1') ?>
<!---->
    <?php  echo $form->field($model, 'the_hottest_jerk_comand_2') ?>
<!---->
    <?php  echo $form->field($model, 'the_biggest_gap_comand_1') ?>
<!---->
    <?php  echo $form->field($model, 'the_biggest_gap_comand_2') ?>
<!---->
    <?php  echo $form->field($model, 'points_from_the_zone_comand_1') ?>
<!---->
    <?php  echo $form->field($model, 'points_from_the_zone_comand_2') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
