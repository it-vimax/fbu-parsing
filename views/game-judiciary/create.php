<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\GameJudiciarys */

$this->title = 'Create Game Judiciarys';
$this->params['breadcrumbs'][] = ['label' => 'Game Judiciarys', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="game-judiciarys-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
