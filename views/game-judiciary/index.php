<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SearchGameJudiciarys */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Game Judiciarys';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="game-judiciarys-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Game Judiciarys', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'game_id',
            'judiciary_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
