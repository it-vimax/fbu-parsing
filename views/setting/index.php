<?php
use yii\helpers\Html;
?>
<h2>Первоначальные данные</h2>
<br>
<div class="row">
    <div class="col-md-12">
        <h4>Сезоны, которые не проходять стандартный парсинг протокола игры</h4>
        <?= Html::a("Настроить", ['/setting-season-for-game-protocol/index'], ['class' => ["btn", "btn-success"]]) ?>
    </div>
</div>