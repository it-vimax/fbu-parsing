<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\GameProtocolCommands */

$this->title = 'Create Game Protocol Commands';
$this->params['breadcrumbs'][] = ['label' => 'Game Protocol Commands', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="game-protocol-commands-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
