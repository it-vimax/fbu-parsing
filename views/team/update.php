<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Teams */

$this->title = 'Update Teams: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Teams', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'team_id' => $model->team_id, 'league_id' => $model->league_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="teams-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
