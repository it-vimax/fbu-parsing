<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TrainersToSeasons */

$this->title = 'Create Trainers To Seasons';
$this->params['breadcrumbs'][] = ['label' => 'Trainers To Seasons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trainers-to-seasons-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
