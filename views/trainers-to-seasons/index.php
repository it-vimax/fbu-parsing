<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SearchTrainersToSeasons */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Trainers To Seasons';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trainers-to-seasons-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Trainers To Seasons', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'trainer_id',
            'season_id',
            'team_name',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
