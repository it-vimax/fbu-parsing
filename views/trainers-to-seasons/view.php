<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TrainersToSeasons */

$this->title = $model->trainer_id;
$this->params['breadcrumbs'][] = ['label' => 'Trainers To Seasons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trainers-to-seasons-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'trainer_id' => $model->trainer_id, 'season_id' => $model->season_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'trainer_id' => $model->trainer_id, 'season_id' => $model->season_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'trainer_id',
            'season_id',
            'team_name',
        ],
    ]) ?>

</div>
