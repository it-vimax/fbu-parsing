<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TrainersToSeasons */

$this->title = 'Update Trainers To Seasons: ' . $model->trainer_id;
$this->params['breadcrumbs'][] = ['label' => 'Trainers To Seasons', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->trainer_id, 'url' => ['view', 'trainer_id' => $model->trainer_id, 'season_id' => $model->season_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="trainers-to-seasons-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
