<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\GameRootedStatistics */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Game Rooted Statistics', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="game-rooted-statistics-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'game_id',
            'team_id',
            'team_status',
            'time_in_game',
            'two_point_kiddies',
            'two_point_kiddies_ok',
            'three_point_kiddies',
            'three_point_kiddies_ok',
            'one_point_kiddies',
            'one_point_kiddies_ok',
            'picking_up_in_an_attack',
            'picking_up_in_defense',
            'overall_picking_up',
            'transmissions',
            'personal_foul',
            'losses',
            'interception',
            'block_shots',
            'efficiency',
            'gained_points',
        ],
    ]) ?>

</div>
