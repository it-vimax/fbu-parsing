<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\GameRootedStatistics */

$this->title = 'Create Game Rooted Statistics';
$this->params['breadcrumbs'][] = ['label' => 'Game Rooted Statistics', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="game-rooted-statistics-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
