<?php
use yii\helpers\Html;
?>
<h2>Первоначальные данные</h2>
<br>
<div class="row">
    <div class="col-md-12">
        <?= Html::a("Лигы", ['/league/index'], ['class' => ["btn", "btn-success"]]) ?>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <?= Html::a("Сезоны", ['/season/index'], ['class' => ["btn", "btn-success"]]) ?>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <?= Html::a("Персонал", ['/personnel/index'], ['class' => ["btn", "btn-success"]]) ?>
    </div>
</div>