<h2>Информация об арбитре</h2>
<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">id судди</th>
        <th scope="col">Имья</th>
        <th scope="col">Фамилия</th>
    </tr>
    </thead>
    <tbody>

    <tr>
        <th scope="row"><?= $judiciary->id ?></th>
        <td><?= $judiciary->first_name ?></td>
        <td><?= $judiciary->last_name ?></td>
    </tr>
    </tbody>
</table>
<br>
<h2>Игры, в которых участвовал</h2>
<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">id игры</th>
        <th scope="col">Имья</th>
        <th scope="col">Фамилия</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($gameJudiciarys as $gameJudiciary): ?>
        <?php foreach($gameJudiciary as $valueOtherJudiciaryInGame): ?>
            <tr>
                <th scope="row"><?= $valueOtherJudiciaryInGame->game_id ?></th>
                <?php if($valueOtherJudiciaryInGame->judiciary_id == $judiciary->id): ?>
                    <td><b><?= $valueOtherJudiciaryInGame->judiciary->first_name ?></b></td>
                    <td><b><?= $valueOtherJudiciaryInGame->judiciary->last_name ?></b></td>
                <?php else: ?>
                    <td><?= $valueOtherJudiciaryInGame->judiciary->first_name ?></td>
                    <td><?= $valueOtherJudiciaryInGame->judiciary->last_name ?></td>
                <?php endif; ?>
            </tr>
        <?php endforeach; ?>
    <?php endforeach; ?>
    </tbody>
</table>