<h2>Информация об команде</h2>
<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">Id игры</th>
        <th scope="col">Лига</th>
        <th scope="col">Название</th>
        <th scope="col">Город</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($team as $item): ?>
        <tr>
            <th scope="row"><?= $item->team_id ?></th>
            <td><?= $item->league->name ?></td>
            <td><?= $item->name ?></td>
            <td><?= $item->city ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>