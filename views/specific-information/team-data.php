<?php
use yii\helpers\Html;
?>
<h2>Все информация об командах</h2>
<br>
<div class="row">
    <div class="col-md-12">
        <?= Html::a("Все команды", ['/team/index'], ['class' => ["btn", "btn-success"]]) ?>
    </div>
</div>