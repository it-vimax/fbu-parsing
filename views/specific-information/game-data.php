<?php
use yii\helpers\Html;
?>
<h2>Все информация об играх</h2>
<br>
<div class="row">
    <div class="col-md-12">
        <?= Html::a("Все игры", ['/game/index'], ['class' => ["btn", "btn-success"]]) ?>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <?= Html::a("Дополнительная статистика по играх", ['/game-other-statistic/index'], ['class' => ["btn", "btn-success"]]) ?>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <?= Html::a("Протокол по играх", ['/game-protocol-commands/index'], ['class' => ["btn", "btn-success"]]) ?>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <?= Html::a("Протокол команд по играх", ['/game-protocol-team-comparisons/index'], ['class' => ["btn", "btn-success"]]) ?>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <?= Html::a("Сравнительная статистика по играх", ['/game-rooted-statistics/index'], ['class' => ["btn", "btn-success"]]) ?>
    </div>
</div>