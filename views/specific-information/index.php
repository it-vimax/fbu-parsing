<?php
use yii\helpers\Html;
?>

<div class="row">
    <?= Html::beginForm(['/specific-information/player'], 'get') ?>
        <div class="col-md-6">
            <?= Html::input("text", "player_id", "", ['class' => ["form-control"], 'placeholder' => 'id игрока']) ?>
        </div>
        <div class="col-md-6">
            <?= Html::submitButton("Посмотреть игрока", ['class' => ["btn", "btn-success"]]) ?>
        </div>
    <?= Html::endForm() ?>
</div>
<br>
<div class="row">
    <?= Html::beginForm(['/specific-information/game'], 'get') ?>
        <div class="col-md-6">
            <?= Html::input("text", "game_id", "", ['class' => ["form-control"], 'placeholder' => 'id игры']) ?>
        </div>
        <div class="col-md-6">
            <?= Html::submitButton("Посмотреть игру", ['class' => ["btn", "btn-success"]]) ?>
        </div>
    <?= Html::endForm() ?>
</div>
<br>
<div class="row">
    <?= Html::beginForm(['/specific-information/team'], 'get') ?>
        <div class="col-md-6">
            <?= Html::input("text", "team_id", "", ['class' => ["form-control"], 'placeholder' => 'id команды']) ?>
        </div>
        <div class="col-md-6">
            <?= Html::submitButton("Посмотреть команду", ['class' => ["btn", "btn-success"]]) ?>
        </div>
    <?= Html::endForm() ?>
</div>
<br>
<div class="row">
    <?= Html::beginForm(['/specific-information/trainer'], 'get') ?>
        <div class="col-md-6">
            <?= Html::input("text", "trainer_id", "", ['class' => ["form-control"], 'placeholder' => 'id тренера']) ?>
        </div>
        <div class="col-md-6">
            <?= Html::submitButton("Посмотреть тренера", ['class' => ["btn", "btn-success"]]) ?>
        </div>
    <?= Html::endForm() ?>
</div>
<br>
<div class="row">
    <?= Html::beginForm(['/specific-information/commissar'], 'get') ?>
        <div class="col-md-6">
            <?= Html::input("text", "commissar_id", "", ['class' => ["form-control"], 'placeholder' => 'id комисара']) ?>
        </div>
        <div class="col-md-6">
            <?= Html::submitButton("Посмотреть комисара", ['class' => ["btn", "btn-success"]]) ?>
        </div>
    <?= Html::endForm() ?>
</div>
<br>
<div class="row">
    <?= Html::beginForm(['/specific-information/judiciary'], 'get') ?>
        <div class="col-md-6">
            <?= Html::input("text", "judiciary_id", "", ['class' => ["form-control"], 'placeholder' => 'id арбитра']) ?>
        </div>
        <div class="col-md-6">
            <?= Html::submitButton("Посмотреть арбитра", ['class' => ["btn", "btn-success"]]) ?>
        </div>
    <?= Html::endForm() ?>
</div>
<br>
