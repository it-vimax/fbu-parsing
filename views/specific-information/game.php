<?php if(!empty($game)): ?>
    <h2>Общее инфо</h2>
    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">id игры</th>
            <th scope="col">Сезон</th>
            <th scope="col">Дата</th>
            <th scope="col">Номер игры</th>
            <th scope="col">Група</th>
            <th scope="col">Шаг</th>
            <th scope="col">Хазяева</th>
            <th scope="col">Гости</th>
            <th scope="col">Зерультат хозяев</th>
            <th scope="col">Зерультат гостей</th>
            <th scope="col">Зал</th>
            <th scope="col">Комисар</th>
            <th scope="col">Количество зрителей</th>
            <th scope="col">Первая четвердь хозяева</th>
            <th scope="col">Первая четвердь гости</th>
            <th scope="col">Вторая четвердь хозяева</th>
            <th scope="col">Вторая четвердь гости</th>
            <th scope="col">Третья четвердь хозяева</th>
            <th scope="col">Третья четвердь гости</th>
            <th scope="col">Четвёртая четвердь хозяева</th>
            <th scope="col">Четвёртая четвердь гости</th>
            <th scope="col">Результат пьятерки хозяева</th>
            <th scope="col">Результат пьятерки гости</th>
            <th scope="col">Забили запасные хозяева</th>
            <th scope="col">Забили запасные гости</th>
            <th scope="col">Найбольший рывок хозяева</th>
            <th scope="col">Найбольший рывок гости</th>
            <th scope="col">Найбольший розрыв хозяева</th>
            <th scope="col">Найбольший розрыв гости</th>
            <th scope="col">Очки с зоны хозяева</th>
            <th scope="col">Очки с зоны гости</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th scope="row"><?= $game->id ?></th>
            <td><?= $game->season->name ?></td>
            <td><?= $game->date ?></td>
            <td><?= $game->number_game ?></td>
            <td><?= $game->group ?></td>
            <td><?= $game->step ?></td>
            <td><?= $game->command1->name ?></td>
            <td><?= $game->command2->name ?></td>
            <td><?= $game->result_command_1 ?></td>
            <td><?= $game->result_command_2 ?></td>
            <td><?= $game->arena ?></td>
            <td><?= $game->commissar->first_name.' '.$game->commissar->last_name ?></td>
            <td><?= $game->viewers ?></td>
            <td><?= $game->first_quarter_comand_1 ?></td>
            <td><?= $game->first_quarter_comand_2 ?></td>
            <td><?= $game->second_quarter_comand_1 ?></td>
            <td><?= $game->second_quarter_comand_2 ?></td>
            <td><?= $game->third_quarter_comand_1 ?></td>
            <td><?= $game->third_quarter_comand_2 ?></td>
            <td><?= $game->fourth_quarter_comand_1 ?></td>
            <td><?= $game->fourth_quarter_comand_2 ?></td>
            <td><?= $game->the_result_of_the_five_comand_1 ?></td>
            <td><?= $game->the_result_of_the_five_comand_2 ?></td>
            <td><?= $game->beat_the_spare_comand_1 ?></td>
            <td><?= $game->beat_the_spare_comand_2 ?></td>
            <td><?= $game->the_hottest_jerk_comand_1 ?></td>
            <td><?= $game->the_hottest_jerk_comand_2 ?></td>
            <td><?= $game->the_biggest_gap_comand_1 ?></td>
            <td><?= $game->the_biggest_gap_comand_2 ?></td>
            <td><?= $game->points_from_the_zone_comand_1 ?></td>
            <td><?= $game->points_from_the_zone_comand_2 ?></td>
        </tr>
        </tbody>
    </table>
    <h2>Протокол по игре</h2>
    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Комана</th>
            <th scope="col">Игрок</th>
            <th scope="col">Номер игрока</th>
            <th scope="col">Время в игре</th>
            <th scope="col">Двухочковых бросков</th>
            <th scope="col">Двухочковых бросков/попал</th>
            <th scope="col">Двухочковых бросков/процент</th>
            <th scope="col">Трехочковых бросков</th>
            <th scope="col">Трехочковых бросков/попал</th>
            <th scope="col">Трехочковых бросков/процент</th>
            <th scope="col">Попыток в атаке</th>
            <th scope="col">Попыток в защите</th>
            <th scope="col">Попыток процент</th>
            <th scope="col">Штрафных бросков</th>
            <th scope="col">Штрафных бросков/попал</th>
            <th scope="col">Штрафных бросков/процент</th>
            <th scope="col">Подборов в атаке</th>
            <th scope="col">Подборов в нападении</th>
            <th scope="col">Подборов все</th>
            <th scope="col">Передачи</th>
            <th scope="col">Персональные фолы</th>
            <th scope="col">Потери</th>
            <th scope="col">Перехваты</th>
            <th scope="col">Блок-шот 1</th>
            <th scope="col">Блок-шот 2</th>
            <th scope="col">Эфэктивность</th>
            <th scope="col">+/-</th>
            <th scope="col">Полученые очки</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($gameProtocolCommands as $key=> $item): ?>
        <tr>
            <th scope="row"><?= $key+1 ?></th>
            <td><?= ($item->team_status == 1) ? $game->command1->name : $game->command2->name ?></td>
            <td><?= $item->player->first_name.' '.$item->player->last_name ?></td>
            <td><?= $item->player_number ?></td>
            <td><?= $item->time_in_game ?></td>
            <td><?= $item->two_point_kiddies ?></td>
            <td><?= $item->two_point_kiddies_ok ?></td>
            <td><?= $item->two_point_kiddies_percent ?></td>
            <td><?= $item->three_point_kiddies ?></td>
            <td><?= $item->three_point_kiddies_ok ?></td>
            <td><?= $item->three_point_kiddies_percent ?></td>
            <td><?= $item->for_game_attempt ?></td>
            <td><?= $item->for_game_attempt_ok ?></td>
            <td><?= $item->for_game_attempt_percent ?></td>
            <td><?= $item->one_point_kiddies ?></td>
            <td><?= $item->one_point_kiddies_ok ?></td>
            <td><?= $item->one_point_kiddies_percent ?></td>
            <td><?= $item->picking_up_in_an_attack ?></td>
            <td><?= $item->picking_up_in_defense ?></td>
            <td><?= $item->overall_picking_up ?></td>
            <td><?= $item->transmissions ?></td>
            <td><?= $item->personal_foul ?></td>
            <td><?= $item->losses ?></td>
            <td><?= $item->interception ?></td>
            <td><?= $item->block_shots_1 ?></td>
            <td><?= $item->block_shots_2 ?></td>
            <td><?= $item->efficiency ?></td>
            <td><?= $item->plus_minus ?></td>
            <td><?= $item->gained_points ?></td>
            <?php endforeach; ?>
        </tbody>
    </table>
    <h2>Сравнение команд</h2>
    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">Комана</th>
            <th scope="col">Двухочковых бросков</th>
            <th scope="col">Двухочковых бросков/попал</th>
            <th scope="col">Двухочковых бросков/процент</th>
            <th scope="col">Трехочковых бросков</th>
            <th scope="col">Трехочковых бросков/попал</th>
            <th scope="col">Трехочковых бросков/процент</th>
            <th scope="col">Штрафных бросков</th>
            <th scope="col">Штрафных бросков/попал</th>
            <th scope="col">Штрафных бросков/процент</th>
            <th scope="col">Подборов в атаке</th>
            <th scope="col">Подборов в нападении</th>
            <th scope="col">Подборов все</th>
            <th scope="col">Передачи</th>
            <th scope="col">Персональные фолы</th>
            <th scope="col">Потери</th>
            <th scope="col">Перехваты</th>
            <th scope="col">Блок-шот 1</th>
            <th scope="col">Эфэктивность</th>
            <th scope="col">Полученые очки</th>
        </tr>
        </thead>
        <tbody>
            <?php foreach($gameProtocolTeamComparisons as $item): ?>
                <tr>
                    <th scope="row"><?= ($item->team_status == 1) ? $game->command1->name : $game->command2->name ?></th>
                    <td><?= $item->two_point_kiddies ?></td>
                    <td><?= $item->two_point_kiddies_ok ?></td>
                    <td><?= $item->two_point_kiddies_percent ?></td>
                    <td><?= $item->three_point_kiddies ?></td>
                    <td><?= $item->three_point_kiddies_ok ?></td>
                    <td><?= $item->three_point_kiddies_percent ?></td>
                    <td><?= $item->one_point_kiddies ?></td>
                    <td><?= $item->one_point_kiddies_ok ?></td>
                    <td><?= $item->one_point_kiddies_percent ?></td>
                    <td><?= $item->picking_up_in_an_attack ?></td>
                    <td><?= $item->picking_up_in_defense ?></td>
                    <td><?= $item->overall_picking_up ?></td>
                    <td><?= $item->transmissions ?></td>
                    <td><?= $item->personal_foul ?></td>
                    <td><?= $item->losses ?></td>
                    <td><?= $item->interception ?></td>
                    <td><?= $item->block_shots_1 ?></td>
                    <td><?= $item->efficiency ?></td>
                    <td><?= $item->gained_points ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <h2>Сравнение старт/лава</h2>
    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">Команда</th>
            <th scope="col">Статус</th>
            <th scope="col">Время в игре</th>
            <th scope="col">Двухочковых бросков</th>
            <th scope="col">Двухочковых бросков/попал</th>
            <th scope="col">Трехочковых бросков</th>
            <th scope="col">Трехочковых бросков/попал</th>
            <th scope="col">Штрафных бросков</th>
            <th scope="col">Штрафных бросков/попал</th>
            <th scope="col">Подборов в атаке</th>
            <th scope="col">Подборов в нападении</th>
            <th scope="col">Подборов все</th>
            <th scope="col">Передачи</th>
            <th scope="col">Персональные фолы</th>
            <th scope="col">Потери</th>
            <th scope="col">Перехваты</th>
            <th scope="col">Блок-шот</th>
            <th scope="col">Эфэктивность</th>
            <th scope="col">Полученые очки</th>
        </tr>
        </thead>
        <tbody>
            <?php foreach($gameRootedStatistics as $item): ?>
                <tr>
                    <th scope="row"><?= $item->team->name ?></th>
                    <td><?= $item->team_status ?></td>
                    <td><?= $item->time_in_game ?></td>
                    <td><?= $item->two_point_kiddies ?></td>
                    <td><?= $item->two_point_kiddies_ok ?></td>
                    <td><?= $item->three_point_kiddies ?></td>
                    <td><?= $item->three_point_kiddies_ok ?></td>
                    <td><?= $item->one_point_kiddies ?></td>
                    <td><?= $item->one_point_kiddies_ok ?></td>
                    <td><?= $item->picking_up_in_an_attack ?></td>
                    <td><?= $item->picking_up_in_defense ?></td>
                    <td><?= $item->overall_picking_up ?></td>
                    <td><?= $item->transmissions ?></td>
                    <td><?= $item->personal_foul ?></td>
                    <td><?= $item->losses ?></td>
                    <td><?= $item->interception ?></td>
                    <td><?= $item->block_shots ?></td>
                    <td><?= $item->efficiency ?></td>
                    <td><?= $item->gained_points ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <h2>Другая статистика</h2>
    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">Название</th>
            <th scope="col">Хозяева</th>
            <th scope="col">Гости</th>
        </tr>
        </thead>
        <tbody>
            <?php foreach($gameOtherStatistic as $item): ?>
                <tr>
                    <th scope="row"><?= $item->name ?></th>
                    <td><?= $item->team_1 ?></td>
                    <td><?= $item->team_2 ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <h2>Судди</h2>
    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Имя</th>
            <th scope="col">Фамилия</th>
        </tr>
        </thead>
        <tbody>
        <?php if(empty($gameJudiciarys)): ?>
            <tr>
                <th scope="row"><h2>Суддей не найдено</h2></th>
            </tr>
        <?php else: ?>
            <?php foreach($gameJudiciarys as $keyGameJudiciarys => $valueGameJudiciarys): ?>
                <tr>
                    <th scope="row"><?= $keyGameJudiciarys ?></th>
                    <td><?= $valueGameJudiciarys->judiciary->first_name ?></td>
                    <td><?= $valueGameJudiciarys->judiciary->last_name ?></td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
        </tbody>
    </table>
<?php endif; ?>