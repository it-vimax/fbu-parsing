<?php
use yii\helpers\Html;
?>
<h2>Все информация об игроках</h2>
<br>
<div class="row">
    <div class="col-md-12">
        <?= Html::a("Все игроки", ['/player/index'], ['class' => ["btn", "btn-success"]]) ?>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <?= Html::a("Карьера игроков", ['/player-career/index'], ['class' => ["btn", "btn-success"]]) ?>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <?= Html::a("Сезоны игроков", ['/player-season/index'], ['class' => ["btn", "btn-success"]]) ?>
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <?= Html::a("Рекорды игроков", ['/player-highs/index'], ['class' => ["btn", "btn-success"]]) ?>
    </div>
</div>