<h2>Информация об комисаре</h2>
<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">id комисара</th>
        <th scope="col">Имья</th>
        <th scope="col">Фамилия</th>
    </tr>
    </thead>
    <tbody>

    <tr>
        <th scope="row"><?= $commissar->id ?></th>
        <td><?= $commissar->first_name ?></td>
        <td><?= $commissar->last_name ?></td>
    </tr>
    </tbody>
</table>
<br>
<h2>Игры, в которых учасвувал</h2>
<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">id игры</th>
        <th scope="col">Сезон</th>
        <th scope="col">Дата</th>
        <th scope="col">Номер игры</th>
        <th scope="col">Група</th>
        <th scope="col">Шаг</th>
        <th scope="col">Хазяева</th>
        <th scope="col">Гости</th>
        <th scope="col">Зерультат хозяев</th>
        <th scope="col">Зерультат гостей</th>
        <th scope="col">Зал</th>
        <th scope="col">Комисар</th>
        <th scope="col">Количество зрителей</th>
        <th scope="col">Первая четвердь хозяева</th>
        <th scope="col">Первая четвердь гости</th>
        <th scope="col">Вторая четвердь хозяева</th>
        <th scope="col">Вторая четвердь гости</th>
        <th scope="col">Третья четвердь хозяева</th>
        <th scope="col">Третья четвердь гости</th>
        <th scope="col">Четвёртая четвердь хозяева</th>
        <th scope="col">Четвёртая четвердь гости</th>
        <th scope="col">Результат пьятерки хозяева</th>
        <th scope="col">Результат пьятерки гости</th>
        <th scope="col">Забили запасные хозяева</th>
        <th scope="col">Забили запасные гости</th>
        <th scope="col">Найбольший рывок хозяева</th>
        <th scope="col">Найбольший рывок гости</th>
        <th scope="col">Найбольший розрыв хозяева</th>
        <th scope="col">Найбольший розрыв гости</th>
        <th scope="col">Очки с зоны хозяева</th>
        <th scope="col">Очки с зоны гости</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($gamesForCommissar as $key=>$game): ?>
        <tr>
            <th scope="row"><?= $game->id ?></th>
            <td><?= $game->season->name ?></td>
            <td><?= $game->date ?></td>
            <td><?= $game->number_game ?></td>
            <td><?= $game->group ?></td>
            <td><?= $game->step ?></td>
            <td><?= $game->command1->name ?></td>
            <td><?= $game->command2->name ?></td>
            <td><?= $game->result_command_1 ?></td>
            <td><?= $game->result_command_2 ?></td>
            <td><?= $game->arena ?></td>
            <td><?= $game->commissar->first_name.' '.$game->commissar->last_name ?></td>
            <td><?= $game->viewers ?></td>
            <td><?= $game->first_quarter_comand_1 ?></td>
            <td><?= $game->first_quarter_comand_2 ?></td>
            <td><?= $game->second_quarter_comand_1 ?></td>
            <td><?= $game->second_quarter_comand_2 ?></td>
            <td><?= $game->third_quarter_comand_1 ?></td>
            <td><?= $game->third_quarter_comand_2 ?></td>
            <td><?= $game->fourth_quarter_comand_1 ?></td>
            <td><?= $game->fourth_quarter_comand_2 ?></td>
            <td><?= $game->the_result_of_the_five_comand_1 ?></td>
            <td><?= $game->the_result_of_the_five_comand_2 ?></td>
            <td><?= $game->beat_the_spare_comand_1 ?></td>
            <td><?= $game->beat_the_spare_comand_2 ?></td>
            <td><?= $game->the_hottest_jerk_comand_1 ?></td>
            <td><?= $game->the_hottest_jerk_comand_2 ?></td>
            <td><?= $game->the_biggest_gap_comand_1 ?></td>
            <td><?= $game->the_biggest_gap_comand_2 ?></td>
            <td><?= $game->points_from_the_zone_comand_1 ?></td>
            <td><?= $game->points_from_the_zone_comand_2 ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>