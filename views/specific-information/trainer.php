<h2>Информация об теренере</h2>
<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">Id тренера</th>
        <th scope="col">Имья</th>
        <th scope="col">Фамилия</th>
    </tr>
    </thead>
    <tbody>
        <tr>
            <th scope="row"><?= $personnelTrainers->id ?></th>
            <td><?= $personnelTrainers->first_name ?></td>
            <td><?= $personnelTrainers->last_name ?></td>
        </tr>
    </tbody>
</table>
<h2>Команды/сезоны</h2>
<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Сезон</th>
        <th scope="col">Команда</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($trainersToSeasons as $keyTrainersToSeasons => $valueTrainersToSeasons): ?>
        <tr>
            <th scope="row"><?= $keyTrainersToSeasons ?></th>
            <td><?= $valueTrainersToSeasons->season->name ?></td>
            <td><?= $valueTrainersToSeasons->team_name ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
