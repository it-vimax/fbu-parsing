<?php if(!empty($player)): ?>
    <h3>Инфо об игроке</h3>
    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">id игрока</th>
            <th scope="col">Имья</th>
            <th scope="col">Фамилия</th>
            <th scope="col">Дата рождения</th>
            <th scope="col">Позиция в игре</th>
            <th scope="col">Рост</th>
            <th scope="col">Вес</th>
        </tr>
        </thead>
        <tbody>

            <tr>
                <th scope="row"><?= $player['id'] ?></th>
                <td><?= $player['first_name'] ?></td>
                <td><?= $player['last_name'] ?></td>
                <td><?= $player['date_of_birth'] ?></td>
                <td><?= $player['position'] ?></td>
                <td><?= $player['height'] ?></td>
                <td><?= $player['weight'] ?></td>
            </tr>
        </tbody>
    </table>

    <h4>В каких сезонах участвовал игрок</h4>
    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">#</th>
<!--            <th scope="col">id игрока</th>-->
            <th scope="col">Сезон</th>
            <th scope="col">В каком клубе играл</th>
            <th scope="col">Количество сыгранных игр</th>
            <th scope="col">Очки</th>
            <th scope="col">Подбирание</th>
            <th scope="col">Передачи</th>
            <th scope="col">Эфективность</th>
            <th scope="col">+/-</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($playerToSeason as $key=>$itemSeason): ?>
            <tr>
                <th scope="row"><?= $key ?></th>
<!--                <td>--><?//= $itemSeason->player_id ?><!--</td>-->
                <td><?= $itemSeason->season->name ?></td>
                <td><?= $itemSeason->club->name ?></td>
                <td><?= $itemSeason->count_game ?></td>
                <td><?= $itemSeason->points ?></td>
                <td><?= $itemSeason->pink_up ?></td>
                <td><?= $itemSeason->forwarding ?></td>
                <td><?= $itemSeason->efficiency ?></td>
                <td><?= $itemSeason->plus_minus ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

    <h4>Карьера игрока</h4>
    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">#</th>
            <!--            <th scope="col">id игрока</th>-->
            <th scope="col">Сезон</th>
            <th scope="col">Клуб</th>
            <th scope="col">Количество игр</th>
            <th scope="col">Количество игрового времени</th>
            <th scope="col">Двохочковых бросков</th>
            <th scope="col">Двохочковых бросков/попал</th>
            <th scope="col">Двохочковых бросков/процент</th>
            <th scope="col">Трехочковых бросков</th>
            <th scope="col">Трехочковых бросков/попал</th>
            <th scope="col">Трехочковых бросков/процент</th>
            <th scope="col">Штрафных бросков</th>
            <th scope="col">Штрафных бросков/попал</th>
            <th scope="col">Штрафных бросков/процент</th>
            <th scope="col">Подбирание у нападе</th>
            <th scope="col">Подбирание в защите</th>
            <th scope="col">Подбирание в общем</th>
            <th scope="col">Передач</th>
            <th scope="col">Фолы игрока1</th>
            <th scope="col">Фолы игрока2</th>
            <th scope="col">Потери</th>
            <th scope="col">Перехваты</th>
            <th scope="col">Блок-шоты1</th>
            <th scope="col">Блок-шоты2</th>
            <th scope="col">Эфективность</th>
            <th scope="col">Добытые очки</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($playerCareers as $key=>$itemCareers): ?>
            <tr>
                <th scope="row"><?= $key ?></th>
                <!--                <td>--><?//= $itemSeason->player_id ?><!--</td>-->
                <td><?= $itemCareers->season->name ?></td>
                <td><?= $itemCareers->club->name ?></td>
                <td><?= $itemCareers->count_games ?></td>
                <td><?= $itemCareers->game_time ?></td>
                <td><?= $itemCareers->two_point_kiddies ?></td>
                <td><?= $itemCareers->two_point_kiddies_ok ?></td>
                <td><?= $itemCareers->two_point_kiddies_percent ?></td>
                <td><?= $itemCareers->three_point_kiddies ?></td>
                <td><?= $itemCareers->three_point_kiddies_ok ?></td>
                <td><?= $itemCareers->three_point_kiddies_percent ?></td>
                <td><?= $itemCareers->one_point_kiddies ?></td>
                <td><?= $itemCareers->one_point_kiddies_ok ?></td>
                <td><?= $itemCareers->one_point_kiddies_percent ?></td>
                <td><?= $itemCareers->picking_up_in_an_attack ?></td>
                <td><?= $itemCareers->picking_up_in_defense ?></td>
                <td><?= $itemCareers->overall_picking_up ?></td>
                <td><?= $itemCareers->transmissions ?></td>
                <td><?= $itemCareers->staff_foul_1 ?></td>
                <td><?= $itemCareers->staff_fouls_2 ?></td>
                <td><?= $itemCareers->losses ?></td>
                <td><?= $itemCareers->interception ?></td>
                <td><?= $itemCareers->block_shots_1 ?></td>
                <td><?= $itemCareers->block_shots_2 ?></td>
                <td><?= $itemCareers->efficiency ?></td>
                <td><?= $itemCareers->gained_points ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

    <h4>Рекорды игрока игрока</h4>
    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">#</th>
<!--            <th scope="col">id игрока</th>-->
            <th scope="col">Сезон</th>
            <th scope="col">Категория</th>
            <th scope="col">Категория</th>
            <th scope="col">Дата</th>
            <th scope="col">Позиция</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($playerHighs as $key=>$itemHighs): ?>
            <tr>
                <th scope="row"><?= $key ?></th>
<!--                <td>--><?//= $itemSeason->player_id ?><!--</td>-->
                <td><?= $itemHighs->season->name ?></td>
                <td><?= $itemHighs->category_name ?></td>
                <td><?= $itemHighs->category_number ?></td>
                <td><?= $itemHighs->date ?></td>
                <td><?= $itemHighs->position ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>

